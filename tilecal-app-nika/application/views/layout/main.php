<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
		<!-- Fontawesome -->
		<script src="https://kit.fontawesome.com/5008f2f377.js" crossorigin="anonymous"></script>
		<!-- Feather icons -->
		<!-- <script src="https://unpkg.com/feather-icons"></script> -->
		<!-- jquery mCustomScrollbar.css -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/component-tree.css">
		<!-- dataTables.min.css -->
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
		<!-- Toastr CSS -->
		<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
		<!-- Datatables -->
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.24/r-2.2.7/sb-1.0.1/sp-1.2.2/datatables.min.css"/>
		
		<title>TileCal-Electronics-DB</title>
	</head>
	<body> 
		<div class="wrapper">	 
			<div class="container-fluid">
			<!--Modals-->
			<!-- Add Mini Drawer Modal -->
			<div class="modal fade" id="add_mini_drawer_modal" tabindex="-1" role="dialog" aria-labelledby="add_mini_drawer_modal_title" aria-hidden="true" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="add_mini_drawer_modal_title">Add Mini Drawer</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="add_mini_drawer_form">
									<div class="form-group">
										<label class="h6" for="id_mini_drawer_frame">Mini Drawer</label>
										<input type="text" class="form-control" name="id_mini_drawer_frame" id="id_mini_drawer_frame" aria-describedby="" placeholder="Mini drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="mini_drawer_status">Status</label>
										<input type="text" class="form-control" name="status" id="mini_drawer_status" aria-describedby="" placeholder="Please enter status e.g. delivered">
										<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
									</div>
									<div class="form-group">
										<label class="h6" for="mini_drawer_current_location">Current location</label>
										<input type="text" class="form-control" name="current_location" id="mini_drawer_current_location" aria-describedby="" placeholder="Please enter current location">
										<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
									</div>
									<div class="form-group">
										<label class="h6" for="remark">Comments</label>
										<textarea class="form-control" name="remark" id="mini_drawer_remark" rows="1" placeholder="Additional comments"></textarea>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" form="add_mini_drawer_form" class="btn btn-sm btn-success add_mini_drawer_form_submit"><i class="fas fa-check pr-1"></i>Save</button>
								<button type="button" class="btn btn-sm btn-default add_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
				</div>
				<!-- Edit Mini Drawer Modal -->
				<div class="modal fade" id="edit_mini_drawer_modal" tabindex="-1" role="dialog" aria-labelledby="edit_mini_drawer_modal_title" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_mini_drawer_modal_title">Update Mini Drawer</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_mini_drawer_form">
										<div class="form-group">
											<label class="h6" for="id_mini_drawer_frame">Mini Drawer</label>
											<input type="text" class="form-control" name="id_mini_drawer_frame" id="edit_id_mini_drawer_frame" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="mini_drawer_status">Status</label>
											<input type="text" class="form-control" name="status" id="edit_mini_drawer_status" aria-describedby="" placeholder="Please enter status e.g. delivered">
											<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
										</div>
										<div class="form-group">
											<label class="h6" for="mini_drawer_current_location">Current location</label>
											<input type="text" class="form-control" name="current_location" id="edit_mini_drawer_current_location" aria-describedby="" placeholder="Please enter current location">
											<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
										</div>
										<div class="form-group">
											<label class="h6" for="remark">Comments</label>
											<textarea class="form-control" name="remark" id="edit_mini_drawer_remark" rows="1" placeholder="Additional comments"></textarea>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_mini_drawer_form" class="btn btn-sm btn-success edit_mini_drawer_form_submit" id="edit_mini_drawer_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>
				<div class="row no-gutters">
					<nav class="col bg-light sidebar">
						<div class="sidebar-header px-3 d-flex align-items-center">
							<a class="navbar-brand" href="#">
							TILECAL ELECTRONICS <b>DATABASE</b>
							</a>
						</div>
						<ul class="tc-tree pt-3">
							<li class="">
								<a id="tc" href="#">
									<i class="far fa-minus-square tc-open-tree trc-open-tree-first"></i>
									<span class="tile-calorimeter">Tile Calorimeter</span>
								</a>
								<ul class="tree tc-tree1">
									<li class="tree-1-li last-component">
										<a id="partition" class="level-2" href="#">
											<i class="far fa-minus-square tc-open-tree"></i>
											<span class="tree-child tree-1-child" id="show_partition_table">Partition <span class="badge badge-success">Filled</span></span>
										</a>
										<ul class="tree tc-tree2">
											<li class="tree-2-li" style="border-left:1px solid #333;">
												<a href="#">
													<i class="far fa-minus-square tc-open-tree"></i> 
													<span class="tree-child tree-2-child" id="show_module_table">Module <span class="badge badge-success">Filled</span></span> 
												</a>
												<ul class="module-tree3 tree tc-tree3">
													<li class="tree-3-li last-component">
														<a href="#"> 
															<i class="far fa-minus-square tc-open-tree"></i>
															<span id="super-drawer" class="tree-child tree-3-child">Super drawer</span>
															
														</a>
														<ul class="superdrawer-tree-4 tree tc-tree4">
															<li class="tree-4-li" style="border-left:1px solid #333;">
																<a href="#">
																	<i class="far fa-minus-square tc-open-tree"></i>
																	<span class="tree-child tree-4-child">Micro drawer</span>
																	
																</a>
																<ul class="tree tc-tree5">
																	<li class="tree-5-li last-component"> 
																		<a href="#">
																			<i class="far fa-minus-square tc-open-tree"></i> 
																			<span class="tree-child tree-5-child" id="show_pmt_block_table">PMT block</span>
																			
																		</a>
																		<ul class="tree tc-tree6">
																			<li class="tree-6-li" style="border-left:1px solid #333;">
																				<a href="#">
																					<span class="tree-child tree-6-child">FENICS</span>
																					
																				</a>
																			</li>
																			<li class="tree-6-li" style="border-left:1px solid #333;">
																				<a href="#">
																					<span class="tree-child tree-6-child">HV divider</span>
																					
																				</a>
																			</li>
																			<li class="tree-6-li last-component">
																				<a href="#" class="show_pmt_table">
																					<span class="tree-child tree-6-child">PMT <span class="badge badge-info">Filled with old Data</span></span>																					
																				</a>
																			</li>
																		</ul>
																	</li>
																</ul>
															</li>
															<li class="tree-4-li last-component">
																<a href="#">
																	<i class="far fa-minus-square tc-open-tree"></i>
																	<span class="tree-child tree-4-child" id="show_mini_drawer_table">Mini drawer</span>
																	
																</a>
																<ul class="mini-drawer-tree5 tree tc-tree5">
																	<li class="tree-5-li" style="border-left:1px solid #333;">
																		<a href="#">
																			<span class="tree-child tree-5-child">Mainboard</span>
																			
																		</a>
																	</li>
																	<li class="tree-5-li" style="border-left:1px solid #333;">
																		<a href="#">
																			<span class="tree-child tree-5-child">Daughterboard</span>
																			
																		</a>
																	</li>
																	<li class="tree-5-li" style="border-left:1px solid #333;">
																		<a href="#">
																			<span class="tree-child tree-5-child">HV distribution board</span>
																			
																		</a>
																	</li>
																	<li class="tree-5-li last-component"> 
																		<a href="#">
																			<i class="far fa-minus-square tc-open-tree"></i> 
																			<span class="tree-child tree-5-child" id="show_pmt_block_table">PMT block</span>
																			
																		</a>
																		<ul class="pmt-block-tree6 tree tc-tree6">
																			<li class="tree-6-li" style="border-left:1px solid #333;">
																				<a href="#">
																					<span class="tree-child tree-6-child">FENICS</span>
																					
																				</a>
																			</li>
																			<li class="tree-6-li" style="border-left:1px solid #333;">
																				<a href="#">
																					<span class="tree-child tree-6-child">HV divider</span>
																					
																				</a>
																			</li>
																			<li class="tree-6-li" style="border-left:1px solid #333;">
																				<a href="#">
																					<span class="tree-child tree-6-child">HV divider <span class="badge badge-warning">Old</span></span>
																					
																				</a>
																			</li>
																			<li class="tree-6-li" style="border-left:1px solid #333;">
																				<a href="#">
																					<span class="tree-child tree-6-child">3_in_1_board <span class="badge badge-warning">Old</span></span>
																					
																				</a>
																			</li>
																			<li class="tree-6-li last-component">
																				<a href="#" class="show_pmt_table">
																					<span class="tree-child tree-6-child">PMT <span class="badge badge-info">Filled with old Data</span></span>
																					
																				</a>
																			</li>
																		</ul>
																	</li>
																</ul>
															</li>
														</ul>
													</li>
												</ul>
											</li> 
										</ul> 
										<ul class="tree tc-tree2 no-pt">
											<li class="tree-2-li last-component">
												<a href="#">
													<i class="far fa-minus-square tc-open-tree"></i>
													<span class="tree-child tree-2-child">PPr blade</span>
													 
												</a>
												<ul class="tree tc-tree3">
													<li class="tree-3-li" style="border-left:1px solid #333;">
														<a href="#">
															<i class="far fa-minus-square tc-open-tree"></i>
															<span class="tree-child tree-3-child ">Carrier board</span>
															 
														</a>
														<ul class="tree tc-tree4">
															<li class="tree-4-li" style="border-left:1px solid #333;">
																<a class="tree-a" href="#">
																	<span class="tree-child tree-4-child">IPMC</span> 
																	
																</a>
															</li>
															<li class="tree-4-li" style="border-left:1px solid #333;">
																<a class="tree-a" href="#">
																	<span class="tree-child tree-4-child">CPM</span> 
																	
																</a>
															</li>
															<li class="tree-4-li" style="border-left:1px solid #333;">
																<a class="tree-a" href="#">
																	<span class="tree-child tree-4-child">TileCoM</span> 
																	
																</a>
															</li>
															<li class="tree-4-li last-component">
																<a class="tree-a" href="#">
																	<span class="tree-child tree-4-child">GbE switch</span>
																	 
																</a>
															</li>
														</ul>
													</li>
												</ul> 
												<ul class="tree tc-tree3">
													<li class="tree-3-li last-component">
														<a href="#">
															<span class="tree-child tree-3-child ">TDAQi</span> 
															
														</a>
													</li>
												</ul>
											</li>
										</ul>
									</li>
								</ul> 
							</li> 
						</ul>
					</nav>
					<div class="col main" role="main">
						<nav class="tc-navbar navbar bg-dark py-2">
							<i class="fas fa-chevron-left sidebar-collapse p-2"></i>
							<!-- Navbar search -->
							<!-- <form class="form-inline my-0">
								<input class="form-control form-control-dark form-control-sm" type="search" placeholder="Search" aria-label="Search">
							</form>  -->
							<ul class="navbar-nav">							
								<li class="nav-item ">								
									<a class="nav-link" href="#">Sign out</a>
								</li>
							</ul>
						</nav> 
						<div class="col main-content pt-3">
							<div class="row d-flex justify-content-start p-1">							
								<div class="btn-group ml-3">
									<div class="btn-group" role="group">										
										<button id="disassembly-btn-grp-drop" class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Disassembly</button>
										<div class="dropdown-menu" aria-labelledby="disassembly-btn-grp-drop">
											<a class="dropdown-item" href="#" data-toggle="modal" data-target="#sd-disassembly-example-modal">Super drawer</a>
											<a class="dropdown-item" href="#" data-toggle="modal" data-target="#pmt-block-disassembly-example-modal">PMT block</a>
											<a class="dropdown-item" href="#" data-toggle="modal" data-target="#stand-alone-pmt-block-disassembly-example-modal">Stand-alone PMT block</a>
										</div>
									</div>
									<div class="btn-group" role="group">
										<button id="disassembly-btn-grp-drop" class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Assembly</button>
										<div class="dropdown-menu" aria-labelledby="assembly-btn-grp-drop">
											<a class="dropdown-item" href="#" data-toggle="modal" data-target="#sd-assembly-example-modal">Super drawer</a>
											<a class="dropdown-item" href="#" data-toggle="modal" data-target="#mini-drawer-assembly-example-modal">Mini drawer</a>
											<a class="dropdown-item" href="#" data-toggle="modal" data-target="#micro-drawer-assembly-example-modal">Micro drawer</a>
											<a class="dropdown-item" href="#" data-toggle="modal" data-target="#pmt-block-assembly-example-modal">PMT block</a>
										</div>
									</div>
									<div class="btn-group" role="group">										
										<button id="insert-btn-grp-drop" class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Insert</button>
									</div>
									<div class="btn-group" role="group">
										<button id="component-delivery-btn-grp-drop" class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Reception of components at CERN</button>
										<div class="dropdown-menu" aria-labelledby="component-delivery-btn-grp-drop">
											<a class="dropdown-item" href="#" data-toggle="modal" data-target="#add_mini_drawer_modal">Mini drawer</a>
											<a class="dropdown-item" href="#" data-toggle="modal" data-target="#add_mainboard_modal">Mainboard</a>
											<a class="dropdown-item" href="#" data-toggle="modal" data-target="#add_daughterboard_modal">Daughterboard</a>
											<a class="dropdown-item" href="#" data-toggle="modal" data-target="#add_fenics_modal">FENICS</a>
										</div>
									</div>
								</div>
							</div>                       
						<div class="col main-content pt-2">
                            <!-- Welcome Message -->
							<!-- <div class="row p-1">
								<div class="alert alert-primary mt-1" role="alert">
								  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Welcome! please keep in mind that this is a demo and application functionality is not working. This only shows what the User Interface can look like and how different functionality can be operated trough user interface.
								</div>
							</div> -->
							<!-- Partition table -->
							<div class="row component-table-row partition-table-row">
								<div class="col table-section partition-col p-1">  
								<h5>Partition</h5>                          
									<div class="partition-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="partition_table">
											<thead>
												<tr>
													<th>Partition</th>
													<th>200 HV Bulk PS ID</th>
													<th>HV Bulk PS ID</th>
													<th>Comment</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div>   
								</div>	                                 
							</div>
							<!-- Module table -->
							<div class="row component-table-row module-table-row">
								<div class="col table-section module-col p-1">
								<h5>Module</h5> 
									<div class="module-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="module_table" style="width:100%">
											<thead>
												<tr>
													<th>Module</th>
													<th>Partition</th>
													<th>Module Number</th>
													<th>LV Box IDr</th>
													<th>PPR Blade ID</th>
													<th>Auxiliary Board ID</th>
													<th>HV Regulation Board ID</th>
													<th>Comment</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>
							<!-- Mini_drawer table -->
							<div class="row component-table-row mini-drawer-table-row">
								<div class="col table-section mini-drawer-col p-1">
								<h5>Mini Drawer</h5> 
									<div class="mini-drawer-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="mini_drawer_table" style="width:100%">
											<thead>
												<tr>
													<th>Mini Drawer ID</th>
													<th>Super Drawer ID</th>
													<th>Test Results</th>
													<th>Status</th>
													<th>Location</th>
													<th>Comment</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>
							<!-- PMT table -->
							<div class="row component-table-row pmt-table-row">
								<div class="col table-section pmt-col p-1">
									<h5>PMT</h5>                          
									<div class="pmt-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="pmt_table" style="width:100%">
											<thead>
												<tr>
													<th>Serial Number</th>
													<th>PMT Block ID</th>
													<th>Phase2 Module</th>
													<th>Old Module</th>
													<th>Phase2 Position in SD</th>
													<th>Old Position</th>
													<th>Beta</th>
													<th>HV Nomimal</th>
													<th>QE</th>
													<th>Type</th>
													<th>Run Number</th>
													<th>Status</th>
													<th>Location</th>
													<th>Comment</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
											<!-- <tfoot>
												<tr>
													<th>Serial Number</th>
													<th>PMT Block ID</th>
													<th>Phase2 Module</th>
													<th>Old Module</th>
													<th>Phase2 Position in SD</th>
													<th>Old Position</th>
													<th>Beta</th>
													<th>HV Nomimal</th>
													<th>QE</th>
													<th>Type</th>
													<th>Run Number</th>
													<th>Status</th>
													<th>Location</th>
													<th>Comment</th>
													<th>Action</th>
												</tr>
											</tfoot> -->
										</table>
									</div>  
								</div>	                                 
							</div>																
				        </div>
			        </div>
		        </div>
	        </div>
        </div>    
		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/script.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/component-tree.js"></script>
		<!-- jQuery Custom Scroller CDN -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
		<!-- jQuery dataTables.min.js CDN -->
		<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
		<!-- Toastr -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
		<!-- Datatables -->
		<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.24/r-2.2.7/sb-1.0.1/sp-1.2.2/datatables.min.js"></script>
		<!-- Sweet Alert 2-->
		<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	</body>
</html>

<script>

	//Fetch partition Records
	$(document).on("click", "#show_partition_table", function(e){
		e.preventDefault();

		$('#partition_table').DataTable().clear().destroy();
		fetchPartition();
	});
	function fetchPartition(){
		$.ajax({
			url: "<?php echo base_url(); ?>partition/fetch",
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.responce == "success"){	
								
					var table = $('#partition_table').DataTable( {
						"data": data.records,
						"rowId": "tc_partition",
						"columns": [
							{ "data": "tc_partition" },
							{ "data": "id_200_hv_bulk_ps" },
							{ "data": "id_hv_bulk_ps" },
							{ "data": "remark" },
							{ "render": function ( data, type, row, meta ){
								var a = `
									<i class="far fa-edit px-1" value="${row.tc_partition}" id ="edit_pmt" aria-hidden="true"></i>
									<i class="fa fa-clock-o px-1" aria-hidden="true"></i>
								`;
								return a;
							} }					
						]
					} );
				}else{
					toastr["error"](data.message);
				}					
			}
		});
	}

	//Fetch module Records
	$(document).on("click", "#show_module_table", function(e){
		e.preventDefault();

		$('#module_table').DataTable().clear().destroy();
		fetchModule();
	});
	function fetchModule(){
		$.ajax({
			url: "<?php echo base_url(); ?>module/fetch",
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.responce == "success"){	
								
					var table = $('#module_table').DataTable( {
						"data": data.records,
						"rowId": "module",
						"columns": [
							{ "data": "module" },
							{ "data": "tc_partition" },
							{ "data": "module_number" },
							{ "data": "id_lv_box" },
							{ "data": "id_ppr_blade" },
							{ "data": "id_auxiliary_board" },
							{ "data": "id_hv_regulation_board" },
							{ "data": "remark" },
							{ "render": function ( data, type, row, meta ){
								var a = `
									<i class="far fa-edit px-1" value="${row.module}" id ="edit_pmt" aria-hidden="true"></i>
									<i class="fa fa-clock-o px-1" aria-hidden="true"></i>
								`;
								return a;
							} }					
						]
					} );
				}else{
					toastr["error"](data.message);
				}					
			}
		});
	}

	//Fetch PMT Records
	$(document).on("click", ".show_pmt_table", function(e){
		e.preventDefault();

		$('#pmt_table').DataTable().clear().destroy();

		// Setup - add a text input to each footer cell
		// $('#pmt_table thead tr').clone(true).appendTo( '#pmt_table thead' );
		// $('#pmt_table thead tr:eq(1) th').each( function (i) {
		// 	var title = $(this).text();
		// 	$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
	
		// 	$( 'input', this ).on( 'keyup change', function () {
		// 		if ( table.column(i).search() !== this.value ) {
		// 			table
		// 				.column(i)
		// 				.search( this.value )
		// 				.draw();
		// 		}
		// 	} );
		// } );

		fetchPmt();
	});
	function fetchPmt(){
		$.ajax({
			url: "<?php echo base_url(); ?>pmt/fetch",
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.responce == "success"){	
								
					var table = $('#pmt_table').DataTable( {
						"data": data.records,
						// orderCellsTop: true,
						// fixedHeader: true,
						"rowId": "serial_number",
						"columns": [
							{ "data": "serial_number" },
							{ "data": "id_pmt_block" },
							{ "data": "module" },
							{ "data": "module_in_legacy_tilecal" },
							{ "data": "pos_in_phase2_tilecal" },
							{ "data": "pos_in_legacy_tilecal" },
							{ "data": "beta" },
							{ "data": "HV_nominal" },
							{ "data": "QE" },
							{ "data": "pmt_type" },
							{ "data": "run_number" },
							{ "data": "component_status" },
							{ "data": "current_location" },
							{ "data": "remark" },
							{ "render": function ( data, type, row, meta ){
								var a = `
									<i class="far fa-edit px-1" value="${row.serial_number}" id ="edit_pmt" aria-hidden="true"></i>
									<i class="fa fa-clock-o px-1" aria-hidden="true"></i>
									<i class="far fa-trash-alt px-1" value="${row.serial_number}" id ="delete_pmt" aria-hidden="true"></i>
								`;
								return a;
							} }					
						]
					} );
				}else{
					toastr["error"](data.message);
				}					
			}
		});
	}

	//Add Mini Drawer
	$(document).on("click", ".add_mini_drawer_form_submit", function(e){
		e.preventDefault();
		var idMiniDrawerFrame = $("#id_mini_drawer_frame").val();
		var miniDrawerStatus = $("#mini_drawer_status").val();	
		var miniDrawerCurrentLocation = $("#mini_drawer_current_location").val();
		var miniDrawerRemark = $("#mini_drawer_remark").val();
		if(miniDrawerCurrentLocation == ""){
			alert("Mini drawer ID is required");
		}else{
			$.ajax({
				url:"<?php echo base_url(); ?>/mini_drawer/insert",
				type: "post",
				dataType: "json",
				data: {
					id_mini_drawer_frame: idMiniDrawerFrame,
					component_status: miniDrawerStatus,
					current_location: miniDrawerCurrentLocation,
					remark: miniDrawerRemark
				},
				success: function(data){
					if(data.responce == "success"){
						$('#mini_drawer_table').DataTable().destroy();
						fetchMinidrawer();
						$('#add_mini_drawer_modal').modal('hide');
						toastr["success"](data.message);
					}else{
						toastr["error"](data.message);
					}
				}			
			});
			$("#add_mini_drawer_form")[0].reset();
		}
	});

	//Fetch Mini Drawer Records
	$(document).on("click", "#show_mini_drawer_table", function(e){
		e.preventDefault();
		$('#mini_drawer_table').DataTable().clear().destroy();
		fetchMinidrawer();
	});
	function fetchMinidrawer(){
		$.ajax({
			url: "<?php echo base_url(); ?>mini_drawer/fetch",
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.responce == "success"){				
					var table = $('#mini_drawer_table').DataTable( {
						"data": data.records,
						"rowId": "id_mini_drawer_frame",
						"columns": [
							{ "data": "id_mini_drawer_frame" },
							{ "data": "id_super_drawer" },
							{ "data": "test_results" },
							{ "data": "component_status" },
							{ "data": "current_location" },
							{ "data": "remark" },
							{ "render": function ( data, type, row, meta ){
								var a = `
									<i class="far fa-edit px-1" value="${row.id_mini_drawer_frame}" id ="edit_mini_drawer" aria-hidden="true"></i>
									<i class="fa fa-clock-o px-1" aria-hidden="true"></i>
									<i class="far fa-trash-alt px-1" value="${row.id_mini_drawer_frame}" id ="delete_mini_drawer" aria-hidden="true"></i>
								`;
								return a;
							} }					
						]
					} );
				}else{
					toastr["error"](data.message);
				}					
			}
		});
	}

	//delete Mini Drawer
	$(document).on("click", "#delete_mini_drawer", function(e){
		e.preventDefault();
		var del_id = $(this).attr("value");

		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: 'btn btn-sm btn-success ml-1',
				cancelButton: 'btn btn-sm btn-danger'
			},
			buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes, Delete!',
			cancelButtonText: 'No, Cancel!',
			reverseButtons: true
			}).then((result) => {
			if (result.isConfirmed) {

				$.ajax({
					url:"<?php echo base_url(); ?>mini_drawer/delete",
					type: "post",
					dataType: "json",
					data: {
						del_id: del_id
					},
					success: function(data){
						// console.log(data);
						if(data.responce == "success"){
							$('#mini_drawer_table').DataTable().clear().destroy();
							fetchMinidrawer();
							swalWithBootstrapButtons.fire(
							'Deleted!',
							'Record has been deleted.',
							'success'
							);
						}else{
							swalWithBootstrapButtons.fire(
							'Cancelled',
							'Record is safe',
							'error'
							);
						}
					}
				});
				
			} else if (				
				result.dismiss === Swal.DismissReason.cancel
			) {
				swalWithBootstrapButtons.fire(
				'Cancelled',
				'Record is safe',
				'error'
				)
			}
		})
	});

	//Edit Mini Drawer
	$(document).on("click", "#edit_mini_drawer", function(e){
		e.preventDefault();
		var edit_id = $(this).attr("value");
		$.ajax({
			url: "<?php echo base_url(); ?>mini_drawer/edit",
				type: "post",
				dataType: "json",
				data: {
					edit_id: edit_id
				},
			success: function(data){
				if(data.responce == "success"){
					$('#edit_mini_drawer_modal').modal('show');
					$("#edit_id_mini_drawer_frame").val(data.record.id_mini_drawer_frame);
					$("#edit_mini_drawer_status").val(data.record.component_status);	
					$("#edit_mini_drawer_current_location").val(data.record.current_location);
					$("#edit_mini_drawer_remark").val(data.record.remark);
				}else{
					toastr["error"](data.message);
				}
			}
		})
	});
	
	//Update Mini Drawer
	$(document).on("click", "#edit_mini_drawer_form_submit", function(e){
		e.preventDefault();
		
		var edit_id_mini_drawer_frame = $("#edit_id_mini_drawer_frame").val();
		var edit_mini_drawer_status = $("#edit_mini_drawer_status").val();
		var edit_mini_drawer_current_location = $("#edit_mini_drawer_current_location").val();
		var edit_mini_drawer_remark = $("#edit_mini_drawer_remark").val();
		if(edit_id_mini_drawer_frame == ""){
			alert('Mini Drawer ID is Required');
		}else{
			$.ajax({
				url: "<?php echo base_url(); ?>mini_drawer/update",
					type: "post",
					dataType: "json",
					data: {
						edit_id_mini_drawer_frame: edit_id_mini_drawer_frame,
						edit_mini_drawer_status: edit_mini_drawer_status,
						edit_mini_drawer_current_location: edit_mini_drawer_current_location,
						edit_mini_drawer_remark: edit_mini_drawer_remark
					},
				success: function(data){

					if(data.responce == "success"){
						$('#mini_drawer_table').DataTable().destroy();
						fetchMinidrawer();
						$('#edit_mini_drawer_modal').modal('hide');
						toastr["success"](data.message);
					}else{
						toastr["error"](data.message);
					}
				}
			})
		}
	});
</script>