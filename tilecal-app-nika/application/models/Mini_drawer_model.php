<?php 

class Mini_drawer_model extends CI_Model {
    //get entries query, etiteba romeli table-dan gvinda wamovigot informacia, funqcia tolfasia shemdegi query-s: SELECT * from mini_drawer;
    public function get_entries()
    {
        $query = $this->db->get('mini_drawer');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
        
    }
    //insert entry query, shemodis cvladi $data(masivi) Mini_drawer.php-s insert funqciidan
    public function insert_entry($data)
    {
       return  $this->db->insert('mini_drawer', $data);
    }
    //delete entry, shemodis cvladi $id Mini_drawer.php delete funqciidan, ris mixedvitac funqcia shlis shesabamisi id-s mqonde row-s.
    public function delete_entry($id)
    {
       return  $this->db->delete('mini_drawer', array('id_mini_drawer_frame' => $id));
    }
    //edit entry, shemodis cvladi $id, Mini_drawer.php edit funqciidan, funqcia abrunebs records am shemosul id-ze.
    public function edit_entry($id)
    {
        $this->db->select("*");
        $this->db->from("mini_drawer");
        $this->db->where("id_mini_drawer_frame", $id);
        $query = $this->db->get();
        if(count($query->result()) > 0){
            return $query->row();
        }
    }
    //update entry, shemodis cvladi(masivi) $data, Mini_drawer.php update funqciidan
    public function update_entry($data)
    {
        return $this->db->update('mini_drawer', $data, array('id_mini_drawer_frame' => $data['id_mini_drawer_frame']));

    }
}

?>