-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 08, 2021 at 03:27 PM
-- Server version: 8.0.26
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tilecal_electronics_hist`
--
CREATE DATABASE IF NOT EXISTS `tilecal_electronics_hist` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `tilecal_electronics_hist`;

-- --------------------------------------------------------

--
-- Table structure for table `board_3_in_1_hist`
--

DROP TABLE IF EXISTS `board_3_in_1_hist`;
CREATE TABLE `board_3_in_1_hist` (
  `id_3_in_1_hist` int NOT NULL,
  `id_3_in_1` int NOT NULL,
  `module` varchar(5) DEFAULT NULL,
  `id_super_drawer` int DEFAULT NULL,
  `id_pmt_block` int DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `carrier_board_hist`
--

DROP TABLE IF EXISTS `carrier_board_hist`;
CREATE TABLE `carrier_board_hist` (
  `id_carrier_board_hist` int NOT NULL,
  `id_carrier_board` int NOT NULL,
  `id_ppr_blade` int DEFAULT NULL,
  `production_batch` varchar(50) DEFAULT NULL,
  `hardware_revision` varchar(50) DEFAULT NULL,
  `firmware_version` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cpm_hist`
--

DROP TABLE IF EXISTS `cpm_hist`;
CREATE TABLE `cpm_hist` (
  `id_cpm_hist` int NOT NULL,
  `id_cpm` int NOT NULL,
  `id_carrier_board` int DEFAULT NULL,
  `production_batch` varchar(50) DEFAULT NULL,
  `hardware_revision` varchar(50) DEFAULT NULL,
  `firmware_version` varchar(50) DEFAULT NULL,
  `mac_address` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `daughterboard_hist`
--

DROP TABLE IF EXISTS `daughterboard_hist`;
CREATE TABLE `daughterboard_hist` (
  `id_daughterboard_hist` int NOT NULL,
  `id_daughterboard` int NOT NULL,
  `id_mini_drawer` int DEFAULT NULL,
  `component_status` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `daughterboard_hist`
--

INSERT INTO `daughterboard_hist` (`id_daughterboard_hist`, `id_daughterboard`, `id_mini_drawer`, `component_status`, `current_location`, `remark`, `end_date`, `created_by`, `changed_by`, `deleted_by`) VALUES
(1, 1, 17, '', '', '', '2021-07-18 07:55:45', NULL, NULL, NULL),
(2, 44, 17, NULL, NULL, NULL, '2021-07-18 08:06:05', 100, 100, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fenics_hist`
--

DROP TABLE IF EXISTS `fenics_hist`;
CREATE TABLE `fenics_hist` (
  `id_fenics_hist` int NOT NULL,
  `id_fenics` int NOT NULL,
  `id_pmt_block` int DEFAULT NULL,
  `component_status` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `fenics_hist`
--

INSERT INTO `fenics_hist` (`id_fenics_hist`, `id_fenics`, `id_pmt_block`, `component_status`, `current_location`, `remark`, `end_date`, `created_by`, `changed_by`, `deleted_by`) VALUES
(1, 2, 444, 'yes', 'yes', 'yes', '2021-07-04 09:55:56', NULL, NULL, NULL),
(2, 1, 444, 'Yes', 'tn', 'rn', '2021-07-04 10:14:57', 100, 100, NULL),
(3, 1, 444, 'Nooooooo', 'tn', '', '2021-07-04 10:15:16', 100, 100, NULL),
(4, 1, 444, 'Yes', 'tbi', '', '2021-07-04 10:15:39', 100, 100, NULL),
(5, 1, 444, 'Yes', 'tbi', '', '2021-07-04 10:16:29', 100, 100, NULL),
(6, 1, 444, 'Yes', 'tbi', 'Tb', '2021-07-04 10:17:41', 100, NULL, 100),
(7, 2, 444, 'Yes', 'Tbilisi', 'Admin', '2021-07-04 10:17:52', NULL, NULL, NULL),
(8, 4, 445, 'Yes', 'Tbilisi', 'Admin', '2021-07-05 07:59:27', NULL, NULL, NULL),
(9, 4, 445, 'tyutyu', 'Tbilisi tyutyutu', 'Adminiiiiiiiii', '2021-07-05 08:00:26', NULL, NULL, NULL),
(10, 35, NULL, 'ttttttttttttttt', 'hhhhhhhhhhhhhhh', 'mmmmmmmmmmmmmmmmm', '2021-07-07 19:42:07', NULL, NULL, NULL),
(11, 33, NULL, 'rty', 'rty', 'rty', '2021-07-07 19:42:11', NULL, NULL, NULL),
(12, 3, NULL, 'No', 'Tbil', 'Admin', '2021-07-07 19:42:14', NULL, NULL, NULL),
(13, 2, NULL, 'Yes', 'Bat', 'Admin', '2021-07-07 19:42:17', NULL, NULL, NULL),
(14, 34, 446, 'ert', 'ert', 'ert', '2021-07-07 19:43:11', NULL, NULL, NULL),
(15, 1, 446, '1', '1', '1', '2021-07-12 07:02:53', NULL, NULL, NULL),
(16, 2, 446, '', '', '', '2021-07-12 07:02:57', NULL, NULL, NULL),
(17, 34, NULL, '1', '2', '3', '2021-07-14 09:06:55', NULL, NULL, NULL),
(18, 1, 445, '', '', '', '2021-07-14 09:07:57', NULL, NULL, NULL),
(19, 1, 445, 'y', '', '', '2021-07-17 09:25:53', NULL, NULL, NULL),
(20, 2, 445, '123', '123', '123', '2021-07-17 09:27:07', NULL, NULL, NULL),
(21, 5, 445, '', '', '', '2021-07-18 11:48:45', NULL, NULL, NULL),
(22, 1, 445, 'y', 'asd', '', '2021-07-20 12:50:31', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gbe_switch_hist`
--

DROP TABLE IF EXISTS `gbe_switch_hist`;
CREATE TABLE `gbe_switch_hist` (
  `id_gbe_switch_hist` int NOT NULL,
  `id_gbe_switch` int NOT NULL,
  `id_carrier_board` int DEFAULT NULL,
  `production_batch` varchar(50) DEFAULT NULL,
  `hardware_revision` varchar(50) DEFAULT NULL,
  `firmware_version` varchar(50) DEFAULT NULL,
  `mac_address` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hv_distribution_board_hist`
--

DROP TABLE IF EXISTS `hv_distribution_board_hist`;
CREATE TABLE `hv_distribution_board_hist` (
  `id_hv_distribution_board_hist` int NOT NULL,
  `id_hv_distribution_board` int NOT NULL,
  `id_mini_drawer` int DEFAULT NULL,
  `component_status` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `hv_distribution_board_hist`
--

INSERT INTO `hv_distribution_board_hist` (`id_hv_distribution_board_hist`, `id_hv_distribution_board`, `id_mini_drawer`, `component_status`, `current_location`, `remark`, `end_date`, `created_by`, `changed_by`, `deleted_by`) VALUES
(1, 1, 17, '', '', '', '2021-07-18 11:34:16', NULL, NULL, NULL),
(2, 56, 17, NULL, NULL, NULL, '2021-07-18 11:48:36', 100, 100, NULL),
(3, 2, 17, '12', '12', '12', '2021-07-18 11:53:30', NULL, NULL, NULL),
(4, 2, 17, '', '', '', '2021-07-18 11:54:49', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hv_divider_hist`
--

DROP TABLE IF EXISTS `hv_divider_hist`;
CREATE TABLE `hv_divider_hist` (
  `id_hv_divider_hist` int NOT NULL,
  `id_hv_divider` int NOT NULL,
  `id_pmt_block` int DEFAULT NULL,
  `component_status` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `hv_divider_hist`
--

INSERT INTO `hv_divider_hist` (`id_hv_divider_hist`, `id_hv_divider`, `id_pmt_block`, `component_status`, `current_location`, `remark`, `end_date`, `created_by`, `changed_by`, `deleted_by`) VALUES
(1, 3, 446, 'y', 'y', 'y', '2021-07-06 10:36:02', NULL, NULL, NULL),
(2, 1, 446, 'Yes', 'y', 'y', '2021-07-06 10:49:00', 100, 100, NULL),
(3, 2, 446, '1', '2', '3', '2021-07-07 19:43:18', NULL, NULL, NULL),
(4, 1, 446, 'No', 'n', 'n', '2021-07-07 19:43:23', 100, 100, NULL),
(5, 1, 446, 'No', '1', '2', '2021-07-12 07:02:30', 100, NULL, 100),
(6, 2, 446, '1', '2', '3', '2021-07-12 07:02:33', NULL, NULL, NULL),
(7, 3, 446, '1', '2', '3', '2021-07-12 07:02:37', NULL, NULL, NULL),
(8, 12, 446, '', '', '', '2021-07-12 07:02:39', NULL, NULL, NULL),
(9, 14, 446, '', '', '12312312', '2021-07-12 07:02:43', NULL, NULL, NULL),
(10, 15, 446, '', '', '', '2021-07-12 07:02:46', NULL, NULL, NULL),
(11, 23, NULL, '', '', '', '2021-07-14 09:07:35', NULL, NULL, NULL),
(12, 1, 445, '', '', '', '2021-07-14 09:07:51', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hv_divider_old_hist`
--

DROP TABLE IF EXISTS `hv_divider_old_hist`;
CREATE TABLE `hv_divider_old_hist` (
  `id_hv_divider_old_hist` int NOT NULL,
  `id_hv_divider_old` int NOT NULL,
  `module` varchar(5) DEFAULT NULL,
  `id_super_drawer` int DEFAULT NULL,
  `id_pmt_block` int DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ipmc_hist`
--

DROP TABLE IF EXISTS `ipmc_hist`;
CREATE TABLE `ipmc_hist` (
  `id_ipmc_hist` int NOT NULL,
  `id_ipmc` int NOT NULL,
  `id_carrier_board` int DEFAULT NULL,
  `production_batch` varchar(50) DEFAULT NULL,
  `hardware_revision` varchar(50) DEFAULT NULL,
  `firmware_version` varchar(50) DEFAULT NULL,
  `mac_address` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mainboard_hist`
--

DROP TABLE IF EXISTS `mainboard_hist`;
CREATE TABLE `mainboard_hist` (
  `id_mainboard_hist` int NOT NULL,
  `id_mainboard` int NOT NULL,
  `id_mini_drawer` int DEFAULT NULL,
  `component_status` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `mainboard_hist`
--

INSERT INTO `mainboard_hist` (`id_mainboard_hist`, `id_mainboard`, `id_mini_drawer`, `component_status`, `current_location`, `remark`, `end_date`, `created_by`, `changed_by`, `deleted_by`) VALUES
(1, 1, 17, '', '', '', '2021-07-17 20:41:57', NULL, NULL, NULL),
(2, 2, 17, '', '', '', '2021-07-17 20:49:49', NULL, NULL, NULL),
(3, 2, 17, 'y', 'y', 'y', '2021-07-17 20:50:18', NULL, NULL, NULL),
(4, 1, 102, '', '', '', '2021-07-17 20:56:43', NULL, NULL, NULL),
(5, 2, 17, 'n', 'n', 'yn', '2021-07-17 20:56:52', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `micro_drawer_hist`
--

DROP TABLE IF EXISTS `micro_drawer_hist`;
CREATE TABLE `micro_drawer_hist` (
  `id_micro_drawer_frame_hist` int NOT NULL,
  `id_micro_drawer_frame` int NOT NULL,
  `id_super_drawer` int DEFAULT NULL,
  `test_results` blob,
  `component_status` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `micro_drawer_hist`
--

INSERT INTO `micro_drawer_hist` (`id_micro_drawer_frame_hist`, `id_micro_drawer_frame`, `id_super_drawer`, `test_results`, `component_status`, `current_location`, `remark`, `end_date`, `created_by`, `changed_by`, `deleted_by`) VALUES
(1, 12, 123, NULL, '', '', '', '2021-07-17 10:48:06', NULL, NULL, NULL),
(2, 3, 123, NULL, '', '', '', '2021-07-17 10:50:29', NULL, NULL, NULL),
(3, 44, 123, NULL, 'Yes', 'tb', NULL, '2021-07-17 13:55:56', 100, NULL, 100),
(4, 1, 123, NULL, '3', '3', '3', '2021-07-17 14:07:36', NULL, NULL, NULL),
(5, 1, 123, NULL, '2', '2', '32', '2021-07-17 14:08:57', NULL, NULL, NULL),
(6, 2, 123, NULL, '2', '', '', '2021-07-17 15:04:51', NULL, NULL, NULL),
(7, 1, 123, NULL, '2', '2', '5555', '2021-07-17 15:07:11', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mini_drawer_hist`
--

DROP TABLE IF EXISTS `mini_drawer_hist`;
CREATE TABLE `mini_drawer_hist` (
  `id_mini_drawer_frame_hist` int NOT NULL,
  `id_mini_drawer_frame` int NOT NULL,
  `id_super_drawer` int DEFAULT NULL,
  `test_results` blob,
  `component_status` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `mini_drawer_hist`
--

INSERT INTO `mini_drawer_hist` (`id_mini_drawer_frame_hist`, `id_mini_drawer_frame`, `id_super_drawer`, `test_results`, `component_status`, `current_location`, `remark`, `end_date`, `created_by`, `changed_by`, `deleted_by`) VALUES
(1, 0, NULL, NULL, 'wsrtwt`', 'gsgfs', 'dhgdhg', '2021-06-05 09:22:10', NULL, NULL, NULL),
(5, 456, NULL, NULL, 'fd', 're', 'rwt', '2021-06-05 09:48:32', NULL, NULL, NULL),
(6, 12, NULL, NULL, 'ASd', 'ASd', 'Asd', '2021-06-09 12:50:22', NULL, NULL, NULL),
(7, 12, NULL, NULL, 'Okay', 'Tb', 'Asd', '2021-06-23 11:39:18', NULL, NULL, NULL),
(8, 13, NULL, NULL, 'true', 'Tb', 'Asd', '2021-06-23 12:16:42', NULL, NULL, NULL),
(9, 14, NULL, NULL, 'asdad', 'asdasdsad', 'asdasdasasad', '2021-06-25 13:12:25', NULL, NULL, NULL),
(10, 13, NULL, NULL, 'fas', 'Tb', 'Asd', '2021-06-29 13:50:57', NULL, NULL, NULL),
(11, 14, NULL, NULL, 'sdafd', 'dgsgf', 'ghdfkg', '2021-06-29 13:51:52', NULL, NULL, NULL),
(12, 14, NULL, NULL, '123', '456', 'ghdfkg', '2021-07-02 17:19:31', NULL, NULL, NULL),
(13, 2, 1, NULL, NULL, NULL, NULL, '2021-07-17 19:54:05', NULL, NULL, NULL),
(14, 14, NULL, NULL, '123', 'y', 'ghdfkg', '2021-07-17 19:54:12', NULL, NULL, NULL),
(15, 17, NULL, NULL, 'Delivered', 'Bld.40', 'From Lasha', '2021-07-17 19:55:46', NULL, NULL, NULL),
(16, 2, NULL, NULL, '3', '', '', '2021-07-17 19:55:53', NULL, NULL, NULL),
(17, 100, 1, NULL, '', '', '', '2021-07-17 19:56:37', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `module_hist`
--

DROP TABLE IF EXISTS `module_hist`;
CREATE TABLE `module_hist` (
  `id_module_hist` int NOT NULL,
  `module` varchar(5) NOT NULL,
  `tc_partition` varchar(3) NOT NULL,
  `module_number` char(2) NOT NULL,
  `id_lv_box` int DEFAULT NULL,
  `id_ppr_blade` int DEFAULT NULL,
  `id_auxiliary_board` int DEFAULT NULL,
  `id_hv_regulation_board` int DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pmt_block_hist`
--

DROP TABLE IF EXISTS `pmt_block_hist`;
CREATE TABLE `pmt_block_hist` (
  `id_pmt_block_hist` int NOT NULL,
  `id_pmt_block` int NOT NULL,
  `id_super_drawer` int DEFAULT NULL,
  `id_mini_drawer` int DEFAULT NULL,
  `id_micro_drawer` int DEFAULT NULL,
  `id_mainboard` int DEFAULT NULL,
  `id_daughterboard` int DEFAULT NULL,
  `id_hv_distribution_board` int DEFAULT NULL,
  `test_results` blob,
  `component_status` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `pmt_block_hist`
--

INSERT INTO `pmt_block_hist` (`id_pmt_block_hist`, `id_pmt_block`, `id_super_drawer`, `id_mini_drawer`, `id_micro_drawer`, `id_mainboard`, `id_daughterboard`, `id_hv_distribution_board`, `test_results`, `component_status`, `current_location`, `remark`, `end_date`, `created_by`, `changed_by`, `deleted_by`) VALUES
(1, 555, NULL, 17, 44, 23, 44, 56, NULL, NULL, NULL, NULL, '2021-07-02 09:36:06', 100, 100, NULL),
(2, 555, NULL, 17, 44, 23, 44, 56, NULL, 'Yes', 'Tb', 'Adm', '2021-07-02 09:40:40', 100, NULL, 100),
(3, 4444, NULL, 17, 44, 23, 44, 56, 0x61, 's', 'd', 'b', '2021-07-02 16:16:52', NULL, NULL, NULL),
(4, 432, NULL, 17, 44, 23, 44, 56, NULL, 'Yes', 'tb', 'adm', '2021-07-02 17:02:05', 100, 100, NULL),
(5, 432, NULL, 17, 44, 23, 44, 56, NULL, 'Yes', 'no', 'adm', '2021-07-02 17:22:28', 100, 100, NULL),
(6, 432, NULL, 17, 44, 23, 44, 56, NULL, 'Yes', 'no', 'adm', '2021-07-02 17:22:35', 100, 100, NULL),
(7, 432, NULL, 17, 44, 23, 44, 56, NULL, 'asd', 'no', 'adm', '2021-07-02 17:22:48', 100, 100, NULL),
(8, 432, NULL, 17, 44, 23, 44, 56, NULL, 'a', 'no', 'adm', '2021-07-02 17:23:42', 100, 100, NULL),
(9, 432, NULL, 17, 44, 23, 44, 56, NULL, 'rav', 'rav', 'adm', '2021-07-02 17:24:19', 100, 100, NULL),
(10, 444, NULL, 17, 44, 23, 44, 56, 0x6173, 'as', 'as', 'as', '2021-07-02 17:24:34', NULL, NULL, NULL),
(11, 455, NULL, 17, 44, 23, 44, 56, 0x61, 's', 'd', 'a', '2021-07-02 17:24:41', NULL, NULL, NULL),
(12, 455, NULL, 17, 44, 23, 44, 56, 0x61, 'Adm', 'Tbilisi', 'a', '2021-07-02 17:25:35', NULL, NULL, NULL),
(13, 432, NULL, 17, 44, 23, 44, 56, NULL, 'adm', 'Tbilisi', 'adm', '2021-07-02 18:09:13', 100, NULL, 100),
(14, 444, NULL, 17, 44, 23, 44, 56, 0x6173, 'Adm', 'Tbilisi', 'as', '2021-07-02 18:09:20', NULL, NULL, NULL),
(15, 444, NULL, 17, 44, 23, 44, 56, 0x6173, 'yeap', 'asd', 'as', '2021-07-02 18:09:34', NULL, NULL, NULL),
(16, 444, NULL, 17, 44, 23, 44, 56, 0x6173, 'A', 'Tbilisi', 'as', '2021-07-04 10:18:54', NULL, NULL, NULL),
(17, 444, NULL, 17, 44, 23, 44, 56, 0x6173, 'Yes', 'Tbilisi', 'as', '2021-07-04 10:18:59', NULL, NULL, NULL),
(18, 445, NULL, 17, 44, 23, 44, 56, 0x596573, 'Yes', 'Tbilisi', 'Admin', '2021-07-05 08:02:03', NULL, NULL, NULL),
(19, 445, NULL, 17, 44, 23, 44, 56, 0x596573, 'negative', 'geneva', 'Admin', '2021-07-05 08:04:02', NULL, NULL, NULL),
(20, 800, NULL, 17, 44, 23, 44, 56, 0x666768666768666768, 'fghfghfghf', 'bnmbnmbnm', 'sghhhhhhhhhhhhhhhhhhhh', '2021-07-05 08:07:06', NULL, NULL, NULL),
(21, 446, NULL, 17, 44, 23, 44, 56, 0x796573, 'ys', 'ys', 'ys', '2021-07-12 07:03:08', NULL, NULL, NULL),
(22, 444, NULL, 17, 44, 23, 44, 56, '', '', '', '', '2021-07-14 09:08:09', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pmt_hist`
--

DROP TABLE IF EXISTS `pmt_hist`;
CREATE TABLE `pmt_hist` (
  `id_pmt_hist` int NOT NULL,
  `serial_number` varchar(6) NOT NULL,
  `id_pmt_block` int DEFAULT NULL,
  `module` varchar(5) DEFAULT NULL,
  `module_in_legacy_tilecal` varchar(5) DEFAULT NULL,
  `pos_in_legacy_tilecal` varchar(4) DEFAULT NULL,
  `pos_in_phase2_tilecal` varchar(4) DEFAULT NULL,
  `beta` float DEFAULT NULL,
  `HV_nominal` float DEFAULT NULL,
  `QE` float DEFAULT NULL,
  `pmt_type` varchar(15) DEFAULT NULL,
  `run_number` int DEFAULT NULL,
  `component_status` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `pmt_hist`
--

INSERT INTO `pmt_hist` (`id_pmt_hist`, `serial_number`, `id_pmt_block`, `module`, `module_in_legacy_tilecal`, `pos_in_legacy_tilecal`, `pos_in_phase2_tilecal`, `beta`, `HV_nominal`, `QE`, `pmt_type`, `run_number`, `component_status`, `current_location`, `remark`, `end_date`, `created_by`, `changed_by`, `deleted_by`) VALUES
(1, '110003', NULL, NULL, NULL, NULL, NULL, 6.482, 708, 20.9, 'R11187\r', 158, NULL, NULL, NULL, '2021-06-09 11:43:52', NULL, NULL, NULL),
(2, '110005', NULL, NULL, NULL, NULL, NULL, 6.581, 687, 20.6, 'R11187\r', 158, NULL, NULL, NULL, '2021-06-09 11:44:06', NULL, NULL, NULL),
(3, '110032', NULL, NULL, NULL, NULL, NULL, 6.385, 733, 21, 'R11187\r', 158, NULL, NULL, NULL, '2021-06-09 11:44:13', NULL, NULL, NULL),
(4, '110041', NULL, NULL, NULL, NULL, NULL, 6.688, 641, 20.5, 'R11187\r', 158, NULL, NULL, NULL, '2021-06-09 11:44:21', NULL, NULL, NULL),
(5, '110050', NULL, NULL, NULL, NULL, NULL, 6.417, 667, 22, 'R11187\r', 158, NULL, NULL, NULL, '2021-06-09 12:34:22', NULL, NULL, NULL),
(6, '110053', NULL, NULL, NULL, NULL, NULL, 6.599, 676, 21.8, 'R11187\r', 158, NULL, NULL, NULL, '2021-06-09 12:34:26', NULL, NULL, NULL),
(7, '110054', NULL, NULL, NULL, NULL, NULL, 6.87, 679, 21.2, 'R11187\r', 158, NULL, NULL, NULL, '2021-06-09 16:46:45', NULL, NULL, NULL),
(8, '110055', NULL, NULL, NULL, NULL, NULL, 6.451, 678, 22.3, 'R11187\r', 158, NULL, NULL, NULL, '2021-06-09 18:37:13', NULL, NULL, NULL),
(9, '110055', NULL, NULL, NULL, NULL, NULL, 6.451, 678, 22.3, 'R11187\r', 158, 'asd', 'asd', 'asd', '2021-06-09 18:37:52', NULL, NULL, NULL),
(10, '110055', NULL, NULL, NULL, NULL, NULL, 6.451, 678, 22.3, 'R11187\r', 158, '', '', '', '2021-06-09 18:54:48', NULL, NULL, NULL),
(11, '110055', NULL, NULL, NULL, NULL, NULL, 6.451, 678, 22.3, 'R11187\r', 158, 'true', 'okay', 'whatever', '2021-06-09 19:02:07', NULL, NULL, NULL),
(12, '110055', NULL, NULL, NULL, NULL, NULL, 6.451, 678, 22.3, 'R11187\r', 158, '', '', '', '2021-06-23 10:35:41', NULL, NULL, NULL),
(13, '110056', NULL, NULL, NULL, NULL, NULL, 6.796, 660, 22.7, 'R11187\r', 158, NULL, NULL, NULL, '2021-07-01 05:40:03', NULL, NULL, NULL),
(14, '110058', NULL, NULL, NULL, NULL, NULL, 6.599, 652, 22.2, 'R11187\r', 158, NULL, NULL, NULL, '2021-07-05 07:45:33', NULL, NULL, NULL),
(15, '110058', NULL, NULL, NULL, NULL, NULL, 6.599, 652, 22.2, 'R11187\r', 158, 'Lasha', 'Bld.42', 'New delivery', '2021-07-05 07:45:37', NULL, NULL, NULL),
(16, '110058', NULL, NULL, NULL, NULL, NULL, 6.599, 652, 22.2, 'R11187\r', 158, 'Lasha', 'Bld.42', 'New delivery', '2021-07-07 19:44:20', NULL, NULL, NULL),
(17, '110062', NULL, NULL, NULL, NULL, NULL, 6.759, 686, 20.5, 'R11187\r', 158, NULL, NULL, NULL, '2021-07-07 19:44:29', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ppr_blade_hist`
--

DROP TABLE IF EXISTS `ppr_blade_hist`;
CREATE TABLE `ppr_blade_hist` (
  `id_ppr_blade_hist` int NOT NULL,
  `id_ppr_blade` int NOT NULL,
  `rack_number` int DEFAULT NULL,
  `shelf_number` int DEFAULT NULL,
  `slot_number` int DEFAULT NULL,
  `production_batch` varchar(50) DEFAULT NULL,
  `hardware_revision` varchar(50) DEFAULT NULL,
  `firmware_version` varchar(50) DEFAULT NULL,
  `installation_date` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `super_drawer_hist`
--

DROP TABLE IF EXISTS `super_drawer_hist`;
CREATE TABLE `super_drawer_hist` (
  `id_super_drawer_frame_hist` int NOT NULL,
  `id_super_drawer_frame` int NOT NULL,
  `module` varchar(5) NOT NULL,
  `test_results` blob,
  `component_status` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `super_drawer_hist`
--

INSERT INTO `super_drawer_hist` (`id_super_drawer_frame_hist`, `id_super_drawer_frame`, `module`, `test_results`, `component_status`, `current_location`, `remark`, `end_date`, `created_by`, `changed_by`, `deleted_by`) VALUES
(1, 111, 'EBA00', NULL, 'Yes', 'tb', 'rm', '2021-07-01 07:20:20', 100, NULL, 100),
(2, 2, 'EBA00', NULL, '', '', '', '2021-07-17 15:05:40', NULL, NULL, NULL),
(3, 2, 'EBA00', NULL, '123', '', '', '2021-07-17 16:05:53', NULL, NULL, NULL),
(4, 2, 'EBA00', NULL, '22', '22', '22', '2021-07-17 16:08:59', NULL, NULL, NULL),
(5, 1, 'EBA00', NULL, '1', '2', '3', '2021-07-17 16:25:24', NULL, NULL, NULL),
(6, 1, 'EBA00', NULL, '19999999', '2', '3', '2021-07-17 16:26:04', NULL, NULL, NULL),
(7, 1, 'EBA00', NULL, '52', '2', '3', '2021-07-17 16:26:34', NULL, NULL, NULL),
(8, 1, 'EBA00', NULL, '52', '000', '3', '2021-07-17 16:27:19', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tc_partition_hist`
--

DROP TABLE IF EXISTS `tc_partition_hist`;
CREATE TABLE `tc_partition_hist` (
  `id_tc_partition_hist` int NOT NULL,
  `tc_partition` varchar(3) NOT NULL,
  `id_200_hv_bulk_ps` int DEFAULT NULL,
  `id_hv_bulk_ps` int DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tdaqi_hist`
--

DROP TABLE IF EXISTS `tdaqi_hist`;
CREATE TABLE `tdaqi_hist` (
  `id_tdaqi_hist` int NOT NULL,
  `id_tdaqi` int NOT NULL,
  `id_ppr_blade` int DEFAULT NULL,
  `production_batch` varchar(50) DEFAULT NULL,
  `hardware_revision` varchar(50) DEFAULT NULL,
  `firmware_version` varchar(50) DEFAULT NULL,
  `mac_address` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tilecom_hist`
--

DROP TABLE IF EXISTS `tilecom_hist`;
CREATE TABLE `tilecom_hist` (
  `id_tilecom_hist` int NOT NULL,
  `id_tilecom` int NOT NULL,
  `id_carrier_board` int DEFAULT NULL,
  `production_batch` varchar(50) DEFAULT NULL,
  `hardware_revision` varchar(50) DEFAULT NULL,
  `firmware_version` varchar(50) DEFAULT NULL,
  `mac_address` varchar(50) DEFAULT NULL,
  `current_location` varchar(80) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int DEFAULT NULL,
  `changed_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `board_3_in_1_hist`
--
ALTER TABLE `board_3_in_1_hist`
  ADD PRIMARY KEY (`id_3_in_1_hist`);

--
-- Indexes for table `carrier_board_hist`
--
ALTER TABLE `carrier_board_hist`
  ADD PRIMARY KEY (`id_carrier_board_hist`);

--
-- Indexes for table `cpm_hist`
--
ALTER TABLE `cpm_hist`
  ADD PRIMARY KEY (`id_cpm_hist`);

--
-- Indexes for table `daughterboard_hist`
--
ALTER TABLE `daughterboard_hist`
  ADD PRIMARY KEY (`id_daughterboard_hist`);

--
-- Indexes for table `fenics_hist`
--
ALTER TABLE `fenics_hist`
  ADD PRIMARY KEY (`id_fenics_hist`);

--
-- Indexes for table `gbe_switch_hist`
--
ALTER TABLE `gbe_switch_hist`
  ADD PRIMARY KEY (`id_gbe_switch_hist`);

--
-- Indexes for table `hv_distribution_board_hist`
--
ALTER TABLE `hv_distribution_board_hist`
  ADD PRIMARY KEY (`id_hv_distribution_board_hist`);

--
-- Indexes for table `hv_divider_hist`
--
ALTER TABLE `hv_divider_hist`
  ADD PRIMARY KEY (`id_hv_divider_hist`);

--
-- Indexes for table `hv_divider_old_hist`
--
ALTER TABLE `hv_divider_old_hist`
  ADD PRIMARY KEY (`id_hv_divider_old_hist`);

--
-- Indexes for table `ipmc_hist`
--
ALTER TABLE `ipmc_hist`
  ADD PRIMARY KEY (`id_ipmc_hist`);

--
-- Indexes for table `mainboard_hist`
--
ALTER TABLE `mainboard_hist`
  ADD PRIMARY KEY (`id_mainboard_hist`);

--
-- Indexes for table `micro_drawer_hist`
--
ALTER TABLE `micro_drawer_hist`
  ADD PRIMARY KEY (`id_micro_drawer_frame_hist`);

--
-- Indexes for table `mini_drawer_hist`
--
ALTER TABLE `mini_drawer_hist`
  ADD PRIMARY KEY (`id_mini_drawer_frame_hist`);

--
-- Indexes for table `module_hist`
--
ALTER TABLE `module_hist`
  ADD PRIMARY KEY (`id_module_hist`);

--
-- Indexes for table `pmt_block_hist`
--
ALTER TABLE `pmt_block_hist`
  ADD PRIMARY KEY (`id_pmt_block_hist`);

--
-- Indexes for table `pmt_hist`
--
ALTER TABLE `pmt_hist`
  ADD PRIMARY KEY (`id_pmt_hist`);

--
-- Indexes for table `ppr_blade_hist`
--
ALTER TABLE `ppr_blade_hist`
  ADD PRIMARY KEY (`id_ppr_blade_hist`);

--
-- Indexes for table `super_drawer_hist`
--
ALTER TABLE `super_drawer_hist`
  ADD PRIMARY KEY (`id_super_drawer_frame_hist`);

--
-- Indexes for table `tc_partition_hist`
--
ALTER TABLE `tc_partition_hist`
  ADD PRIMARY KEY (`id_tc_partition_hist`);

--
-- Indexes for table `tdaqi_hist`
--
ALTER TABLE `tdaqi_hist`
  ADD PRIMARY KEY (`id_tdaqi_hist`);

--
-- Indexes for table `tilecom_hist`
--
ALTER TABLE `tilecom_hist`
  ADD PRIMARY KEY (`id_tilecom_hist`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `board_3_in_1_hist`
--
ALTER TABLE `board_3_in_1_hist`
  MODIFY `id_3_in_1_hist` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `carrier_board_hist`
--
ALTER TABLE `carrier_board_hist`
  MODIFY `id_carrier_board_hist` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cpm_hist`
--
ALTER TABLE `cpm_hist`
  MODIFY `id_cpm_hist` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daughterboard_hist`
--
ALTER TABLE `daughterboard_hist`
  MODIFY `id_daughterboard_hist` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fenics_hist`
--
ALTER TABLE `fenics_hist`
  MODIFY `id_fenics_hist` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `gbe_switch_hist`
--
ALTER TABLE `gbe_switch_hist`
  MODIFY `id_gbe_switch_hist` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hv_distribution_board_hist`
--
ALTER TABLE `hv_distribution_board_hist`
  MODIFY `id_hv_distribution_board_hist` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `hv_divider_hist`
--
ALTER TABLE `hv_divider_hist`
  MODIFY `id_hv_divider_hist` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `hv_divider_old_hist`
--
ALTER TABLE `hv_divider_old_hist`
  MODIFY `id_hv_divider_old_hist` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ipmc_hist`
--
ALTER TABLE `ipmc_hist`
  MODIFY `id_ipmc_hist` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mainboard_hist`
--
ALTER TABLE `mainboard_hist`
  MODIFY `id_mainboard_hist` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `micro_drawer_hist`
--
ALTER TABLE `micro_drawer_hist`
  MODIFY `id_micro_drawer_frame_hist` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `mini_drawer_hist`
--
ALTER TABLE `mini_drawer_hist`
  MODIFY `id_mini_drawer_frame_hist` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `module_hist`
--
ALTER TABLE `module_hist`
  MODIFY `id_module_hist` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `pmt_block_hist`
--
ALTER TABLE `pmt_block_hist`
  MODIFY `id_pmt_block_hist` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `pmt_hist`
--
ALTER TABLE `pmt_hist`
  MODIFY `id_pmt_hist` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `ppr_blade_hist`
--
ALTER TABLE `ppr_blade_hist`
  MODIFY `id_ppr_blade_hist` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `super_drawer_hist`
--
ALTER TABLE `super_drawer_hist`
  MODIFY `id_super_drawer_frame_hist` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tc_partition_hist`
--
ALTER TABLE `tc_partition_hist`
  MODIFY `id_tc_partition_hist` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tdaqi_hist`
--
ALTER TABLE `tdaqi_hist`
  MODIFY `id_tdaqi_hist` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tilecom_hist`
--
ALTER TABLE `tilecom_hist`
  MODIFY `id_tilecom_hist` int NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
