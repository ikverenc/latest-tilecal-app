/*
	######## ##     ## ######## ##    ## ########  ######  
	##       ##     ## ##       ###   ##    ##    ##    ## 
	##       ##     ## ##       ####  ##    ##    ##       
	######   ##     ## ######   ## ## ##    ##     ######  
	##        ##   ##  ##       ##  ####    ##          ## 
	##         ## ##   ##       ##   ###    ##    ##    ## 
	########    ###    ######## ##    ##    ##     ###### 
*/


// show old super drawer records
$(document).on("click", "#show_old_super_drawer_table", function (e) 
{
    e.preventDefault();
    $('#old_super_drawer_table').DataTable().clear().destroy();
    fetchOldSuperDrawer();
});

// delete from old super drawer
$(document).on("click", ".delete_old_super_drawer", function (e) 
{
    e.preventDefault();
	delete_record(this, "delete_record_from_old_super_drawer" );
});

$(document).on("click", ".edit_old_super_drawer", function (e) 
{    
    e.preventDefault();
    let edit_id = $(this).attr("value");

    $(".edit_old_super_drawer_form_submit").removeClass("notReady");
    $("#id_edit_disassembly_pmt_block_of_super_drawer").addClass("ready_to_insert");
    $("#edit_disassembly_pmt_block_position_of_super_drawer").addClass("ready_to_insert");
    
    $("#edit_disassembly_info_of_pmt_block_of_super_drawer").text("");
    $("#edit_disassembly_info_of_pmt_block_pos_of_super_drawer").text("");

    $.ajax({
        url: "old_super_drawer/edit",
        type: "post",
        dataType: "json",
        data: {
            edit_id: edit_id
        },
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#edit_disassembly_super_drawer').modal('show');
                $("#originval_of_old_super_drawer_edit").text(data.record.old_super_drawer_id_frame);
                $("#id_edit_disassembly_super_drawer").val(data.record.old_super_drawer_id);
                $("#id_edit_disassembly_pmt_block_of_super_drawer").val(data.record.pmt_block_id);
                $("#edit_disassembly_pmt_block_position_of_super_drawer").val(data.record.pos_pmt_block);
                $("#edit_disassembly_super_drawer_remark").val(data.record.comments);
            } else {
                toastr["error"](data.message);
            }
        }
    });
});

// download test results
$(document).ready( function () 
{
    $(document).on ("click", ".download_test_results_of_old_sp_drawer", function () 
    {
        let id = $(this).parent().parent().children().last().find(">:first-child").attr("value");

        if ( id == undefined || id == null ) 
        {
            toastr["error"](warnings["unknown_problem"]);
            return;
        }

        $.ajax({
            url: "old_super_drawer/fetch_test",
            type: "post",
            dataType: "json",
            data: {
                id: id
            },
            success: function (data) 
            {       
                if (data.responce == "success") 
                {
                    let results = data.record.test_results;
                    
                    if(results == null || results.length < 1 )
                    {
                        toastr["error"](warnings["nodata"]);
                        return;
                    }else{
                        download_test_results(id,results);
                    }
                }else{
                    toastr["error"](warnings["unknown_problem"]);
                }
            },
            error: function()
            {
                toastr["error"](warnings["unknown_problem"]);
            }
        });    
    });
});

/*
	######## ##     ## ##    ##  ######  ######## ####  #######  ##    ##  ######  
	##       ##     ## ###   ## ##    ##    ##     ##  ##     ## ###   ## ##    ## 
	##       ##     ## ####  ## ##          ##     ##  ##     ## ####  ## ##       
	######   ##     ## ## ## ## ##          ##     ##  ##     ## ## ## ##  ######  
	##       ##     ## ##  #### ##          ##     ##  ##     ## ##  ####       ## 
	##       ##     ## ##   ### ##    ##    ##     ##  ##     ## ##   ### ##    ## 
	##        #######  ##    ##  ######     ##    ####  #######  ##    ##  ######  
*/

// fetch Old Super Drawer
function fetchOldSuperDrawer() 
{
    $.ajax({
        url: "old_super_drawer/fetch",
        type: "post",
        dataType: "json",
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#old_super_drawer_table').DataTable({
                    "data": data.records,
                    "rowId": "old_id_super_drawer_frame",
                    "columns": [
                        { "data": "old_super_drawer_id" },
                        { "data": "pmt_block_id" },
                        { "data": "pos_pmt_block" },
                        { "data": null, "defaultContent": `<a class="px-1 download_test_result download_test_results_of_old_sp_drawer" aria-hidden="true" style='font-weight:bold; color:#007bff !important;'>Download</a>`,"targets": -1},
                        { "data": "comments" },
                        { "data": "created_at" },
                        { "data": "updated_at" },
                        { "data": "created_by" },
                        { "data": "updated_by" },
                        {
                            "render": function (data, type, row, meta) 
                            {
                                var a = `
								<i class="far fa-edit px-1 edit_old_super_drawer" value="${row.old_super_drawer_id_frame}" aria-hidden="true"></i>
								<i class="far fa-trash-alt px-1 delete_old_super_drawer" value="${row.old_super_drawer_id_frame}" aria-hidden="true"></i>
							`;
                                return a;
                            }
                        }
                    ]
                });
            } else {
                toastr["error"](data.message);
            }
        }
    });
};

function delete_record_from_old_super_drawer( id, window )
{
	$.ajax({
        url: "old_super_drawer/delete",
        type: "post",
        dataType: "json",
        data: {
            del_id: id
        },
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#old_super_drawer_table').DataTable().clear().destroy();
                fetchOldSuperDrawer();
				
				window.fire(
					'Deleted!',
					'Record has been deleted.',
					'success'
				);

			} else 
			{
				window.fire(
					'Cancelled',
					'Record is safe',
					'error'
				);
			}
		},
		error: function()
		{
			toastr["error"](warnings["unknown_problem"]);
		}
	});
}

function checkPmtBlockIdOfSuperDrawer(id) 
{

	return $.ajax({
		url: "old_super_drawer/checkPmtBlockEntry",
		type: "post",
		dataType: "json",
		data: {
			del_id: id
		}
	});
};

function get_old_pmt_block_positions(id) 
{
    if ($("#id_dissasembly_super_drawer").val()=="")
    {
        return 'empty_field';
    }
    return $.ajax({
        url: "old_super_drawer/fetchPmtBlockPositions",
        type: "post",
        dataType: "json",
        data: {
            id: id
        }
    });
};

function get_old_pmt_block_positions_fot_edit(id) 
{
    if ($("#id_edit_disassembly_super_drawer").val()=="")
    {
        return 'empty_field';
    }
    return $.ajax({
        url: "old_super_drawer/fetchPmtBlockPositions",
        type: "post",
        dataType: "json",
        data: {
            id: $("#id_edit_disassembly_super_drawer").val()
        }
    });
};

function disassembly_old_super_drawer_fn(obj)
{
    let id_super_drawer = obj['id_dissasembly_super_drawer'];
    let pmt_block_id = obj['id_dissasembly_pmt_block_of_old_super_drawer'];
    let pmt_block_pos = obj['id_dissasembly_pmt_block_position'];
    let test_results = obj['test_results'];
    let comments = obj['remark'];
 
    if( test_results != undefined ) 
    {
        $.ajax({
            url: "old_super_drawer/insert",
            type: "post",
            dataType: "json",
            data: {
                old_super_drawer_id: id_super_drawer,
                pmt_block_id: pmt_block_id,
                pos_pmt_block: pmt_block_pos,
                test_results: test_results,
                comments: comments
            },
            success: function (data) 
            {
                if (data.responce == "success") 
                {
                    $('#old_super_drawer_table').DataTable().destroy();
                    $('#dissasembly_super_drawer').modal('hide');
                    fetchOldSuperDrawer();                
    
                    toastr["success"](data.message);
                    
                    $.ajax({
                        url: "pmt_block/insert_in_old_pmt_block",
                        type: "post",
                        dataType: "json",
                        data: {
                            id_pmt_block: pmt_block_id,
                            super_drawer_id: id_super_drawer
                        },
                        success: function (data){ 
                            console.log(data);
                        },
                        error: function(data){
                            console.log(data);
                        }
                    })
                
                } else {
                    toastr["error"](data.message);
                }
            },
            error: function (data)
            {
                toastr["error"](warnings["unknown_problem"]);            
            }
        });
    }else{
        $.ajax({
            url: "old_super_drawer/insert",
            type: "post",
            dataType: "json",
            data: {
                old_super_drawer_id: id_super_drawer,
                pmt_block_id: pmt_block_id,
                pos_pmt_block: pmt_block_pos,
                test_results: null,
                comments: comments
            },
            success: function (data) 
            {
                if (data.responce == "success") 
                {
                    $('#old_super_drawer_table').DataTable().destroy();
                    $('#dissasembly_super_drawer').modal('hide');
                    fetchOldSuperDrawer();                
    
                    toastr["success"](data.message);
                    
                    $.ajax({
                        url: "pmt_block/insert_in_old_pmt_block",
                        type: "post",
                        dataType: "json",
                        data: {
                            id_pmt_block: pmt_block_id,
                            super_drawer_id: id_super_drawer
                        },
                        success: function(){},
                        error: function(){}
                    });
                } else {
                    toastr["error"](data.message);
                }
            },
            error: function (data)
            {
                toastr["error"](warnings["unknown_problem"]);            
            }
        });
    }
}

function update_edit_old_super_drawer( obj )
{
    let origin_id = $("#originval_of_old_super_drawer_edit").text();
    let old_super_drawer_id = obj["id_edit_disassembly_super_drawer"];
    let pmt_block_id =  obj["id_edit_disassembly_pmt_block_of_super_drawer"];
    let pos_pmt_block = obj["edit_disassembly_pmt_block_position_of_super_drawer"];
    let test_results = obj["test_results"];
    let comments = obj["remark"];

    if( origin_id == undefined )
    {
        toastr["error"](warnings["unknown_problem"]);
        return;
    }

    if( test_results != undefined )
    {
        $.ajax({
            url: "old_super_drawer/update",
            type: "post",
            dataType: "json",
            data: {
                old_super_drawer_id_frame: origin_id,
                old_super_drawer_id: old_super_drawer_id,
                pmt_block_id: pmt_block_id,
                pos_pmt_block: pos_pmt_block,
                test_results: test_results,
                comments: comments
            },
            success: function (data) 
            {
                if (data.responce == "success") 
                {
                    $('#old_super_drawer_table').DataTable().destroy();
                    $('#edit_disassembly_super_drawer').modal('hide');
                    fetchOldSuperDrawer();
                    toastr["success"](data.message);
                } else {
                    toastr["error"](data.message);
                }
            },
            error: function()
            {
                toastr["error"](warnings["unknown_problem"])
            }
        });            
    } else{
        $.ajax({
            url: "old_super_drawer/update",
            type: "post",
            dataType: "json",
            data: {
                old_super_drawer_id_frame: origin_id,
                old_super_drawer_id: old_super_drawer_id,
                pmt_block_id: pmt_block_id,
                pos_pmt_block: pos_pmt_block,
                test_results: null,
                comments: comments
            },
            success: function (data) 
            {
                if (data.responce == "success") 
                {
                    $('#old_super_drawer_table').DataTable().destroy();
                    $('#edit_disassembly_super_drawer').modal('hide');
                    fetchOldSuperDrawer();
                    toastr["success"](data.message);
                } 
                else 
                {
                    toastr["error"](data.message);
                }
            },
            error: function(){
                toastr["error"](warnings["unknown_problem"])
            }
        });
    }
}

function check_old_super_drawer_id_entry(id)
{
    return $.ajax({
        url: "old_super_drawer/checkSuperDrawerEntry",
        type: "post",
        dataType: "json",
        data: {
            id: id
        }
    });
}

function add_old_super_drawer_in_reception_fn( obj )
{
    let id = obj["id_old_super_drawer_in_reception"];
    let remark = obj["remark"];

    $.ajax({
        url: "old_super_drawer/insert",
        type: "post",
        dataType: "json",
        data: {
            old_super_drawer_id: id,
            comments: remark
        },
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#old_super_drawer_table').DataTable().destroy();
                $('#add_old_super_drawer_in_reception_modal').modal('hide');
                fetchOldSuperDrawer();                

                toastr["success"](data.message);
            
            } else {
                toastr["error"](data.message);
            }
        },
        error: function ()
        {
            toastr["error"](warnings["unknown_problem"]);            
        }
    });
}

/*

##     ##    ###    ########   ######        ##     ##    ###    ########   ######     
###   ###   ## ##   ##     ## ##    ##       ###   ###   ## ##   ##     ## ##    ##    
#### ####  ##   ##  ##     ## ##             #### ####  ##   ##  ##     ## ##          
## ### ## ##     ## ########   ######        ## ### ## ##     ## ########   ######     
##     ## ######### ##              ##       ##     ## ######### ##              ##    
##     ## ##     ## ##        ##    ##       ##     ## ##     ## ##        ##    ##    
##     ## ##     ## ##         ######        ##     ## ##     ## ##         ######       	 

*/


// add modal of old pmt block
let disassembly_old_super_drawer_map = new Map();
    disassembly_old_super_drawer_map.set("id_dissasembly_super_drawer", {
		important: true,
		fn : undefined,
	});
    disassembly_old_super_drawer_map.set("id_dissasembly_pmt_block_of_old_super_drawer", {
		important: true,
		fn : "checkPmtBlockIdOfSuperDrawer",
		check : function(data)
		{
			let record = new Object();

			if( data )
			{
				record.responce = false;
				record.warn = warnings["disassembled"];
				
				return record;
			
			}else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;	
			}
		}
	});
    disassembly_old_super_drawer_map.set("id_dissasembly_pmt_block_position", {
		important: true,
		fn : "get_old_pmt_block_positions",
		check : function(data)
		{
			let record = new Object();
            
            if (data=='empty_field')
            {
                record.responce = false;
				record.warn = 'Please fill Super Drawer ID field';
				
				return record;
            }
            else if (data.responce=='error')
            {
                record.responce = true;
				record.warn = undefined;
				
				return record;
            }
			else if ( !data )
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;
			
			}else if (data.records.indexOf($("#id_dissasembly_pmt_block_position").val())!=-1) 
			{
				record.responce = false;
				record.warn = warnings['slot_empty'];
				
				return record;	
			}
            else 
            {
                record.responce = true;
				record.warn = undefined;
				
				return record;

            }
		}
	});

//edit old super drawer
let edit_disassembly_of_old_super_drawer_map = new Map();
	edit_disassembly_of_old_super_drawer_map.set("id_edit_disassembly_super_drawer", {
		important: false,
		fn : undefined,
	});

	edit_disassembly_of_old_super_drawer_map.set("id_edit_disassembly_pmt_block_of_super_drawer", {
		important: true,
		fn : "checkPmtBlockIdOfSuperDrawer",
		check : function(data)
		{
			let record = new Object();

			if( data )
			{
				record.responce = false;
				record.warn = warnings["disassembled"];
				
				return record;
			
			}else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;	
			}
		}
	});

    edit_disassembly_of_old_super_drawer_map.set("edit_disassembly_pmt_block_position_of_super_drawer", {
		important: true,
		fn : "get_old_pmt_block_positions_fot_edit",
		check : function(data)
		{
			let record = new Object();
            console.log(data);
            console.log($("#id_edit_disassembly_super_drawer").val())
            if (data=='empty_field')
            {
                record.responce = false;
				record.warn = 'Please fill Super Drawer ID field';
				
				return record;
            }
			else if ( !data )
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;
			
			}else if (data.records.indexOf($("#edit_disassembly_pmt_block_position_of_super_drawer").val())!=-1) 
			{
				record.responce = false;
				record.warn = warnings['slot_empty'];
				
				return record;	
			}
            else 
            {
                record.responce = true;
				record.warn = undefined;
				
				return record;

            }
		}
	});

// add modal of old super drawer
let rec_old_super_drawer_map = new Map();
    rec_old_super_drawer_map.set("id_old_super_drawer_in_reception", {
		important: true,
		fn : "check_old_super_drawer_id_entry",
		check : function(val)
		{
			let record = new Object();

			if( val ) 
			{
                record.responce = false;
				record.warn = warnings["inDB"];
				
				return record;
				
			}
            else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;
			} 
		}
	});
    

/*

 ######   #######  ##     ## ########   #######  ##    ## ######## ##    ## ########  ######  
##    ## ##     ## ###   ### ##     ## ##     ## ###   ## ##       ###   ##    ##    ##    ## 
##       ##     ## #### #### ##     ## ##     ## ####  ## ##       ####  ##    ##    ##       
##       ##     ## ## ### ## ########  ##     ## ## ## ## ######   ## ## ##    ##     ######  
##       ##     ## ##     ## ##        ##     ## ##  #### ##       ##  ####    ##          ## 
##    ## ##     ## ##     ## ##        ##     ## ##   ### ##       ##   ###    ##    ##    ## 
 ######   #######  ##     ## ##         #######  ##    ## ######## ##    ##    ##     ######  

*/


let disassembly_old_super_drawer= new Object();
    disassembly_old_super_drawer.target = "dissasembly_super_drawer_modal_target";
    disassembly_old_super_drawer.formID = "dissasembly_super_drawer_form";
    disassembly_old_super_drawer.standart_inputs = disassembly_old_super_drawer_map;
    disassembly_old_super_drawer.comment = "id_dissasembly_super_drawer_comment";
    disassembly_old_super_drawer.test_results = "id_dissasembly_super_drawer_test_results";
    disassembly_old_super_drawer.remove_file = "remove_file_of_disassembly_super_drawer";
    disassembly_old_super_drawer.submit_class = {
		cls: "dissasemble_super_drawer_form_submit",
		fn: "disassembly_old_super_drawer_fn"
	};

	// edit disassemble pmt block
	disassembly_old_super_drawer.edit_target = "edit_old_super_drawer_form_submit";
	disassembly_old_super_drawer.edit_formID = "edit_disassembly_super_drawer_form";
	disassembly_old_super_drawer.edit_standart_inputs = edit_disassembly_of_old_super_drawer_map;
	disassembly_old_super_drawer.edit_comment = "edit_disassembly_super_drawer_remark";
	disassembly_old_super_drawer.edit_test_results = "edit_disassembly_test_results_of_super_drawer";
	disassembly_old_super_drawer.edit_submit_class = {
		cls: "edit_old_super_drawer_form_submit",
		fn: "update_edit_old_super_drawer"
	};
	disassembly_old_super_drawer.edit_cancel_class = "edit_old_super_drawer_form_cancel";
	disassembly_old_super_drawer.edit_close_class = "close_btn_of_edit_old_sp_drawer";

let disassembly_old_super_drawer_component = new Object_control(disassembly_old_super_drawer);
	disassembly_old_super_drawer_component.assaignEvents();  

let rec_old_super_drawer = new Object();
    rec_old_super_drawer.target = "old_super_drawer_modal_target";
    rec_old_super_drawer.formID = "add_old_super_drawer_in_reception_form";
    rec_old_super_drawer.standart_inputs = rec_old_super_drawer_map;
    rec_old_super_drawer.comment = "remark_for_old_super_drawer_in_reception";
    rec_old_super_drawer.test_results = undefined;
    rec_old_super_drawer.submit_class = {
		cls: "add_old_super_drawer_in_reception_form_submit",
		fn: "add_old_super_drawer_in_reception_fn"
	};

let reception_old_super_drawer = new Object_control(rec_old_super_drawer);
    reception_old_super_drawer.assaignEvents();

/*
	##     ## ####  ######  ########  #######  ########  ##    ## 
	##     ##  ##  ##    ##    ##    ##     ## ##     ##  ##  ##  
	##     ##  ##  ##          ##    ##     ## ##     ##   ####   
	#########  ##   ######     ##    ##     ## ########     ##    
	##     ##  ##        ##    ##    ##     ## ##   ##      ##    
	##     ##  ##  ##    ##    ##    ##     ## ##    ##     ##    
	##     ## ####  ######     ##     #######  ##     ##    ##    
*/


//Fetch OLD Super drawer modifcation Records
$(document).on("click", "#show_old_super_drawer_modification_history_table", function (e) 
{
    e.preventDefault();
    $('#modification_super_drawer_table').DataTable().clear().destroy();
    fetchOldSuperDrawer_modification_history();
});

// fetch Old Super Drawer modification history
function fetchOldSuperDrawer_modification_history() 
{
    $.ajax({
        url: "old_super_drawer/fetchHistory",
        type: "post",
        dataType: "json",
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#modification_super_drawer_table').DataTable({
                    "data": data.records,
                    "rowId": "old_super_drawer_id",
                    "columns": [
                        { "data": "old_super_drawer_id" },
                        { "data": "pmt_block_id" },
                        { "data": "pos_pmt_block" },
                        { "data": null, "defaultContent": `<a class="px-1 download_test_result download_test_results_of_modified_old_sp_drawer" aria-hidden="true" style='font-weight:bold; color:#007bff !important;'>Download</a>`,"targets": -1},
                        { "data": "comments" },
                        { "data": "start_date" },
                        { "data": "end_date" },
                        { "data": "created_by" },
                        { "data": "changed_by" },
                        { "data": "deleted_by" },
                        {
                            "render": function (data, type, row, meta) {

                                var a = `
								<i class="far fa-trash-alt px-1 delete_old_super_drawer_hist" value="${row.old_super_drawer_id_frame}" aria-hidden="true"></i>
							        `;
                                return a;
                            }
                        }
                    ]
                });
            } else {
                toastr["error"](data.message);
            }
        }
    });
};


// download test results
$(document).ready( function () 
{
    $(document).on ("click", ".download_test_results_of_modified_old_sp_drawer", function () 
    {
        let id = $(this).parent().parent().children().last().find(">:first-child").attr("value");

        if ( id == undefined || id == null ) 
        {
            toastr["error"](warnings["unknown_problem"]);
            return;
        }
        
        $.ajax({
            url: "old_super_drawer/fetch_test_from_hist",
            type: "post",
            dataType: "json",
            data: {
                id: id
            },
            success: function (data) 
            {       
                if (data.responce == "success") 
                {
                    let results = data.record.test_results;
                    
                    if(results == null || results.length < 1 )
                    {
                        toastr["error"](warnings["nodata"]);
                        return;

                    }
                    else{
                        download_test_results(id,results);
                    }
                }else{
                    toastr["error"](warnings["unknown_problem"]);
                }
            },
            error: function(data)
            {
                toastr["error"](warnings["unknown_problem"]);
            }
        });    
    });
});




//delete from old super drawer modification history
$(document).on("click", ".delete_old_super_drawer_hist", function (e) 
{
    e.preventDefault();
    delete_record(this, "delete_record_from_modification_of_old_super_drawer" );
});

function delete_record_from_modification_of_old_super_drawer(id, window)
{
    $.ajax({
		url: "old_super_drawer/delete_from_hist",
		type: "post",
		dataType: "json",
		data: {
			del_id: id
		},
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#modification_super_drawer_table').DataTable().clear().destroy();
				fetchOldSuperDrawer_modification_history();
				
				window.fire(
					'Deleted!',
					'Record has been deleted.',
					'success'
				);

			} else 
			{
				window.fire(
					'Cancelled',
					'Record is safe',
					'error'
				);
			}
		},
		error: function()
		{
			toastr["error"](warnings["unknown_problem"]);
		}
	});
}


// // download test results
// $(document).ready( function () 
// {
//     $(document).on ("click", ".download_test_results_of_old_sp_drawer", function () 
//     {
//         let id = $(this).parent().parent().children().last().find(">:first-child").attr("value");

//         if ( id == undefined || id == null ) 
//         {
//             toastr["error"](warnings["unknown_problem"]);
//             return;
//         }

//         $.ajax({
//             url: "old_super_drawer/fetch_test",
//             type: "post",
//             dataType: "json",
//             data: {
//                 id: id
//             },
//             success: function (data) 
//             {       
//                 if (data.responce == "success") 
//                 {
//                     let results = data.record.test_results;
                    
//                     if(results == null || results.length < 1 )
//                     {
//                         toastr["error"](warnings["nodata"]);
//                         return;
//                     }else{
//                         download_test_results(id,results);
//                     }
//                 }else{
//                     toastr["error"](warnings["unknown_problem"]);
//                 }
//             },
//             error: function()
//             {
//                 toastr["error"](warnings["unknown_problem"]);
//             }
//         });    
//     });
// });

// check if all values are correct in disassembly of super drawer 
// function insert_access_in_old_super_drawer ( editMode = false )
// {
//     if ( editMode == false )
//     {
//         if ($("#id_dissasembly_super_drawer").hasClass("ready_to_insert") && 
//             $("#id_dissasembly_pmt_block_of_old_super_drawer").hasClass("ready_to_insert") && 
//             $("#id_dissasembly_pmt_block_position").hasClass("ready_to_insert")){
//             $(".dissasemble_super_drawer_form_submit").removeClass("notReady");
//         }
//     }
//     else{
//         if ( $("#id_edit_disassembly_pmt_block_of_super_drawer").hasClass("ready_to_insert") &&
//              $("#edit_disassembly_pmt_block_position_of_super_drawer").hasClass("ready_to_insert")){
//              $("#edit_old_super_drawer_form_submit").removeClass("notReady");
//         }
//     }
// }

//Fetch OLD Super drawer Records



// restrict SPACE
// $("#id_dissasembly_super_drawer, #id_dissasembly_pmt_block_of_old_super_drawer, #id_dissasembly_pmt_block_position").on('keydown', function (e) 
// {
// 	let invalidChars = ['%', '&', '<', '>', '[', ']', '{', '}', ' '];
//     if( invalidChars.indexOf(e.key) != -1 )
//     { 
//         alert(warnings["restricted_characters"]);
//         e.preventDefault();
//         return;  
//     }
// });

// check id of super drawer
// $("#id_dissasembly_super_drawer").on('input', function () 
// {
//     if( $("#id_dissasembly_super_drawer").val() == "" )
//     {
//         $("#dissasembly_super_drawer_id_info").text(warnings["fill"]);        
//         $("#id_dissasembly_super_drawer").removeClass("ready_to_insert");
//         $("#id_dissasembly_super_drawer").addClass("not_ready_border_col");
//         $(".dissasemble_super_drawer_form_submit").addClass("notReady");
//     }
//     else if($("#id_dissasembly_super_drawer").val() != "" )
//     {
//         $("#dissasembly_super_drawer_id_info").text("");
//         $("#id_dissasembly_super_drawer").removeClass("not_ready_border_col");
//         $("#id_dissasembly_super_drawer").addClass("ready_to_insert");
//         insert_access_in_old_super_drawer();
//     }
// });

// check pmt block id
// $("#id_dissasembly_pmt_block_of_old_super_drawer").on("input", function()
// {    
//     if( $("#id_dissasembly_pmt_block_of_old_super_drawer").val() == "" )
//     {
//         $("#dissasembly_pmt_block_of_super_drawer_id_info").text(warnings["fill"]);
//         $("#id_dissasembly_pmt_block_of_old_super_drawer").removeClass("ready_to_insert");
//         $("#id_dissasembly_pmt_block_of_old_super_drawer").addClass("not_ready_border_col");
//         $(".dissasemble_super_drawer_form_submit").addClass("notReady");
//         return;        
//     }

//     waitPmtBlockIdCheckOfSuperDrawer("id_dissasembly_pmt_block_of_old_super_drawer").then(
//         function(value)
//         {
//             if( value ) 
//             {
//                 $("#id_dissasembly_pmt_block_of_old_super_drawer").removeClass("ready_to_insert");
//                 $("#id_dissasembly_pmt_block_of_old_super_drawer").addClass("not_ready_border_col");
//                 $("#dissasembly_pmt_block_of_super_drawer_id_info").text(warnings["disassembled"]);
//                 $(".dissasemble_super_drawer_form_submit").addClass("notReady");
//             }else{
//                 $("#id_dissasembly_pmt_block_of_old_super_drawer").removeClass("not_ready_border_col");
//                 $("#id_dissasembly_pmt_block_of_old_super_drawer").addClass("ready_to_insert");
//                 $("#dissasembly_pmt_block_of_super_drawer_id_info").text("");
//                 insert_access_in_old_super_drawer();
//             }
//         }
//     )
// });

// $("#id_dissasembly_pmt_block_position").on("input", function()
// {
//     if ( $("#id_dissasembly_super_drawer").val() == "" ) 
//     {
//         $("#dissasembly_pmt_block_position_of_super_drawer_id_info").text("Please fill Super Drawer ID field");
//         $("#id_dissasembly_pmt_block_position").addClass("not_ready_border_col");
//         $("#id_dissasembly_super_drawer").addClass("not_ready_border_col");
//         $(".dissasemble_super_drawer_form_submit").addClass("notReady");
//         return;
//     }
//     else if( !$("#id_dissasembly_pmt_block_of_old_super_drawer").hasClass("ready_to_insert"))
//     {
//         $("#dissasembly_pmt_block_position_of_super_drawer_id_info").text("Please fill PMT Block ID field");
//         $("#id_dissasembly_pmt_block_of_old_super_drawer").addClass("not_ready_border_col");
//         $("#id_dissasembly_pmt_block_position").addClass("not_ready_border_col");
//         $(".dissasemble_super_drawer_form_submit").addClass("notReady");
//         return;
//     }
//     else if( $("#id_dissasembly_pmt_block_position").val() == "")
//     {
//         $("#dissasembly_pmt_block_position_of_super_drawer_id_info").text(" " +warnings["fill"]);
//         $("#id_dissasembly_pmt_block_position").addClass("not_ready_border_col");
//         $("#id_dissasembly_pmt_block_position").removeClass("ready_to_insert");
//         $(".dissasemble_super_drawer_form_submit").addClass("notReady");
//         return;
//     }
//     else{
//         let id = $("#id_dissasembly_super_drawer").val();
//         $.ajax({
//             url: "old_super_drawer/fetchPmtBlockPositions",
//             type: "post",
//             dataType: "json",
//             data: {
//                 id: id
//             },
//             success: function(data)
//             {
//                 if( data.records == undefined )
//                 {
//                     $("#dissasembly_pmt_block_position_of_super_drawer_id_info").text("");
//                     $("#id_dissasembly_pmt_block_position").removeClass("not_ready_border_col");
//                     $("#id_dissasembly_pmt_block_position").addClass("ready_to_insert");                    
//                     insert_access_in_old_super_drawer();
//                 }
//                 else if( data.records.indexOf($("#id_dissasembly_pmt_block_position").val()) != -1 )
//                 {
//                     $("#dissasembly_pmt_block_position_of_super_drawer_id_info").text(" : There is no PMT Block in this slot.");
//                     $("#id_dissasembly_pmt_block_position").removeClass("ready_to_insert");                    
//                     $("#id_dissasembly_pmt_block_position").addClass("not_ready_border_col");
//                     $(".dissasemble_super_drawer_form_submit").addClass("notReady");
//                 }else{
//                     $("#dissasembly_pmt_block_position_of_super_drawer_id_info").text("");
//                     $("#id_dissasembly_pmt_block_position").removeClass("not_ready_border_col");
//                     $("#id_dissasembly_pmt_block_position").addClass("ready_to_insert");
//                     insert_access_in_old_super_drawer();
//                 }   
//             },
//             error: function()
//             {
//                 $("#dissasembly_pmt_block_position_of_super_drawer_id_info").text(warnings["unknown_problem"]);
//                 $("#id_dissasembly_pmt_block_position").removeClass("ready_to_insert");                    
//                 $("#id_dissasembly_pmt_block_position").addClass("not_ready_border_col");
//                 $(".dissasemble_super_drawer_form_submit").addClass("notReady");
//             }
//         });    
//     };
// });

// Add OLD Super drawer
// $(document).on("click", ".dissasemble_super_drawer_form_submit", function (e) 
// {
//     e.preventDefault();

//     if ($(".dissasemble_super_drawer_form_submit").hasClass("notReady")) return;

//     let id_super_drawer = $("#id_dissasembly_super_drawer").val();
//     let pmt_block_id = $("#id_dissasembly_pmt_block_of_old_super_drawer").val();
//     let pmt_block_pos = $("#id_dissasembly_pmt_block_position").val();
//     let test_results = $("#id_dissasembly_super_drawer_test_results")[0].files[0];
//     let comments = $("#id_dissasembly_super_drawer_comment").val();

//     if( test_results != undefined ) 
//     {
//         let reader = new FileReader();
//         reader.readAsText(test_results);
//         reader.onload = function() 
//         {
//             test_results = reader.result;
//             $.ajax({
//                 url: "old_super_drawer/insert",
//                 type: "post",
//                 dataType: "json",
//                 data: {
//                     old_super_drawer_id: id_super_drawer,
//                     pmt_block_id: pmt_block_id,
//                     pos_pmt_block: pmt_block_pos,
//                     test_results: test_results,
//                     comments: comments
//                 },
//                 success: function (data) 
//                 {
//                     if (data.responce == "success") 
//                     {
//                         $('#old_super_drawer_table').DataTable().destroy();
//                         $('#dissasembly_super_drawer').modal('hide');
//                         fetchOldSuperDrawer();                
        
//                         toastr["success"](data.message);
                        
//                         $.ajax({
//                             url: "pmt_block/insert_in_old_pmt_block",
//                             type: "post",
//                             dataType: "json",
//                             data: {
//                                 id_pmt_block: pmt_block_id,
//                                 super_drawer_id: id_super_drawer
//                             },
//                             success: function (){ 
//                             },
//                             error: function(data){
//                             }
//                         })
                    
//                     } else {
//                         toastr["error"](data.message);
//                     }
//                 },
//                 error: function (data)
//                 {
//                     toastr["error"](warnings["unknown_problem"]);            
//                 }
//             });
//         };
//     }else{
//         $.ajax({
//             url: "old_super_drawer/insert",
//             type: "post",
//             dataType: "json",
//             data: {
//                 old_super_drawer_id: id_super_drawer,
//                 pmt_block_id: pmt_block_id,
//                 pos_pmt_block: pmt_block_pos,
//                 test_results: null,
//                 comments: comments
//             },
//             success: function (data) 
//             {
//                 if (data.responce == "success") 
//                 {
//                     $('#old_super_drawer_table').DataTable().destroy();
//                     $('#dissasembly_super_drawer').modal('hide');
//                     fetchOldSuperDrawer();                
    
//                     toastr["success"](data.message);
                    
//                     $.ajax({
//                         url: "pmt_block/insert_in_old_pmt_block",
//                         type: "post",
//                         dataType: "json",
//                         data: {
//                             id_pmt_block: pmt_block_id,
//                             super_drawer_id: id_super_drawer
//                         },
//                         success: function(){},
//                         error: function(){}
//                     });
//                 } else {
//                     toastr["error"](data.message);
//                 }
//             },
//             error: function (data)
//             {
//                 toastr["error"](warnings["unknown_problem"]);            
//             }
//         });
//     }
//     $("#dissasembly_super_drawer_form")[0].reset();      
// });


// clear data 
// $(".dissasembly_super_drawer_modal_target").on("click", function()
// {
//     $("#id_dissasembly_super_drawer").removeClass("ready_to_insert");
//     $("#id_dissasembly_pmt_block_position").removeClass("ready_to_insert");
//     $("#id_dissasembly_pmt_block_of_old_super_drawer").removeClass("ready_to_insert");

//     $("#id_dissasembly_super_drawer").removeClass("not_ready_border_col");
//     $("#id_dissasembly_pmt_block_position").removeClass("not_ready_border_col");    
//     $("#id_dissasembly_pmt_block_of_old_super_drawer").removeClass("not_ready_border_col");

//     $("#id_dissasembly_super_drawer_test_results")[0].files = undefined;
//     $("#dissasembly_super_drawer_id_info").text("");
//     $("#dissasembly_pmt_block_of_super_drawer_id_info").text("");
//     $("#dissasembly_pmt_block_position_of_super_drawer_id_info").text("");
//     $("#id_dissasembly_super_drawer_comment").val("");


//     $(".dissasemble_super_drawer_form_submit").addClass("notReady");
//     $("#dissasembly_super_drawer_form")[0].reset();
// });




//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT
//////////////////////////////////// EDIT


//Fetch infromation of OLD super drawer from database to fill fields in edit window
// $(document).on("click", ".edit_old_super_drawer", function (e) 
// {    
//     e.preventDefault();
//     let edit_id = $(this).attr("value");

//     $(".edit_old_super_drawer_form_submit").removeClass("notReady");
//     $("#id_edit_disassembly_pmt_block_of_super_drawer").addClass("ready_to_insert");
//     $("#edit_disassembly_pmt_block_position_of_super_drawer").addClass("ready_to_insert");
    
//     $("#edit_disassembly_info_of_pmt_block_of_super_drawer").text("");
//     $("#edit_disassembly_info_of_pmt_block_pos_of_super_drawer").text("");

//     $.ajax({
//         url: "old_super_drawer/edit",
//         type: "post",
//         dataType: "json",
//         data: {
//             edit_id: edit_id
//         },
//         success: function (data) 
//         {
//             if (data.responce == "success") 
//             {
//                 $('#edit_disassembly_super_drawer').modal('show');
//                 $("#originval_of_old_super_drawer_edit").text(data.record.old_super_drawer_id_frame);
//                 $("#id_edit_disassembly_super_drawer").val(data.record.old_super_drawer_id);
//                 $("#id_edit_disassembly_pmt_block_of_super_drawer").val(data.record.pmt_block_id);
//                 $("#edit_disassembly_pmt_block_position_of_super_drawer").val(data.record.pos_pmt_block);
//                 $("#edit_disassembly_super_drawer_remark").val(data.record.comments);
//             } else {
//                 toastr["error"](data.message);
//             }
//         }
//     });
// });

// $("#id_edit_disassembly_super_drawer, #id_edit_disassembly_pmt_block_of_super_drawer, #edit_disassembly_pmt_block_position_of_super_drawer").on('keydown', function (e) 
// {
//     let invalidChars = ['%', '&', '<', '>', '[', ']', '{', '}', ' '];
//     if( invalidChars.indexOf(e.key) != -1 )
//     { 
//         alert(warnings["restricted_characters"]);
//         e.preventDefault();
//         return;  
//     }
// });


// $("#id_edit_disassembly_pmt_block_of_super_drawer").on("input", function()
// {
//     if ( this.value == "" )
//     {
//         $("#edit_disassembly_info_of_pmt_block_of_super_drawer").text(warnings["fill"]);
//         $("#id_edit_disassembly_pmt_block_of_super_drawer").addClass("not_ready_border_col");
//         $("#edit_old_super_drawer_form_submit").addClass("notReady");
//         return;
//     }

//     waitPmtBlockIdCheckOfSuperDrawer("id_edit_disassembly_pmt_block_of_super_drawer").then(
//         function(value)
//         {
//             if( value ) 
//             {
//                 $("#id_edit_disassembly_pmt_block_of_super_drawer").removeClass("ready_to_insert");
//                 $("#id_edit_disassembly_pmt_block_of_super_drawer").addClass("not_ready_border_col");
//                 $("#edit_disassembly_info_of_pmt_block_of_super_drawer").text(warnings["disassembled"]);
//                 $("#edit_old_super_drawer_form_submit").addClass("notReady");
//             }else{
//                 $("#id_edit_disassembly_pmt_block_of_super_drawer").removeClass("not_ready_border_col");
//                 $("#id_edit_disassembly_pmt_block_of_super_drawer").addClass("ready_to_insert");
//                 $("#edit_disassembly_info_of_pmt_block_of_super_drawer").text("");
//                 insert_access_in_old_super_drawer(true);
//             }
//         }
//     )
// });

// $("#edit_disassembly_pmt_block_position_of_super_drawer").on("input", function()
// {
//     if( this.value == "")
//     {
//         $("#edit_disassembly_info_of_pmt_block_pos_of_super_drawer").text(warnings["fill"]);
//         $("#edit_disassembly_pmt_block_position_of_super_drawer").removeClass("ready_to_insert");
//         $("#edit_disassembly_pmt_block_position_of_super_drawer").addClass("not_ready_border_col");
//         $("#edit_old_super_drawer_form_submit").addClass("notReady");
//         return;
//     }else{
//         let id = $("#id_edit_disassembly_super_drawer").val();
//         $.ajax({
//             url: "old_super_drawer/fetchPmtBlockPositions",
//             type: "post",
//             dataType: "json",
//             data: {
//                 id: id
//             },
//             success: function(data)
//             {
//                 if( data.records == undefined )
//                 {
//                     $("#edit_disassembly_info_of_pmt_block_pos_of_super_drawer").text("");
//                     $("#edit_disassembly_pmt_block_position_of_super_drawer").removeClass("not_ready_border_col");
//                     $("#edit_disassembly_pmt_block_position_of_super_drawer").addClass("ready_to_insert");                 
//                     insert_access_in_old_super_drawer(true);   
//                 }
//                 else if( data.records.indexOf($("#edit_disassembly_pmt_block_position_of_super_drawer").val()) != -1 )
//                 {
//                     $("#edit_disassembly_info_of_pmt_block_pos_of_super_drawer").text(" : There is no PMT Block in this slot.");
//                     $("#edit_disassembly_pmt_block_position_of_super_drawer").removeClass("ready_to_insert");                    
//                     $("#edit_disassembly_pmt_block_position_of_super_drawer").addClass("not_ready_border_col");
//                     $("#edit_old_super_drawer_form_submit").addClass("notReady");
//                 }else{
//                     $("#edit_disassembly_info_of_pmt_block_pos_of_super_drawer").text("");
//                     $("#edit_disassembly_pmt_block_position_of_super_drawer").removeClass("not_ready_border_col");
//                     $("#edit_disassembly_pmt_block_position_of_super_drawer").addClass("ready_to_insert");
//                     insert_access_in_old_super_drawer(true);
//                 }   
//             },
//             error: function(){
//                 toastr["error"](warnings["unknown_problem"]);
//             }
//         }); 
//     }
// })



// Update OLD Super drawer 
// $(document).on("click", "#edit_old_super_drawer_form_submit", function (e) 
// {    
//     e.preventDefault();

//     let origin_id = $("#originval_of_old_super_drawer_edit").text();
//     let old_super_drawer_id = $("#id_edit_disassembly_super_drawer").val();
//     let pmt_block_id = $("#id_edit_disassembly_pmt_block_of_super_drawer").val();
//     let pos_pmt_block = $("#edit_disassembly_pmt_block_position_of_super_drawer").val()
//     let test_results = $("#edit_disassembly_test_results_of_super_drawer")[0].files[0];
//     let comments = $("#edit_disassembly_super_drawer_remark").val();

//     if( origin_id == undefined )
//     {
//         toastr["error"](warnings["unknown_problem"]);
//         return;
//     }

//     if( test_results != undefined )
//     {
//         let reader = new FileReader();
//         reader.readAsText(test_results);
//         reader.onload = function() 
//         {
//             let res = reader.result;
//             $.ajax({
//                 url: "old_super_drawer/update",
//                 type: "post",
//                 dataType: "json",
//                 data: {
//                     old_super_drawer_id_frame: origin_id,
//                     old_super_drawer_id: old_super_drawer_id,
//                     pmt_block_id: pmt_block_id,
//                     pos_pmt_block: pos_pmt_block,
//                     test_results: res,
//                     comments: comments
//                 },
//                 success: function (data) 
//                 {
//                     if (data.responce == "success") 
//                     {
//                         $('#old_super_drawer_table').DataTable().destroy();
//                         $('#edit_disassembly_super_drawer').modal('hide');
//                         fetchOldSuperDrawer();
//                         toastr["success"](data.message);
//                     } else {
//                         toastr["error"](data.message);
//                     }
//                 },
//                 error: function(){
//                     toastr["error"](warnings["unknown_problem"])
//                 }
//             });        
//         }    
//     } else{
//         $.ajax({
//             url: "old_super_drawer/update",
//             type: "post",
//             dataType: "json",
//             data: {
//                 old_super_drawer_id_frame: origin_id,
//                 old_super_drawer_id: old_super_drawer_id,
//                 pmt_block_id: pmt_block_id,
//                 pos_pmt_block: pos_pmt_block,
//                 comments: comments
//             },
//             success: function (data) 
//             {
//                 if (data.responce == "success") 
//                 {
//                     $('#old_super_drawer_table').DataTable().destroy();
//                     $('#edit_disassembly_super_drawer').modal('hide');
//                     fetchOldSuperDrawer();
//                     toastr["success"](data.message);
//                 } else {
//                     toastr["error"](data.message);
//                 }
//             },
//             error: function(){
//                 toastr["error"](warnings["unknown_problem"])
//             }
//         });
//     }
// });





/////////////////////////////////////////////////////////////// DELETE
/////////////////////////////////////////////////////////////// DELETE
/////////////////////////////////////////////////////////////// DELETE
/////////////////////////////////////////////////////////////// DELETE
/////////////////////////////////////////////////////////////// DELETE
/////////////////////////////////////////////////////////////// DELETE
/////////////////////////////////////////////////////////////// DELETE
/////////////////////////////////////////////////////////////// DELETE
/////////////////////////////////////////////////////////////// DELETE
/////////////////////////////////////////////////////////////// DELETE
/////////////////////////////////////////////////////////////// DELETE
/////////////////////////////////////////////////////////////// DELETE
/////////////////////////////////////////////////////////////// DELETE
/////////////////////////////////////////////////////////////// DELETE
/////////////////////////////////////////////////////////////// DELETE


// // Delete OLD super drawer 
// $(document).on("click", ".delete_old_super_drawer", function (e) 
// {
//     e.preventDefault();
//     let del_id = $(this).attr("value");

//     const swalWithBootstrapButtons = Swal.mixin({
//         customClass: {
//             confirmButton: 'btn btn-sm btn-success ml-1',
//             cancelButton: 'btn btn-sm btn-danger'
//         },
//         buttonsStyling: false
//     })

//     swalWithBootstrapButtons.fire({
//         title: 'Are you sure?',
//         text: "You won't be able to revert this!",
//         icon: 'warning',
//         showCancelButton: true,
//         confirmButtonText: 'Yes, Delete!',
//         cancelButtonText: 'No, Cancel!',
//         reverseButtons: true
//     }).then((result) => {
//         if (result.isConfirmed) {

//             $.ajax({
//                 url: "old_super_drawer/delete",
//                 type: "post",
//                 dataType: "json",
//                 data: {
//                     del_id: del_id
//                 },
//                 success: function (data) 
//                 {
//                     if (data.responce == "success") 
//                     {
//                         $('#old_super_drawer_table').DataTable().clear().destroy();
//                         fetchOldSuperDrawer();
//                         swalWithBootstrapButtons.fire(
//                             'Deleted!',
//                             'Record has been deleted.',
//                             'success'
//                         );
//                     } else 
//                     {
//                         swalWithBootstrapButtons.fire(
//                             'Cancelled',
//                             'Record is safe',
//                             'error'
//                         );
//                     }
//                 },
//                 error: function (data)
//                 {
//                     console.log( data );
//                 }
//             });

//         } else if (
//             result.dismiss === Swal.DismissReason.cancel
//         ) {
//             swalWithBootstrapButtons.fire(
//                 'Cancelled',
//                 'Record is safe',
//                 'error'
//             )
//         }
//     })
// });


////////////////////////////////// ADD IN RECEPTION
////////////////////////////////// ADD IN RECEPTION
////////////////////////////////// ADD IN RECEPTION
////////////////////////////////// ADD IN RECEPTION
////////////////////////////////// ADD IN RECEPTION
////////////////////////////////// ADD IN RECEPTION
////////////////////////////////// ADD IN RECEPTION







// clear data on each call
// $(".old_super_drawer_modal_target").on("click", function()
// {
//     $("#id_old_super_drawer_in_reception").removeClass("not_ready_border_col");
//     $("#id_old_super_drawer_in_reception").removeClass("ready_to_insert");
//     $("#old_super_drawer_in_reception_info").text("");
//     $("#remark_for_old_super_drawer_in_reception").val();
    
//     $(".add_old_super_drawer_in_reception_form_submit").addClass("notReady");
//     $("#add_old_super_drawer_in_reception_form")[0].reset();
// });

// restrict SPACE
// $("#id_old_super_drawer_in_reception").on('keydown', function (e) 
// {
// 	let invalidChars = ['%', '&', '<', '>', '[', ']', '{', '}', ' '];
//     if( invalidChars.indexOf(e.key) != -1 )
//     { 
//         alert(warnings["restricted_characters"]);
//         e.preventDefault();
//         return;  
//     }
// });


// $("#id_old_super_drawer_in_reception").on('input', function () 
// {
//     if( this.value == "" )
//     {
//         $("#old_super_drawer_in_reception_info").text(warnings["fill"]);
//         $("#id_old_super_drawer_in_reception").removeClass("ready_to_insert");
//         $("#id_old_super_drawer_in_reception").addClass("not_ready_border_col");
//         $(".add_old_super_drawer_in_reception_form_submit").addClass("notReady");
//         return;
//     }else{
//         $.ajax({
//             url: "old_super_drawer/checkSuperDrawerEntry",
//             type: "post",
//             dataType: "json",
//             data: {
//                 id: this.value
//             },
//             success: function (data) 
//             {
//                 if( data )
//                 {
//                     $("#old_super_drawer_in_reception_info").text("With this ID is already in Database");
//                     $("#id_old_super_drawer_in_reception").removeClass("ready_to_insert");
//                     $("#id_old_super_drawer_in_reception").addClass("not_ready_border_col");
//                     $(".add_old_super_drawer_in_reception_form_submit").addClass("notReady");
//                 }else{
//                     $("#old_super_drawer_in_reception_info").text("");
//                     $("#id_old_super_drawer_in_reception").addClass("ready_to_insert");
//                     $("#id_old_super_drawer_in_reception").removeClass("not_ready_border_col");
//                     $(".add_old_super_drawer_in_reception_form_submit").removeClass("notReady");
//                 }

//             },
//             error: function()
//             {
//                 toastr["error"](warnings["unknown_problem"]);
//             }
//         })
//     }
// });

// $(document).on("click", ".add_old_super_drawer_in_reception_form_submit", function (e) 
// {    
//     e.preventDefault();

//     if( $(".add_old_super_drawer_in_reception_form_submit").hasClass("notReady") ) return;

    // let id = $("#id_old_super_drawer_in_reception").val();
    // let remark = $("#remark_for_old_super_drawer_in_reception").val();

    // $.ajax({
    //     url: "old_super_drawer/insert",
    //     type: "post",
    //     dataType: "json",
    //     data: {
    //         old_super_drawer_id: id,
    //         comments: remark
    //     },
    //     success: function (data) 
    //     {
    //         if (data.responce == "success") 
    //         {
    //             $('#old_super_drawer_table').DataTable().destroy();
    //             $('#add_old_super_drawer_in_reception_modal').modal('hide');
    //             fetchOldSuperDrawer();                

    //             toastr["success"](data.message);
            
    //         } else {
    //             toastr["error"](data.message);
    //         }
    //     },
    //     error: function ()
    //     {
    //         toastr["error"](warnings["unknown_problem"]);            
    //     }
    // });
// });
