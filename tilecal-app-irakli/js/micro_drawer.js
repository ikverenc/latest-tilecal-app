/*
	######## ##     ## ######## ##    ## ########  ######  
	##       ##     ## ##       ###   ##    ##    ##    ## 
	##       ##     ## ##       ####  ##    ##    ##       
	######   ##     ## ######   ## ## ##    ##     ######  
	##        ##   ##  ##       ##  ####    ##          ## 
	##         ## ##   ##       ##   ###    ##    ##    ## 
	########    ###    ######## ##    ##    ##     ###### 
*/




//Fetch Fenics Records
$(document).on("click", "#show_micro_drawer_table", function (e) {
    e.preventDefault();
    $('#micro_drawer_table').DataTable().clear().destroy();
    fetchMicroDrawer();
});

$(document).on("click", ".delete_micro_drawer", function (e) 
{
    e.preventDefault();
    console.log('hi');
	delete_record(this, "delete_record_from_micro_drawer" );
});


//Fetch infromation of micro drawer from database to fill fields in edit window
$(document).on("click", ".edit_micro_drawer", function (e) {
    e.preventDefault();
    let edit_id = $(this).attr("value");

    $.ajax({
        url: "micro_drawer/edit",
        type: "post",
        dataType: "json",
        data: {
            edit_id: edit_id
        },
        success: function (data) {
            if (data.responce == "success") 
            {
                $("#originValMicroDrawer").val(data.record.id_micro_drawer_frame);
                $('#edit_assembly_micro_drawer').modal('show');
                $("#edit_id_assembly_micro_drawer").val(data.record.id_micro_drawer);
                $("#edit_id_super_drawer_of_assembly_micro_drawer").val(data.record.id_super_drawer);
                $("#edit_id_module_of_assembly_micro_drawer").val(data.record.module_id);
                $("#id_edit_assembly_pmt_block_of_micro_drawer").val(data.record.pmt_block_id);
                $("#id_edit_assembly_pmt_block_pos_of_micro_drawer").val(data.record.pos_pmt_block);
                $("#edit_assembly_micro_drawer_current_location").val(data.record.current_location);
                $("#edit_micro_drawer_remark").val(data.record.remark);

            } else {
                toastr["error"](data.message);
            }
        }
    })
});


/*
	######## ##     ## ##    ##  ######  ######## ####  #######  ##    ##  ######  
	##       ##     ## ###   ## ##    ##    ##     ##  ##     ## ###   ## ##    ## 
	##       ##     ## ####  ## ##          ##     ##  ##     ## ####  ## ##       
	######   ##     ## ## ## ## ##          ##     ##  ##     ## ## ## ##  ######  
	##       ##     ## ##  #### ##          ##     ##  ##     ## ##  ####       ## 
	##       ##     ## ##   ### ##    ##    ##     ##  ##     ## ##   ### ##    ## 
	##        #######  ##    ##  ######     ##    ####  #######  ##    ##  ######  
*/


function fetchMicroDrawer() {
    $.ajax({
        url: "micro_drawer/fetch",
        type: "post",
        dataType: "json",
        success: function (data) {

            if (data.responce == "success") {

                $('#micro_drawer_table').DataTable({
                    "data": data.records,
                    "rowId": "id_micro_drawer",
                    "columns": [
                        { "data": "id_micro_drawer" },
                        { "data": "id_super_drawer" },
                        { "data": "module_id" },
                        { "data": "pmt_block_id" },
                        { "data": "pos_pmt_block" },
                        { "data": "current_location" },
                        { "data": "remark" },
                        { "data": "created_at" },
                        { "data": "updated_at" },
                        { "data": "created_by" },
                        { "data": "updated_by" },
                        {
                            "render": function (data, type, row, meta) {

                                var a = `
								<i class="far fa-edit px-1 edit_micro_drawer" value="${row.id_micro_drawer_frame}" id ="" aria-hidden="true"></i>
								<i class="far fa-trash-alt px-1 delete_micro_drawer" value="${row.id_micro_drawer_frame}" id ="" aria-hidden="true"></i>
							`;
                                return a;
                            }
                        }
                    ]
                });
            } else {
                toastr["error"](data.message);
            }
        }
    });
};

function check_micro_drawer_id(val)
{
	if( val.length > 0 )
	{
		return true;
	}else{
		return false
	}
}

function fetch_pmt_block_records_from_micro_mini_drawer(id)
{
    return $.ajax({
        url: "pmt_block/fetchPmtBlockRecordsFromMicroMiniDrawer",
        type: "post",
        dataType: "json", 
    });
}

function fetch_positions_of_pmt_block_from_micro_drawer(id)
{
    return $.ajax({
        url: "pmt_block/fetchPosFromMicroDrawer",
        type: "post",
        dataType: "json",
        data: {
            del_id: $("#id_micro_drawer").val()
        },
    });
}

function check_micro_drawer_id_entry(id)
{
    return $.ajax({
        url: "pmt_block/fetchPosFromMicroDrawer",
        type: "post",
        dataType: "json",
        data: {
            del_id: id
        },
    });
}

function fetch_positions_of_pmt_block_from_micro_drawer_edit(id)
{
    return $.ajax({
        url: "pmt_block/fetchPosFromMicroDrawer",
        type: "post",
        dataType: "json",
        data: {
            del_id: $("#edit_id_assembly_micro_drawer").val()
        },
    });
}

function assemble_micro_drawer_fn(obj)
{
    let id_micro_drawer = obj["id_micro_drawer"];
    let id_super_drawer = obj["id_super_drawer_of_assembly_micro_drawer"];
    let id_module = obj["id_module_of_assembly_micro_drawer"];
    let id_pmt_block = obj["id_pmt_block_of_assembly_micro_drawer"];
    let pos_pmt_block =  obj["id_pmt_block_position_of_assembly_micro_drawer"];
    let current_location = $("#micro_drawer_current_location").val();
    let remark = obj["remark"];

    $.ajax({
        url: "micro_drawer/insert",
        type: "post",
        dataType: "json",
        data: {
            id_micro_drawer: id_micro_drawer,
            id_super_drawer: id_super_drawer,
            module_id: id_module,
            pmt_block_id: id_pmt_block,
            pos_pmt_block: pos_pmt_block,
            current_location: current_location,
            remark: remark
        },
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#micro_drawer_table').DataTable().destroy();
                $('#assembly_micro_drawer').modal('hide');
                fetchMicroDrawer();

                toastr["success"](data.message);
            } else {
                toastr["error"](data.message);
            }
        },
        error: function(data){
            console.log(data);
        }
    });
}

function delete_record_from_micro_drawer( id, window )
{
    $.ajax({
        url: "micro_drawer/delete",
        type: "post",
        dataType: "json",
        data: {
            del_id: id
        },
        success: function (data) {

            if (data.responce == "success") 
            {
                $('#micro_drawer_table').DataTable().clear().destroy();
                fetchMicroDrawer();

                window.fire(
                    'Deleted!',
                    'Record has been deleted.',
                    'success'
                );
            } else {
                window.fire(
                    'Cancelled',
                    'Record is safe',
                    'error'
                );
            }
        },
        error: function()
        {
            toastr["error"](warnings["unknown_problem"]);
        }
    });
}


function update_assembly_of_micro_drawer(obj)
{
    let id_micro_drawer_frame = $("#originValMicroDrawer").val();
    let id_micro_drawer = obj["edit_id_assembly_micro_drawer"];
    let id_super_drawer = obj["edit_id_super_drawer_of_assembly_micro_drawer"];
    let module_id = obj["edit_id_module_of_assembly_micro_drawer"];
    let pmt_block_id = obj["id_edit_assembly_pmt_block_of_micro_drawer"];
    let pos_pmt_block = obj["id_edit_assembly_pmt_block_pos_of_micro_drawer"];
    let current_location = $("#edit_assembly_micro_drawer_current_location").val();
    let remark = obj["remark"];

    $.ajax({
        url: "micro_drawer/update",
        type: "post",
        dataType: "json",
        data: {
            id_micro_drawer_frame: id_micro_drawer_frame,
            id_micro_drawer: id_micro_drawer,
            id_super_drawer: id_super_drawer,
            module_id: module_id,
            pmt_block_id: pmt_block_id,
            pos_pmt_block: pos_pmt_block,
            current_location:current_location,
            remark: remark
        },
        success: function (data) {

            if (data.responce == "success") {
                $('#micro_drawer_table').DataTable().destroy();
                $('#edit_assembly_micro_drawer').modal('hide');
                fetchMicroDrawer();
                toastr["success"](data.message);
            } else {
                toastr["error"](data.message);
            }
        }
    });
}


function add_micro_drawer_in_reception_fn(obj)
{
    let id_micro_drawer = obj["id_micro_drawer_in_reception"];
    let remark = obj["remark"];

    $.ajax({
        url: "micro_drawer/insert",
        type: "post",
        dataType: "json",
        data: {
            id_micro_drawer: id_micro_drawer,
            remark: remark
        },
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#micro_drawer_table').DataTable().destroy();
                $('#add_micro_drawer_in_reception_modal').modal('hide');
                fetchMicroDrawer();

                toastr["success"](data.message);
            }else {
                toastr["error"](data.message);
            }
        },
        error: function (data){
            toastr["error"](warnings["unknown_problem"]);
        }
    });
}

/*

    ##     ##    ###    ########   ######        ##     ##    ###    ########   ######     
    ###   ###   ## ##   ##     ## ##    ##       ###   ###   ## ##   ##     ## ##    ##    
    #### ####  ##   ##  ##     ## ##             #### ####  ##   ##  ##     ## ##          
    ## ### ## ##     ## ########   ######        ## ### ## ##     ## ########   ######     
    ##     ## ######### ##              ##       ##     ## ######### ##              ##    
    ##     ## ##     ## ##        ##    ##       ##     ## ##     ## ##        ##    ##    
    ##     ## ##     ## ##         ######        ##     ## ##     ## ##         ######       	 

*/


// add modal of old mini drawer
let assembly_micro_drawer_map = new Map();
    assembly_micro_drawer_map.set("id_micro_drawer", {
		important: true,
		fn : "check_micro_drawer_id",
		check : function(val)
		{
			let record = new Object();
			
			if( val == false) 
			{
                record.responce = false;
				record.warn = "Please enter Micro Drawer ID"
				
				return record;
				
			}
            else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;
			} 
		}
	});

    assembly_micro_drawer_map.set("id_super_drawer_of_assembly_micro_drawer", {
		important: false
	});

    assembly_micro_drawer_map.set("id_module_of_assembly_micro_drawer", {
		important: false
	});

    assembly_micro_drawer_map.set("id_pmt_block_of_assembly_micro_drawer", {
		important: true,
		fn : "fetch_pmt_block_records_from_micro_mini_drawer",
		check : function(val)
		{
			let record = new Object();
			
			if( val.records[0].indexOf($("#id_pmt_block_of_assembly_micro_drawer").val()) != -1) 
			{
                record.responce = false;
				record.warn = "with this ID is already assembled in Micro Drawer";
				
				return record;
				
			}
            else if( val.records[1].indexOf($("#id_pmt_block_of_assembly_micro_drawer").val()) != -1 )
            {
                record.responce = false;
				record.warn = "with this ID is already assembled in Mini Drawer";
				
				return record;
            }
            else
			{
				record.responce = true;
				record.warn = undefined;

                waitPmtBlockIdCheck("id_pmt_block_of_assembly_micro_drawer").then(
                    function (value) 
                    {
                        if (value) 
                        {
                            $("#pmt_block_info_of_assembly_micro_drawer").text("");
                            $("#id_pmt_block_of_assembly_micro_drawer").removeClass("not_ready_border_col");
                            $("#id_pmt_block_of_assembly_micro_drawer").addClass("ready_to_insert");
                            assembly_micro_drawer_control.handle_form();
            
                        } else {
                            $("#pmt_block_info_of_assembly_micro_drawer").text(warnings["id_notreachable"]);
                            $("#id_pmt_block_of_assembly_micro_drawer").addClass("not_ready_border_col");
                            $("#id_pmt_block_of_assembly_micro_drawer").removeClass("ready_to_insert");
                            assembly_micro_drawer_control.handle_form();
                        }
                    }
                );
				
				return record;
			} 
		}
	});


    assembly_micro_drawer_map.set("id_pmt_block_position_of_assembly_micro_drawer", {
		important: true,
		fn : "fetch_positions_of_pmt_block_from_micro_drawer",
		check : function(val)
		{
			let record = new Object();
            let pmt_block_pos = $("#id_pmt_block_position_of_assembly_micro_drawer").val();

			if( $("#id_micro_drawer").val() == "" )
            {
                record.responce = false;
                record.warn = "Please enter ID in Micro Drawer field";

                return record;
            
            } else if( val.responce == "error" )
            {
                record.responce = true;
                record.warn = "";

                return record;
            } 

            else if ( val.responce == "success" )
            {
                if( val.records.indexOf(pmt_block_pos) != -1)
                {
                    record.responce = false;
                    record.warn = warnings["slot_filled"];
                }
                else
                {
                    record.responce = true;
                    record.warn = "";
                }

                return record;
            }
		}
	});

let edit_assmebly_micro_drawer_map = new Map();
    edit_assmebly_micro_drawer_map.set("edit_id_assembly_micro_drawer", {
		important: true,
		fn : "check_micro_drawer_id",
		check : function(val)
		{
			let record = new Object();
			
			if( val == false) 
			{
                record.responce = false;
				record.warn = "Please enter Micro Drawer ID"
				
				return record;
				
			}
            else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;
			} 
		}
	});

    edit_assmebly_micro_drawer_map.set("edit_id_super_drawer_of_assembly_micro_drawer", {
		important: false
	});

    edit_assmebly_micro_drawer_map.set("edit_id_module_of_assembly_micro_drawer", {
		important: false
	});

    edit_assmebly_micro_drawer_map.set("id_edit_assembly_pmt_block_of_micro_drawer", {
		important: true,
		fn : "fetch_pmt_block_records_from_micro_mini_drawer",
		check : function(val)
		{
			let record = new Object();
			
			if( val.records[0].indexOf($("#id_edit_assembly_pmt_block_of_micro_drawer").val()) != -1) 
			{
                record.responce = false;
				record.warn = "with this ID is already assembled in Micro Drawer";
				
				return record;
				
			}
            else if( val.records[1].indexOf($("#id_edit_assembly_pmt_block_of_micro_drawer").val()) != -1 )
            {
                record.responce = false;
				record.warn = "with this ID is already assembled in Mini Drawer";
				
				return record;
            }
            else
			{
				record.responce = true;
				record.warn = undefined;

                waitPmtBlockIdCheck("id_edit_assembly_pmt_block_of_micro_drawer").then(
                    function (value) 
                    {
                        if (value) 
                        {
                            $("#edit_assembly_pmt_block_id_of_micro_drawer_info").text("");
                            $("#id_edit_assembly_pmt_block_of_micro_drawer").removeClass("not_ready_border_col");
                            $("#id_edit_assembly_pmt_block_of_micro_drawer").addClass("ready_to_insert");
                            assembly_micro_drawer_control.handle_edit_form();
            
                        } else {
                            $("#edit_assembly_pmt_block_id_of_micro_drawer_info").text(warnings["id_notreachable"]);
                            $("#id_edit_assembly_pmt_block_of_micro_drawer").addClass("not_ready_border_col");
                            $("#id_edit_assembly_pmt_block_of_micro_drawer").removeClass("ready_to_insert");
                            assembly_micro_drawer_control.handle_edit_form();
                        }
                    }
                );
				
				return record;
			} 
		}
	});

    edit_assmebly_micro_drawer_map.set("id_edit_assembly_pmt_block_pos_of_micro_drawer", {
		important: true,
		fn : "fetch_positions_of_pmt_block_from_micro_drawer_edit",
		check : function(val)
		{
			let record = new Object();
            let pmt_block_pos = $("#id_edit_assembly_pmt_block_pos_of_micro_drawer").val();

			if( $("#originValMicroDrawer").val() == "" )
            {
                record.responce = false;
				record.warn = "Something went wrong...";

				toastr["error"](warnings["unknown_problem"]);
            
            } else if( val.responce == "error" )
            {
                record.responce = true;
                record.warn = "";

                return record;
            } 

            else if ( val.responce == "success" )
            {
                if( val.records.indexOf(pmt_block_pos) != -1)
                {
                    record.responce = false;
                    record.warn = warnings["slot_filled"];
                }
                else
                {
                    record.responce = true;
                    record.warn = "";
                }

                return record;
            }
		}
	});

let reception_of_micro_drawer_map = new Map();
    reception_of_micro_drawer_map.set("id_micro_drawer_in_reception", {
		important: true,
		fn : "check_micro_drawer_id_entry",
		check : function(val)
		{
            let record = new Object();

            if( val == true )
            {
                record.responce = false;
                record.warn = warnings["inDB"];
            
                return record;
            }
            else
            {
                record.responce = true;
                record.warn = "";
                
                return record;
            }
        }
    })

/*

    ######   #######  ##     ## ########   #######  ##    ## ######## ##    ## ########  ######  
    ##    ## ##     ## ###   ### ##     ## ##     ## ###   ## ##       ###   ##    ##    ##    ## 
    ##       ##     ## #### #### ##     ## ##     ## ####  ## ##       ####  ##    ##    ##       
    ##       ##     ## ## ### ## ########  ##     ## ## ## ## ######   ## ## ##    ##     ######  
    ##       ##     ## ##     ## ##        ##     ## ##  #### ##       ##  ####    ##          ## 
    ##    ## ##     ## ##     ## ##        ##     ## ##   ### ##       ##   ###    ##    ##    ## 
    ######   #######  ##     ## ##         #######  ##    ## ######## ##    ##    ##     ######  

*/

let assembly_micro_drawer = new Object();
    assembly_micro_drawer.target = "assembly_micro_drawer_modal_target";
    assembly_micro_drawer.formID = "assembly_micro_drawer_form";
    assembly_micro_drawer.standart_inputs = assembly_micro_drawer_map;
    assembly_micro_drawer.comment = "micro_drawer_remark";
    assembly_micro_drawer.test_results = undefined;
    assembly_micro_drawer.submit_class = {
            cls: "add_micro_drawer_form_submit",
            fn: "assemble_micro_drawer_fn"
        };

    assembly_micro_drawer.edit_target = "edit_micro_drawer_form_submit";
    assembly_micro_drawer.edit_formID = "edit_edit_assembly_micro_drawer_form";
    assembly_micro_drawer.edit_standart_inputs = edit_assmebly_micro_drawer_map;
    assembly_micro_drawer.edit_comment = "edit_micro_drawer_remark";
    assembly_micro_drawer.edit_test_results = undefined
    assembly_micro_drawer.edit_cancel_class = "edit_mini_drawer_form_cancel";
    assembly_micro_drawer.edit_close_class = "edit_mini_drawer_form_cancel";

    assembly_micro_drawer.edit_submit_class = {
        cls: "edit_micro_drawer_form_submit",
        fn: "update_assembly_of_micro_drawer"
    };

let assembly_micro_drawer_control = new Object_control(assembly_micro_drawer);
    assembly_micro_drawer_control.assaignEvents();


let reception_of_micro_drawer = new Object();
    reception_of_micro_drawer.target = "add_micro_drawer_in_reception_target";
    reception_of_micro_drawer.formID = "add_micro_drawer_in_reception_form";
    reception_of_micro_drawer.standart_inputs = reception_of_micro_drawer_map;
    reception_of_micro_drawer.comment = "remark_for_micro_drawer_in_reception";
    reception_of_micro_drawer.test_results = undefined;
    reception_of_micro_drawer.submit_class = {
            cls: "add_micro_drawer_in_reception_form_submit",
            fn: "add_micro_drawer_in_reception_fn"
        };

let add_micro_drawer_in_reception_control = new Object_control(reception_of_micro_drawer);
	add_micro_drawer_in_reception_control.assaignEvents();


/*

	######## #### ##       ######## ######## ########  #### ##    ##  ######   
	##        ##  ##          ##    ##       ##     ##  ##  ###   ## ##    ##  
	##        ##  ##          ##    ##       ##     ##  ##  ####  ## ##        
	######    ##  ##          ##    ######   ########   ##  ## ## ## ##   #### 
	##        ##  ##          ##    ##       ##   ##    ##  ##  #### ##    ##  
	##        ##  ##          ##    ##       ##    ##   ##  ##   ### ##    ##  
	##       #### ########    ##    ######## ##     ## #### ##    ##  ######   

*/

$("#filter_new_micro_drawer_by_micro_drawer_id").on("input", function(){
    $('#micro_drawer_table').DataTable().columns( 0 ).search( this.value ).draw();
});

$("#filter_super_drawer_id_in_micro_drawer").on("input", function(){
    $('#micro_drawer_table').DataTable().columns( 1 ).search( this.value ).draw();
});

$("#filter_module_id_in_micro_drawer").on("input", function(){
    $('#micro_drawer_table').DataTable().columns( 2 ).search( this.value ).draw();
});

$("#filter_pmt_block_id_in_micro_drawer").on("input", function(){
    $('#micro_drawer_table').DataTable().columns( 3 ).search( this.value ).draw();
});

$("#filter_position_of_pmt_block_in_micro_drawer").on("input", function(){
    $('#micro_drawer_table').DataTable().columns( 4 ).search( this.value ).draw();
});

$("#filter_current_location_in_micro_drawer").on("input", function(){
    $('#micro_drawer_table').DataTable().columns( 5 ).search( this.value ).draw();
});

$("#filter_remark_in_micro_drawer").on("input", function(){
    $('#micro_drawer_table').DataTable().columns( 6 ).search( this.value ).draw();
});

$("#filter_creation_time_in_micro_drawer").on("input", function(){
    $('#micro_drawer_table').DataTable().columns( 7 ).search( this.value ).draw();
});

$("#filter_updated_time_in_micro_drawer").on("input", function(){
    $('#micro_drawer_table').DataTable().columns( 8 ).search( this.value ).draw();
});

$("#filter_created_by_in_micro_drawer").on("input", function(){
    $('#micro_drawer_table').DataTable().columns( 9 ).search( this.value ).draw();
});

$("#filter_updated_by_in_micro_drawer").on("input", function(){
    $('#micro_drawer_table').DataTable().columns( 10 ).search( this.value ).draw();
});




/*
	##     ## ####  ######  ########  #######  ########  ##    ## 
	##     ##  ##  ##    ##    ##    ##     ## ##     ##  ##  ##  
	##     ##  ##  ##          ##    ##     ## ##     ##   ####   
	#########  ##   ######     ##    ##     ## ########     ##    
	##     ##  ##        ##    ##    ##     ## ##   ##      ##    
	##     ##  ##  ##    ##    ##    ##     ## ##    ##     ##    
	##     ## ####  ######     ##     #######  ##     ##    ##    
*/


//Fetch micro drawer modifcation Records
$(document).on("click", "#show_micro_drawer_modification_table", function (e) 
{
    e.preventDefault();
    $('#micro_drawer_modification_table').DataTable().clear().destroy();
    fetch_micro_drawer_modification_history();
});


function fetch_micro_drawer_modification_history() {
    $.ajax({
        url: "micro_drawer/fetch_old",
        type: "post",
        dataType: "json",
        success: function (data) {

            if (data.responce == "success") {

                $('#micro_drawer_modification_table').DataTable({
                    "data": data.records,
                    "rowId": "id_micro_drawer_frame",
                    "columns": [
                        { "data": "id_micro_drawer_frame" },
                        { "data": "id_super_drawer" },
                        { "data": "module_id" },
                        { "data": "pmt_block_id" },
                        { "data": "pos_pmt_block" },
                        { "data": "current_location" },
                        { "data": "remark" },
                        { "data": "start_date" },
                        { "data": "end_date" },
                        { "data": "created_by" },
                        { "data": "changed_by" },
                        {
                            "render": function (data, type, row, meta) {

                                var a = `
								<i class="far fa-trash-alt px-1 delete_micro_drawer_hist" value="${row.id_micro_drawer_frame}" id ="" aria-hidden="true"></i>
							`;
                                return a;
                            }
                        }
                    ]
                });
            } else {
                toastr["error"](data.message);
            }
        }


    });
};



$("#filter_new_micro_drawer_by_micro_drawer_id_hist").on("input", function(){
    $('#micro_drawer_modification_table').DataTable().columns( 0 ).search( this.value ).draw();
});

$("#filter_super_drawer_id_in_micro_drawer_hist").on("input", function(){
    $('#micro_drawer_modification_table').DataTable().columns( 1 ).search( this.value ).draw();
});

$("#filter_module_id_in_micro_drawer_hist").on("input", function(){
    $('#micro_drawer_modification_table').DataTable().columns( 2 ).search( this.value ).draw();
});

$("#filter_pmt_block_id_in_micro_drawer_hist").on("input", function(){
    $('#micro_drawer_modification_table').DataTable().columns( 3 ).search( this.value ).draw();
});

$("#filter_position_of_pmt_block_in_micro_drawer_hist").on("input", function(){
    $('#micro_drawer_modification_table').DataTable().columns( 4 ).search( this.value ).draw();
});

$("#filter_current_location_in_micro_drawer_hist").on("input", function(){
    $('#micro_drawer_modification_table').DataTable().columns( 5 ).search( this.value ).draw();
});

$("#filter_remark_in_micro_drawer_hist").on("input", function(){
    $('#micro_drawer_modification_table').DataTable().columns( 6 ).search( this.value ).draw();
});

$("#filter_creation_time_in_micro_drawer_hist").on("input", function(){
    $('#micro_drawer_modification_table').DataTable().columns( 7 ).search( this.value ).draw();
});

$("#filter_updated_time_in_micro_drawer_hist").on("input", function(){
    $('#micro_drawer_modification_table').DataTable().columns( 8 ).search( this.value ).draw();
});

$("#filter_created_by_in_micro_drawer_hist").on("input", function(){
    $('#micro_drawer_modification_table').DataTable().columns( 9 ).search( this.value ).draw();
});

$("#filter_updated_by_in_micro_drawer_hist").on("input", function(){
    $('#micro_drawer_modification_table').DataTable().columns( 10 ).search( this.value ).draw();
});





//////////////////////////////////  FUNCTIONS
//////////////////////////////////  FUNCTIONS
//////////////////////////////////  FUNCTIONS
//////////////////////////////////  FUNCTIONS
//////////////////////////////////  FUNCTIONS
//////////////////////////////////  FUNCTIONS
//////////////////////////////////  FUNCTIONS
//////////////////////////////////  FUNCTIONS
//////////////////////////////////  FUNCTIONS
//////////////////////////////////  FUNCTIONS
//////////////////////////////////  FUNCTIONS



function getPmtBlockFromMicroMiniDrawer()
{
    $.ajax({
        url: "pmt_block/fetchPmtBlockRecordsFromMicroMiniDrawer",
        type: "post",
        dataType: "json",
        success: function (data) {
            recordsData = data.records;
        },
        error: function(data){
            console.log(data.responseText);
        }   
    });
};
