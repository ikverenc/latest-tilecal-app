// Fetch partition Records

$(document).on("click", "#show_partition_table", function(e){
    e.preventDefault();
    $('#partition_table').DataTable().clear().destroy();
    fetchPartition();

});

function fetchPartition(){
    $.ajax({
        url: "partition/fetch",
        type: "post",
        dataType: "json",
        success: function(data){
            if(data.responce == "success"){	
                            
                var table = $('#partition_table').DataTable( {
                    "data": data.records,
                    "rowId": "tc_partition",
                    "columns": [
                        { "data": "tc_partition" },
                        { "data": "id_200_hv_bulk_ps" },
                        { "data": "id_hv_bulk_ps" },
                        { "data": "remark" },
                        { "render": function ( data, type, row, meta ){
                            var a = `
                                <i class="far fa-edit px-1" value="${row.tc_partition}" id ="edit_pmt" aria-hidden="true"></i>
                            `;
                            return a;
                        }}					
                    ]
                } );
            }else{
                toastr["error"](data.message);
            }					
        }
    });
}