//Fetch module Records
$(document).on("click", "#show_module_table", function(e){
	e.preventDefault();

	$('#module_table').DataTable().clear().destroy();
	fetchModule();
});

function fetchModule(){
	$.ajax({
		url: "module/fetch",
		type: "post",
		dataType: "json",
		success: function(data){
			if(data.responce == "success"){	
				
							
				var table = $('#module_table').DataTable( {
					"data": data.records,
					"rowId": "module",
					"columns": [
						{ "data": "module" },
						{ "data": "tc_partition" },
						{ "data": "module_number" },
						{ "data": "id_lv_box" },
						{ "data": "id_ppr_blade" },
						{ "data": "id_auxiliary_board" },
						{ "data": "id_hv_regulation_board" },
						{ "data": "remark" },
						{ "render": function ( data, type, row, meta ){
							var a = `
								<i class="far fa-edit px-1" value="${row.module}" id ="edit_pmt" aria-hidden="true"></i>
							`;
							return a;
						}}					
					]
				} );
			}else{
				toastr["error"](data.message);
			}					
		}
	});
}