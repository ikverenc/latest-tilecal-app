$(document).ready(function () 
{
	$(".md-in-sd-assembly-added").hover(function () 
	{
		$(this).find("i").toggleClass("fa-check fa-times");
		$(this).toggleClass("btn-success btn-danger");
	});
	
	$(".pmt-added-in-md").hover(function () 
	{
		$(this).toggleClass("btn-success btn-danger");
		$(this).find("i").toggleClass("fa-check fa-times");
	});
	
	$(".pmt-added-in-sd").hover(function () 
	{
		$(this).toggleClass("btn-success btn-danger");
		$(this).find("i").toggleClass("fa-check fa-times");
	});
	
	$(".pmt-added-in-mmd").hover(function () 
	{
		$(this).toggleClass("btn-success btn-danger");
		$(this).find("i").toggleClass("fa-check fa-times");
	});
});

// show mini drawer table
$("#show_mini_drawer_table").click(function () 
{
	$(".component-table-row").hide();
	$(".mini-drawer-table-row").show();
});

$("#show_mini_drawer_table_modification_history").click(function ()
{
	$(".component-table-row").hide();
	$(".mini-drawer-modification-history-table-row").show();
})

$(".show_pmt_table").click(function () 
{
	$(".component-table-row").hide();
	$(".pmt-table-row").show();
});

$("#show_old_pmt_table").click(function () 
{
	$(".component-table-row").hide();
	$(".pmt-table-row").show();
});

$("#show_partition_table").click(function () 
{
	$(".component-table-row").hide();
	$(".partition-table-row").show();
});

$("#show_module_table").click(function () 
{
	$(".component-table-row").hide();
	$(".module-table-row").show();
});

$("#show_pmt_block_table").click(function () 
{
	$(".component-table-row").hide();
	$(".pmt-block-table-row").show();
});


$("#show_pmt_block_table_of_micro_drawer").click(function(){
	$(".component-table-row").hide();
	$(".pmt-block-table-row").show();
});

// show micro drawer
$("#show_micro_drawer_table").click(function () 
{
	$(".component-table-row").hide();
	$(".micro-drawer-table-row").show();
});

$("#show_micro_drawer_modification_table").click(function () 
{
	$(".component-table-row").hide();
	$(".micro-drawer-modification-hist-table-row").show();
});



$("#show_super_drawer_table").click(function () 
{
	$(".component-table-row").hide();
	$(".super-drawer-table-row").show();
});


// show fenics table
$("#show_fenics_table").click(function () 
{
	$(".component-table-row").hide();
	$(".fenics-table-row").show();
});

$("#show_second_fenics_table").click(function () 
{
	$(".component-table-row").hide();
	$(".fenics-table-row").show();
});


// show hv divider talbe
$("#show_hv_divider_table").click(function () 
{
	$(".component-table-row").hide();
	$(".hv-divier-table-row").show();
});

$("#show_second_hv_divider_table").click(function () 
{
	$(".component-table-row").hide();
	$(".hv-divier-table-row").show();
});



$("#show_mainboard_table").click(function () 
{
	$(".component-table-row").hide();
	$(".mainboard-table-row").show();
});

$("#show_daughterboard_table").click(function () 
{
	$(".component-table-row").hide();
	$(".daughterboard-table-row").show();
});

$("#show_hv_distribution_board_table").click(function () 
{
	$(".component-table-row").hide();
	$(".hv-distribution-board-table-row").show();
});

$("#show_ppr_blade_table").click(function () 
{
	$(".component-table-row").hide();
	$(".ppr-blade-table-row").show();
});

// old super drawer
$("#show_old_super_drawer_table").click(function () 
{
	$(".component-table-row").hide();
	$(".old-super-drawer-table-row").show();
});

$("#show_old_super_drawer_modification_history_table").click(function()
{
	$(".component-table-row").hide();
	$(".super-drawer-modification-table-row").show();
});

// old pmt block
$("#show_old_pmt_block_table").click(function () 
{
	$(".component-table-row").hide();
	$(".old-pmt-block-table-row").show();
});

$("#show_old_pmt_block_modification_history_table").click(function()
{
	$(".component-table-row").hide();
	$(".old_pmt_block_modification_table_row").show();
});

// old hv divider
$("#show_old_hv_divider_table").click(function()
{
	$(".component-table-row").hide();
	$(".old-hv-divider-table-row").show();
});

$("#show_old_hv_divider_table_modification_history").click(function()
{
	$(".component-table-row").hide();
	$(".modification-old-hv-divider-table-row").show();
});

// old board 3 in 1
$("#show_old_3_in_1_board_table").click(function()
{
	$(".component-table-row").hide();
	$(".old-board-3-in-1-table-row").show();
});

$("#show_old_3_in_1_board_table_modification_history").click(function()
{
	$(".component-table-row").hide();
	$(".modification-old-board-3-in-1-table-row").show();
});


function download_test_results(id, data)
{
    const d = new Date();
    let date = "";
    date += d.getFullYear() + "_" + d.getMonth() + "_" + d.getDate() + "_" + d.getHours() + "_" + d.getMinutes() + "_id_" + id+".txt"; 
    let blob = new Blob([data], { type: "text/plain;charset=utf-8" });
    saveAs(blob, date);
}

function delete_record( target, fn)
{
	let del_id = $(target).attr("value");

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-sm btn-success ml-1',
            cancelButton: 'btn btn-sm btn-danger'
        },
        buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Delete!',
        cancelButtonText: 'No, Cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) 
        {
			window[fn](del_id, swalWithBootstrapButtons);

        } else if(result.dismiss === Swal.DismissReason.cancel) 
        {
            swalWithBootstrapButtons.fire(
                'Cancelled',
                'Record is safe',
                'error'
            )
        }
    });
}



