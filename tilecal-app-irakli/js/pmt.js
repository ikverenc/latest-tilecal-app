//Fetch PMT Records
$(document).on("click", ".show_pmt_table", function (e) {
	e.preventDefault();

	$('#pmt_table').DataTable().clear().destroy();
	fetchPmt();

});

//Fetch PMT Records
$(document).on("click", "#show_old_pmt_table", function (e) {
	e.preventDefault();
	$('#pmt_table').DataTable().clear().destroy();
	fetchPmt();

});

function fetchPmt() {
	$.ajax({
		url: "pmt/fetch",
		type: "post",
		dataType: "json",
		success: function (data) {
			if (data.responce == "success") {

				var table = $('#pmt_table').DataTable({
					"data": data.records,
					// orderCellsTop: true,
					// fixedHeader: true,
					"rowId": "serial_number",
					"columns": [
						{ "data": "serial_number" },
						{ "data": "id_pmt_block" },
						{ "data": "module" },
						{ "data": "module_in_legacy_tilecal" },
						{ "data": "pos_in_phase2_tilecal" },
						{ "data": "pos_in_legacy_tilecal" },
						{ "data": "beta" },
						{ "data": "HV_nominal" },
						{ "data": "QE" },
						{ "data": "pmt_type" },
						{ "data": "run_number" },
						{ "data": "component_status" },
						{ "data": "current_location" },
						{ "data": "remark" },
						{
							"render": function (data, type, row, meta) {
								var a = `
								<i class="far fa-edit px-1" value="${row.serial_number}" id ="edit_pmt" aria-hidden="true"></i>
								<i class="fa fa-clock-o px-1" aria-hidden="true"></i>
								<i class="far fa-trash-alt px-1" value="${row.serial_number}" id ="delete_pmt" aria-hidden="true"></i>
							`;
								return a;
							}
						}
					]
				});
			} else {
				toastr["error"](data.message);
			}
		}
	});
}

// Delete PMT record
$(document).on("click", "#delete_pmt", function (e) {
	e.preventDefault();
	var del_id = $(this).attr("value");

	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
			confirmButton: 'btn btn-sm btn-success ml-1',
			cancelButton: 'btn btn-sm btn-danger'
		},
		buttonsStyling: false
	})

	swalWithBootstrapButtons.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Yes, Delete!',
		cancelButtonText: 'No, Cancel!',
		reverseButtons: true
	}).then((result) => {

		if (result.isConfirmed) {

			$.ajax({
				url: "pmt/delete",
				type: "post",
				dataType: "json",
				data: {
					del_id: del_id
				},
				success: function (data) {

					if (data.responce == "success") {
						$('#pmt_table').DataTable().clear().destroy();
						fetchPmt();
						swalWithBootstrapButtons.fire(
							'Deleted!',
							'Record has been deleted.',
							'success'
						);
					} else {
						swalWithBootstrapButtons.fire(
							'Cancelled',
							'Record is safe',
							'error'
						);
					}
				}
			});

		} else if (
			result.dismiss === Swal.DismissReason.cancel
		) {
			swalWithBootstrapButtons.fire(
				'Cancelled',
				'Record is safe',
				'error'
			)
		}
	})
});

//Edit PMT Record
$(document).on("click", "#edit_pmt", function (e) {

	e.preventDefault();
	var edit_id = $(this).attr("value");

	$.ajax({
		url: "pmt/edit",
		type: "post",
		dataType: "json",
		data: {
			edit_id: edit_id
		},
		success: function (data) {
			if (data.responce == "success") {

				$('#edit_pmt_modal').modal('show');
				$("#edit_id_pmt_frame").val(data.record.serial_number);
				$("#edit_pmt_status").val(data.record.component_status);
				$("#edit_pmt_current_location").val(data.record.current_location);
				$("#edit_pmt_remark").val(data.record.remark);
			} else {
				toastr["error"](data.message);

			}
		}
	})
});

//Update PMT record
$(document).on("click", "#edit_pmt_form_submit", function (e) {
	e.preventDefault();

	var edit_id_pmt_frame = $("#edit_id_pmt_frame").val();
	var edit_pmt_status = $("#edit_pmt_status").val();
	var edit_pmt_current_location = $("#edit_pmt_current_location").val();
	var edit_pmt_remark = $("#edit_pmt_remark").val();

	if (edit_id_pmt_frame == "") {
		alert('Mini Drawer ID is Required');
	} else {
		$.ajax({
			url: "pmt/update",
			type: "post",
			dataType: "json",
			data: {
				edit_id_pmt_frame: edit_id_pmt_frame,
				edit_pmt_status: edit_pmt_status,
				edit_pmt_current_location: edit_pmt_current_location,
				edit_pmt_remark: edit_pmt_remark
			},
			success: function (data) {

				if (data.responce == "success") {
					$('#pmt_table').DataTable().destroy();
					fetchPmt();
					$('#edit_pmt_modal').modal('hide');
					toastr["success"](data.message);
				} else {
					toastr["error"](data.message);
				}
			}
		})
	}
});