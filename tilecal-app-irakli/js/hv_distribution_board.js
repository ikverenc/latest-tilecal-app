$(".hv_distribution_board_modal_target").click(function () {

    $("#add_hv_distribution_board_form")[0].reset();
    $("#id_hv_distribution_board").css('border-color', "#ced4da");
    $("#id_mini_drawer_of_hv_distribution_board").css("border-color", "#ced4da");
    $(".add_hv_distribution_board_form_submit").addClass("notReady");

});



//Fetch HV distribution board Records
$(document).on("click", "#show_hv_distribution_board_table", function (e) {
    e.preventDefault();
    $('#hv_distribution_board_table').DataTable().clear().destroy();
    fetchHvDistributionBoard();
});

function fetchHvDistributionBoard() {
    $.ajax({
        url: "hv_distribution_board/fetch",
        type: "post",
        dataType: "json",
        success: function (data) {

            if (data.responce == "success") {

                $('#hv_distribution_board_table').DataTable({
                    "data": data.records,
                    "rowId": "id_hv_distribution_board",
                    "columns": [
                        { "data": "id_hv_distribution_board" },
                        { "data": "id_mini_drawer" },
                        { "data": "component_status" },
                        { "data": "current_location" },
                        { "data": "remark" },
                        {
                            "render": function (data, type, row, meta) {
                                var a = `
								<i class="far fa-edit px-1" value="${row.id_hv_distribution_board}" id ="edit_hv_distribution_board_id" aria-hidden="true"></i>
								<i class="far fa-trash-alt px-1" value="${row.id_hv_distribution_board}" id ="delete_hv_distribution_board_id" aria-hidden="true"></i>
							`;
                                return a;
                            }
                        }
                    ]
                });
            } else {
                toastr["error"](data.message);
            }
        }
    });
}


// Add Fenics
$(document).on("click", ".add_hv_distribution_board_form_submit", function (e) {
    e.preventDefault();

    // if ($(".add_fenics_form_submit").hasClass("notReady")) return;

    let id_hv_distribution_board = $("#id_hv_distribution_board").val();
    let id_mini_drawer = $("#id_mini_drawer_of_hv_distribution_board").val();
    let status = $("#hv_distribution_board_status").val();
    let current_location = $("#hv_distribution_board_current_location").val();
    let remark = $("#hv_distribution_board_remark").val();

    waitHvDistributionBoardIdCheck("id_hv_distribution_board").then(
        function (value) {
            if (value == false) {
                waitMiniDrawerIdCheck("id_mini_drawer_of_hv_distribution_board").then(
                    function (value) {
                        if (value == true) {

                            $.ajax({
                                url: "hv_distribution_board/insert",
                                type: "post",
                                dataType: "json",
                                data: {
                                    id_hv_distribution_board: id_hv_distribution_board,
                                    id_mini_drawer: id_mini_drawer,
                                    component_status: status,
                                    current_location: current_location,
                                    remark: remark
                                },
                                success: function (data) {

                                    if (data.responce == "success") {
                                        $('#hv_distribution_board_table').DataTable().destroy();
                                        $('#add_hv_distribution_board_modal').modal('hide');

                                        fetchHvDistributionBoard();
                                        toastr["success"](data.message);
                                    } else {
                                        toastr["error"](data.message);
                                    }
                                }
                            });

                            $("#add_hv_distribution_board_form")[0].reset();

                        }
                    }
                )
            }
        }
    );
});

// Delete fenics 
$(document).on("click", "#delete_hv_distribution_board_id", function (e) {
    e.preventDefault();

    let del_id = $(this).attr("value");

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-sm btn-success ml-1',
            cancelButton: 'btn btn-sm btn-danger'
        },
        buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Delete!',
        cancelButtonText: 'No, Cancel!',
        reverseButtons: true
    }).then((result) => {

        if (result.isConfirmed) {

            $.ajax({
                url: "hv_distribution_board/delete",
                type: "post",
                dataType: "json",
                data: {
                    del_id: del_id
                },
                success: function (data) {

                    if (data.responce == "success") {
                        $('#hv_distribution_board_table').DataTable().clear().destroy();
                        fetchHvDistributionBoard();

                        swalWithBootstrapButtons.fire(
                            'Deleted!',
                            'Record has been deleted.',
                            'success'
                        );
                    } else {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Record is safe',
                            'error'
                        );
                    }
                }
            });

        } else if (
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
                'Cancelled',
                'Record is safe',
                'error'
            )
        }
    })
});



//Fetch infromation of FENICSSS from database to fill fields in edit window
$(document).on("click", "#edit_hv_distribution_board_id", function (e) {
    e.preventDefault();
    let edit_id = $(this).attr("value");

    $.ajax({
        url: "hv_distribution_board/edit",
        type: "post",
        dataType: "json",
        data: {
            edit_id: edit_id
        },
        success: function (data) {
            if (data.responce == "success") {
                $('#edit_hv_distribution_board_modal').modal('show');
                $("#edit_id_hv_distribution_board").val(data.record.id_hv_distribution_board);
                $("#edit_id_mini_drawer_of_hv_distribution_board").val(data.record.id_mini_drawer);
                $("#edit_hv_distribution_board").val(data.record.component_status);
                $("#edit_hv_distribution_board_current_location").val(data.record.current_location);
                $("#edit_hv_distribution_board_remark").val(data.record.remark);
            } else {
                toastr["error"](data.message);
            }
        }
    })
});

//Update Fenics
$(document).on("click", "#edit_hv_distribution_board_form_submit", function (e) {
    e.preventDefault();

    let edit_id_hv_distribution_board = $("#edit_id_hv_distribution_board").val();
    let edit_id_mini_drawer = $("#edit_id_mini_drawer_of_hv_distribution_board").val();
    let status = $("#edit_hv_distribution_board").val();
    let current_location = $("#edit_hv_distribution_board_current_location").val();
    let remark = $("#edit_hv_distribution_board_remark").val();


    if (edit_id_hv_distribution_board == "") {
        alert('HV Distribution board ID is Required');
        return;
    } else if (edit_id_mini_drawer == "") {
        alert('Mini Drawer ID is Required');
        return;
    } else {
        $.ajax({
            url: "hv_distribution_board/update",
            type: "post",
            dataType: "json",
            data: {
                id_hv_distribution_board: edit_id_hv_distribution_board,
                id_mini_drawer: edit_id_mini_drawer,
                component_status: status,
                current_location: current_location,
                remark: remark
            },
            success: function (data) {

                if (data.responce == "success") {
                    $('#hv_distribution_board_table').DataTable().destroy();
                    $('#edit_hv_distribution_board_modal').modal('hide');

                    fetchHvDistributionBoard();
                    toastr["success"](data.message);
                } else {
                    toastr["error"](data.message);
                }
            }
        })
    }
});

// check fenics ID if exists
$("#id_hv_distribution_board").on('input', function () {

    $("#id_hv_distribution_board").css('box-shadow', '1px 1px 5px #888');
    $(".add_hv_distribution_board_form_submit").addClass("notReady")

    waitHvDistributionBoardIdCheck("id_hv_distribution_board").then(
        function (value) {

            if (value == false) {
                $("#hv_distribution_board_id_info").text("");

                $("#id_hv_distribution_board").removeClass("not_ready_border_col");
                $("#id_hv_distribution_board").css('border-color', "#00ff00");

            } else {
                $("#hv_distribution_board_id_info").text("HV Dist. board with this ID already exist");
                $("#id_hv_distribution_board").addClass("not_ready_border_col");
            }
        }
    );

});

$("#id_mini_drawer_of_hv_distribution_board").on('input', function () {

    $("#id_mini_drawer_of_hv_distribution_board").css('box-shadow', '1px 1px 5px #888');
    $(".add_hv_distribution_board_form_submit").addClass("notReady");

    waitMiniDrawerIdCheck("id_mini_drawer_of_hv_distribution_board").then(
        function (value) {
            if (value) {
                $("#mini_drawer_of_hv_distribution_board_info").text("");

                $("#id_mini_drawer_of_hv_distribution_board").removeClass("not_ready_border_col");
                $("#id_mini_drawer_of_hv_distribution_board").css('border-color', "#00ff00");

                if (!$("#id_hv_distribution_board").hasClass("not_ready_border_col") && !$("#id_mini_drawer_of_hv_distribution_board").hasClass("not_ready_border_col")) {
                    $(".add_hv_distribution_board_form_submit").removeClass("notReady");
                }

            } else {
                $("#mini_drawer_of_hv_distribution_board_info").text("Mini Drawer with this ID doesn't exist");
                $("#id_mini_drawer_of_hv_distribution_board").addClass("not_ready_border_col");

            }
        }
    );

});