/*
	######## ##     ## ######## ##    ## ########  ######  
	##       ##     ## ##       ###   ##    ##    ##    ## 
	##       ##     ## ##       ####  ##    ##    ##       
	######   ##     ## ######   ## ## ##    ##     ######  
	##        ##   ##  ##       ##  ####    ##          ## 
	##         ## ##   ##       ##   ###    ##    ##    ## 
	########    ###    ######## ##    ##    ##     ###### 
*/

// show old super drawer records
$(document).on("click", "#show_old_hv_divider_table", function (e) 
{
    e.preventDefault();
    $('#old_hv_divider_table').DataTable().clear().destroy();
    fetch_old_hv_divider();
    
});

// delete from old super drawer
$(document).on("click", ".delete_old_hv_divider", function (e) 
{
    e.preventDefault();
	delete_record(this, "delete_record_from_old_hv_divider" );
});


//Fetch infromation from db to fill fields in edit window
$(document).on("click", ".edit_old_hv_divider", function (e) 
{	
	e.preventDefault();
	let edit_id = $(this).attr("value");

	$.ajax({
		url: "hv_divider/edit_old",
		type: "post",
		dataType: "json",
		data: {
			edit_id: edit_id
		},
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#edit_old_hv_divider_modal').modal('show');
				$("#edit_old_hv_divider").val(data.record.id_hv_divider_old);
				$("#edit_id_pmt_block_of_old_hv_divider").val(data.record.id_pmt_block)
				$("#edit_remark_of_old_hv_divider").val(data.record.remark);
				
			} else {
				toastr["error"](data.message);
	 		}
		}
	});
});
/*
	######## ##     ## ##    ##  ######  ######## ####  #######  ##    ##  ######  
	##       ##     ## ###   ## ##    ##    ##     ##  ##     ## ###   ## ##    ## 
	##       ##     ## ####  ## ##          ##     ##  ##     ## ####  ## ##       
	######   ##     ## ## ## ## ##          ##     ##  ##     ## ## ## ##  ######  
	##       ##     ## ##  #### ##          ##     ##  ##     ## ##  ####       ## 
	##       ##     ## ##   ### ##    ##    ##     ##  ##     ## ##   ### ##    ## 
	##        #######  ##    ##  ######     ##    ####  #######  ##    ##  ######  
*/

// fetch Old Super Drawer
function fetch_old_hv_divider() 
{
    $.ajax({
        url: "hv_divider/fetch_old",
        type: "post",
        dataType: "json",
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#old_hv_divider_table').DataTable({
                    "data": data.records,
                    "rowId": "id_hv_divider_old",
                    "columns": [
                        { "data": "id_hv_divider_old" },
                        { "data": "id_pmt_block" },
                        { "data": "remark" },
                        { "data": "created_at" },
                        { "data": "updated_at" },
                        { "data": "created_by" },
                        { "data": "updated_by" },
                        {
                            "render": function (data, type, row, meta) 
                            {
                                var a = `
								<i class="far fa-edit px-1 edit_old_hv_divider" value="${row.id_hv_divider_old}" aria-hidden="true"></i>
								<i class="far fa-trash-alt px-1 delete_old_hv_divider" value="${row.id_hv_divider_old}" aria-hidden="true"></i>
							`;
                                return a;
                            }
                        }
                    ]
                });
            } else {
                toastr["error"](data.message);
            }
        }
    });
};

function delete_record_from_old_hv_divider( id, window )
{
	$.ajax({
        url: "hv_divider/delete_old",
        type: "post",
        dataType: "json",
        data: {
            del_id: id
        },
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#old_hv_divider_table').DataTable().clear().destroy();
                fetch_old_hv_divider();
				
				window.fire(
					'Deleted!',
					'Record has been deleted.',
					'success'
				);

			} else 
			{
				window.fire(
					'Cancelled',
					'Record is safe',
					'error'
				);
			}
		},
		error: function()
		{
			toastr["error"](warnings["unknown_problem"]);
		}
	});
}

function check_old_hv_divider_id_entry(id)
{
    return $.ajax({
        url: "hv_divider/checkOldDivider",
        type: "post",
        dataType: "json",
        data: {
            id: id
        }
    });
}


function update_old_hv_divider( obj )
{
    let id = $("#edit_old_hv_divider").val();
    let pmt_block_id = obj["edit_id_pmt_block_of_old_hv_divider"];
    let remark = obj["remark"];

    $.ajax({
        url: "hv_divider/update_old",
        type: "post",
        dataType: "json",
        data: {
            id_hv_divider_old: id,
            id_pmt_block: pmt_block_id,
            remark: remark
        },
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#old_hv_divider_table').DataTable().destroy();
                $('#edit_old_hv_divider_modal').modal('hide');
                fetch_old_hv_divider();                

                toastr["success"](data.message);
            
            } else {
                toastr["error"](data.message);
            }
        },
        error: function ()
        {
            toastr["error"](warnings["unknown_problem"]);            
        }
    });
}

function add_old_hv_divider_in_reception_fn( obj )
{
    let id = obj["id_old_hv_divider_in_reception"];
    let remark = obj["remark"];

    $.ajax({    
        url: "hv_divider/insert_in_old_hv_divider",
        type: "post",
        dataType: "json",
        data: {
            id_hv_divider_old: id,
            remark: remark
        },
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#old_hv_divider_table').DataTable().destroy();
                $('#add_old_hv_divider_in_reception_modal').modal('hide');
                fetch_old_hv_divider();                

                toastr["success"](data.message);
            
            } else {
                toastr["error"](data.message);
            }
        },
        error: function ()
        {
            toastr["error"](warnings["unknown_problem"]);            
        }
    });
}

/*

    ##     ##    ###    ########   ######        ##     ##    ###    ########   ######     
    ###   ###   ## ##   ##     ## ##    ##       ###   ###   ## ##   ##     ## ##    ##    
    #### ####  ##   ##  ##     ## ##             #### ####  ##   ##  ##     ## ##          
    ## ### ## ##     ## ########   ######        ## ### ## ##     ## ########   ######     
    ##     ## ######### ##              ##       ##     ## ######### ##              ##    
    ##     ## ##     ## ##        ##    ##       ##     ## ##     ## ##        ##    ##    
    ##     ## ##     ## ##         ######        ##     ## ##     ## ##         ######       	 

*/


// add modal of old super drawer
let rec_old_hv_divider_map = new Map();
    rec_old_hv_divider_map.set("id_old_hv_divider_in_reception", {
		important: true,
		fn : "check_old_hv_divider_id_entry",
		check : function(val)
		{
			let record = new Object();

			if( val ) 
			{
                record.responce = false;
				record.warn = warnings["inDB"];
				
				return record;
				
			}
            else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;
			} 
		}
	});

// edit modal of old pmt block
let edit_old_hv_divider = new Map();
    edit_old_hv_divider.set("edit_id_pmt_block_of_old_hv_divider", {
    important: false
});


/*

    ######   #######  ##     ## ########   #######  ##    ## ######## ##    ## ########  ######  
    ##    ## ##     ## ###   ### ##     ## ##     ## ###   ## ##       ###   ##    ##    ##    ## 
    ##       ##     ## #### #### ##     ## ##     ## ####  ## ##       ####  ##    ##    ##       
    ##       ##     ## ## ### ## ########  ##     ## ## ## ## ######   ## ## ##    ##     ######  
    ##       ##     ## ##     ## ##        ##     ## ##  #### ##       ##  ####    ##          ## 
    ##    ## ##     ## ##     ## ##        ##     ## ##   ### ##       ##   ###    ##    ##    ## 
    ######   #######  ##     ## ##         #######  ##    ## ######## ##    ##    ##     ######  

*/

let rec_old_hv_divider = new Object();
    rec_old_hv_divider.target = "old_hv_divider_modal_target";
    rec_old_hv_divider.formID = "add_old_hv_divider_in_reception_form";
    rec_old_hv_divider.standart_inputs = rec_old_hv_divider_map;
    rec_old_hv_divider.comment = "remark_for_old_hv_divider_in_reception";
    rec_old_hv_divider.test_results = undefined;
    rec_old_hv_divider.submit_class = {
            cls: "add_old_hv_divider_in_reception_form_submit",
            fn: "add_old_hv_divider_in_reception_fn"
        };
    
    // edit old hv divider
	rec_old_hv_divider.edit_target = "edit_old_hv_divider_form_submit";
	rec_old_hv_divider.edit_formID = "edit_old_hv_divider_form";
	rec_old_hv_divider.edit_standart_inputs = edit_old_hv_divider;
	rec_old_hv_divider.edit_comment = "edit_remark_of_old_hv_divider";
	rec_old_hv_divider.edit_test_results = undefined
    rec_old_hv_divider.edit_cancel_class = "edit_old_hv_divider_form_cancel";
	rec_old_hv_divider.edit_close_class = "close_btn_of_old_hv_divider";
	
	rec_old_hv_divider.edit_submit_class = {
		cls: "edit_old_hv_divider_form_submit",
		fn: "update_old_hv_divider"
	};
	

let reception_hv_divider = new Object_control(rec_old_hv_divider);
    reception_hv_divider.assaignEvents();





/*
	##     ## ####  ######  ########  #######  ########  ##    ## 
	##     ##  ##  ##    ##    ##    ##     ## ##     ##  ##  ##  
	##     ##  ##  ##          ##    ##     ## ##     ##   ####   
	#########  ##   ######     ##    ##     ## ########     ##    
	##     ##  ##        ##    ##    ##     ## ##   ##      ##    
	##     ##  ##  ##    ##    ##    ##     ## ##    ##     ##    
	##     ## ####  ######     ##     #######  ##     ##    ##    
*/



//Fetch OLD HV Divider modifcation Records
$(document).on("click", "#show_old_hv_divider_table_modification_history", function (e) 
{
    e.preventDefault();
    $('#modification_old_hv_divider_table').DataTable().clear().destroy();
    fetch_old_hv_divider_modification_history();
});


function fetch_old_hv_divider_modification_history() 
{
    $.ajax({
        url: "hv_divider/fetch_old_modification_history",
        type: "post",
        dataType: "json",
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#modification_old_hv_divider_table').DataTable({
                    "data": data.records,
                    "rowId": "id_hv_divider_old",
                    "columns": [
                        { "data": "id_hv_divider_old" },
                        { "data": "id_pmt_block" },
                        { "data": "remark" },
                        { "data": "start_date" },
                        { "data": "end_date" },
                        { "data": "created_by" },
                        { "data": "changed_by" },
                        {
                            "render": function (data, type, row, meta) 
                            {
                                var a = `
								<i class="far fa-trash-alt px-1 delete_old_hv_divider_hist" value="${row.id_hv_divider_old_frame}" aria-hidden="true"></i>
							`;
                                return a;
                            }
                        }
                    ]
                });
            } else {
                toastr["error"](data.message);
            }
        }
    });
};


//delete from old pmt block modification history
$(document).on("click", ".delete_old_hv_divider_hist", function (e) 
{
    e.preventDefault();
	delete_record(this, "delete_record_from_modification_of_old_hv_divider" );
});

function delete_record_from_modification_of_old_hv_divider( id, window )
{
	$.ajax({
		url: "hv_divider/delete_old_modification_entry",
		type: "post",
		dataType: "json",
		data: {
			del_id: id
		},
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#modification_old_hv_divider_table').DataTable().clear().destroy();
				fetch_old_hv_divider_modification_history();
				
				window.fire(
					'Deleted!',
					'Record has been deleted.',
					'success'
				);

			} else 
			{
				window.fire(
					'Cancelled',
					'Record is safe',
					'error'
				);
			}
		},
		error: function()
		{
			toastr["error"](warnings["unknown_problem"]);
		}
	});
}