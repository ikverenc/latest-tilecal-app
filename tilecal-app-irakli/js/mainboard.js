// clear data on each call
$(".mainboard_modal_target").click(function () {

    $("#add_mainboard_form")[0].reset();
    $("#id_mainboard").css('border-color', "#ced4da");
    $("#id_mini_drawer_mainboard").css("border-color", "#ced4da");
    $(".add_mainboard_form_submit").addClass("notReady");

});

//Fetch Mainboard Records
$(document).on("click", "#show_mainboard_table", function (e) {
    e.preventDefault();
    $('#mainboard_table').DataTable().clear().destroy();
    fetchMainboard();
});

function fetchMainboard() {
    $.ajax({
        url: "mainboard/fetch",
        type: "post",
        dataType: "json",
        success: function (data) {

            if (data.responce == "success") {

                $('#mainboard_table').DataTable({
                    "data": data.records,
                    "rowId": "id_mainboard",
                    "columns": [
                        { "data": "id_mainboard" },
                        { "data": "id_mini_drawer" },
                        { "data": "component_status" },
                        { "data": "current_location" },
                        { "data": "remark" },
                        {
                            "render": function (data, type, row, meta) {
                                var a = `
								<i class="far fa-edit px-1" value="${row.id_mainboard}" id ="edit_mainboard" aria-hidden="true"></i>
								<i class="far fa-trash-alt px-1" value="${row.id_mainboard}" id ="delete_mainboard" aria-hidden="true"></i>
							`;
                                return a;
                            }
                        }
                    ]
                });
            } else {
                toastr["error"](data.message);
            }
        }
    });
};



$(document).on("click", ".add_mainboard_form_submit", function (e) {
    e.preventDefault();

    // if ($(".add_mainboard_form_submit").hasClass("notReady")) return;

    // let id_mainboard = $("#id_mainboard").val();
    // let id_mini_drawer = $("#id_mini_drawer_mainboard").val();
    // let status = $("#mainboard_status").val();
    // let current_location = $("#mainboard_current_location").val();
    // let remark = $("#mainboard_remark").val();
    let id_mainboard = $("#id_mainboard").val();
    let test_results = $("#mainboard_test_results")[0].files[0];
    let res;

    let reader = new FileReader();
        reader.readAsText(test_results);
        reader.onload = function() {
            console.log(reader.result);
            $.ajax({
                url: "mainboard/insert",
                type: "post",
                dataType: "json",
                data: {
                    id_mainboard: id_mainboard,
                    test_results: reader.result
                },
                success: function (data) {
                    console.log(data);
                },
                error: function(data){
                    console.log(data);
                }
            });        
        };
/*
    $.ajax({
        url: "mainboard/insert",
        type: "post",
        dataType: "json",
        data: {
            id_mainboard: id_mainboard,
            id_mini_drawer: id_mini_drawer,
            component_status: status,
            current_location: current_location,
            remark: remark
        },
        success: function (data) {
            console.log(data);
            // if (data.responce == "success") {
            //     $('#mainboard_table').DataTable().destroy();
            //     $('#add_mainboard_modal').modal('hide');

            //     fetchMainboard();
            //     toastr["success"](data.message);
            // } else {
            //     toastr["error"](data.message);
            // }
        },
        error: function(data){
            console.log(data);
        }
    });

    $("#add_mainboard_form")[0].reset();
*/
});

// Delete fenics 
$(document).on("click", "#delete_mainboard", function (e) {
    e.preventDefault();

    let del_id = $(this).attr("value");

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-sm btn-success ml-1',
            cancelButton: 'btn btn-sm btn-danger'
        },
        buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Delete!',
        cancelButtonText: 'No, Cancel!',
        reverseButtons: true
    }).then((result) => {

        if (result.isConfirmed) {

            $.ajax({
                url: "mainboard/delete",
                type: "post",
                dataType: "json",
                data: {
                    del_id: del_id
                },
                success: function (data) {

                    if (data.responce == "success") {

                        $('#mainboard_table').DataTable().clear().destroy();
                        fetchMainboard();

                        swalWithBootstrapButtons.fire(
                            'Deleted!',
                            'Record has been deleted.',
                            'success'
                        );
                    } else {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Record is safe',
                            'error'
                        );
                    }
                }
            });

        } else if (
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
                'Cancelled',
                'Record is safe',
                'error'
            )
        }
    })
});


//Fetch infromation of mainboard from database to fill fields in edit window
$(document).on("click", "#edit_mainboard", function (e) {
    e.preventDefault();

    let edit_id = $(this).attr("value");

    $.ajax({
        url: "mainboard/edit",
        type: "post",
        dataType: "json",
        data: {
            edit_id: edit_id
        },
        success: function (data) {
            if (data.responce == "success") {
                $('#edit_mainboard_modal').modal('show');
                $("#edit_id_mainboard").val(data.record.id_mainboard);
                $("#edit_id_mini_drawer_of_mainboard").val(data.record.id_mini_drawer);
                $("#edit_mainboard_status").val(data.record.component_status);
                $("#edit_mainboard_current_location").val(data.record.current_location);
                $("#edit_mainboard_remark").val(data.record.remark);
            } else {
                toastr["error"](data.message);
            }
        }
    })
});

//Update Fenics
$(document).on("click", "#edit_mainboard_form_submit", function (e) {
    e.preventDefault();

    let edit_id_mainboard = $("#edit_id_mainboard").val();
    let edit_id_mini_drawer = $("#edit_id_mini_drawer_of_mainboard").val();
    let edit_mainboard_status = $("#edit_mainboard_status").val();
    let edit_mainboard_current_location = $("#edit_mainboard_current_location").val();
    let edit_mainboard_remark = $("#edit_mainboard_remark").val();

    if (edit_id_mainboard == "") {
        alert('Mainboard ID is Required');
        return;
    } else if (edit_id_mini_drawer == "") {
        alert('Mini Drawer ID is Required');
        return;
    } else {
        $.ajax({
            url: "mainboard/update",
            type: "post",
            dataType: "json",
            data: {
                id_mainboard: edit_id_mainboard,
                id_mini_drawer: edit_id_mini_drawer,
                component_status: edit_mainboard_status,
                current_location: edit_mainboard_current_location,
                remark: edit_mainboard_remark
            },
            success: function (data) {

                if (data.responce == "success") {

                    $('#mainboard_table').DataTable().destroy();
                    $('#edit_mainboard_modal').modal('hide');
                    fetchMainboard();

                    toastr["success"](data.message);
                } else {
                    toastr["error"](data.message);
                }
            }
        })
    }
});


// check fenics ID if exists
$("#id_mainboard").on('input', function () {

    $("#id_mainboard").css('box-shadow', '1px 1px 5px #888');
    $(".add_mainboard_form_submit").addClass("notReady")

    waitMainboardIdCheck("id_mainboard").then(
        function (value) {

            if (value == false) {
                $("#mainboard_id_info").text("");

                $("#id_mainboard").removeClass("not_ready_border_col");
                $("#id_mainboard").css('border-color', "#00ff00");

            } else {
                $("#mainboard_id_info").text("Mainboard with this ID already exist");
                $("#id_mainboard").addClass("not_ready_border_col");
            }
        }
    );

});

$("#id_mini_drawer_mainboard").on('input', function () {

    $("#id_mini_drawer_mainboard").css('box-shadow', '1px 1px 5px #888');
    $(".add_mainboard_form_submit").addClass("notReady");

    waitMiniDrawerIdCheck("id_mini_drawer_mainboard").then(
        function (value) {
            if (value) {
                $("#mainboard_mini_drawer_info").text("");

                $("#id_mini_drawer_mainboard").removeClass("not_ready_border_col");
                $("#id_mini_drawer_mainboard").css('border-color', "#00ff00");

                if (!$("#id_mainboard").hasClass("not_ready_border_col") && !$("#id_mini_drawer_mainboard").hasClass("not_ready_border_col")) {
                    $(".add_mainboard_form_submit").removeClass("notReady");
                }

            } else {
                $("#mainboard_mini_drawer_info").text("Mini Drawer with this ID doesn't exist");
                $("#id_mini_drawer_mainboard").addClass("not_ready_border_col");

            }
        }
    );

});
