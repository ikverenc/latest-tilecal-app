// clear data on each call
$(".hv_divider_modal_target").click(function () {

	$("#add_hv_divider_form")[0].reset();
	$("#id_hv_divider").css('border-color', "#ced4da");
	$("#id_pmt_block_hv_divider").css("border-color", "#ced4da");
	$(".add_hv_divider_form_submit").addClass("notReady");

});


//Fetch HV_divider Records
$(document).on("click", "#show_hv_divider_table", function (e) {
	e.preventDefault();
	$('#hv_divider_table').DataTable().clear().destroy();
	fetchHvDivider();
});

$(document).on("click", "#show_second_hv_divider_table", function (e) {
	e.preventDefault();
	$('#hv_divider_table').DataTable().clear().destroy();
	fetchHvDivider();
});

function fetchHvDivider() {
	$.ajax({
		url: "hv_divider/fetch",
		type: "post",
		dataType: "json",
		success: function (data) {

			if (data.responce == "success") {

				$('#hv_divider_table').DataTable({
					"data": data.records,
					"rowId": "hv_divider_id",
					"columns": [
						{ "data": "id_hv_divider" },
						{ "data": "id_pmt_block" },
						{ "data": "component_status" },
						{ "data": "current_location" },
						{ "data": "remark" },
						{
							"render": function (data, type, row, meta) {
								var a = `
								<i class="far fa-edit px-1" value="${row.id_hv_divider}" id ="edit_hv_divider" aria-hidden="true"></i>
								<i class="far fa-trash-alt px-1" value="${row.id_hv_divider}" id ="delete_hv_divider" aria-hidden="true"></i>
							`;
								return a;
							}
						}
					]
				});
			} else {
				toastr["error"](data.message);
			}
		}
	});
}

// Add HV_divider
$(document).on("click", ".add_hv_divider_form_submit", function (e) {
	e.preventDefault();

	if ($(".add_hv_divider_form_submit").hasClass("notReady")) return;

	$("#id_hv_divider").css('border', "0");
	$("#id_pmt_block_hv_divider").css("border", "0");

	let id_hv_divider = $("#id_hv_divider").val();
	let id_pmt_block_hv_divider = $("#id_pmt_block_hv_divider").val();
	let hv_divider_status = $("#hv_divider_status").val();
	let hv_divider_current_location = $("#hv_divider_current_location").val();
	let hv_divider_remark = $("#hv_divider_remark").val();


	waitHvDividerIdCheck("id_hv_divider").then(
		function (value) {
			if (value == false) {
				waitPmtBlockIdCheck("id_pmt_block_hv_divider").then(
					function (value) {
						if (value == true) {

							$.ajax({
								url: "hv_divider/insert",
								type: "post",
								dataType: "json",
								data: {
									id_hv_divider: id_hv_divider,
									id_pmt_block: id_pmt_block_hv_divider,
									component_status: hv_divider_status,
									current_location: hv_divider_current_location,
									remark: hv_divider_remark
								},
								success: function (data) {

									if (data.responce == "success") {
										$('#hv_divider_table').DataTable().destroy();
										fetchHvDivider();
										$('#add_hv_divider_modal').modal('hide');
										toastr["success"](data.message);
									} else {
										toastr["error"](data.message);
									}
								}
							});

							// $("#id_hv_divider").css('border', '0' );
							// $("#hv_divider_id_info").text("");

							// $("#id_pmt_block_hv_divider").css('border', "0");
							// $("#pmt_id_info").text("");


							$("#add_hv_divider_form")[0].reset();

						}
					}
				)
			}
		}
	);
});

// Delete HV_divider
$(document).on("click", "#delete_hv_divider", function (e) {
	e.preventDefault();

	let del_id = $(this).attr("value");

	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
			confirmButton: 'btn btn-sm btn-success ml-1',
			cancelButton: 'btn btn-sm btn-danger'
		},
		buttonsStyling: false
	})

	swalWithBootstrapButtons.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Yes, Delete!',
		cancelButtonText: 'No, Cancel!',
		reverseButtons: true
	}).then((result) => {

		if (result.isConfirmed) {

			$.ajax({
				url: "hv_divider/delete",
				type: "post",
				dataType: "json",
				data: {
					del_id: del_id
				},
				success: function (data) {

					if (data.responce == "success") {
						$('#hv_divider_table').DataTable().clear().destroy();
						fetchHvDivider();
						swalWithBootstrapButtons.fire(
							'Deleted!',
							'Record has been deleted.',
							'success'
						);
					} else {
						swalWithBootstrapButtons.fire(
							'Cancelled',
							'Record is safe',
							'error'
						);
					}
				}
			});

		} else if (
			result.dismiss === Swal.DismissReason.cancel
		) {
			swalWithBootstrapButtons.fire(
				'Cancelled',
				'Record is safe',
				'error'
			)
		}
	})
});

//Fetch infromation of HV_divider from database to fill fields in edit window
$(document).on("click", "#edit_hv_divider", function (e) {
	e.preventDefault();
	let edit_id = $(this).attr("value");

	$.ajax({
		url: "hv_divider/edit",
		type: "post",
		dataType: "json",
		data: {
			edit_id: edit_id
		},
		success: function (data) {
			if (data.responce == "success") {
				$('#edit_hv_divider_modal').modal('show');
				$("#edit_id_hv_divider").val(data.record.id_hv_divider);
				$("#edit_id_hv_divider_pmt_block").val(data.record.id_pmt_block);
				$("#edit_hv_divider_status").val(data.record.component_status);
				$("#edit_hv_divider_current_location").val(data.record.current_location);
				$("#edit_hv_divider_remark").val(data.record.remark);
			} else {
				toastr["error"](data.message);
			}
		}
	})
});

//Update HV_divider
$(document).on("click", "#edit_hv_divider_form_submit", function (e) {
	e.preventDefault();

	let edit_id_hv_divider = $("#edit_id_hv_divider").val();
	let edit_id_pmt_block = $("#edit_id_hv_divider_pmt_block").val();
	let edit_hv_divider_status = $("#edit_hv_divider_status").val();
	let edit_hv_divider_current_location = $("#edit_hv_divider_current_location").val();
	let edit_hv_divider_remark = $("#edit_hv_divider_remark").val();


	if (edit_id_hv_divider == "") {
		alert('HV_divider ID is Required');
		return;
	} else if (edit_id_pmt_block == "") {
		alert('Pmt Block ID is Required');
		return;
	} else {
		$.ajax({
			url: "hv_divider/update",
			type: "post",
			dataType: "json",
			data: {
				id_hv_divider: edit_id_hv_divider,
				id_pmt_block: edit_id_pmt_block,
				component_status: edit_hv_divider_status,
				current_location: edit_hv_divider_current_location,
				remark: edit_hv_divider_remark
			},
			success: function (data) {

				if (data.responce == "success") {
					$('#hv_divider_table').DataTable().destroy();
					fetchHvDivider();
					$('#edit_hv_divider_modal').modal('hide');
					toastr["success"](data.message);
				} else {
					toastr["error"](data.message);
				}
			}
		})
	}
});



// check hv_divider ID if exists
$("#id_hv_divider").on('input', function () {

	$("#id_hv_divider").css('box-shadow', '1px 1px 5px #888');
	$(".add_hv_divider_form_submit").addClass("notReady")

	waitHvDividerIdCheck("id_hv_divider").then(
		function (value) {
			if (value == false) {
				$("#hv_divider_id_info").text("");

				$("#id_hv_divider").removeClass("not_ready_border_col");
				$("#id_hv_divider").css('border-color', "#00ff00");

			} else {
				$("#hv_divider_id_info").text("HV Divider with this ID already exist");
				$("#id_hv_divider").addClass("not_ready_border_col");
			}
		}
	);

});

// check pmt block ID if exists
$("#id_pmt_block_hv_divider").on('input', function () {

	$("#id_pmt_block_hv_divider").css('box-shadow', '1px 1px 5px #888');
	$(".add_hv_divider_form_submit").addClass("notReady");

	waitPmtBlockIdCheck("id_pmt_block_hv_divider").then(
		function (value) {
			if (value) {
				$("#pmt_block_of_fenics_id_info").text("");

				$("#id_pmt_block_hv_divider").removeClass("not_ready_border_col");
				$("#id_pmt_block_hv_divider").css('border-color', "#00ff00");

				if (!$("#id_hv_divider").hasClass("not_ready_border_col") && !$("#id_pmt_block_hv_divider").hasClass("not_ready_border_col")) {
					$(".add_hv_divider_form_submit").removeClass("notReady");
				}

			} else {
				$("#pmt_block_of_fenics_id_info").text("Pmt Block with this ID doesn't exist");
				$("#id_pmt_block_hv_divider").addClass("not_ready_border_col");

			}
		}
	);

});


