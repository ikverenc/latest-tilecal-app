// clear data on each call
$(".fenics_modal_target").click(function () {

	$("#add_fenics_form")[0].reset();
	$("#id_fenics").css('border-color', "#ced4da");
	$("#id_pmt_block_fenics").css("border-color", "#ced4da");
	$(".add_fenics_form_submit").addClass("notReady");

});

//Fetch Fenics Records
$(document).on("click", "#show_fenics_table", function (e) {
	e.preventDefault();
	$('#fenics_table').DataTable().clear().destroy();
	fetchFenics();
});

$(document).on("click", "#show_second_fenics_table", function (e) {
	
	e.preventDefault();
	$('#fenics_table').DataTable().clear().destroy();
	fetchFenics();
});

function fetchFenics() {
	$.ajax({
		url: "fenics/fetch",
		type: "post",
		dataType: "json",
		success: function (data) {

			if (data.responce == "success") {

				$('#fenics_table').DataTable({
					"data": data.records,
					"rowId": "id_fenics",
					"columns": [
						{ "data": "id_fenics" },
						{ "data": "id_pmt_block" },
						{ "data": "component_status" },
						{ "data": "current_location" },
						{ "data": "remark" },
						{
							"render": function (data, type, row, meta) {
								var a = `
								<i class="far fa-edit px-1" value="${row.id_fenics}" id ="edit_fenics" aria-hidden="true"></i>
								<i class="far fa-trash-alt px-1" value="${row.id_fenics}" id ="delete_fenics" aria-hidden="true"></i>
							`;
								return a;
							}
						}
					]
				});
			} else {
				toastr["error"](data.message);
			}
		}
	});
}

// Add Fenics
$(document).on("click", ".add_fenics_form_submit", function (e) {
	e.preventDefault();

	if ($(".add_fenics_form_submit").hasClass("notReady")) return;

	let id_fenics = $("#id_fenics").val();
	let id_pmt_block_fenics = $("#id_pmt_block_fenics").val();
	let fenics_status = $("#fenics_status").val();
	let fenics_current_location = $("#fenics_current_location").val();
	let fenics_remark = $("#fenics_remark").val();


	waitFenicsIdCheck("id_fenics").then(
		function (value) {
			if (value == false) {
				waitPmtBlockIdCheck("id_pmt_block_fenics").then(
					function (value) {
						if (value == true) {

							$.ajax({
								url: "fenics/insert",
								type: "post",
								dataType: "json",
								data: {
									id_fenics: id_fenics,
									id_pmt_block: id_pmt_block_fenics,
									component_status: fenics_status,
									current_location: fenics_current_location,
									remark: fenics_remark
								},
								success: function (data) {

									if (data.responce == "success") {
										$('#fenics_table').DataTable().destroy();
										fetchFenics();
										$('#add_fenics_modal').modal('hide');
										toastr["success"](data.message);
									} else {
										toastr["error"](data.message);
									}
								}
							});

							$("#add_fenics_form")[0].reset();

						}
					}
				)
			}
		}
	);
});

// Delete fenics 
$(document).on("click", "#delete_fenics", function (e) {
	e.preventDefault();

	let del_id = $(this).attr("value");

	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
			confirmButton: 'btn btn-sm btn-success ml-1',
			cancelButton: 'btn btn-sm btn-danger'
		},
		buttonsStyling: false
	})

	swalWithBootstrapButtons.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Yes, Delete!',
		cancelButtonText: 'No, Cancel!',
		reverseButtons: true
	}).then((result) => {

		if (result.isConfirmed) {

			$.ajax({
				url: "fenics/delete",
				type: "post",
				dataType: "json",
				data: {
					del_id: del_id
				},
				success: function (data) {

					if (data.responce == "success") {
						$('#fenics_table').DataTable().clear().destroy();
						fetchFenics();
						swalWithBootstrapButtons.fire(
							'Deleted!',
							'Record has been deleted.',
							'success'
						);
					} else {
						swalWithBootstrapButtons.fire(
							'Cancelled',
							'Record is safe',
							'error'
						);
					}
				}
			});

		} else if (
			result.dismiss === Swal.DismissReason.cancel
		) {
			swalWithBootstrapButtons.fire(
				'Cancelled',
				'Record is safe',
				'error'
			)
		}
	})
});


//Fetch infromation of FENICSSS from database to fill fields in edit window
$(document).on("click", "#edit_fenics", function (e) {
	e.preventDefault();
	let edit_id = $(this).attr("value");

	$.ajax({
		url: "fenics/edit",
		type: "post",
		dataType: "json",
		data: {
			edit_id: edit_id
		},
		success: function (data) {
			if (data.responce == "success") {
				$('#edit_fenics_modal').modal('show');
				$("#edit_id_fenics").val(data.record.id_fenics);
				$("#edit_id_fenics_pmt_block").val(data.record.id_pmt_block);
				$("#edit_fenics_status").val(data.record.component_status);
				$("#edit_fenics_current_location").val(data.record.current_location);
				$("#edit_fenics_remark").val(data.record.remark);
			} else {
				toastr["error"](data.message);
			}
		}
	})
});

//Update Fenics
$(document).on("click", "#edit_fenics_form_submit", function (e) {
	e.preventDefault();

	let edit_id_fenics = $("#edit_id_fenics").val();
	let edit_id_pmt_block = $("#edit_id_fenics_pmt_block").val();
	let edit_fenics_status = $("#edit_fenics_status").val();
	let edit_fenics_current_location = $("#edit_fenics_current_location").val();
	let edit_fenics_remark = $("#edit_fenics_remark").val();


	if (edit_id_fenics == "") {
		alert('Fenics ID is Required');
		return;
	} else if (edit_id_pmt_block == "") {
		alert('Fenics ID is Required');
		return;
	} else {
		$.ajax({
			url: "fenics/update",
			type: "post",
			dataType: "json",
			data: {
				id_fenics: edit_id_fenics,
				id_pmt_block: edit_id_pmt_block,
				component_status: edit_fenics_status,
				current_location: edit_fenics_current_location,
				remark: edit_fenics_remark
			},
			success: function (data) {

				if (data.responce == "success") {
					$('#fenics_table').DataTable().destroy();
					fetchFenics();
					$('#edit_fenics_modal').modal('hide');
					toastr["success"](data.message);
				} else {
					toastr["error"](data.message);
				}
			}
		})
	}
});


// check fenics ID if exists
$("#id_fenics").on('input', function () {

	$("#id_fenics").css('box-shadow', '1px 1px 5px #888');
	$(".add_fenics_form_submit").addClass("notReady")

	waitFenicsIdCheck("id_fenics").then(
		function (value) {

			if (value == false) {
				$("#fenics_id_info").text("");

				$("#id_fenics").removeClass("not_ready_border_col");
				$("#id_fenics").css('border-color', "#00ff00");

			} else {
				$("#fenics_id_info").text("Fenics with this ID already exist");
				$("#id_fenics").addClass("not_ready_border_col");
			}
		}
	);

});

$("#id_pmt_block_fenics").on('input', function () {

	$("#id_pmt_block_fenics").css('box-shadow', '1px 1px 5px #888');
	$(".add_fenics_form_submit").addClass("notReady");

	waitPmtBlockIdCheck("id_pmt_block_fenics").then(
		function (value) {
			if (value) {
				$("#fenics_pmt_block_info").text("");

				$("#id_pmt_block_fenics").removeClass("not_ready_border_col");
				$("#id_pmt_block_fenics").css('border-color', "#00ff00");

				if (!$("#id_fenics").hasClass("not_ready_border_col") && !$("#id_pmt_block_fenics").hasClass("not_ready_border_col")) {
					$(".add_fenics_form_submit").removeClass("notReady");
				}

			} else {
				$("#fenics_pmt_block_info").text("Pmt Block with this ID doesn't exist");
				$("#id_pmt_block_fenics").addClass("not_ready_border_col");

			}
		}
	);

});


function fetchOldFenics()
{
	$.ajax({
		url: "fenics/fetchOldFenics",
		type: "post",
		dataType: "json",
		success: function (data) {
			console.log(data);
		},
		error: function(data){
			console.log(data);
		}	
	});
};

// fetchOldFenics();