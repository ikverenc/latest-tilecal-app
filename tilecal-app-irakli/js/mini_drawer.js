/*
	######## ##     ## ######## ##    ## ########  ######  
	##       ##     ## ##       ###   ##    ##    ##    ## 
	##       ##     ## ##       ####  ##    ##    ##       
	######   ##     ## ######   ## ## ##    ##     ######  
	##        ##   ##  ##       ##  ####    ##          ## 
	##         ## ##   ##       ##   ###    ##    ##    ## 
	########    ###    ######## ##    ##    ##     ###### 
*/


//Fetch Mini Drawer Records
$(document).on("click", "#show_mini_drawer_table", function (e) {
	e.preventDefault();
	$('#mini_drawer_table').DataTable().clear().destroy();
	fetchMinidrawer();
});


// delete from old super drawer
$(document).on("click", "#delete_mini_drawer", function (e) 
{
    e.preventDefault();
	delete_record(this, "delete_record_from_mini_drawer" );
});



//Fetch infromation from db to fill fields in edit window
$(document).on("click", "#edit_mini_drawer", function (e) {
	e.preventDefault();

	let edit_id = $(this).attr("value");

	$.ajax({
		url: "mini_drawer/edit",
		type: "post",
		dataType: "json",
		data: {
			edit_id: edit_id
		},
		success: function (data) {

			if (data.responce == "success") 
			{
				$('#edit_mini_drawer_modal').modal('show');
				$("#originValOfMiniDrawer").val(data.record.id_mini_drawer_frame);
				$("#edit_id_of_assembly_mini_drawer").val(data.record.id_mini_drawer);
				$("#edit_super_drawer_id_of_assembly_mini_drawer").val(data.record.id_super_drawer);
				$("#edit_module_id_of_assembly_mini_drawer").val(data.record.module_id);
				$("#edit_mainboard_id_of_mini_drawer").val(data.record.mainboard_id);
				$("#edit_daughterboard_id_of_mini_drawer").val(data.record.daughterboard_id);
				$("#edit_hv_distribution_board_id_of_mini_drawer").val(data.record.hv_distribution_board_id);
				$("#edit_pmt_block_id_of_mini_drawer").val(data.record.pmt_block_id);
				$("#edit_id_of_position_of_pmt_block_of_mini_drawer").val(data.record.pos_pmt_block);
				$("#edit_current_location_of_mini_drawer").val(data.record.current_location);
				$("#edit_comments_of_mini_drawer").val(data.record.remark);
	
			} else {
				toastr["error"](data.message);
			}
		}
	})
});

/*
	######## ##     ## ##    ##  ######  ######## ####  #######  ##    ##  ######  
	##       ##     ## ###   ## ##    ##    ##     ##  ##     ## ###   ## ##    ## 
	##       ##     ## ####  ## ##          ##     ##  ##     ## ####  ## ##       
	######   ##     ## ## ## ## ##          ##     ##  ##     ## ## ## ##  ######  
	##       ##     ## ##  #### ##          ##     ##  ##     ## ##  ####       ## 
	##       ##     ## ##   ### ##    ##    ##     ##  ##     ## ##   ### ##    ## 
	##        #######  ##    ##  ######     ##    ####  #######  ##    ##  ######  
*/

function fetchMinidrawer() {
	$.ajax({
		url: "mini_drawer/fetch",
		type: "post",
		dataType: "json",
		success: function (data) {
			if (data.responce == "success") 
			{
				var table = $('#mini_drawer_table').DataTable({
					"data": data.records,
					"rowId": "id_mini_drawer_frame",
					"columns": [
						{ "data": "id_mini_drawer" },
						{ "data": "id_super_drawer" },
						{ "data": "module_id" },
						{ "data": "mainboard_id" },
						{ "data": "daughterboard_id" },
						{ "data": "hv_distribution_board_id" },
						{ "data": "pmt_block_id" },
						{ "data": "pos_pmt_block" },
						{ "data": "current_location"},
						{ "data": "remark" },
						{ "data": "created_at"},
						{ "data": "updated_at"},
						{ "data": "created_by"},
						{ "data": "updated_by"},
						{
							"render": function (data, type, row, meta) {
								var a = `
									<i class="far fa-edit px-1" value="${row.id_mini_drawer_frame}" id ="edit_mini_drawer" aria-hidden="true"></i>
									<i class="far fa-trash-alt px-1" value="${row.id_mini_drawer_frame}" id ="delete_mini_drawer" aria-hidden="true"></i>
								`;
								return a;
							}
						}
					]
				});
			} else {
				toastr["error"](data.message);
			}
		}
	});
}


function delete_record_from_mini_drawer( id, window )
{
	$.ajax({
		url: "mini_drawer/delete",
		type: "post",
		dataType: "json",
		data: {
			del_id: id
		},
		success: function (data) {

			if (data.responce == "success") 
			{
				$('#mini_drawer_table').DataTable().clear().destroy();
				fetchMinidrawer();

				window.fire(
					'Deleted!',
					'Record has been deleted.',
					'success'
				);
			} else {
				window.fire(
					'Cancelled',
					'Record is safe',
					'error'
				);
			}
		},
		error: function()
		{
			toastr["error"](warnings["unknown_problem"]);
		}
	});
}

function check_mini_drawer_id(val)
{
	if( val.length > 0 )
	{
		return true;
	}else{
		return false
	}
}

function check_mini_drawer_id_entry(val)
{
	return $.ajax({
		url: "mini_drawer/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: val
		}
	});
}

function check_super_drawer_id(val)
{
	if( val.length > 0 )
	{
		return true;
	}else{
		return false
	}
}

function wait_mainboard_id_check(id)
{
	return $.ajax({
		url: "mainboard/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: id
		}
	});
}

function daughterboard_id_check(id)
{
	return $.ajax({
		url: "daughterboard/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: id
		}
	});
}

function new_hv_distribution_board_id_check(id)
{
	return $.ajax({
		url: "hv_distribution_board/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: id
		}
	});
}

function get_pmt_block_rewcords_from_micro_drawer()
{
	return $.ajax({
        url: "pmt_block/fetchPmtBlockRecordsFromMicroMiniDrawer",
        type: "post",
        dataType: "json"
    });
}

function fetch_position_from_mini_drawer(id)
{
	return $.ajax({
		url: "pmt_block/fetchPosFromMiniDrawer",
		type: "post",
		dataType: "json",
		data: {
			del_id: $("#assembly_mini_drawer_id").val()
		},
	});
}

function fetch_position_from_mini_drawer_edit(id)
{
	return $.ajax({
		url: "pmt_block/fetchPosFromMiniDrawer",
		type: "post",
		dataType: "json",
		data: {
			del_id: $("#edit_id_of_assembly_mini_drawer").val()
		},
	});
}

function assemble_mini_drawer(obj)
{
	let mini_drawer_id = obj["assembly_mini_drawer_id"];
	let super_drawer_id = obj["assembly_super_drawer_id_of_mini_drawer"];
	let module_id = obj["assembly_module_id_of_mini_drawer"];
	let mainboard_id = 	obj["assembly_mainboard_id_of_mini_drawer"];
	let daughterboard_id = obj["assembly_daughterboard_id_of_mini_drawer"];
	let hv_distribution_board_id = obj["assembly_hv_distribution_board_id_of_mini_drawer"];
	let pmt_block_id = obj["assembly_pmt_block_id_of_mini_drawer"];
	let pos_pmt_block = obj["assembly_pmt_block_position_of_mini_drawer"];
	let current_location = $("#current_location_of_mini_drawer").val();
	let remark = obj["remark"];

	$.ajax({
		url: "mini_drawer/insert",
		type: "post",
		dataType: "json",
		data: {
			id_mini_drawer: mini_drawer_id,
			id_super_drawer: super_drawer_id,
			module_id: module_id,
			mainboard_id: mainboard_id,
			daughterboard_id: daughterboard_id,
			hv_distribution_board_id: hv_distribution_board_id,
			pmt_block_id: pmt_block_id,
			pos_pmt_block: pos_pmt_block,
			current_location: current_location,
			remark: remark
		},
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#mini_drawer_table').DataTable().destroy();
				$('#add_mini_drawer_modal').modal('hide');
				fetchMinidrawer();

				toastr["success"](data.message);
			}else {
				toastr["error"](data.message);
			}
		},
		error: function (data){
			console.log(data);
		}
	});
}


function add_mini_drawer_in_reception_fn(obj)
{
	let id_mini_drawer = obj["id_mini_drawer_in_reception"];
	let remark = obj["remark"];

	$.ajax({
		url: "mini_drawer/insert",
		type: "post",
		dataType: "json",
		data: {
			id_mini_drawer: id_mini_drawer,
			remark: remark
		},
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#mini_drawer_table').DataTable().destroy();
				$('#add_mini_drawer_in_reception_modal').modal('hide');
				fetchMinidrawer();

				toastr["success"](data.message);
			}else {
				toastr["error"](data.message);
			}
		},
		error: function (data){
			toastr["error"](warnings["unknown_problem"]);
		}
	});
}


function update_assembly_of_mini_drawer(obj)
{
	let id_mini_drawer_frame = $("#originValOfMiniDrawer").val();
	let edit_id_mini_drawer = obj["edit_id_of_assembly_mini_drawer"];
	let edit_id_super_drawer = obj["edit_super_drawer_id_of_assembly_mini_drawer"];
	let edit_id_module = obj["edit_module_id_of_assembly_mini_drawer"];
	let edit_id_mainboard = obj["edit_mainboard_id_of_mini_drawer"];
	let edit_id_daughterboard = obj["edit_daughterboard_id_of_mini_drawer"];
	let edit_id_hv_distribution_board = obj["edit_hv_distribution_board_id_of_mini_drawer"];
	let edit_id_pmt_block = obj["edit_pmt_block_id_of_mini_drawer"];
	let edit_pos_pmt_block = obj["edit_id_of_position_of_pmt_block_of_mini_drawer"];
	let current_location = $("#edit_current_location_of_mini_drawer").val();
	let edit_remark = obj["remark"];

	$.ajax({
		url: "mini_drawer/update",
		type: "post",
		dataType: "json",
		data: {
			id_mini_drawer_frame: id_mini_drawer_frame,
			edit_id_mini_drawer: edit_id_mini_drawer,
			edit_id_super_drawer: edit_id_super_drawer,
			edit_id_module: edit_id_module,
			edit_id_mainboard: edit_id_mainboard,
			edit_id_daughterboard: edit_id_daughterboard,
			edit_id_hv_distribution_board: edit_id_hv_distribution_board,
			edit_id_pmt_block: edit_id_pmt_block,
			edit_pos_pmt_block: edit_pos_pmt_block,
			current_location: current_location,
			edit_remark: edit_remark
		},
		success: function (data) {

			if (data.responce == "success") {
				$('#mini_drawer_table').DataTable().destroy();
				$('#edit_mini_drawer_modal').modal('hide');
				fetchMinidrawer();

				toastr["success"](data.message);
			} else {
				toastr["error"](data.message);
			}
		}
	});
}


/*

    ##     ##    ###    ########   ######        ##     ##    ###    ########   ######     
    ###   ###   ## ##   ##     ## ##    ##       ###   ###   ## ##   ##     ## ##    ##    
    #### ####  ##   ##  ##     ## ##             #### ####  ##   ##  ##     ## ##          
    ## ### ## ##     ## ########   ######        ## ### ## ##     ## ########   ######     
    ##     ## ######### ##              ##       ##     ## ######### ##              ##    
    ##     ## ##     ## ##        ##    ##       ##     ## ##     ## ##        ##    ##    
    ##     ## ##     ## ##         ######        ##     ## ##     ## ##         ######       	 

*/


// add modal of old mini drawer
let assembly_mini_drawer_map = new Map();
	assembly_mini_drawer_map.set("assembly_mini_drawer_id", {
		important: true,
		fn : "check_mini_drawer_id",
		check : function(val)
		{
			let record = new Object();
			
			if( val == false) 
			{
                record.responce = false;
				record.warn = "Please enter Mini Drawer ID"
				
				return record;
				
			}
            else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;
			} 
		}
	});

	assembly_mini_drawer_map.set("assembly_super_drawer_id_of_mini_drawer", {
		important: false,
	});

	assembly_mini_drawer_map.set("assembly_module_id_of_mini_drawer", {
		important: false,
	});

	assembly_mini_drawer_map.set("assembly_mainboard_id_of_mini_drawer", {
		important: true,
		fn : "wait_mainboard_id_check",
		check : function(val)
		{
			let record = new Object();

			if( val == false ) 
			{
                record.responce = false;
				record.warn = warnings["id_notreachable"];
				
				return record;
			}
            else
			{
				record.responce = true;
				record.warn = "";

				$.ajax({
					url: "mini_drawer/mainboard_id_check",
					type: "post",
					dataType: "json",
					data:{
						id: $("#assembly_mainboard_id_of_mini_drawer").val()
					},
					success: function (data) {
						if( data.record )
						{
							$("#mainboard_id_info_of_assembly_mini_drawer").text(warnings["disassembled"]);
							$("#assembly_mainboard_id_of_mini_drawer").addClass("not_ready_border_col");
							$("#assembly_mainboard_id_of_mini_drawer").removeClass("ready_to_insert");
							assembly_mini_drawer_control.handle_form();
							return;	
						}else{
							$("#mainboard_id_info_of_assembly_mini_drawer").text("");
							$("#assembly_mainboard_id_of_mini_drawer").removeClass("not_ready_border_col");
							$("#assembly_mainboard_id_of_mini_drawer").addClass("ready_to_insert");
							assembly_mini_drawer_control.handle_form();
							return;
						}
					},
					error: function(data){
					}
				});

				return record;
			} 
		}
	});

	assembly_mini_drawer_map.set("assembly_daughterboard_id_of_mini_drawer", {
		important: true,
		fn : "daughterboard_id_check",
		check : function(val)
		{
			let record = new Object();
			
			if( val == false ) 
			{
                record.responce = false;
				record.warn = warnings["id_notreachable"];
				
				return record;
			}
            else
			{
				record.responce = true;
				record.warn = "";

				$.ajax({
					url: "mini_drawer/daughterboard_id_check",
					type: "post",
					dataType: "json",
					data:{
						id: $("#assembly_daughterboard_id_of_mini_drawer").val()
					},
					success: function (data) {
						if( data.record ){
							$("#daughterboard_id_info_of_assembly_mini_drawer").text(warnings["disassembled"]);
							$("#assembly_daughterboard_id_of_mini_drawer").addClass("not_ready_border_col");
							$("#assembly_daughterboard_id_of_mini_drawer").removeClass("ready_to_insert");
							assembly_mini_drawer_control.handle_form();
							return;	
						}else{
							$("#daughterboard_id_info_of_assembly_mini_drawer").text("");
							$("#assembly_daughterboard_id_of_mini_drawer").removeClass("not_ready_border_col");
							$("#assembly_daughterboard_id_of_mini_drawer").addClass("ready_to_insert");
							assembly_mini_drawer_control.handle_form();
						}
					},
					error: function(data){
						console.log(data);
					}
				})

				return record;
			} 
		}
	});

	assembly_mini_drawer_map.set("assembly_hv_distribution_board_id_of_mini_drawer", {
		important: true,
		fn : "new_hv_distribution_board_id_check",
		check : function(val)
		{
			let record = new Object();
			
			if( val == false ) 
			{
                record.responce = false;
				record.warn = warnings["id_notreachable"];
				
				return record;
			}
            else
			{
				record.responce = true;
				record.warn = "";

				$.ajax({
					url: "mini_drawer/hv_distribution_board_id_check",
					type: "post",
					dataType: "json",
					data:{
						id: $("#assembly_hv_distribution_board_id_of_mini_drawer").val()
					},
					success: function (data) {
						if( data.record ){
							$("#hv_distribution_board_id_info_of_assembly_mini_drawer").text(warnings["disassembled"]);
							$("#assembly_hv_distribution_board_id_of_mini_drawer").addClass("not_ready_border_col");
							$("#assembly_hv_distribution_board_id_of_mini_drawer").removeClass("ready_to_insert");
							assembly_mini_drawer_control.handle_form();
							return;	
						}else{
							$("#hv_distribution_board_id_info_of_assembly_mini_drawer").text("");
							$("#assembly_hv_distribution_board_id_of_mini_drawer").removeClass("not_ready_border_col");
							$("#assembly_hv_distribution_board_id_of_mini_drawer").addClass("ready_to_insert");
							assembly_mini_drawer_control.handle_form();
							return;
						}
					},
					error: function(data){
						console.log(data);
					}
				});

				return record;
			} 
		}
	});

	assembly_mini_drawer_map.set("assembly_pmt_block_id_of_mini_drawer", {
		important: true,
		fn : "get_pmt_block_rewcords_from_micro_drawer",
		check : function(val)
		{
			let record = new Object();
			
			if(val.records[0].indexOf($("#assembly_pmt_block_id_of_mini_drawer").val()) != -1 )
			{
				record.responce = false;
				record.warn = "with this ID is already assembled in Micro Drawer";
				return record;

			}
			else if(val.records[1].indexOf($("#assembly_pmt_block_id_of_mini_drawer").val()) != -1 ) 
			{
				record.responce = false;
				record.warn = "with this ID is already assembled in Mini Drawer";
				
				return record;
			}
			else
			{
				record.responce = true;
				record.warn = "";

				waitPmtBlockIdCheck("assembly_pmt_block_id_of_mini_drawer").then(
					function (value) 
					{
						if (value) 
						{
							$("#pmt_block_id_info_of_assembly_mini_drawer").text("");
							$("#assembly_pmt_block_id_of_mini_drawer").removeClass("not_ready_border_col");
							$("#assembly_pmt_block_id_of_mini_drawer").addClass("ready_to_insert");
							assembly_mini_drawer_control.handle_form();
			
						} else 
						{
							$("#pmt_block_id_info_of_assembly_mini_drawer").text(warnings["id_notreachable"]);
							$("#assembly_pmt_block_id_of_mini_drawer").addClass("not_ready_border_col");
							$("#assembly_pmt_block_id_of_mini_drawer").removeClass("ready_to_insert");
							assembly_mini_drawer_control.handle_form();
						}
					}
				);
				return record;
			}
		}
	});

	assembly_mini_drawer_map.set("assembly_pmt_block_position_of_mini_drawer", {
		important: true,
		fn : "fetch_position_from_mini_drawer",
		check : function(val)
		{
			console.log( val );
			let record = new Object();
			let position = $("#assembly_pmt_block_position_of_mini_drawer").val();
			
			if( $("#assembly_mini_drawer_id").val() == "" )
			{
				record.responce = false;
				record.warn = "Please fill Mini Drawer ID field first";
			
			}else if ( val.responce == "error" )
			{
				record.responce = true;
				record.warn = "";
			
			}else
			{
				if( val.records.indexOf(position) != -1)
				{
					record.responce = false;
					record.warn = warnings["slot_filled"];
				}
				else
				{
					record.responce = true;
					record.warn = "";
				}
			}
			
			console.log(record);
			return record;
		}
	});

// edit
let edit_assmebly_mini_drawer_map = new Map();
	edit_assmebly_mini_drawer_map.set("edit_id_of_assembly_mini_drawer", {
		important: true,
		fn : "check_mini_drawer_id",
		check : function(val)
		{
			let record = new Object();
			
			if( val == false) 
			{
                record.responce = false;
				record.warn = "Please enter Mini Drawer ID"
				
				return record;
				
			}
            else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;
			} 
		}
	});

	edit_assmebly_mini_drawer_map.set("edit_super_drawer_id_of_assembly_mini_drawer", {
		important: false,
	});

	edit_assmebly_mini_drawer_map.set("edit_module_id_of_assembly_mini_drawer", {
		important: false,
	});

	edit_assmebly_mini_drawer_map.set("edit_mainboard_id_of_mini_drawer", {
		important: true,
		fn : "wait_mainboard_id_check",
		check : function(val)
		{
			let record = new Object();

			if( val == false ) 
			{
                record.responce = false;
				record.warn = warnings["id_notreachable"];
				
				return record;
			}
            else
			{
				record.responce = true;
				record.warn = "";

				$.ajax({
					url: "mini_drawer/mainboard_id_check",
					type: "post",
					dataType: "json",
					data:{
						id: $("#edit_mainboard_id_of_mini_drawer").val()
					},
					success: function (data) {
						if( data.record ){
							console.log(warnings["disassembled"])
							$("#edit_id_mainboard_info_of_assembly_mini_drawer").text(warnings["disassembled"]);
							$("#edit_mainboard_id_of_mini_drawer").addClass("not_ready_border_col");
							$("#edit_mainboard_id_of_mini_drawer").removeClass("ready_to_insert");
							assembly_mini_drawer_control.handle_edit_form();
							return;	
						}else{
							$("#edit_id_mainboard_info_of_assembly_mini_drawer").text("");
							$("#edit_mainboard_id_of_mini_drawer").removeClass("not_ready_border_col");
							$("#edit_mainboard_id_of_mini_drawer").addClass("ready_to_insert");
							assembly_mini_drawer_control.handle_edit_form();
							return;
						}
					},
					error: function(data){
					}
				});

				return record;
			} 
		}
	});

	edit_assmebly_mini_drawer_map.set("edit_daughterboard_id_of_mini_drawer", {
		important: true,
		fn : "daughterboard_id_check",
		check : function(val)
		{
			let record = new Object();
			
			if( val == false ) 
			{
                record.responce = false;
				record.warn = warnings["id_notreachable"];
				
				return record;
			}
            else
			{
				record.responce = true;
				record.warn = "";

				$.ajax({
					url: "mini_drawer/daughterboard_id_check",
					type: "post",
					dataType: "json",
					data:{
						id: $("#edit_daughterboard_id_of_mini_drawer").val()
					},
					success: function (data) {
						if( data.record ){
							$("#edit_id_daughterboard_info_of_assembly_mini_drawer").text(warnings["disassembled"]);
							$("#edit_daughterboard_id_of_mini_drawer").addClass("not_ready_border_col");
							$("#edit_daughterboard_id_of_mini_drawer").removeClass("ready_to_insert");
							assembly_mini_drawer_control.handle_edit_form();
							return;	
						}else{
							$("#edit_id_daughterboard_info_of_assembly_mini_drawer").text("");
							$("#edit_daughterboard_id_of_mini_drawer").removeClass("not_ready_border_col");
							$("#edit_daughterboard_id_of_mini_drawer").addClass("ready_to_insert");
							assembly_mini_drawer_control.handle_edit_form();
						}
					},
					error: function(data){
						console.log(data);
					}
				})

				return record;
			} 
		}
	});

	edit_assmebly_mini_drawer_map.set("edit_hv_distribution_board_id_of_mini_drawer", {
		important: true,
		fn : "new_hv_distribution_board_id_check",
		check : function(val)
		{
			let record = new Object();
			
			if( val == false ) 
			{
                record.responce = false;
				record.warn = warnings["id_notreachable"];
				
				return record;
			}
            else
			{
				record.responce = true;
				record.warn = "";

				$.ajax({
					url: "mini_drawer/hv_distribution_board_id_check",
					type: "post",
					dataType: "json",
					data:{
						id: $("#edit_hv_distribution_board_id_of_mini_drawer").val()
					},
					success: function (data) {
						if( data.record ){
							$("#edit_id_hv_distribution_board_info_of_assembly_mini_drawer").text(warnings["disassembled"]);
							$("#edit_hv_distribution_board_id_of_mini_drawer").addClass("not_ready_border_col");
							$("#edit_hv_distribution_board_id_of_mini_drawer").removeClass("ready_to_insert");
							assembly_mini_drawer_control.handle_edit_form();
							return;	
						}else{
							$("#edit_id_hv_distribution_board_info_of_assembly_mini_drawer").text("");
							$("#edit_hv_distribution_board_id_of_mini_drawer").removeClass("not_ready_border_col");
							$("#edit_hv_distribution_board_id_of_mini_drawer").addClass("ready_to_insert");
							assembly_mini_drawer_control.handle_edit_form();
							return;
						}
					},
					error: function(data){
						console.log(data);
					}
				});

				return record;
			} 
		}
	});

	edit_assmebly_mini_drawer_map.set("edit_pmt_block_id_of_mini_drawer", {
		important: true,
		fn : "get_pmt_block_rewcords_from_micro_drawer",
		check : function(val)
		{
			let record = new Object();
			
			if(val.records[0].indexOf($("#edit_pmt_block_id_of_mini_drawer").val()) != -1 )
			{
				record.responce = false;
				record.warn = "with this ID is already assembled in Micro Drawer";
				return record;

			}
			else if(val.records[1].indexOf($("#edit_pmt_block_id_of_mini_drawer").val()) != -1 ) 
			{
				record.responce = false;
				record.warn = "with this ID is already assembled in Mini Drawer";
				
				return record;
			}
			else
			{
				record.responce = true;
				record.warn = "";

				waitPmtBlockIdCheck("edit_pmt_block_id_of_mini_drawer").then(
					function (value) 
					{
						if (value) 
						{
							$("#edit_id_pmt_block_info_of_assembly_mini_drawer").text("");
							$("#edit_pmt_block_id_of_mini_drawer").removeClass("not_ready_border_col");
							$("#edit_pmt_block_id_of_mini_drawer").addClass("ready_to_insert");
							assembly_mini_drawer_control.handle_edit_form();
			
						} else 
						{
							$("#edit_id_pmt_block_info_of_assembly_mini_drawer").text(warnings["id_notreachable"]);
							$("#edit_pmt_block_id_of_mini_drawer").addClass("not_ready_border_col");
							$("#edit_pmt_block_id_of_mini_drawer").removeClass("ready_to_insert");
							assembly_mini_drawer_control.handle_edit_form();
						}
					}
				);
				return record;
			}
		}
	});

	edit_assmebly_mini_drawer_map.set("edit_id_of_position_of_pmt_block_of_mini_drawer", {
		important: true,
		fn : "fetch_position_from_mini_drawer_edit",
		check : function(val)
		{
			let record = new Object();
			let position = $("#edit_id_of_position_of_pmt_block_of_mini_drawer").val();
			
			if( $("#originValOfMiniDrawer").val() == "" )
			{
				record.responce = false;
				record.warn = "Something went wrong...";

				toastr["error"](warnings["unknown_problem"]);
			
			}else if ( val.responce == "error" )
			{
				record.responce = true;
				record.warn = "";
			
			}else
			{
				if( val.records.indexOf(position) != -1)
				{
					record.responce = false;
					record.warn = warnings["slot_filled"];
				}
				else
				{
					record.responce = true;
					record.warn = "";
				}
			}
			
			return record;
		}
	});
	

let add_mini_drawer_in_reception_map = new Map();
	add_mini_drawer_in_reception_map.set("id_mini_drawer_in_reception", {
		important: true,
		fn : "check_mini_drawer_id_entry",
		check : function(val)
		{
			let record = new Object();
			
			if( val == false) 
			{
                record.responce = true;
				record.warn = ""
				
				return record;
				
			}
            else
			{
				record.responce = false;
				record.warn = warnings["inDB"];
				
				return record;
			} 
		}
	});

/*

    ######   #######  ##     ## ########   #######  ##    ## ######## ##    ## ########  ######  
    ##    ## ##     ## ###   ### ##     ## ##     ## ###   ## ##       ###   ##    ##    ##    ## 
    ##       ##     ## #### #### ##     ## ##     ## ####  ## ##       ####  ##    ##    ##       
    ##       ##     ## ## ### ## ########  ##     ## ## ## ## ######   ## ## ##    ##     ######  
    ##       ##     ## ##     ## ##        ##     ## ##  #### ##       ##  ####    ##          ## 
    ##    ## ##     ## ##     ## ##        ##     ## ##   ### ##       ##   ###    ##    ##    ## 
    ######   #######  ##     ## ##         #######  ##    ## ######## ##    ##    ##     ######  

*/

let assembly_mini_drawer = new Object();
	assembly_mini_drawer.target = "assembly_mini_drawer_modal_target";
	assembly_mini_drawer.formID = "add_mini_drawer_form";
	assembly_mini_drawer.standart_inputs = assembly_mini_drawer_map;
	assembly_mini_drawer.comment = "mini_drawer_remark";
	assembly_mini_drawer.test_results = undefined;
	assembly_mini_drawer.submit_class = {
            cls: "add_mini_drawer_form_submit",
            fn: "assemble_mini_drawer"
        };

	assembly_mini_drawer.edit_target = "edit_mini_drawer_form_submit";
	assembly_mini_drawer.edit_formID = "edit_mini_drawer_form";
	assembly_mini_drawer.edit_standart_inputs = edit_assmebly_mini_drawer_map;
	assembly_mini_drawer.edit_comment = "edit_comments_of_mini_drawer";
	assembly_mini_drawer.edit_test_results = undefined
	assembly_mini_drawer.edit_cancel_class = "edit_mini_drawer_form_cancel";
	assembly_mini_drawer.edit_close_class = "close_btn_of_update_assembly_mini_drawer_window";

	assembly_mini_drawer.edit_submit_class = {
		cls: "edit_mini_drawer_form_submit",
		fn: "update_assembly_of_mini_drawer"
	};

let assembly_mini_drawer_control = new Object_control(assembly_mini_drawer);
	assembly_mini_drawer_control.assaignEvents();


let reception_of_mini_drawer = new Object();
	reception_of_mini_drawer.target = "add_mini_drawer_in_reception_target";
	reception_of_mini_drawer.formID = "add_mini_drawer_in_reception_form";
	reception_of_mini_drawer.standart_inputs = add_mini_drawer_in_reception_map;
	reception_of_mini_drawer.comment = "remark_for_mini_drawer_in_reception";
	reception_of_mini_drawer.test_results = undefined;
	reception_of_mini_drawer.submit_class = {
            cls: "add_mini_drawer_in_reception_form_submit",
            fn: "add_mini_drawer_in_reception_fn"
        };

let add_mini_drawer_in_reception_control = new Object_control(reception_of_mini_drawer);
	add_mini_drawer_in_reception_control.assaignEvents();

/*

	######## #### ##       ######## ######## ########  #### ##    ##  ######   
	##        ##  ##          ##    ##       ##     ##  ##  ###   ## ##    ##  
	##        ##  ##          ##    ##       ##     ##  ##  ####  ## ##        
	######    ##  ##          ##    ######   ########   ##  ## ## ## ##   #### 
	##        ##  ##          ##    ##       ##   ##    ##  ##  #### ##    ##  
	##        ##  ##          ##    ##       ##    ##   ##  ##   ### ##    ##  
	##       #### ########    ##    ######## ##     ## #### ##    ##  ######   

*/
$("#filter_new_mini_drawer").on("input", function(){
    $('#mini_drawer_table').DataTable().columns( 0 ).search( this.value ).draw();
});

$("#filter_super_drawer_of_mini_drawer").on("input", function(){
    $('#mini_drawer_table').DataTable().columns( 1 ).search( this.value ).draw();
});

$("#filter_module_of_mini_drawer").on("input", function(){
    $('#mini_drawer_table').DataTable().columns( 2 ).search( this.value ).draw();
});

$("#filter_mainboard_of_mini_drawer").on("input", function(){
    $('#mini_drawer_table').DataTable().columns( 3 ).search( this.value ).draw();
});

$("#filter_daughterboard_of_mini_drawer").on("input", function(){
    $('#mini_drawer_table').DataTable().columns( 4 ).search( this.value ).draw();
});

$("#filter_hv_distribution_board_of_mini_drawer").on("input", function(){
    $('#mini_drawer_table').DataTable().columns( 5 ).search( this.value ).draw();
});

$("#filter_pmt_block_id_of_mini_drawer").on("input", function(){
    $('#mini_drawer_table').DataTable().columns( 6 ).search( this.value ).draw();
});

$("#filter_position_of_pmt_block_of_mini_drawer").on("input", function(){
    $('#mini_drawer_table').DataTable().columns( 7 ).search( this.value ).draw();
});

$("#filter_current_location_of_mini_drawer").on("input", function(){
    $('#mini_drawer_table').DataTable().columns( 8 ).search( this.value ).draw();
});

$("#filter_comment_of_mini_drawer").on("input", function(){
    $('#mini_drawer_table').DataTable().columns( 9 ).search( this.value ).draw();
});

$("#filter_by_creation_time_of_mini_drawer").on("input", function(){
    $('#mini_drawer_table').DataTable().columns( 10 ).search( this.value ).draw();
});

$("#filter_by_updated_time_of_mini_drawer").on("input", function(){
    $('#mini_drawer_table').DataTable().columns( 11 ).search( this.value ).draw();
});

$("#filter_created_by_of_mini_drawer").on("input", function(){
    $('#mini_drawer_table').DataTable().columns( 12 ).search( this.value ).draw();
});

$("#filter_updated_by_of_mini_drawer").on("input", function(){
    $('#mini_drawer_table').DataTable().columns( 13 ).search( this.value ).draw();
});


/*
	##     ## ####  ######  ########  #######  ########  ##    ## 
	##     ##  ##  ##    ##    ##    ##     ## ##     ##  ##  ##  
	##     ##  ##  ##          ##    ##     ## ##     ##   ####   
	#########  ##   ######     ##    ##     ## ########     ##    
	##     ##  ##        ##    ##    ##     ## ##   ##      ##    
	##     ##  ##  ##    ##    ##    ##     ## ##    ##     ##    
	##     ## ####  ######     ##     #######  ##     ##    ##    
*/



//Fetch OLD HV Divider modifcation Records
$(document).on("click", "#show_mini_drawer_table_modification_history", function (e) 
{
    e.preventDefault();
    $('#mini_drawer_modification_history_table').DataTable().clear().destroy();
    fetch_mini_drawer_modification_history();
});


function fetch_mini_drawer_modification_history() 
{
    $.ajax({
        url: "mini_drawer/fetch_old",
        type: "post",
        dataType: "json",
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#mini_drawer_modification_history_table').DataTable({
                    "data": data.records,
                    "rowId": "id_mini_drawer",
                    "columns": [
                        { "data": "id_mini_drawer" },
                        { "data": "id_super_drawer" },
                        { "data": "module_id" },
                        { "data": "mainboard_id" },
                        { "data": "daughterboard_id" },
                        { "data": "hv_distribution_board_id" },
                        { "data": "pmt_block_id" },
						{ "data": "pos_pmt_block" },
						{ "data": "current_location" },
						{ "data": "remark" },
						{ "data": "start_date" },
						{ "data": "end_date" },
						{ "data": "created_by" },
						{ "data": "changed_by" },
                        {
                            "render": function (data, type, row, meta) 
                            {
                                var a = `
								<i class="far fa-trash-alt px-1 delete_mini_drawer_hist" value="${row.id_mini_drawer_frame_hist}" aria-hidden="true"></i>
							`;
                                return a;
                            }
                        }
                    ]
                });
            } else {
                toastr["error"](data.message);
            }
        }
    });
};


//delete from old pmt block modification history
$(document).on("click", ".delete_mini_drawer_hist", function (e) 
{
    e.preventDefault();
	delete_record(this, "delete_record_from_modification_of_mini_drawer" );
});

function delete_record_from_modification_of_mini_drawer( id, window )
{
	$.ajax({
		url: "mini_drawer/delete_old_modification_entry",
		type: "post",
		dataType: "json",
		data: {
			del_id: id
		},
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#mini_drawer_modification_history_table').DataTable().clear().destroy();
				fetch_mini_drawer_modification_history();
				
				window.fire(
					'Deleted!',
					'Record has been deleted.',
					'success'
				);

			} else 
			{
				window.fire(
					'Cancelled',
					'Record is safe',
					'error'
				);
			}
		},
		error: function()
		{
			toastr["error"](warnings["unknown_problem"]);
		}
	});
}


/*

	######## #### ##       ######## ######## ########  #### ##    ##  ######   
	##        ##  ##          ##    ##       ##     ##  ##  ###   ## ##    ##  
	##        ##  ##          ##    ##       ##     ##  ##  ####  ## ##        
	######    ##  ##          ##    ######   ########   ##  ## ## ## ##   #### 
	##        ##  ##          ##    ##       ##   ##    ##  ##  #### ##    ##  
	##        ##  ##          ##    ##       ##    ##   ##  ##   ### ##    ##  
	##       #### ########    ##    ######## ##     ## #### ##    ##  ######   

*/
$("#filter_new_mini_drawer_hist").on("input", function(){
    $('#mini_drawer_modification_history_table').DataTable().columns( 0 ).search( this.value ).draw();
});

$("#filter_super_drawer_of_mini_drawer_hist").on("input", function(){
    $('#mini_drawer_modification_history_table').DataTable().columns( 1 ).search( this.value ).draw();
});

$("#filter_module_of_mini_drawer_hist").on("input", function(){
    $('#mini_drawer_modification_history_table').DataTable().columns( 2 ).search( this.value ).draw();
});

$("#filter_mainboard_of_mini_drawer_hist").on("input", function(){
    $('#mini_drawer_modification_history_table').DataTable().columns( 3 ).search( this.value ).draw();
});

$("#filter_daughterboard_of_mini_drawer_hist").on("input", function(){
    $('#mini_drawer_modification_history_table').DataTable().columns( 4 ).search( this.value ).draw();
});

$("#filter_hv_distribution_board_of_mini_drawer_hist").on("input", function(){
    $('#mini_drawer_modification_history_table').DataTable().columns( 5 ).search( this.value ).draw();
});

$("#filter_pmt_block_id_of_mini_drawer_hist").on("input", function(){
    $('#mini_drawer_modification_history_table').DataTable().columns( 6 ).search( this.value ).draw();
});

$("#filter_position_of_pmt_block_of_mini_drawer_hist").on("input", function(){
    $('#mini_drawer_modification_history_table').DataTable().columns( 7 ).search( this.value ).draw();
});

$("#filter_current_location_of_mini_drawer_hist").on("input", function(){
    $('#mini_drawer_modification_history_table').DataTable().columns( 8 ).search( this.value ).draw();
});

$("#filter_comment_of_mini_drawer_hist").on("input", function(){
    $('#mini_drawer_modification_history_table').DataTable().columns( 9 ).search( this.value ).draw();
});

$("#filter_by_creation_time_of_mini_drawer_hist").on("input", function(){
    $('#mini_drawer_modification_history_table').DataTable().columns( 10 ).search( this.value ).draw();
});

$("#filter_by_updated_time_of_mini_drawer_hist").on("input", function(){
    $('#mini_drawer_modification_history_table').DataTable().columns( 11 ).search( this.value ).draw();
});

$("#filter_created_by_of_mini_drawer_hist").on("input", function(){
    $('#mini_drawer_modification_history_table').DataTable().columns( 12 ).search( this.value ).draw();
});

$("#filter_updated_by_of_mini_drawer_hist").on("input", function(){
    $('#mini_drawer_modification_history_table').DataTable().columns( 13 ).search( this.value ).draw();
});
