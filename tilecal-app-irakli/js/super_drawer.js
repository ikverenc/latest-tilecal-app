/*
	######## ##     ## ######## ##    ## ########  ######  
	##       ##     ## ##       ###   ##    ##    ##    ## 
	##       ##     ## ##       ####  ##    ##    ##       
	######   ##     ## ######   ## ## ##    ##     ######  
	##        ##   ##  ##       ##  ####    ##          ## 
	##         ## ##   ##       ##   ###    ##    ##    ## 
	########    ###    ######## ##    ##    ##     ###### 
*/


//Fetch Super drawer Records
$(document).on("click", "#show_super_drawer_table", function (e) 
{
    e.preventDefault();
    $('#super_drawer_table').DataTable().clear().destroy();
    fetchSuperDrawer();
});


$(document).on("click", ".edit_super_drawer", function (e) {
    e.preventDefault();

    let edit_id = $(this).attr("value");

    $.ajax({
        url: "super_drawer/edit",
        type: "post",
        dataType: "json",
        data: {
            edit_id: edit_id
        },
        success: function (data) {
            if (data.responce == "success") {
                $('#edit_assembly_super_drawer').modal('show');
                $("#origin_value_of_super_drawer").val(data.record.id_super_drawer_frame)
                $("#id_edit_assembly_super_drawer").val(data.record.id_super_drawer);
                $("#id_edit_assembly_mini_drawer1_of_super_drawer").val(data.record.mini_drawer_id1);
                $("#id_edit_assembly_mini_drawer2_of_super_drawer").val(data.record.mini_drawer_id2);
                $("#id_edit_assembly_mini_drawer3_of_super_drawer").val(data.record.mini_drawer_id3);
                $("#id_edit_assembly_mini_drawer4_of_super_drawer").val(data.record.mini_drawer_id4);
                $("#id_edit_assembly_micro_drawer1_of_super_drawer").val(data.record.micro_drawer_id1);
                $("#id_edit_assembly_micro_drawer2_of_super_drawer").val(data.record.micro_drawer_id2);
                $("#edit_assembly_super_drawer_status").val(data.record.component_status);
                $("#edit_assembly_super_drawer_current_location").val(data.record.current_location);
                $("#edit_super_drawer_remark").val(data.record.remark);
            } else {
                toastr["error"](data.message);
            }
        }
    })
});


$('#super_drawer_types').on('change', function() 
{
    if( this.value == "ext_super_drawer" )
    {
        $("#assembly_of_super_drawer_title").text("Assembly of Extended Super Drawer");
        $("#id_assembly_mini_drawer4_of_super_drawer").parent().addClass("displ_none");
        $("#id_assembly_mini_drawer4_of_super_drawer").attr("name", "removed-element");
        $("#id_assembly_micro_drawer1_of_super_drawer").parent().removeClass("displ_none");
        $("#id_assembly_micro_drawer2_of_super_drawer").parent().removeClass("displ_none");
    }else{
        $("#assembly_of_super_drawer_title").text("Assembly of Regular Super Drawer");
        $("#id_assembly_mini_drawer4_of_super_drawer").parent().removeClass("displ_none");
        $("#id_assembly_mini_drawer4_of_super_drawer").attr("name", "mini_drawer");
        $("#id_assembly_micro_drawer1_of_super_drawer").parent().addClass("displ_none");
        $("#id_assembly_micro_drawer2_of_super_drawer").parent().addClass("displ_none");
    }
});

/*
	######## ##     ## ##    ##  ######  ######## ####  #######  ##    ##  ######  
	##       ##     ## ###   ## ##    ##    ##     ##  ##     ## ###   ## ##    ## 
	##       ##     ## ####  ## ##          ##     ##  ##     ## ####  ## ##       
	######   ##     ## ## ## ## ##          ##     ##  ##     ## ## ## ##  ######  
	##       ##     ## ##  #### ##          ##     ##  ##     ## ##  ####       ## 
	##       ##     ## ##   ### ##    ##    ##     ##  ##     ## ##   ### ##    ## 
	##        #######  ##    ##  ######     ##    ####  #######  ##    ##  ######  
*/

function fetchSuperDrawer() {
    $.ajax({
        url: "super_drawer/fetch",
        type: "post",
        dataType: "json",
        success: function (data) {
    
            if (data.responce == "success") 
            {
                $('#super_drawer_table').DataTable({
                    "data": data.records,
                    "columns": [
                        { "data": "id_super_drawer" },
                        { "data": "mini_drawer_id1" },
                        { "data": "mini_drawer_id2" },
                        { "data": "mini_drawer_id3" },
                        { "data": "mini_drawer_id4" },
                        { "data": "micro_drawer_id1" },
                        { "data": "micro_drawer_id2" },
                        { "data": null, "defaultContent": `<a class="px-1 download_test_result" aria-hidden="true" value="${data.id_super_drawer_frame}" id="download_new_sp_tr" style='font-weight:bold; color:#007bff !important;'>Download</a>`,"targets": -1},
                        { "data": "component_status" },
                        { "data": "current_location" },
                        { "data": "remark" },
                        { "data": "created_at" },
                        { "data": "updated_at" },
                        { "data": "created_by" },
                        { "data": "updated_by" },
                        {
                            "render": function (data, type, row, meta) {

                                var a = `
								<i class="far fa-edit px-1 edit_super_drawer" value="${row.id_super_drawer_frame}" id ="" aria-hidden="true"></i>
								<i class="far fa-trash-alt px-1 delete_super_drawer" value="${row.id_super_drawer_frame}" id ="" aria-hidden="true"></i>
							`;
                                return a;
                            }
                        }
                    ],
                });
            } else 
            {
                toastr["error"](data.message);
            }
        }
    });
};

function check_assembly_of_super_drawer_id(id)
{
    return $.ajax({
		url: "super_drawer/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: id
		}
	});
}

function fetch_mini_drawers_from_super_drawer() 
{
    return $.ajax({
        url: "super_drawer/fetchminidrawers",
        type: "post",
        dataType: "json"
    })
};

function fetch_micro_drawers_from_super_drawer()
{
    return $.ajax({
        url: "super_drawer/fetchmicrodrawers",
        type: "post",
        dataType: "json"
    })
}




// // check if all inputs are correctly filled 
function check_same_inputs_in_mini_drawer()
{
    let mini_drawers = [ $("#id_assembly_mini_drawer1_of_super_drawer").val(), $("#id_assembly_mini_drawer2_of_super_drawer").val(), $("#id_assembly_mini_drawer3_of_super_drawer").val(), $("#id_assembly_mini_drawer4_of_super_drawer").val() ];
    let arr = [];

    for( var i=0; i<mini_drawers.length; i++ )
    {
        if( mini_drawers[i] != "" )
        {
            arr.push( mini_drawers[i] );

        }else{
            continue;
        }
    }

    let duplicates = check_for_duplicates(arr);

    if( duplicates.length < 1 )
    {
        $("#id_assembly_mini_drawer1_of_super_drawer").removeClass("not_ready_border_col");
        $("#id_assembly_mini_drawer2_of_super_drawer").removeClass("not_ready_border_col");
        $("#id_assembly_mini_drawer3_of_super_drawer").removeClass("not_ready_border_col");
        $("#id_assembly_mini_drawer4_of_super_drawer").removeClass("not_ready_border_col");

        $("#id_assembly_mini_drawer1_of_super_drawer").addClass("ready_to_insert");
        $("#id_assembly_mini_drawer2_of_super_drawer").addClass("ready_to_insert");
        $("#id_assembly_mini_drawer3_of_super_drawer").addClass("ready_to_insert");
        $("#id_assembly_mini_drawer4_of_super_drawer").addClass("ready_to_insert");

        $("#assembly_mini_drawer1_id_info_of_super_drawer").text("");
        $("#assembly_mini_drawer2_id_info_of_super_drawer").text("");
        $("#assembly_mini_drawer3_id_info_of_super_drawer").text("");
        $("#assembly_mini_drawer4_id_info_of_super_drawer").text("");

        return true;
    
    }else
    {
        $("#id_assembly_mini_drawer1_of_super_drawer").addClass("not_ready_border_col");
        $("#id_assembly_mini_drawer2_of_super_drawer").addClass("not_ready_border_col");
        $("#id_assembly_mini_drawer3_of_super_drawer").addClass("not_ready_border_col");
        $("#id_assembly_mini_drawer4_of_super_drawer").addClass("not_ready_border_col");

        $("#id_assembly_mini_drawer1_of_super_drawer").removeClass("ready_to_insert");
        $("#id_assembly_mini_drawer2_of_super_drawer").removeClass("ready_to_insert");
        $("#id_assembly_mini_drawer3_of_super_drawer").removeClass("ready_to_insert");
        $("#id_assembly_mini_drawer4_of_super_drawer").removeClass("ready_to_insert");

        $("#assembly_mini_drawer1_id_info_of_super_drawer").text("Duplicate ID's in Mini Drawer field isn't allowed");
        $("#assembly_mini_drawer2_id_info_of_super_drawer").text("Duplicate ID's in Mini Drawer field isn't allowed");
        $("#assembly_mini_drawer3_id_info_of_super_drawer").text("Duplicate ID's in Mini Drawer field isn't allowed");
        $("#assembly_mini_drawer4_id_info_of_super_drawer").text("Duplicate ID's in Mini Drawer field isn't allowed");

        return false;
    }
}


// // check if all inputs are correctly filled 
function check_same_inputs_in_edit_mini_drawer()
{
    let mini_drawers = [ $("#id_edit_assembly_mini_drawer1_of_super_drawer").val(), $("#id_edit_assembly_mini_drawer2_of_super_drawer").val(), $("#id_edit_assembly_mini_drawer3_of_super_drawer").val(), $("#id_edit_assembly_mini_drawer4_of_super_drawer").val() ];
    let arr = [];

    for( var i=0; i<mini_drawers.length; i++ )
    {
        if( mini_drawers[i] != "" )
        {
            arr.push( mini_drawers[i] );

        }else{
            continue;
        }
    }

    let duplicates = check_for_duplicates(arr);

    if( duplicates.length < 1 )
    {
        $("#id_edit_assembly_mini_drawer1_of_super_drawer").removeClass("not_ready_border_col");
        $("#id_edit_assembly_mini_drawer2_of_super_drawer").removeClass("not_ready_border_col");
        $("#id_edit_assembly_mini_drawer3_of_super_drawer").removeClass("not_ready_border_col");
        $("#id_edit_assembly_mini_drawer4_of_super_drawer").removeClass("not_ready_border_col");

        $("#id_edit_assembly_mini_drawer1_of_super_drawer").addClass("ready_to_insert");
        $("#id_edit_assembly_mini_drawer2_of_super_drawer").addClass("ready_to_insert");
        $("#id_edit_assembly_mini_drawer3_of_super_drawer").addClass("ready_to_insert");
        $("#id_edit_assembly_mini_drawer4_of_super_drawer").addClass("ready_to_insert");

        $("#edit_assembly_mini_drawer1_id_info_of_super_drawer").text("");
        $("#edit_assembly_mini_drawer2_id_info_of_super_drawer").text("");
        $("#edit_assembly_mini_drawer3_id_info_of_super_drawer").text("");
        $("#edit_assembly_mini_drawer4_id_info_of_super_drawer").text("");

        return true;
    
    }else
    {
        $("#id_edit_assembly_mini_drawer1_of_super_drawer").addClass("not_ready_border_col");
        $("#id_edit_assembly_mini_drawer2_of_super_drawer").addClass("not_ready_border_col");
        $("#id_edit_assembly_mini_drawer3_of_super_drawer").addClass("not_ready_border_col");
        $("#id_edit_assembly_mini_drawer4_of_super_drawer").addClass("not_ready_border_col");

        $("#id_edit_assembly_mini_drawer1_of_super_drawer").removeClass("ready_to_insert");
        $("#id_edit_assembly_mini_drawer2_of_super_drawer").removeClass("ready_to_insert");
        $("#id_edit_assembly_mini_drawer3_of_super_drawer").removeClass("ready_to_insert");
        $("#id_edit_assembly_mini_drawer4_of_super_drawer").removeClass("ready_to_insert");

        $("#edit_assembly_mini_drawer1_id_info_of_super_drawer").text("Duplicate ID's in Mini Drawer field isn't allowed");
        $("#edit_assembly_mini_drawer2_id_info_of_super_drawer").text("Duplicate ID's in Mini Drawer field isn't allowed");
        $("#edit_assembly_mini_drawer3_id_info_of_super_drawer").text("Duplicate ID's in Mini Drawer field isn't allowed");
        $("#edit_assembly_mini_drawer4_id_info_of_super_drawer").text("Duplicate ID's in Mini Drawer field isn't allowed");

        return false;
    }
}


// // check if all inputs are correctly filled 
function check_same_inputs_in_micro_drawer()
{
    
    if( $("#id_assembly_micro_drawer1_of_super_drawer").val() == "" || $("#id_assembly_micro_drawer2_of_super_drawer").val() == "")
    {
        $("#id_assembly_micro_drawer1_of_super_drawer").removeClass("not_ready_border_col");
        $("#id_assembly_micro_drawer2_of_super_drawer").removeClass("not_ready_border_col");

        $("#id_assembly_micro_drawer1_of_super_drawer").addClass("ready_to_insert");
        $("#id_assembly_micro_drawer2_of_super_drawer").addClass("ready_to_insert");
        
        $("#assembly_micro_drawer1_id_info_of_super_drawer").text("");
        $("#assembly_micro_drawer2_id_info_of_super_drawer").text("");
        
        return true;
    }
    else if( $("#id_assembly_micro_drawer1_of_super_drawer").val() != $("#id_assembly_micro_drawer2_of_super_drawer").val())
    {
        $("#id_assembly_micro_drawer1_of_super_drawer").removeClass("not_ready_border_col");
        $("#id_assembly_micro_drawer2_of_super_drawer").removeClass("not_ready_border_col");

        $("#id_assembly_micro_drawer1_of_super_drawer").addClass("ready_to_insert");
        $("#id_assembly_micro_drawer2_of_super_drawer").addClass("ready_to_insert");
        
        $("#assembly_micro_drawer1_id_info_of_super_drawer").text("");
        $("#assembly_micro_drawer2_id_info_of_super_drawer").text("");
        
        return true;
    
    }else
    {
        $("#id_assembly_micro_drawer1_of_super_drawer").addClass("not_ready_border_col");
        $("#id_assembly_micro_drawer2_of_super_drawer").addClass("not_ready_border_col");

        $("#id_assembly_micro_drawer1_of_super_drawer").removeClass("ready_to_insert");
        $("#id_assembly_micro_drawer2_of_super_drawer").removeClass("ready_to_insert");
        
        $("#assembly_micro_drawer1_id_info_of_super_drawer").text("Duplicate ID's in Micro Drawer field isn't allowed");
        $("#assembly_micro_drawer2_id_info_of_super_drawer").text("Duplicate ID's in Micro Drawer field isn't allowed");

        return false;
    }
}

// // check if all inputs are correctly filled 
function check_same_inputs_in_edit_micro_drawer()
{
    
    if( $("#id_edit_assembly_micro_drawer1_of_super_drawer").val() == "" || $("#id_edit_assembly_micro_drawer2_of_super_drawer").val() == "")
    {
        $("#id_edit_assembly_micro_drawer1_of_super_drawer").removeClass("not_ready_border_col");
        $("#id_edit_assembly_micro_drawer2_of_super_drawer").removeClass("not_ready_border_col");

        $("#id_edit_assembly_micro_drawer1_of_super_drawer").addClass("ready_to_insert");
        $("#id_edit_assembly_micro_drawer2_of_super_drawer").addClass("ready_to_insert");
        
        $("#edit_assembly_micro_drawer1_id_info_of_super_drawer").text("");
        $("#edit_assembly_micro_drawer2_id_info_of_super_drawer").text("");
        
        return true;
    }
    else if( $("#id_edit_assembly_micro_drawer1_of_super_drawer").val() != $("#id_edit_assembly_micro_drawer2_of_super_drawer").val())
    {
        $("#id_edit_assembly_micro_drawer1_of_super_drawer").removeClass("not_ready_border_col");
        $("#id_edit_assembly_micro_drawer2_of_super_drawer").removeClass("not_ready_border_col");

        $("#id_edit_assembly_micro_drawer1_of_super_drawer").addClass("ready_to_insert");
        $("#id_edit_assembly_micro_drawer2_of_super_drawer").addClass("ready_to_insert");
        
        $("#edit_assembly_micro_drawer1_id_info_of_super_drawer").text("");
        $("#edit_assembly_micro_drawer2_id_info_of_super_drawer").text("");
        
        return true;
    
    }else
    {
        $("#id_edit_assembly_micro_drawer1_of_super_drawer").addClass("not_ready_border_col");
        $("#id_edit_assembly_micro_drawer2_of_super_drawer").addClass("not_ready_border_col");

        $("#id_edit_assembly_micro_drawer1_of_super_drawer").removeClass("ready_to_insert");
        $("#id_edit_assembly_micro_drawer2_of_super_drawer").removeClass("ready_to_insert");
        
        $("#edit_assembly_micro_drawer2_id_info_of_super_drawer").text("Duplicate ID's in Micro Drawer field isn't allowed");
        $("#edit_assembly_micro_drawer2_id_info_of_super_drawer").text("Duplicate ID's in Micro Drawer field isn't allowed");

        return false;
    }
}


function assemble_super_drawer( obj )
{   
    
    let id_super_drawer = obj["id_super_drawer"];
    let id_mini_drawer1 = obj["id_assembly_mini_drawer1_of_super_drawer"];
    let id_mini_drawer2 = obj["id_assembly_mini_drawer2_of_super_drawer"];
    let id_mini_drawer3 = obj["id_assembly_mini_drawer3_of_super_drawer"];
    let id_mini_drawer4 = obj["id_assembly_mini_drawer4_of_super_drawer"];
    let id_micro_drawer1 = obj["id_assembly_micro_drawer1_of_super_drawer"];
    let id_micro_drawer2 = obj["id_assembly_micro_drawer2_of_super_drawer"];
    let status = $("#super_drawer_status").val();
    let current_location = $("#super_drawer_current_location").val();
    let remark = obj["remark"];
    console.log( id_super_drawer)
    $.ajax({
        url: "super_drawer/insert",
        type: "post",
        dataType: "json",
        data: {
            id_super_drawer: id_super_drawer,
            mini_drawer_id1: id_mini_drawer1,
            mini_drawer_id2: id_mini_drawer2,
            mini_drawer_id3: id_mini_drawer3,
            mini_drawer_id4: id_mini_drawer4,
            micro_drawer_id1: id_micro_drawer1,
            micro_drawer_id2: id_micro_drawer2,
            component_status: status,
            current_location: current_location,
            remark: remark
        },
        success: function (data) 
        {
            if (data.responce == "success") {
                $('#super_drawer_table').DataTable().destroy();
                $('#assembly_super_drawer').modal('hide');
                fetchSuperDrawer();
                toastr["success"](data.message);
            } else {
                toastr["error"](data.message);
            }
        },
        error: function(data)
        {
            console.log(data.responseText);
        }
    });
}

/*

    ##     ##    ###    ########   ######        ##     ##    ###    ########   ######     
    ###   ###   ## ##   ##     ## ##    ##       ###   ###   ## ##   ##     ## ##    ##    
    #### ####  ##   ##  ##     ## ##             #### ####  ##   ##  ##     ## ##          
    ## ### ## ##     ## ########   ######        ## ### ## ##     ## ########   ######     
    ##     ## ######### ##              ##       ##     ## ######### ##              ##    
    ##     ## ##     ## ##        ##    ##       ##     ## ##     ## ##        ##    ##    
    ##     ## ##     ## ##         ######        ##     ## ##     ## ##         ######       	 

*/


// add modal of suoer drawer
let assembly_super_drawer_map = new Map();
    assembly_super_drawer_map.set("id_super_drawer", {
		important: true,
		fn : "check_assembly_of_super_drawer_id",
		check : function(val)
		{
			let record = new Object();
			
			if( val == false) 
			{
                record.responce = true;
				record.warn = "";
				
				return record;
				
			}
            else
			{
				record.responce = false;
				record.warn = warnings["assembled"];
				
				return record;
			} 
		}
	});

    assembly_super_drawer_map.set("id_assembly_mini_drawer1_of_super_drawer", {
		important: true,
		fn : "fetch_mini_drawers_from_super_drawer",
		check : function(val)
		{
			let record = new Object();
            
			if ( val.records.indexOf( $("#id_assembly_mini_drawer1_of_super_drawer").val() ) != -1 )
            {
                record.responce = false;
                record.warn = warnings["inDB"];

                return record;
            }

            else 
            {
                
                let status = check_same_inputs_in_mini_drawer();
                
                if( status == false )
                {
                    assembly_super_drawer_control.handle_form();
                } 

                else 
                { 
                    waitMiniDrawerIdCheck($("#id_assembly_mini_drawer1_of_super_drawer").val()).then(
                        function (value) 
                        {
                            if (value) 
                            {
                                $("#assembly_mini_drawer1_id_info_of_super_drawer").text("");
                                $("#id_assembly_mini_drawer1_of_super_drawer").removeClass("not_ready_border_col");
                                $("#id_assembly_mini_drawer1_of_super_drawer").addClass("ready_to_insert");
                                assembly_super_drawer_control.handle_form();
                
                            } else 
                            {
                                $("#assembly_mini_drawer1_id_info_of_super_drawer").text(warnings["id_notreachable"]);
                                $("#id_assembly_mini_drawer1_of_super_drawer").addClass("not_ready_border_col");
                                $("#id_assembly_mini_drawer1_of_super_drawer").removeClass("ready_to_insert");
                                assembly_super_drawer_control.handle_form();
                            }
                        }
                    );
                }

                record.responce = true;
                record.warn = "";

                return record;
            }
		}
	});

    assembly_super_drawer_map.set("id_assembly_mini_drawer2_of_super_drawer", {
		important: true,
		fn : "fetch_mini_drawers_from_super_drawer",
		check : function(val)
		{
			let record = new Object();
            
			if ( val.records.indexOf( $("#id_assembly_mini_drawer2_of_super_drawer").val() ) != -1 )
            {
                record.responce = false;
                record.warn = warnings["inDB"];

                return record;
            }

            else 
            {
                
                let status = check_same_inputs_in_mini_drawer();
                
                if( status == false )
                {
                    assembly_super_drawer_control.handle_form();
                } 

                else 
                { 
                    waitMiniDrawerIdCheck($("#id_assembly_mini_drawer2_of_super_drawer").val()).then(
                        function (value) 
                        {
                            if (value) 
                            {
                                $("#assembly_mini_drawer2_id_info_of_super_drawer").text("");
                                $("#id_assembly_mini_drawer2_of_super_drawer").removeClass("not_ready_border_col");
                                $("#id_assembly_mini_drawer2_of_super_drawer").addClass("ready_to_insert");
                                assembly_super_drawer_control.handle_form();
                
                            } else 
                            {
                                $("#assembly_mini_drawer2_id_info_of_super_drawer").text(warnings["id_notreachable"]);
                                $("#id_assembly_mini_drawer2_of_super_drawer").addClass("not_ready_border_col");
                                $("#id_assembly_mini_drawer2_of_super_drawer").removeClass("ready_to_insert");
                                assembly_super_drawer_control.handle_form();
                            }
                        }
                    );
                }

                record.responce = true;
                record.warn = "";

                return record;
            }
		}
	});

    assembly_super_drawer_map.set("id_assembly_mini_drawer3_of_super_drawer", {
		important: true,
		fn : "fetch_mini_drawers_from_super_drawer",
		check : function(val)
		{
			let record = new Object();
            
			if ( val.records.indexOf( $("#id_assembly_mini_drawer3_of_super_drawer").val() ) != -1 )
            {
                record.responce = false;
                record.warn = warnings["inDB"];

                return record;
            }

            else 
            {
                let status = check_same_inputs_in_mini_drawer();
                
                if( status == false )
                {
                    assembly_super_drawer_control.handle_form();
                } 

                else 
                { 
                    waitMiniDrawerIdCheck($("#id_assembly_mini_drawer3_of_super_drawer").val()).then(
                        function (value) 
                        {
                            if (value) 
                            {
                                $("#assembly_mini_drawer3_id_info_of_super_drawer").text("");
                                $("#id_assembly_mini_drawer3_of_super_drawer").removeClass("not_ready_border_col");
                                $("#id_assembly_mini_drawer3_of_super_drawer").addClass("ready_to_insert");
                                assembly_super_drawer_control.handle_form();
                
                            } else 
                            {
                                $("#assembly_mini_drawer3_id_info_of_super_drawer").text(warnings["id_notreachable"]);
                                $("#id_assembly_mini_drawer3_of_super_drawer").addClass("not_ready_border_col");
                                $("#id_assembly_mini_drawer3_of_super_drawer").removeClass("ready_to_insert");
                                assembly_super_drawer_control.handle_form();
                            }
                        }
                    );
                }

                record.responce = true;
                record.warn = "";

                return record;
            }
		}
	});

    assembly_super_drawer_map.set("id_assembly_mini_drawer4_of_super_drawer", {
		important: true,
		fn : "fetch_mini_drawers_from_super_drawer",
		check : function(val)
		{
			let record = new Object();
            
			if ( val.records.indexOf( $("#id_assembly_mini_drawer4_of_super_drawer").val() ) != -1 )
            {
                record.responce = false;
                record.warn = warnings["inDB"];

                return record;
            }

            else 
            {
                
                let status = check_same_inputs_in_mini_drawer();
                
                if( status == false )
                {
                    assembly_super_drawer_control.handle_form();
                } 

                else 
                { 
                    waitMiniDrawerIdCheck($("#id_assembly_mini_drawer4_of_super_drawer").val()).then(
                        function (value) 
                        {
                            if (value) 
                            {
                                $("#assembly_mini_drawer4_id_info_of_super_drawer").text("");
                                $("#id_assembly_mini_drawer4_of_super_drawer").removeClass("not_ready_border_col");
                                $("#id_assembly_mini_drawer4_of_super_drawer").addClass("ready_to_insert");
                                assembly_super_drawer_control.handle_form();
                
                            } else 
                            {
                                $("#assembly_mini_drawer4_id_info_of_super_drawer").text(warnings["id_notreachable"]);
                                $("#id_assembly_mini_drawer4_of_super_drawer").addClass("not_ready_border_col");
                                $("#id_assembly_mini_drawer4_of_super_drawer").removeClass("ready_to_insert");
                                assembly_super_drawer_control.handle_form();
                            }
                        }
                    );
                }

                record.responce = true;
                record.warn = "";

                return record;
            }
		}
	});

    assembly_super_drawer_map.set("id_assembly_micro_drawer1_of_super_drawer", {
		important: true,
		fn : "fetch_micro_drawers_from_super_drawer",
		check : function(val)
		{
			let record = new Object();
            
			if ( val.records.indexOf( $("#id_assembly_micro_drawer1_of_super_drawer").val() ) != -1 )
            {
                record.responce = false;
                record.warn = warnings["inDB"];

                return record;
            }

            else 
            {
                
                let status = check_same_inputs_in_micro_drawer();
                
                if( status == false )
                {
                    assembly_super_drawer_control.handle_form();
                } 

                else 
                { 
                    waitMicroDrawerIdCheck($("#id_assembly_micro_drawer1_of_super_drawer").val()).then(
                        function (value) 
                        {
                            console.log(value);
                            if (value) 
                            {
                                $("#assembly_micro_drawer1_id_info_of_super_drawer").text("");
                                $("#id_assembly_micro_drawer1_of_super_drawer").removeClass("not_ready_border_col");
                                $("#id_assembly_micro_drawer1_of_super_drawer").addClass("ready_to_insert");
                                assembly_super_drawer_control.handle_form();
                
                            } else 
                            {
                                $("#assembly_micro_drawer1_id_info_of_super_drawer").text(warnings["id_notreachable"]);
                                $("#id_assembly_micro_drawer1_of_super_drawer").addClass("not_ready_border_col");
                                $("#id_assembly_micro_drawer1_of_super_drawer").removeClass("ready_to_insert");
                                assembly_super_drawer_control.handle_form();
                            }
                        }
                    );
                }

                record.responce = true;
                record.warn = "";

                return record;
            }
		}
	});


    assembly_super_drawer_map.set("id_assembly_micro_drawer2_of_super_drawer", {
		important: true,
		fn : "fetch_micro_drawers_from_super_drawer",
		check : function(val)
		{
			let record = new Object();
            
			if ( val.records.indexOf( $("#id_assembly_micro_drawer2_of_super_drawer").val() ) != -1 )
            {
                record.responce = false;
                record.warn = warnings["inDB"];

                return record;
            }

            else 
            {
                
                let status = check_same_inputs_in_micro_drawer();
                
                if( status == false )
                {
                    assembly_super_drawer_control.handle_form();
                } 

                else 
                { 
                    waitMicroDrawerIdCheck($("#id_assembly_micro_drawer2_of_super_drawer").val()).then(
                        function (value) 
                        {
                            if (value) 
                            {
                                $("#assembly_micro_drawer2_id_info_of_super_drawer").text("");
                                $("#id_assembly_micro_drawer2_of_super_drawer").removeClass("not_ready_border_col");
                                $("#id_assembly_micro_drawer2_of_super_drawer").addClass("ready_to_insert");
                                assembly_super_drawer_control.handle_form();
                
                            } else 
                            {
                                $("#assembly_micro_drawer2_id_info_of_super_drawer").text(warnings["id_notreachable"]);
                                $("#id_assembly_micro_drawer2_of_super_drawer").addClass("not_ready_border_col");
                                $("#id_assembly_micro_drawer2_of_super_drawer").removeClass("ready_to_insert");
                                assembly_super_drawer_control.handle_form();
                            }
                        }
                    );
                }

                record.responce = true;
                record.warn = "";

                return record;
            }
		}
	});

    // edit

let edit_assembly_super_drawer_map = new Map();
    edit_assembly_super_drawer_map.set("id_edit_assembly_super_drawer", {
		important: true,
		fn : "check_assembly_of_super_drawer_id",
		check : function(val)
		{
			let record = new Object();
			
			if( val == false) 
			{
                record.responce = true;
				record.warn = "";
				
				return record;
				
			}
            else
			{
				record.responce = false;
				record.warn = warnings["assembled"];
				
				return record;
			} 
		}
	});

    edit_assembly_super_drawer_map.set("id_edit_assembly_mini_drawer1_of_super_drawer", {
		important: true,
		fn : "fetch_mini_drawers_from_super_drawer",
		check : function(val)
		{
			let record = new Object();
            
			if ( val.records.indexOf( $("#id_edit_assembly_mini_drawer1_of_super_drawer").val() ) != -1 )
            {
                record.responce = false;
                record.warn = warnings["inDB"];

                return record;
            }

            else 
            {
                
                let status = check_same_inputs_in_edit_mini_drawer();
                
                if( status == false )
                {
                    assembly_super_drawer_control.handle_edit_form();
                } 

                else 
                { 
                    waitMiniDrawerIdCheck($("#id_edit_assembly_mini_drawer1_of_super_drawer").val()).then(
                        function (value) 
                        {
                            if (value) 
                            {
                                $("#edit_assembly_mini_drawer1_id_info_of_super_drawer").text("");
                                $("#id_edit_assembly_mini_drawer1_of_super_drawer").removeClass("not_ready_border_col");
                                $("#id_edit_assembly_mini_drawer1_of_super_drawer").addClass("ready_to_insert");
                                assembly_super_drawer_control.handle_edit_form();
                
                            } else 
                            {
                                $("#edit_assembly_mini_drawer1_id_info_of_super_drawer").text(warnings["id_notreachable"]);
                                $("#id_edit_assembly_mini_drawer1_of_super_drawer").addClass("not_ready_border_col");
                                $("#id_edit_assembly_mini_drawer1_of_super_drawer").removeClass("ready_to_insert");
                                assembly_super_drawer_control.handle_edit_form();
                            }
                        }
                    );
                }

                record.responce = true;
                record.warn = "";

                return record;
            }
		}
	});

    edit_assembly_super_drawer_map.set("id_edit_assembly_mini_drawer2_of_super_drawer", {
		important: true,
		fn : "fetch_mini_drawers_from_super_drawer",
		check : function(val)
		{
			let record = new Object();
            
			if ( val.records.indexOf( $("#id_edit_assembly_mini_drawer2_of_super_drawer").val() ) != -1 )
            {
                record.responce = false;
                record.warn = warnings["inDB"];

                return record;
            }

            else 
            {
                
                let status = check_same_inputs_in_edit_mini_drawer();
                
                if( status == false )
                {
                    assembly_super_drawer_control.handle_edit_form();
                } 

                else 
                { 
                    waitMiniDrawerIdCheck($("#id_edit_assembly_mini_drawer2_of_super_drawer").val()).then(
                        function (value) 
                        {
                            if (value) 
                            {
                                $("#edit_assembly_mini_drawer2_id_info_of_super_drawer").text("");
                                $("#id_edit_assembly_mini_drawer2_of_super_drawer").removeClass("not_ready_border_col");
                                $("#id_edit_assembly_mini_drawer2_of_super_drawer").addClass("ready_to_insert");
                                assembly_super_drawer_control.handle_edit_form();
                
                            } else 
                            {
                                $("#edit_assembly_mini_drawer2_id_info_of_super_drawer").text(warnings["id_notreachable"]);
                                $("#id_edit_assembly_mini_drawer2_of_super_drawer").addClass("not_ready_border_col");
                                $("#id_edit_assembly_mini_drawer2_of_super_drawer").removeClass("ready_to_insert");
                                assembly_super_drawer_control.handle_edit_form();
                            }
                        }
                    );
                }

                record.responce = true;
                record.warn = "";

                return record;
            }
		}
	});
    edit_assembly_super_drawer_map.set("id_edit_assembly_mini_drawer3_of_super_drawer", {
		important: true,
		fn : "fetch_mini_drawers_from_super_drawer",
		check : function(val)
		{
			let record = new Object();
            
			if ( val.records.indexOf( $("#id_edit_assembly_mini_drawer3_of_super_drawer").val() ) != -1 )
            {
                record.responce = false;
                record.warn = warnings["inDB"];

                return record;
            }

            else 
            {
                
                let status = check_same_inputs_in_edit_mini_drawer();
                
                if( status == false )
                {
                    assembly_super_drawer_control.handle_edit_form();
                } 

                else 
                { 
                    waitMiniDrawerIdCheck($("#id_edit_assembly_mini_drawer3_of_super_drawer").val()).then(
                        function (value) 
                        {
                            if (value) 
                            {
                                $("#edit_assembly_mini_drawer3_id_info_of_super_drawer").text("");
                                $("#id_edit_assembly_mini_drawer3_of_super_drawer").removeClass("not_ready_border_col");
                                $("#id_edit_assembly_mini_drawer3_of_super_drawer").addClass("ready_to_insert");
                                assembly_super_drawer_control.handle_edit_form();
                
                            } else 
                            {
                                $("#edit_assembly_mini_drawer3_id_info_of_super_drawer").text(warnings["id_notreachable"]);
                                $("#id_edit_assembly_mini_drawer3_of_super_drawer").addClass("not_ready_border_col");
                                $("#id_edit_assembly_mini_drawer3_of_super_drawer").removeClass("ready_to_insert");
                                assembly_super_drawer_control.handle_edit_form();
                            }
                        }
                    );
                }

                record.responce = true;
                record.warn = "";

                return record;
            }
		}
	});
    edit_assembly_super_drawer_map.set("id_edit_assembly_mini_drawer4_of_super_drawer", {
		important: true,
		fn : "fetch_mini_drawers_from_super_drawer",
		check : function(val)
		{
			let record = new Object();
            
			if ( val.records.indexOf( $("#id_edit_assembly_mini_drawer4_of_super_drawer").val() ) != -1 )
            {
                record.responce = false;
                record.warn = warnings["inDB"];

                return record;
            }

            else 
            {
                
                let status = check_same_inputs_in_edit_mini_drawer();
                
                if( status == false )
                {
                    assembly_super_drawer_control.handle_edit_form();
                } 

                else 
                { 
                    waitMiniDrawerIdCheck($("#id_edit_assembly_mini_drawer4_of_super_drawer").val()).then(
                        function (value) 
                        {
                            if (value) 
                            {
                                $("#edit_assembly_mini_drawer4_id_info_of_super_drawer").text("");
                                $("#id_edit_assembly_mini_drawer4_of_super_drawer").removeClass("not_ready_border_col");
                                $("#id_edit_assembly_mini_drawer4_of_super_drawer").addClass("ready_to_insert");
                                assembly_super_drawer_control.handle_edit_form();
                
                            } else 
                            {
                                $("#edit_assembly_mini_drawer4_id_info_of_super_drawer").text(warnings["id_notreachable"]);
                                $("#id_edit_assembly_mini_drawer4_of_super_drawer").addClass("not_ready_border_col");
                                $("#id_edit_assembly_mini_drawer4_of_super_drawer").removeClass("ready_to_insert");
                                assembly_super_drawer_control.handle_edit_form();
                            }
                        }
                    );
                }

                record.responce = true;
                record.warn = "";

                return record;
            }
		}
	});

    edit_assembly_super_drawer_map.set("id_edit_assembly_micro_drawer1_of_super_drawer", {
		important: true,
		fn : "fetch_micro_drawers_from_super_drawer",
		check : function(val)
		{
			let record = new Object();
            
			if ( val.records.indexOf( $("#id_edit_assembly_micro_drawer1_of_super_drawer").val() ) != -1 )
            {
                record.responce = false;
                record.warn = warnings["inDB"];

                return record;
            }

            else 
            {
                
                let status = check_same_inputs_in_edit_micro_drawer();
                
                if( status == false )
                {
                    assembly_super_drawer_control.handle_form();
                } 

                else 
                { 
                    waitMicroDrawerIdCheck($("#id_edit_assembly_micro_drawer1_of_super_drawer").val()).then(
                        function (value) 
                        {
                            console.log(value);
                            if (value) 
                            {
                                $("#edit_assembly_micro_drawer1_id_info_of_super_drawer").text("");
                                $("#id_edit_assembly_micro_drawer1_of_super_drawer").removeClass("not_ready_border_col");
                                $("#id_edit_assembly_micro_drawer1_of_super_drawer").addClass("ready_to_insert");
                                assembly_super_drawer_control.handle_edit_form();
                
                            } else 
                            {
                                $("#edit_assembly_micro_drawer1_id_info_of_super_drawer").text(warnings["id_notreachable"]);
                                $("#id_edit_assembly_micro_drawer1_of_super_drawer").addClass("not_ready_border_col");
                                $("#id_edit_assembly_micro_drawer1_of_super_drawer").removeClass("ready_to_insert");
                                assembly_super_drawer_control.handle_edit_form();
                            }
                        }
                    );
                }

                record.responce = true;
                record.warn = "";

                return record;
            }
		}
	});

    edit_assembly_super_drawer_map.set("id_edit_assembly_micro_drawer2_of_super_drawer", {
		important: true,
		fn : "fetch_micro_drawers_from_super_drawer",
		check : function(val)
		{
			let record = new Object();
            
			if ( val.records.indexOf( $("#id_edit_assembly_micro_drawer2_of_super_drawer").val() ) != -1 )
            {
                record.responce = false;
                record.warn = warnings["inDB"];

                return record;
            }

            else 
            {
                
                let status = check_same_inputs_in_edit_micro_drawer();
                
                if( status == false )
                {
                    assembly_super_drawer_control.handle_form();
                } 

                else 
                { 
                    waitMicroDrawerIdCheck($("#id_edit_assembly_micro_drawer2_of_super_drawer").val()).then(
                        function (value) 
                        {
                            console.log(value);
                            if (value) 
                            {
                                $("#edit_assembly_micro_drawer2_id_info_of_super_drawer").text("");
                                $("#id_edit_assembly_micro_drawer2_of_super_drawer").removeClass("not_ready_border_col");
                                $("#id_edit_assembly_micro_drawer2_of_super_drawer").addClass("ready_to_insert");
                                assembly_super_drawer_control.handle_edit_form();
                
                            } else 
                            {
                                $("#edit_assembly_micro_drawer2_id_info_of_super_drawer").text(warnings["id_notreachable"]);
                                $("#id_edit_assembly_micro_drawer2_of_super_drawer").addClass("not_ready_border_col");
                                $("#id_edit_assembly_micro_drawer2_of_super_drawer").removeClass("ready_to_insert");
                                assembly_super_drawer_control.handle_edit_form();
                            }
                        }
                    );
                }

                record.responce = true;
                record.warn = "";

                return record;
            }
		}
	});

/*

    ######   #######  ##     ## ########   #######  ##    ## ######## ##    ## ########  ######  
    ##    ## ##     ## ###   ### ##     ## ##     ## ###   ## ##       ###   ##    ##    ##    ## 
    ##       ##     ## #### #### ##     ## ##     ## ####  ## ##       ####  ##    ##    ##       
    ##       ##     ## ## ### ## ########  ##     ## ## ## ## ######   ## ## ##    ##     ######  
    ##       ##     ## ##     ## ##        ##     ## ##  #### ##       ##  ####    ##          ## 
    ##    ## ##     ## ##     ## ##        ##     ## ##   ### ##       ##   ###    ##    ##    ## 
    ######   #######  ##     ## ##         #######  ##    ## ######## ##    ##    ##     ######  

*/

let assembly_super_drawer = new Object();
    assembly_super_drawer.target = "assembly_super_drawer_modal_target";
    assembly_super_drawer.formID = "assembly_super_drawer_form";
    assembly_super_drawer.standart_inputs = assembly_super_drawer_map;
    assembly_super_drawer.comment = "super_drawer_remark";
    assembly_super_drawer.test_results = undefined;
    assembly_super_drawer.submit_class = {
            cls: "add_super_drawer_form_submit",
            fn: "assemble_super_drawer"
        };

    assembly_super_drawer.edit_target = "edit_super_drawer_form_submit";
    assembly_super_drawer.edit_formID = "edit_assembly_super_drawer_form";
    assembly_super_drawer.edit_standart_inputs = edit_assembly_super_drawer_map;
    assembly_super_drawer.edit_comment = "edit_super_drawer_remark";
    assembly_super_drawer.edit_test_results = undefined
    assembly_super_drawer.edit_cancel_class = "edit_super_drawer_form_cancel";
    assembly_super_drawer.edit_close_class = "close-update-super-drawer-btn";

    assembly_super_drawer.edit_submit_class = {
        cls: "edit_super_drawer_form_submit",
        fn: "update_assemble_super_drawer"
    };

let assembly_super_drawer_control = new Object_control(assembly_super_drawer);
    assembly_super_drawer_control.assaignEvents();


function update_assemble_super_drawer(obj)
{
    let id_super_drawer_frame = $("#origin_value_of_super_drawer").val();
    let id_super_drawer = obj["id_edit_assembly_super_drawer"]
    let id_mini_drawer1 = obj["id_edit_assembly_mini_drawer1_of_super_drawer"]
    let id_mini_drawer2 = obj["id_edit_assembly_mini_drawer2_of_super_drawer"]
    let id_mini_drawer3 = obj["id_edit_assembly_mini_drawer3_of_super_drawer"]
    let id_mini_drawer4 = obj["id_edit_assembly_mini_drawer4_of_super_drawer"]
    let id_micro_drawer1 = obj["id_edit_assembly_micro_drawer1_of_super_drawer"];
    let id_micro_drawer2 = obj["id_edit_assembly_micro_drawer2_of_super_drawer"];
    let status = $("#edit_assembly_super_drawer_status").val();
    let current_location = $("#edit_assembly_super_drawer_current_location").val();
    let remark = obj["remark"];

    $.ajax({
        url: "super_drawer/update",
        type: "post",
        dataType: "json",
        data: {
            id_super_drawer_frame: id_super_drawer_frame,
            id_super_drawer: id_super_drawer,
            mini_drawer_id1: id_mini_drawer1,
            mini_drawer_id2: id_mini_drawer2,
            mini_drawer_id3: id_mini_drawer3,
            mini_drawer_id4: id_mini_drawer4,
            micro_drawer_id1: id_micro_drawer1,
            micro_drawer_id2: id_micro_drawer2,
            component_status: status,
            current_location: current_location,
            remark: remark
        },
        success: function (data) {
            if (data.responce == "success") {
                $('#super_drawer_table').DataTable().destroy();
                $('#edit_assembly_super_drawer').modal('hide');
                fetchSuperDrawer();
                toastr["success"](data.message);
            } else {
                toastr["error"](data.message);
            }
        },
        error: function ( data ){
            console.log(data);
        }
    })
}
/*

	######## #### ##       ######## ######## ########  #### ##    ##  ######   
	##        ##  ##          ##    ##       ##     ##  ##  ###   ## ##    ##  
	##        ##  ##          ##    ##       ##     ##  ##  ####  ## ##        
	######    ##  ##          ##    ######   ########   ##  ## ## ## ##   #### 
	##        ##  ##          ##    ##       ##   ##    ##  ##  #### ##    ##  
	##        ##  ##          ##    ##       ##    ##   ##  ##   ### ##    ##  
	##       #### ########    ##    ######## ##     ## #### ##    ##  ######   

*/
$("#filter_new_super_drawer_by_sp_id").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 0 ).search( this.value ).draw();
});

$("#filter_by_mini_drawer_1_of_sp_drawer").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 1 ).search( this.value ).draw();
});

$("#filter_by_mini_drawer_2_of_sp_drawer").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 2 ).search( this.value ).draw();
});

$("#filter_by_mini_drawer_3_of_sp_drawer").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 3 ).search( this.value ).draw();
});

$("#filter_by_mini_drawer_4_of_sp_drawer").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 4 ).search( this.value ).draw();
});

$("#filter_by_micro_drawer_1_of_sp_drawer").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 5 ).search( this.value ).draw();
});

$("#filter_by_micro_drawer_2_of_sp_drawer").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 6 ).search( this.value ).draw();
});

$("#filter_by_component_status_of_sp_drawer").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 8 ).search( this.value ).draw();
});

$("#filter_by_current_location_of_sp_drawer").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 9 ).search( this.value ).draw();
});

$("#filter_by_comment_status_of_sp_drawer").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 10 ).search( this.value ).draw();
});

$("#filter_by_creation_time_status_of_sp_drawer").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 11 ).search( this.value ).draw();
});

$("#filter_by_update_time_of_sp_drawer").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 12 ).search( this.value ).draw();
});

$("#filter_by_created_by_of_sp_drawer").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 13 ).search( this.value ).draw();
});

$("#filter_by_updated_by_of_sp_drawer").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 14 ).search( this.value ).draw();
});






// // Add Super drawer
// $(document).on("click", ".add_super_drawer_form_submit", function (e) {
//     e.preventDefault();

//     if ($(".add_super_drawer_form_submit").hasClass("notReady")) return;

//     let id_super_drawer = $("#id_super_drawer").val();
//     let id_mini_drawer1 = $("#id_assembly_mini_drawer1_of_super_drawer").val();
//     let id_mini_drawer2 = $("#id_assembly_mini_drawer2_of_super_drawer").val();
//     let id_mini_drawer3 = $("#id_assembly_mini_drawer3_of_super_drawer").val();
//     let id_mini_drawer4 = $("#id_assembly_mini_drawer4_of_super_drawer").val();
//     let status = $("#super_drawer_status").val();
//     let current_location = $("#super_drawer_current_location").val();
//     let remark = $("#super_drawer_remark").val();

//     $.ajax({
//         url: "super_drawer/insert",
//         type: "post",
//         dataType: "json",
//         data: {
//             id_super_drawer_frame: id_super_drawer,
//             mini_drawer_id1: id_mini_drawer1,
//             mini_drawer_id2: id_mini_drawer2,
//             mini_drawer_id3: id_mini_drawer3,
//             mini_drawer_id4: id_mini_drawer4,
//             component_status: status,
//             current_location: current_location,
//             remark: remark
//         },
//         success: function (data) 
//         {
//             if (data.responce == "success") {
//                 $('#super_drawer_table').DataTable().destroy();
//                 $('#assembly_super_drawer').modal('hide');
//                 fetchSuperDrawer();
//                 recordsData = undefined;
//                 toastr["success"](data.message);
//             } else {
//                 toastr["error"](data.message);
//             }

//             $("#id_assembly_mini_drawer1_of_super_drawer").removeClass("ready_to_insert"); 
//             $("#id_assembly_mini_drawer2_of_super_drawer").removeClass("ready_to_insert"); 
//             $("#id_assembly_mini_drawer3_of_super_drawer").removeClass("ready_to_insert"); 
//             $("#id_assembly_mini_drawer4_of_super_drawer").removeClass("ready_to_insert");

//         }
//     });

//     $("#assembly_super_drawer_form")[0].reset();

// });

//Fetch infromation of super drawer from database to fill fields in edit window


// // Prevent space use 
// $("#id_edit_assembly_mini_drawer1_of_super_drawer").on('keydown', function (e) 
// {
//     if( e.keyCode == 32 )
//     { 
//         alert("SPACE isn't allowed");
//         e.preventDefault();
//         return;  
//     }
// });
// // Prevent space use 
// $("#id_edit_assembly_mini_drawer2_of_super_drawer").on('keydown', function (e) 
// {
//     if( e.keyCode == 32 )
//     { 
//         alert("SPACE isn't allowed");
//         e.preventDefault();
//         return;  
//     }
// });
// // Prevent space use 
// $("#id_edit_assembly_mini_drawer3_of_super_drawer").on('keydown', function (e) 
// {
//     if( e.keyCode == 32 )
//     { 
//         alert("SPACE isn't allowed");
//         e.preventDefault();
//         return;  
//     }
// });
// // Prevent space use 
// $("#id_edit_assembly_mini_drawer4_of_super_drawer").on('keydown', function (e) 
// {
//     if( e.keyCode == 32 )
//     { 
//         alert("SPACE isn't allowed");
//         e.preventDefault();
//         return;  
//     }
// });

// check if first MINI DRAWER exists
// $("#id_edit_assembly_mini_drawer1_of_super_drawer").on('input', function () 
// {
//     $(".edit_super_drawer_form_submit").addClass("notReady");

//     if( recordsData.indexOf( $("#id_edit_assembly_mini_drawer1_of_super_drawer").val() ) != -1 )
//     {
//         $("#edit_assembly_mini_drawer1_id_info_of_super_drawer").text("Mini Drawer with this ID is already installed");
//         $("#id_edit_assembly_mini_drawer1_of_super_drawer").addClass("not_ready_border_col");
//         $("#id_edit_assembly_mini_drawer1_of_super_drawer").removeClass("ready_to_insert");
//         $(".edit_super_drawer_form_submit").addClass("notReady");   
//         return;
//     }
    
//     let status = checkSameInputs("#edit_assembly_super_drawer_form",':input[name="mini_drawer"]', this.id);
    
//     if( status == false )
//     {
//         $("#edit_assembly_mini_drawer1_id_info_of_super_drawer").text("Mini Drawers with same ID's isn't allowed");
//         $(".edit_super_drawer_form_submit").addClass("notReady");
//         return;    
//     } 

//     waitMiniDrawerIdCheck($("#id_edit_assembly_mini_drawer1_of_super_drawer").val()).then(
//         function (value) 
//         {
//             if (value) 
//             {
//                 $("#edit_assembly_mini_drawer1_id_info_of_super_drawer").text("");
//                 $("#id_edit_assembly_mini_drawer1_of_super_drawer").removeClass("not_ready_border_col");
//                 $("#id_edit_assembly_mini_drawer1_of_super_drawer").addClass("ready_to_insert");
//                 insert_access_in_super_drawer(true);

//             } else 
//             {
//                 $("#edit_assembly_mini_drawer1_id_info_of_super_drawer").text("Mini Drawer with this ID doesn't exist");
//                 $("#id_edit_assembly_mini_drawer1_of_super_drawer").addClass("not_ready_border_col");
//                 $("#id_edit_assembly_mini_drawer1_of_super_drawer").removeClass("ready_to_insert");
//                 $(".edit_super_drawer_form_submit").addClass("notReady");
//             }
//         }
//     );
// });

// // check if second MINI DRAWER exists
// $("#id_edit_assembly_mini_drawer2_of_super_drawer").on('input', function () 
// {
//     $(".edit_super_drawer_form_submit").addClass("notReady");

//     if( recordsData.indexOf( $("#id_edit_assembly_mini_drawer2_of_super_drawer").val() ) != -1 )
//     {
//         $("#edit_assembly_mini_drawer2_id_info_of_super_drawer").text("Mini Drawer with this ID is already installed");
//         $("#id_edit_assembly_mini_drawer2_of_super_drawer").addClass("not_ready_border_col");
//         $("#id_edit_assembly_mini_drawer2_of_super_drawer").removeClass("ready_to_insert");
//         $(".edit_super_drawer_form_submit").addClass("notReady");   
//         return;
//     }
    
//     let status = checkSameInputs("#edit_assembly_super_drawer_form",':input[name="mini_drawer"]', this.id);
    
//     if( status == false )
//     {
//         $("#edit_assembly_mini_drawer2_id_info_of_super_drawer").text("Mini Drawers with same ID's isn't allowed");
//         $(".edit_super_drawer_form_submit").addClass("notReady");
//         return;    
//     } 

//     waitMiniDrawerIdCheck($("#id_edit_assembly_mini_drawer2_of_super_drawer").val()).then(
//         function (value) 
//         {
//             if (value) 
//             {
//                 $("#edit_assembly_mini_drawer2_id_info_of_super_drawer").text("");
//                 $("#id_edit_assembly_mini_drawer2_of_super_drawer").removeClass("not_ready_border_col");
//                 $("#id_edit_assembly_mini_drawer2_of_super_drawer").addClass("ready_to_insert");
//                 insert_access_in_super_drawer(true);

//             } else 
//             {
//                 $("#edit_assembly_mini_drawer2_id_info_of_super_drawer").text("Mini Drawer with this ID doesn't exist");
//                 $("#id_edit_assembly_mini_drawer2_of_super_drawer").addClass("not_ready_border_col");
//                 $("#id_edit_assembly_mini_drawer2_of_super_drawer").removeClass("ready_to_insert");
//                 $(".edit_super_drawer_form_submit").addClass("notReady");
//             }
//         }
//     );
// });

// // check if third MINI DRAWER exists
// $("#id_edit_assembly_mini_drawer3_of_super_drawer").on('input', function () 
// {
//     $(".edit_super_drawer_form_submit").addClass("notReady");

//     if( recordsData.indexOf( $("#id_edit_assembly_mini_drawer3_of_super_drawer").val() ) != -1 )
//     {
//         $("#edit_assembly_mini_drawer3_id_info_of_super_drawer").text("Mini Drawer with this ID is already installed");
//         $("#id_edit_assembly_mini_drawer3_of_super_drawer").addClass("not_ready_border_col");
//         $("#id_edit_assembly_mini_drawer3_of_super_drawer").removeClass("ready_to_insert");
//         $(".edit_super_drawer_form_submit").addClass("notReady");   
//         return;
//     }
    
//     let status = checkSameInputs("#edit_assembly_super_drawer_form",':input[name="mini_drawer"]', this.id);
    
//     if( status == false )
//     {
//         $("#edit_assembly_mini_drawer3_id_info_of_super_drawer").text("Mini Drawers with same ID's isn't allowed");
//         $(".edit_super_drawer_form_submit").addClass("notReady");
//         return;    
//     } 

//     waitMiniDrawerIdCheck($("#id_edit_assembly_mini_drawer3_of_super_drawer").val()).then(
//         function (value) 
//         {
//             if (value) 
//             {
//                 $("#edit_assembly_mini_drawer3_id_info_of_super_drawer").text("");
//                 $("#id_edit_assembly_mini_drawer3_of_super_drawer").removeClass("not_ready_border_col");
//                 $("#id_edit_assembly_mini_drawer3_of_super_drawer").addClass("ready_to_insert");
//                 insert_access_in_super_drawer(true);

//             } else 
//             {
//                 $("#edit_assembly_mini_drawer3_id_info_of_super_drawer").text("Mini Drawer with this ID doesn't exist");
//                 $("#id_edit_assembly_mini_drawer3_of_super_drawer").addClass("not_ready_border_col");
//                 $("#id_edit_assembly_mini_drawer3_of_super_drawer").removeClass("ready_to_insert");
//                 $(".edit_super_drawer_form_submit").addClass("notReady");
//             }
//         }
//     );
// });

// // check if fourth MINI DRAWER exists
// $("#id_edit_assembly_mini_drawer4_of_super_drawer").on('input', function () 
// {
//     $(".edit_super_drawer_form_submit").addClass("notReady");

//     if( recordsData.indexOf( $("#id_edit_assembly_mini_drawer4_of_super_drawer").val() ) != -1 )
//     {
//         $("#edit_assembly_mini_drawer4_id_info_of_super_drawer").text("Mini Drawer with this ID is already installed");
//         $("#id_edit_assembly_mini_drawer4_of_super_drawer").addClass("not_ready_border_col");
//         $("#id_edit_assembly_mini_drawer4_of_super_drawer").removeClass("ready_to_insert");
//         $(".edit_super_drawer_form_submit").addClass("notReady");   
//         return;
//     }
    
//     let status = checkSameInputs("#edit_assembly_super_drawer_form",':input[name="mini_drawer"]', this.id);
    
//     if( status == false )
//     {
//         $("#edit_assembly_mini_drawer4_id_info_of_super_drawer").text("Mini Drawers with same ID's isn't allowed");
//         $(".edit_super_drawer_form_submit").addClass("notReady");
//         return;    
//     } 

//     waitMiniDrawerIdCheck($("#id_edit_assembly_mini_drawer4_of_super_drawer").val()).then(
//         function (value) 
//         {
//             if (value) 
//             {
//                 $("#edit_assembly_mini_drawer4_id_info_of_super_drawer").text("");
//                 $("#id_edit_assembly_mini_drawer4_of_super_drawer").removeClass("not_ready_border_col");
//                 $("#id_edit_assembly_mini_drawer4_of_super_drawer").addClass("ready_to_insert");
//                 insert_access_in_super_drawer(true);

//             } else 
//             {
//                 $("#edit_assembly_mini_drawer4_id_info_of_super_drawer").text("Mini Drawer with this ID doesn't exist");
//                 $("#id_edit_assembly_mini_drawer4_of_super_drawer").addClass("not_ready_border_col");
//                 $("#id_edit_assembly_mini_drawer4_of_super_drawer").removeClass("ready_to_insert");
//                 $(".edit_super_drawer_form_submit").addClass("notReady");
//             }
//         }
//     );
// });


// // UPDATE Super drawer 
// $(document).on("click", "#edit_super_drawer_form_submit", function (e) {
//     e.preventDefault();

//     if( $(".edit_super_drawer_form_submit").hasClass("notReady") ) return; 

//     let id_super_drawer_frame = $("#origin_value_of_super_drawer").val();
//     let id_super_drawer = $("#id_edit_assembly_super_drawer").val();
//     let id_mini_drawer1 = $("#id_edit_assembly_mini_drawer1_of_super_drawer").val();
//     let id_mini_drawer2 = $("#id_edit_assembly_mini_drawer2_of_super_drawer").val();
//     let id_mini_drawer3 = $("#id_edit_assembly_mini_drawer3_of_super_drawer").val();
//     let id_mini_drawer4 = $("#id_edit_assembly_mini_drawer4_of_super_drawer").val();
//     let status = $("#edit_assembly_super_drawer_status").val();
//     let current_location = $("#edit_assembly_super_drawer_current_location").val();
//     let remark = $("#edit_super_drawer_remark").val();

//     if (id_super_drawer == "") {
//         alert('Super Drawer ID is Required');
//         return;
//     } else {
//         $.ajax({
//             url: "super_drawer/update",
//             type: "post",
//             dataType: "json",
//             data: {
//                 id_super_drawer_frame: id_super_drawer,
//                 mini_drawer_id1: id_mini_drawer1,
//                 mini_drawer_id2: id_mini_drawer2,
//                 mini_drawer_id3: id_mini_drawer3,
//                 mini_drawer_id4: id_mini_drawer4,
//                 component_status: status,
//                 current_location: current_location,
//                 remark: remark
//             },
//             success: function (data) {
//                 if (data.responce == "success") {
//                     $('#super_drawer_table').DataTable().destroy();
//                     $('#edit_assembly_super_drawer').modal('hide');
//                     fetchSuperDrawer();
//                     recordsData = undefined;
//                     toastr["success"](data.message);
//                 } else {
//                     toastr["error"](data.message);
//                 }
//             },
//             error: function ( data ){
//                 console.log(data);
//             }
//         })
//     }
// });

// Delete super_drawer 
$(document).on("click", ".delete_super_drawer", function (e) {
    e.preventDefault();

    let del_id = $(this).attr("value");

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-sm btn-success ml-1',
            cancelButton: 'btn btn-sm btn-danger'
        },
        buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Delete!',
        cancelButtonText: 'No, Cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {

            $.ajax({
                url: "super_drawer/delete",
                type: "post",
                dataType: "json",
                data: {
                    del_id: del_id
                },
                success: function (data) 
                {
                    if (data.responce == "success") 
                    {
                        $('#super_drawer_table').DataTable().clear().destroy();
                        fetchSuperDrawer();
                        swalWithBootstrapButtons.fire(
                            'Deleted!',
                            'Record has been deleted.',
                            'success'
                        );
                    } else 
                    {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Record is safe',
                            'error'
                        );
                    }
                },
                error: function (data)
                {
                    console.log( data );
                }
            });

        } else if (
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
                'Cancelled',
                'Record is safe',
                'error'
            )
        }
    })
});

//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions
//////////////////////////////////////////////////////////// Functions






$("#filter_new_super_drawer_by_sp_id").on("input", function(){
    $('#super_drawer_table').DataTable().columns( 0 ).search( this.value ).draw();
});

$("#filter_new_super_drawer_by_mini_drawer_id").on("input", function()
{
    if( $('#super_drawer_table').DataTable().columns( 1 ).data()[0].indexOf(this.value) > -1 )
    {
        $('#super_drawer_table').DataTable().columns( 1 ).search( this.value ).draw();        
    }
    else if( $('#super_drawer_table').DataTable().columns( 2 ).data()[0].indexOf(this.value) > -1 )
    {
        $('#super_drawer_table').DataTable().columns( 2 ).search( this.value ).draw();
    }
    else if( $('#super_drawer_table').DataTable().columns( 3 ).data()[0].indexOf(this.value) > -1 ) 
    {
        $('#super_drawer_table').DataTable().columns( 3 ).search( this.value ).draw();
    }
    else if( $('#super_drawer_table').DataTable().columns( 4 ).data()[0].indexOf(this.value) > -1 ) 
    {
        $('#super_drawer_table').DataTable().columns( 4 ).search( this.value ).draw();
    }else
    {
        $('#super_drawer_table').DataTable().columns([1,2,3,4]).search("").draw();        
    }
});
// download test result of new super drawer
$(document).ready ( function () {
    $(document).on ("click", "#download_new_sp_tr", function () 
    {
        let id = $(this).parent().parent().children().last().find(">:first-child").attr("value");
        $.ajax({
            url: "super_drawer/fetch_test",
            type: "post",
            dataType: "json",
            data: {
                id: id
            },
            success: function (data) 
            {       
                if (data.responce == "success") 
                {
                    let results = data.record.test_results;
                    download_test_results(id,results);
                }
            }
        });    
    });
});












