/*
	######## ##     ## ######## ##    ## ########  ######  
	##       ##     ## ##       ###   ##    ##    ##    ## 
	##       ##     ## ##       ####  ##    ##    ##       
	######   ##     ## ######   ## ## ##    ##     ######  
	##        ##   ##  ##       ##  ####    ##          ## 
	##         ## ##   ##       ##   ###    ##    ##    ## 
	########    ###    ######## ##    ##    ##     ###### 
*/

// show old pmt block records
$(document).on("click", "#show_old_pmt_block_table", function (e) 
{
	e.preventDefault();
	$('#old_pmt_block_table').DataTable().clear().destroy();
    fetchOldPmtBlock();
});

//delete record from old pmt block
$(document).on("click", ".delete_diss_pmt_block", function (e) 
{
    e.preventDefault();
	delete_record(this, "delete_record_from_old_pmt_block" );
});


//Fetch infromation from db to fill fields in edit window
$(document).on("click", ".edit_diss_pmt_block", function (e) 
{	
	e.preventDefault();
	let edit_id = $(this).attr("value");

	$.ajax({
		url: "pmt_block/edit_old_pmt_block",
		type: "post",
		dataType: "json",
		data: {
			edit_id: edit_id
		},
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#edit_disassembly_pmt_block').modal('show');
				$("#edit_disassembly_pmt_block_id").val(data.record.id_pmt_block);
				$("#edit_disassembly_super_drawer_id_of_pmt_block").val(data.record.super_drawer_id)
				$("#edit_disassembly_hv_divider_id_of_pmt_block").val(data.record.id_hv_divider);
				$("#edit_disassembly_3_in_1_board_id_of_pmt_block").val(data.record.board_3_in_1_id);
				$("#edit_disassembly_pmt_id_of_pmt_block").val(data.record.id_pmt);
				$("#edit_comment_of_disassembly_pmt_block").val(data.record.remark);
				$("#edit_disassembly_test_results_of_pmt_block")[0].files[0] = null;

				$("#edit_disassembly_hv_divider_id_of_pmt_block").addClass("ready_to_insert");
				$("#edit_disassembly_hv_divider_id_of_pmt_block").removeClass("not_ready_border_col");
				$("#edit_disassembly_3_in_1_board_id_of_pmt_block").addClass("ready_to_insert");
				$("#edit_disassembly_3_in_1_board_id_of_pmt_block").removeClass("not_ready_border_col");
				$("#edit_disassembly_pmt_id_of_pmt_block").addClass("ready_to_insert");
				$("#edit_disassembly_pmt_id_of_pmt_block").removeClass("not_ready_border_col");		

				$("#edit_old_pmt_block_form_submit").addClass("notReady");
				
				fetchPmtBlockRecords();
			} else {
				toastr["error"](data.message);
	 		}
		}
	});
});


// download test results of old pmt block
$(document).ready( function () 
{
    $(document).on ("click", ".download_test_results_of_old_pmt_block", function () 
    {
        let id = $(this).parent().parent().children().last().find(">:first-child").attr("value");
        
        if ( id == undefined || id == null ) 
        {
            toastr["error"](warnings["unknown_problem"]);
            return;
        }

        $.ajax({
            url: "pmt_block/fetch_test_of_old_pmt_block",
            type: "post",
            dataType: "json",
            data: {
                id: id
            },
            success: function (data) 
            {       
                if (data.responce == "success") 
                {
                    let results = data.record.test_results;
                    
                    if(results == null || results.length < 1 )
                    {
                        toastr["error"](warnings["nodata"]);
                        return;
                    }else{
                        download_test_results(id,results);
                    }
                }else{
                    toastr["error"](warnings["unknown_problem"]);
                }
            },
            error: function()
            {
                toastr["error"](warnings["unknown_problem"]);
            }
        });    
    });
});

/*
	######## ##     ## ##    ##  ######  ######## ####  #######  ##    ##  ######  
	##       ##     ## ###   ## ##    ##    ##     ##  ##     ## ###   ## ##    ## 
	##       ##     ## ####  ## ##          ##     ##  ##     ## ####  ## ##       
	######   ##     ## ## ## ## ##          ##     ##  ##     ## ## ## ##  ######  
	##       ##     ## ##  #### ##          ##     ##  ##     ## ##  ####       ## 
	##       ##     ## ##   ### ##    ##    ##     ##  ##     ## ##   ### ##    ## 
	##        #######  ##    ##  ######     ##    ####  #######  ##    ##  ######  
*/

function fetchOldPmtBlock()
{
	$.ajax({
		url: "pmt_block/fetchOldPmtBlock",
		type: "post",
		dataType: "json",
		success: function (data) 
        {
			if (data.responce == "success") 
            {
				$('#old_pmt_block_table').DataTable({
					"data": data.records,
					"rowId": "id_pmt_block",
					"columns": [
						{ "data": "id_pmt_block" },
						{ "data": "super_drawer_id"},
						{ "data": "id_hv_divider" },
						{ "data": "board_3_in_1_id" },
						{ "data": "id_pmt" },
                        { "data": null, "defaultContent": `<a class="px-1 download_test_result download_test_results_of_old_pmt_block" aria-hidden="true" style='font-weight:bold; color:#007bff !important;'>Download</a>`,"targets": -1},
						{ "data": "remark" },
						{ "data": "created_at" },
						{ "data": "updated_at" },
						{ "data": "created_by" },
						{ "data": "updated_by" },
						{
							"render": function (data, type, row, meta) 
                            {
								var a = `
								<i class="far fa-edit px-1 edit_diss_pmt_block" value="${row.id_pmt_block}" aria-hidden="true"></i>
								<i class="far fa-trash-alt px-1 delete_diss_pmt_block" value="${row.id_pmt_block}" aria-hidden="true"></i>
							`;
								return a;
							}
						}
					]
				});
			} else {
				toastr["error"](data.message);
			}
		},
		error: function () 
        {
			toastr["error"](warnings["unknown_problem"]);
		}
	});
};

function delete_record_from_old_pmt_block( id, window )
{
	$.ajax({
		url: "pmt_block/delete_from_old",
		type: "post",
		dataType: "json",
		data: {
			del_id: id
		},
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#old_pmt_block_table').DataTable().clear().destroy();
				fetchOldPmtBlock();
				
				window.fire(
					'Deleted!',
					'Record has been deleted.',
					'success'
				);

			} else 
			{
				window.fire(
					'Cancelled',
					'Record is safe',
					'error'
				);
			}
		},
		error: function()
		{
			toastr["error"](warnings["unknown_problem"]);
		}
	});
}

function edit_disassembly_of_old_pmt_block()
{
	$.ajax({
		url: "pmt_block/edit_old_pmt_block",
		type: "post",
		dataType: "json",
		data: {
			edit_id: edit_id
		},
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#edit_disassembly_pmt_block').modal('show');
				$("#edit_disassembly_pmt_block_id").val(data.record.id_pmt_block);
				$("#edit_disassembly_super_drawer_id_of_pmt_block").val(data.record.super_drawer_id)
				$("#edit_disassembly_hv_divider_id_of_pmt_block").val(data.record.id_hv_divider);
				$("#edit_disassembly_3_in_1_board_id_of_pmt_block").val(data.record.board_3_in_1_id);
				$("#edit_disassembly_pmt_id_of_pmt_block").val(data.record.id_pmt);
				$("#edit_comment_of_disassembly_pmt_block").val(data.record.remark);

				$("#edit_disassembly_test_results_of_pmt_block").attr("type", "text");
				$("#edit_disassembly_test_results_of_pmt_block").attr("type", "file");

				$("#edit_disassembly_hv_divider_id_of_pmt_block").addClass("ready_to_insert");
				$("#edit_disassembly_hv_divider_id_of_pmt_block").removeClass("not_ready_border_col");

				$("#edit_disassembly_3_in_1_board_id_of_pmt_block").addClass("ready_to_insert");
				$("#edit_disassembly_3_in_1_board_id_of_pmt_block").removeClass("not_ready_border_col");
				
				$("#edit_disassembly_pmt_id_of_pmt_block").addClass("ready_to_insert");
				$("#edit_disassembly_pmt_id_of_pmt_block").removeClass("not_ready_border_col");		

				$("#edit_old_pmt_block_form_submit").addClass("notReady");
				
				fetchPmtBlockRecords();
			} 
			else 
			{
				toastr["error"](data.message);
				}
		}
	});
}
	
//reception_old_pmt_block_id
function add_old_pmt_block_in_reception( obj )
{
	let id = obj["id_old_pmt_block_in_reception"];
	let remark = obj["remark"];

	$.ajax({
		url: "pmt_block/insert_in_old_pmt_block",
		type: "post",
		dataType: "json",
		data: {
			id_pmt_block: id,
			remark: remark
		},
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#old_pmt_block_table').DataTable().destroy();
				$('#add_old_pmt_block_in_reception_modal').modal('hide');
				fetchOldPmtBlock();                

				toastr["success"](data.message);
			
			} else {
				toastr["error"](data.message);
			}
		},
		error: function ()
		{
			toastr["error"](warnings["unknown_problem"]);            
		}
	});	
}

function diss_old_pmt_block( obj )
{
	let pmt_block_id = obj["id_dissasembly_pmt_block"];
	let super_drawer_id = obj["id_super_drawer_of_disassembly_pmt_block"];
	let hv_divider_id = obj["id_dissasembly_hv_divider_of_pmt_block"];
	let board_3_in_1_id = obj["id_dissasembly_3_in_1_board_of_pmt_block"];
	let pmt_id = obj["id_dissasembly_pmt_of_pmt_block"];
	let test_results = obj["test_results"];
	let remark = obj["remark"];

	if( test_results != undefined ) 
	{
		$.ajax({
			url: "pmt_block/update_old_pmt_block",
			type: "post",
			dataType: "json",
			data: {
				id_pmt_block: pmt_block_id,
				super_drawer_id: super_drawer_id,
				id_hv_divider: hv_divider_id,
				board_3_in_1_id: board_3_in_1_id,
				id_pmt: pmt_id,
				test_results: test_results,
				remark: remark
			},
			success: function (data) 
			{
				if (data.responce == "success") 
				{
					$('#old_pmt_block_table').DataTable().destroy();
					$('#dissasembly_pmt_block').modal('hide');
	
					insertInOldHvDivider(hv_divider_id);
					insertIn3in1Board(board_3_in_1_id);
					insertIn3inPMT(pmt_id);
					fetchOldPmtBlock();
	
					toastr["success"](data.message);
				} else {
					toastr["error"](data.message);
				}
			},
			error: function () 
			{
				toastr["error"](warnings["unknown_problem"]);
			}
		});
	}else{
		$.ajax({
			url: "pmt_block/update_old_pmt_block",
			type: "post",
			dataType: "json",
			data: {
				id_pmt_block: pmt_block_id,
				super_drawer_id: super_drawer_id,
				id_hv_divider: hv_divider_id,
				board_3_in_1_id: board_3_in_1_id,
				id_pmt: pmt_id,
				test_results: test_results,
				remark: remark
			},
			success: function (data) 
			{
				if (data.responce == "success") 
				{
					$('#old_pmt_block_table').DataTable().destroy();
					$('#dissasembly_pmt_block').modal('hide');
	
					insertInOldHvDivider(hv_divider_id);
					insertIn3in1Board(board_3_in_1_id);
					insertIn3inPMT(pmt_id);
					fetchOldPmtBlock();
	
					toastr["success"](data.message);
				} else {
					toastr["error"](data.message);
				}
			},
			error: function (data) 
			{
				toastr["error"](warnings["unknown_problem"]);
			}
		});
	}
}
	
function update_disassemble_old_pmt_block ( obj )
{

	let pmt_block_id = $("#edit_disassembly_pmt_block_id").val();
	let super_drawer_id = obj["edit_disassembly_super_drawer_id_of_pmt_block"];
	let hv_divider_id = obj["edit_disassembly_hv_divider_id_of_pmt_block"];
	let board_3_in_1_id = obj["edit_disassembly_3_in_1_board_id_of_pmt_block"];
	let pmt_id = obj["edit_disassembly_pmt_id_of_pmt_block"];
	let test_results = obj["test_results"];
	let remark = obj["remark"];

	if( test_results != undefined ){
		$.ajax({
			url: "pmt_block/update_old_pmt_block",
			type: "post",
			dataType: "json",
			data: {
				id_pmt_block: pmt_block_id,
				super_drawer_id: super_drawer_id,
				id_hv_divider: hv_divider_id,
				board_3_in_1_id: board_3_in_1_id,
				test_results: test_results,
				id_pmt: pmt_id,
				remark: remark
			},	
			success: function (data) 
			{
				if (data.responce == "success") 
				{
					$('#old_pmt_block_table').DataTable().destroy();
					$('#edit_disassembly_pmt_block').modal('hide');
			
					fetchOldPmtBlock();
					toastr["success"](data.message);

				}else 
				{
					toastr["error"](data.message);
				}
			},
			error: function(data) 
			{
				console.log(data);
				toastr["error"](warnings["unknown_problem"]);
			}
		});
	}else{
		$.ajax({
			url: "pmt_block/update_old_pmt_block",
			type: "post",
			dataType: "json",
			data: {
				id_pmt_block: pmt_block_id,
				super_drawer_id: super_drawer_id,
				id_hv_divider: hv_divider_id,
				board_3_in_1_id: board_3_in_1_id,
				test_results: null,
				id_pmt: pmt_id,
				remark: remark
			},	
			success: function (data) 
			{
				if (data.responce == "success") 
				{
					$('#old_pmt_block_table').DataTable().destroy();
					$('#edit_disassembly_pmt_block').modal('hide');
			
					fetchOldPmtBlock();
					toastr["success"](data.message);

				}else
				{
					toastr["error"](data.message);
				}
			},
			error: function() 
			{
				toastr["error"](warnings["unknown_problem"]);
			}
		});
	}
}	
	
function check_pmt( val )
{
	return $.ajax({
		url: "pmt/check",
		type: "post",
		dataType: "json",
		data:{
			id: val
		}
	})
}

function check_board_3_in_1( val )
{
	return $.ajax({
		url: "board_3_in_1/check",
		type: "post",
		dataType: "json",
		data:{
			id: val
		}
	});
}

function check_old_hv_divider( val )
{
	return $.ajax({
		url: "hv_divider/checkOldDivider",
		type: "post",
		dataType: "json",
		data:{
			id: val
		}
	});
}


function check_old_pmt_block( val )
{
	return $.ajax({
		url: "pmt_block/checkOldPMtBlockID",
		type: "post",
		dataType: "json",
		data: {
			id: val
		}
	});
};


function insertInOldHvDivider(id)
{
	$.ajax({
		url: "hv_divider/insert_in_old_hv_divider",
		type: "post",
		dataType: "json",
		data: {
			id_hv_divider_old: id,
		},
		success: function (){},
		error: function (){}
	});
};

function insertIn3in1Board(id)
{
	$.ajax({
		url: "board_3_in_1/insert",
		type: "post",
		dataType: "json",
		data: {
			id_3_in_1: id,
		},
		success: function (){},
		error: function (){}
	});
};

function insertIn3inPMT(id)
{
	$.ajax({
		url: "pmt/insert",
		type: "post",
		dataType: "json",
		data: {
			serial_number: id,
		},
		success: function (){},
		error: function (){}
	});
};
	

/*

##     ##    ###    ########   ######        ##     ##    ###    ########   ######     
###   ###   ## ##   ##     ## ##    ##       ###   ###   ## ##   ##     ## ##    ##    
#### ####  ##   ##  ##     ## ##             #### ####  ##   ##  ##     ## ##          
## ### ## ##     ## ########   ######        ## ### ## ##     ## ########   ######     
##     ## ######### ##              ##       ##     ## ######### ##              ##    
##     ## ##     ## ##        ##    ##       ##     ## ##     ## ##        ##    ##    
##     ## ##     ## ##         ######        ##     ## ##     ## ##         ######       	 

*/
	



// add modal of old pmt block
let rec_old_pmt_block_map = new Map();
	rec_old_pmt_block_map.set("id_old_pmt_block_in_reception", {
		important: true,
		fn : "check_old_pmt_block",
		check : function(val)
		{
			let record = new Object();

			if( val == false || val == null ) 
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;

			}else
			{
				record.responce = false;
				record.warn = warnings["inDB"];
				
				return record;
			} 
		}
	});
	

// disassembly modal of old pmt block
let disassembly_old_pmt_block_map = new Map();
	disassembly_old_pmt_block_map.set("id_dissasembly_pmt_block", {
		important: true,
		fn : "check_old_pmt_block",
		check : function(data)
		{
			let record = new Object();

			if(data == null || data == undefined )
			{
				record.responce = false;
				record.warn = warnings["id_notreachable"];

				return record;
			
			}
			else if( data.id_hv_divider || data.board_3_in_1_id || data.id_pmt 
				){

				record.responce = false;
				record.warn = warnings["disassembled"];
				
				return record;
			
			}else
			{
				record.responce = true;
				record.warn = undefined;

				return record;
			}
		}
	});

	disassembly_old_pmt_block_map.set("id_super_drawer_of_disassembly_pmt_block", {
		important: false,
		fn : undefined,
		check : function()
		{
			let record = new Object();
			record.responce = true;

			return record;
		}
	});

	disassembly_old_pmt_block_map.set("id_dissasembly_hv_divider_of_pmt_block", {
		important: true,
		fn : "check_old_hv_divider",
		check : function(data)
		{
			let record = new Object();

			if( data )
			{
				record.responce = false;
				record.warn = warnings["disassembled"]
				
				return record;
			
			}else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;	
			}
		}
	});

	disassembly_old_pmt_block_map.set("id_dissasembly_3_in_1_board_of_pmt_block", {
		important: true,
		fn : "check_board_3_in_1",
		check : function(data)
		{
			let record = new Object();

			if( data )
			{
				record.responce = false;
				record.warn = warnings["disassembled"]
					
				return record;
			
			}else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;
			}
		}
	});

	disassembly_old_pmt_block_map.set("id_dissasembly_pmt_of_pmt_block", {
		important: true,
		fn : "check_pmt",
		check : function(data)
		{
			let record = new Object();

			if( data )
			{
				record.responce = false;
				record.warn = warnings["disassembled"]
					
				return record;
			
			}else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;
			}
		}
	});

// edit modal of old pmt block
let edit_disassembly_of_pmt_block_map = new Map();
	edit_disassembly_of_pmt_block_map.set("edit_disassembly_super_drawer_id_of_pmt_block", {
		important: false,
		fn : undefined,
	});

	edit_disassembly_of_pmt_block_map.set("edit_disassembly_hv_divider_id_of_pmt_block", {
		important: true,
		fn : "check_old_hv_divider",
		check : function(val)
		{
			let record = new Object();

			if( val ) 
			{
				record.responce = false;
				record.warn = warnings["disassembled"];
				
				return record;
				
			}else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;
			} 
		}
	});

	edit_disassembly_of_pmt_block_map.set("edit_disassembly_3_in_1_board_id_of_pmt_block", {
		important: true,
		fn : "check_board_3_in_1",
		check : function(val)
		{
			let record = new Object();

			if( val ) 
			{
				record.responce = false;
				record.warn = warnings["disassembled"];
				
				return record;

			}else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;
			} 
		}
	});

	edit_disassembly_of_pmt_block_map.set("edit_disassembly_pmt_id_of_pmt_block", {
		important: true,
		fn : "check_pmt",
		check : function(val)
		{
			let record = new Object();

			if( val ) 
			{
				record.responce = false;
				record.warn = warnings["disassembled"];
				
				return record;

			}else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;
			} 
		}
	});

/*

 ######   #######  ##     ## ########   #######  ##    ## ######## ##    ## ########  ######  
##    ## ##     ## ###   ### ##     ## ##     ## ###   ## ##       ###   ##    ##    ##    ## 
##       ##     ## #### #### ##     ## ##     ## ####  ## ##       ####  ##    ##    ##       
##       ##     ## ## ### ## ########  ##     ## ## ## ## ######   ## ## ##    ##     ######  
##       ##     ## ##     ## ##        ##     ## ##  #### ##       ##  ####    ##          ## 
##    ## ##     ## ##     ## ##        ##     ## ##   ### ##       ##   ###    ##    ##    ## 
 ######   #######  ##     ## ##         #######  ##    ## ######## ##    ##    ##     ######  

*/

// disassemble pmt block 
let disassembly_old_pmt_block = new Object();
	disassembly_old_pmt_block.target = "dissasembly_pmt_block_modal_target";
	disassembly_old_pmt_block.formID = "disassembly_pmt_block_form";
	disassembly_old_pmt_block.standart_inputs = disassembly_old_pmt_block_map;
	disassembly_old_pmt_block.comment = "id_dissasembly_pmt_block_comment";
	disassembly_old_pmt_block.test_results = "id_test_results_of_pmt_block";
	disassembly_old_pmt_block.remove_file = "remove_file_of_disassembly_pmt_block";
	disassembly_old_pmt_block.submit_class = {
		cls: "dissasemble_pmt_block_form_submit",
		fn: "diss_old_pmt_block"
	};

	// edit disassemble pmt block
	disassembly_old_pmt_block.edit_target = "edit_old_pmt_block_form_submit";
	disassembly_old_pmt_block.edit_formID = "edit_disassembly_pmt_block_form";
	disassembly_old_pmt_block.edit_standart_inputs = edit_disassembly_of_pmt_block_map;
	disassembly_old_pmt_block.edit_comment = "edit_comment_of_disassembly_pmt_block";
	disassembly_old_pmt_block.edit_test_results = "edit_disassembly_test_results_of_pmt_block";
	
	disassembly_old_pmt_block.edit_submit_class = {
		cls: "edit_old_pmt_block_form_submit",
		fn: "update_disassemble_old_pmt_block"
	};
	disassembly_old_pmt_block.edit_cancel_class = "edit_old_pmt_block_form_cancel";
	disassembly_old_pmt_block.edit_close_class = "close_btn_of_old_pmt_block";

let disassembly_old_pmt_block_component = new Object_control(disassembly_old_pmt_block);
	disassembly_old_pmt_block_component.assaignEvents();

// add in reception old pmt block 
let rec_old_pmt_block = new Object();
	rec_old_pmt_block.target = "old_pmt_block_modal_target";
	rec_old_pmt_block.standart_inputs = rec_old_pmt_block_map;
	rec_old_pmt_block.comment = "remark_for_old_pmt_block_in_reception";
	rec_old_pmt_block.test_results = undefined;
	rec_old_pmt_block.submit_class = {
		cls: "add_old_pmt_block_in_reception_form_submit",
		fn: "add_old_pmt_block_in_reception"
	};
	rec_old_pmt_block.formID = "add_old_pmt_block_in_reception_form";

let reception_old_pmt_block = new Object_control(rec_old_pmt_block);
	reception_old_pmt_block.assaignEvents();




/*
	##     ## ####  ######  ########  #######  ########  ##    ## 
	##     ##  ##  ##    ##    ##    ##     ## ##     ##  ##  ##  
	##     ##  ##  ##          ##    ##     ## ##     ##   ####   
	#########  ##   ######     ##    ##     ## ########     ##    
	##     ##  ##        ##    ##    ##     ## ##   ##      ##    
	##     ##  ##  ##    ##    ##    ##     ## ##    ##     ##    
	##     ## ####  ######     ##     #######  ##     ##    ##    
*/


//Fetch OLD Super drawer modifcation Records
$(document).on("click", "#show_old_pmt_block_modification_history_table", function (e) 
{
    e.preventDefault();
    $('#modification_old_pmt_block_table').DataTable().clear().destroy();
    fetchOldPmtBlock_modification_history();
});

// fetch Old Super Drawer modification history
function fetchOldPmtBlock_modification_history() 
{
    $.ajax({
        url: "pmt_block/fetchHistory",
        type: "post",
        dataType: "json",
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#modification_old_pmt_block_table').DataTable({
                    "data": data.records,
                    "rowId": "id_pmt_block",
                    "columns": [
                        { "data": "id_pmt_block" },
                        { "data": "super_drawer_id" },
                        { "data": "id_hv_divider" },
						{ "data": "board_3_in_1_id" },
						{ "data": "id_pmt" },
                        { "data": null, "defaultContent": `<a class="px-1 download_test_result download_test_results_of_modified_old_pmt_block" aria-hidden="true" style='font-weight:bold; color:#007bff !important;'>Download</a>`,"targets": -1},
                        { "data": "remark" },
                        { "data": "start_date" },
                        { "data": "end_date" },
                        { "data": "created_by" },
                        { "data": "changed_by" },
                        { "data": "deleted_by" },
                        {
                            "render": function (data, type, row, meta) {

                                var a = `
								<i class="far fa-trash-alt px-1 delete_old_pmt_block_hist" value="${row.id_pmt_block_frame}" aria-hidden="true"></i>
							        `;
                                return a;
                            }
                        }
                    ]
                });
            } else {
                toastr["error"](data.message);
            }
        }
    });
};


// download test results of modified old pmt block
$(document).ready( function () 
{
    $(document).on ("click", ".download_test_results_of_modified_old_pmt_block", function () 
    {
        let id = $(this).parent().parent().children().last().find(">:first-child").attr("value");

        if ( id == undefined || id == null ) 
        {
            toastr["error"](warnings["unknown_problem"]);
            return;
        }
        
        $.ajax({
            url: "pmt_block/fetch_test_from_hist",
            type: "post",
            dataType: "json",
            data: {
                id: id
            },
            success: function (data) 
            {       
                if (data.responce == "success") 
                {
                    let results = data.record.test_results;
                    
                    if(results == null || results.length < 1 )
                    {
                        toastr["error"](warnings["nodata"]);
                        return;
                    }else{
                        download_test_results(id,results);
                    }
                }else{
                    toastr["error"](warnings["unknown_problem"]);
                }
            },
            error: function(data)
            {
                toastr["error"](warnings["unknown_problem"]);
            }
        });    
    });
});


//delete from old pmt block modification history
$(document).on("click", ".delete_old_pmt_block_hist", function (e) 
{
    e.preventDefault();
	delete_record(this, "delete_record_from_modification_of_old_pmt_block" );
});

function delete_record_from_modification_of_old_pmt_block( id, window )
{
	$.ajax({
		url: "pmt_block/delete_from_hist",
		type: "post",
		dataType: "json",
		data: {
			del_id: id
		},
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#modification_old_pmt_block_table').DataTable().clear().destroy();
				fetchOldPmtBlock_modification_history();
				
				window.fire(
					'Deleted!',
					'Record has been deleted.',
					'success'
				);

			} else 
			{
				window.fire(
					'Cancelled',
					'Record is safe',
					'error'
				);
			}
		},
		error: function()
		{
			toastr["error"](warnings["unknown_problem"]);
		}
	});
}

