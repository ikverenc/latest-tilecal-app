// ------------------- PMT BLOCK NEW 
// ------------------- PMT BLOCK NEW 
// ------------------- PMT BLOCK NEW 
// ------------------- PMT BLOCK NEW 
// ------------------- PMT BLOCK NEW 
// ------------------- PMT BLOCK NEW 
// ------------------- PMT BLOCK NEW 
// ------------------- PMT BLOCK NEW 
// ------------------- PMT BLOCK NEW 
// ------------------- PMT BLOCK NEW 
// ------------------- PMT BLOCK NEW 
// ------------------- PMT BLOCK NEW 

//////////////////////////// EVENTS
//////////////////////////// EVENTS
//////////////////////////// EVENTS
//////////////////////////// EVENTS
//////////////////////////// EVENTS
//////////////////////////// EVENTS
//////////////////////////// EVENTS
//////////////////////////// EVENTS


// clear data on each call
$(".assembly_pmt_block_target").click(function () 
{
	$("#id_pmt_block_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_pmt_block_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#id_super_drawer_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_super_drawer_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#id_assembly_fenics_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_assembly_fenics_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#id_assembly_hv_divider_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_assembly_hv_divider_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#id_assembly_pmt_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_assembly_pmt_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#assembly_pmt_block_form")[0].reset();
	$(".add_pmt_block_form_submit").addClass("notReady");
	recordsData = undefined;
	fetchPmtBlockRecords();
});

$(".close_assembly_pmy_block_btn").click(function(){
	$("#id_pmt_block_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_pmt_block_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#id_super_drawer_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_super_drawer_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#id_assembly_fenics_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_assembly_fenics_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#id_assembly_hv_divider_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_assembly_hv_divider_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#id_assembly_pmt_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_assembly_pmt_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#assembly_pmt_block_form")[0].reset();
	$(".add_pmt_block_form_submit").addClass("notReady");
	recordsData = undefined;
});

$(".add_pmt_block_form_cancel").click(function(){
	$("#id_pmt_block_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_pmt_block_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#id_super_drawer_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_super_drawer_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#id_assembly_fenics_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_assembly_fenics_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#id_assembly_hv_divider_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_assembly_hv_divider_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#id_assembly_pmt_of_assembly_pmt_block").removeClass("not_ready_border_col");
	$("#id_assembly_pmt_of_assembly_pmt_block").removeClass("ready_to_insert");
	$("#assembly_pmt_block_form")[0].reset();
	$(".add_pmt_block_form_submit").addClass("notReady");
	recordsData = undefined;
});

$("#id_pmt_block_of_assembly_pmt_block").on('keydown', function (e) 
{
    if( e.keyCode == 32 )
    { 
        alert("SPACE isn't allowed");
        e.preventDefault();
        return;  
    }
});

$("#id_super_drawer_of_assembly_pmt_block").on('keydown', function (e) 
{
    if( e.keyCode == 32 )
    { 
        alert("SPACE isn't allowed");
        e.preventDefault();
        return;  
    }
});
$("#id_assembly_fenics_of_assembly_pmt_block").on('keydown', function (e) 
{
    if( e.keyCode == 32 )
    { 
        alert("SPACE isn't allowed");
        e.preventDefault();
        return;  
    }
});


$("#id_assembly_hv_divider_of_assembly_pmt_block").on('keydown', function (e) 
{
    if( e.keyCode == 32 )
    { 
        alert("SPACE isn't allowed");
        e.preventDefault();
        return;  
    }
});

$("#id_assembly_pmt_of_assembly_pmt_block").on('keydown', function (e) 
{
    if( e.keyCode == 32 )
    { 
        alert("SPACE isn't allowed");
        e.preventDefault();
        return;  
    }
});

$("#id_mini_drawer_of_assembly_pmt_block").on("keydown", function(e){
	if( e.keyCode == 32 )
	{
		alert("SPACE isn't allowed");
		e.preventDefault();
		return;
	}
});

$("#id_micro_drawer_of_assembly_pmt_block").on("keydown", function(e){
	if( e.keyCode == 32 )
	{
		alert("SPACE isn't allowed");
		e.preventDefault();
		return;
	}
});


// fetch pmt block
$(document).on("click", "#show_pmt_block_table_of_micro_drawer", function (e) 
{
	e.preventDefault();
	$('#pmt_block_table').DataTable().clear().destroy();
	fetchPmtBlock();
});

// fetch pmt block
$(document).on("click", "#show_pmt_block_table", function (e) 
{
	e.preventDefault();
	$('#pmt_block_table').DataTable().clear().destroy();
	fetchPmtBlock();
});


// check pmt block ID if exists
$("#id_pmt_block_of_assembly_pmt_block").on('input', function () {

	if( this.value == "" )
	{
		$("#assembly_pmt_block_id_info").text("Please fill this field");
		$("#id_pmt_block_of_assembly_pmt_block").addClass("not_ready_border_col");
		$("#id_pmt_block_of_assembly_pmt_block").removeClass("ready_to_insert");
		$(".add_pmt_block_form_submit").addClass("notReady");
		return;	
	}

	waitPmtBlockIdCheck("id_pmt_block_of_assembly_pmt_block").then(
		function (value) {
			if (value == false) {

				$("#assembly_pmt_block_id_info").text("");
				$("#id_pmt_block_of_assembly_pmt_block").removeClass("not_ready_border_col");
				$("#id_pmt_block_of_assembly_pmt_block").addClass("ready_to_insert");
				checkAssemblyPmtBlockForm();

			} else {
				$("#assembly_pmt_block_id_info").text("Pmt Block with this ID already exist");
				$("#id_pmt_block_of_assembly_pmt_block").addClass("not_ready_border_col");
				$("#id_pmt_block_of_assembly_pmt_block").removeClass("ready_to_insert");
				$(".add_pmt_block_form_submit").addClass("notReady");
			}
		}
	);
});

// check super drawer ID if exists
$("#id_super_drawer_of_assembly_pmt_block").on('input', function () {

	if( this.value == "" )
	{
		$("#assembly_super_drawer_id_info_of_assembly_pmt_block").text("Please fill this field");
		$("#id_super_drawer_of_assembly_pmt_block").addClass("not_ready_border_col");
		$("#id_super_drawer_of_assembly_pmt_block").removeClass("ready_to_insert");		
		$(".add_pmt_block_form_submit").addClass("notReady");
		return;
	}

	waitSuperDrawerIdCheck("id_super_drawer_of_assembly_pmt_block").then(
		function (value) 
		{
			if (value == true) 
			{
				$("#assembly_super_drawer_id_info_of_assembly_pmt_block").text("");
				$("#id_super_drawer_of_assembly_pmt_block").removeClass("not_ready_border_col");
				$("#id_super_drawer_of_assembly_pmt_block").addClass("ready_to_insert");
				checkAssemblyPmtBlockForm();

			} else 
			{
				$("#assembly_super_drawer_id_info_of_assembly_pmt_block").text("Super Drawer with this ID doesn't exists");
				$("#id_super_drawer_of_assembly_pmt_block").addClass("not_ready_border_col");
				$("#id_super_drawer_of_assembly_pmt_block").removeClass("ready_to_insert");
				$(".add_pmt_block_form_submit").addClass("notReady");
			}
		}
	);
});

// check fenics ID if exists
$("#id_assembly_fenics_of_assembly_pmt_block").on('input', function () {

	if( this.value == "" )
	{
		$("#assembly_fenics_id_info_of_assembly_pmt_block").text("Please fill this field");
		$("#id_assembly_fenics_of_assembly_pmt_block").addClass("not_ready_border_col");
		$("#id_assembly_fenics_of_assembly_pmt_block").removeClass("ready_to_insert");	
		$(".add_pmt_block_form_submit").addClass("notReady");
		return;
	}

	if( recordsData[0].indexOf(this.value) != -1  ){
		$("#assembly_fenics_id_info_of_assembly_pmt_block").text("Fenics with this ID is already assembled");
		$("#id_assembly_fenics_of_assembly_pmt_block").addClass("not_ready_border_col");
		$("#id_assembly_fenics_of_assembly_pmt_block").removeClass("ready_to_insert");	
		$(".add_pmt_block_form_submit").addClass("notReady");
		return;
	}

	waitFenicsIdCheck("id_assembly_fenics_of_assembly_pmt_block").then(
		function (value) 
		{
			if (value == true) 
			{
				$("#assembly_fenics_id_info_of_assembly_pmt_block").text("");
				$("#id_assembly_fenics_of_assembly_pmt_block").removeClass("not_ready_border_col");
				$("#id_assembly_fenics_of_assembly_pmt_block").addClass("ready_to_insert");
				checkAssemblyPmtBlockForm();

			} else 
			{
				$("#assembly_fenics_id_info_of_assembly_pmt_block").text("Fenics with this ID doesn't exists");
				$("#id_assembly_fenics_of_assembly_pmt_block").addClass("not_ready_border_col");
				$("#id_assembly_fenics_of_assembly_pmt_block").removeClass("ready_to_insert");
				$(".add_pmt_block_form_submit").addClass("notReady");
			}
		}
	);

});

// check hv divider ID if exists
$("#id_assembly_hv_divider_of_assembly_pmt_block").on('input', function () {

	if( this.value == "" )
	{
		$("#assembly_hv_divider_id_info_of_assembly_pmt_block").text("Please fill this field");
		$("#id_assembly_hv_divider_of_assembly_pmt_block").addClass("not_ready_border_col");
		$("#id_assembly_hv_divider_of_assembly_pmt_block").removeClass("ready_to_insert");
		$(".add_pmt_block_form_submit").addClass("notReady");
		return;
	}

	if( recordsData[1].indexOf(this.value) != -1  ){
		$("#assembly_hv_divider_id_info_of_assembly_pmt_block").text("HV Divider with this ID is already assembled");
		$("#id_assembly_hv_divider_of_assembly_pmt_block").addClass("not_ready_border_col");
		$("#id_assembly_hv_divider_of_assembly_pmt_block").removeClass("ready_to_insert");
		$(".add_pmt_block_form_submit").addClass("notReady");
		return;
	}

	waitHvDividerIdCheck("id_assembly_hv_divider_of_assembly_pmt_block").then(
		function (value) 
		{
			if (value == true) 
			{
				$("#assembly_hv_divider_id_info_of_assembly_pmt_block").text("");
				$("#id_assembly_hv_divider_of_assembly_pmt_block").removeClass("not_ready_border_col");
				$("#id_assembly_hv_divider_of_assembly_pmt_block").addClass("ready_to_insert");
				checkAssemblyPmtBlockForm();
			} else 
			{
				$("#assembly_hv_divider_id_info_of_assembly_pmt_block").text("HV Divider with this ID doesn't exists");
				$("#id_assembly_hv_divider_of_assembly_pmt_block").addClass("not_ready_border_col");
				$("#id_assembly_hv_divider_of_assembly_pmt_block").removeClass("ready_to_insert");
				$(".add_pmt_block_form_submit").addClass("notReady");
			}
		}
	);
});

// check PMT ID if exists
$("#id_assembly_pmt_of_assembly_pmt_block").on('input', function () {

	if( this.value == "" )
	{
		$("#assembly_pmt_id_info_of_assembly_pmt_block").text("Please fill this field");
		$("#id_assembly_pmt_of_assembly_pmt_block").addClass("not_ready_border_col");
		$("#id_assembly_pmt_of_assembly_pmt_block").removeClass("ready_to_insert");
		$(".add_pmt_block_form_submit").addClass("notReady");
		return;
	}
	
	if( recordsData[2].indexOf(this.value) != -1 )
	{
		$("#assembly_pmt_id_info_of_assembly_pmt_block").text("PMT with this ID is already assembled");
		$("#id_assembly_pmt_of_assembly_pmt_block").addClass("not_ready_border_col");
		$("#id_assembly_pmt_of_assembly_pmt_block").removeClass("ready_to_insert");
		$(".add_pmt_block_form_submit").addClass("notReady");
		return;
	}

	waitPmtIdCheck("id_assembly_pmt_of_assembly_pmt_block").then(
		function (value) 
		{
			if (value == true) 
			{
				$("#assembly_pmt_id_info_of_assembly_pmt_block").text("");
				$("#id_assembly_pmt_of_assembly_pmt_block").removeClass("not_ready_border_col");
				$("#id_assembly_pmt_of_assembly_pmt_block").addClass("ready_to_insert");
				checkAssemblyPmtBlockForm();

			} else 
			{
				$("#assembly_pmt_id_info_of_assembly_pmt_block").text("PMT with this ID doesn't exists");
				$("#id_assembly_pmt_of_assembly_pmt_block").addClass("not_ready_border_col");
				$("#id_assembly_pmt_of_assembly_pmt_block").removeClass("ready_to_insert");
				$(".add_pmt_block_form_submit").addClass("notReady");
			}
		}
	);
});

// Add Pmt Block
$(document).on("click", ".add_pmt_block_form_submit", function (e) {
	e.preventDefault();

	if ($(".add_pmt_block_form_submit").hasClass("notReady")) return;

	let id_pmt_block = $("#id_pmt_block_of_assembly_pmt_block").val();
	let id_super_drawer = $("#id_super_drawer_of_assembly_pmt_block").val();
	let id_mini_drawer = $("#id_mini_drawer_of_assembly_pmt_block").val();
	let id_micro_drawer = $("#id_micro_drawer_of_assembly_pmt_block").val();
	let id_fenics = $("#id_assembly_fenics_of_assembly_pmt_block").val();
	let id_hv_divider = $("#id_assembly_hv_divider_of_assembly_pmt_block").val();
	let id_pmt = $("#id_assembly_pmt_of_assembly_pmt_block").val()
	let id_test_results = $("#id_assembly_test_results_of_assembly_pmt_block").val();
	let curr_locatipon = $("#assembly_pmt_block_current_location").val();
	let comments = $("#assembly_pmt_block_remark").val();

	$.ajax({
		url: "pmt_block/insert",
		type: "post",
		dataType: "json",
		data: {
			id_pmt_block: id_pmt_block,
			id_super_drawer: id_super_drawer,
			id_mini_drawer: id_mini_drawer,
			id_micro_drawer: id_micro_drawer,
			id_fenics: id_fenics,
			id_hv_divider: id_hv_divider,
			id_pmt: id_pmt,
			test_results: id_test_results,
			current_location: curr_locatipon,
			remark: comments
		},
		success: function (data) {
			if (data.responce == "success") {
				$('#pmt_block_table').DataTable().destroy();
				$('#assembly_pmt_block').modal('hide');
				recordsData = undefined;
				fetchPmtBlock();
				toastr["success"](data.message);
			} else {
				toastr["error"](data.message);
			}
		},
		error: function (data) {
			console.log(data.responseText);
		}
	});
	$("#assembly_pmt_block_form")[0].reset();
});

// delete PMT block
$(document).on("click", "#delete_pmt_block", function (e) {
	e.preventDefault();
	let del_id = $(this).attr("value");

	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
			confirmButton: 'btn btn-sm btn-success ml-1',
			cancelButton: 'btn btn-sm btn-danger'
		},
		buttonsStyling: false
	});

	swalWithBootstrapButtons.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Yes, Delete!',
		cancelButtonText: 'No, Cancel!',
		reverseButtons: true
	}).then((result) => {
		if (result.isConfirmed) {

			$.ajax({
				url: "pmt_block/delete",
				type: "post",
				dataType: "json",
				data: {
					del_id: del_id
				},
				success: function (data) {

					if (data.responce == "success") {
						$('#pmt_block_table').DataTable().destroy();
						fetchPmtBlock();
						swalWithBootstrapButtons.fire(
							'Deleted!',
							'Record has been deleted.',
							'success'
						);
					} else {
						swalWithBootstrapButtons.fire(
							'Cancelled',
							'Record is safe',
							'error'
						);
					}
				}
			});

		} else if (
			result.dismiss === Swal.DismissReason.cancel
		) {
			swalWithBootstrapButtons.fire(
				'Cancelled',
				'Record is safe',
				'error'
			)
		}
	})
});

//Fetch infromation from database to fill fields in edit window
$(document).on("click", "#edit_pmt_block", function (e) {
	
	e.preventDefault();
	let edit_id = $(this).attr("value");

	$.ajax({
		url: "pmt_block/edit",
		type: "post",
		dataType: "json",
		data: {
			edit_id: edit_id
		},
		success: function (data) {
			if (data.responce == "success") {
				$('#edit_assembly_pmt_block').modal('show');
				$("#edit_assembly_pmt_block_id").val(data.record.id_pmt_block);
				$("#edit_assembly_super_drawer_id_of_pmt_block").val(data.record.id_super_drawer);
				$("#edit_assembly_mini_drawer_id_of_pmt_block").val(data.record.id_mini_drawer);
				$("#edit_assembly_micro_drawer_id_of_pmt_block").val(data.record.id_micro_drawer);
				$("#edit_assembly_fenics_id_of_pmt_block").val(data.record.id_fenics);
				$("#edit_assembly_hv_divider_id_of_pmt_block").val(data.record.id_hv_divider);
				$("#edit_assembly_pmt_id_of_pmt_block").val(data.record.id_pmt);
				$("#edit_assembly_pmt_block_current_location").val(data.record.current_location);
				$("#edit_assembly_pmt_block_remark").val(data.record.remark);

				$("#edit_assembly_super_drawer_id_of_pmt_block").addClass("ready_to_insert");
				$("#edit_assembly_fenics_id_of_pmt_block").addClass("ready_to_insert");
				$("#edit_assembly_hv_divider_id_of_pmt_block").addClass("ready_to_insert");
				$("#edit_assembly_pmt_id_of_pmt_block").addClass("ready_to_insert");
				$("#edit_pmt_block_form_submit").addClass("notReady");

				fetchPmtBlockRecords();
			} else {
				toastr["error"](data.message);
			}
		}
	});
});

// clear data on each call
$(".close-update-super-drawer-btn").on("click", function(){

	$("#edit_assembly_super_drawer_of_pmt_block_info").text("");
	$("#edit_assembly_fenics_of_pmt_block_info").text("");
	$("#edit_assembly_hv_divider_of_pmt_block_info").text("");
	$("#edit_assembly_pmt_of_pmt_block_info").text("");

	$("#edit_pmt_block_form_submit").removeClass("notReady");
});


// clear data on each call
$(".edit_super_drawer_form_cancel").on("click", function(){

	$("#edit_assembly_super_drawer_of_pmt_block_info").text("");
	$("#edit_assembly_fenics_of_pmt_block_info").text("");
	$("#edit_assembly_hv_divider_of_pmt_block_info").text("");
	$("#edit_assembly_pmt_of_pmt_block_info").text("");

	$("#edit_pmt_block_form_submit").removeClass("notReady");
});


// clear data on each call
$(".edit_super_drawer_form_submit").on("click", function(){

	$("#edit_assembly_super_drawer_of_pmt_block_info").text("");
	$("#edit_assembly_fenics_of_pmt_block_info").text("");
	$("#edit_assembly_hv_divider_of_pmt_block_info").text("");
	$("#edit_assembly_pmt_of_pmt_block_info").text("");

	$("#edit_pmt_block_form_submit").removeClass("notReady");
});

$("#edit_assembly_super_drawer_id_of_pmt_block").on("input", function(e){
	if( e.keyCode == 32 ){
		alert("SPACE isn't allowed");
		e.preventDefault();
		return;
	}
});

$("#edit_assembly_mini_drawer_id_of_pmt_block").on("input", function(e){
	if( e.keyCode == 32 ){
		alert("SPACE isn't allowed");
		e.preventDefault();
		return;
	}
});

$("#edit_assembly_micro_drawer_id_of_pmt_block").on("input", function(e){
	if( e.keyCode == 32 ){
		alert("SPACE isn't allowed");
		e.preventDefault();
		return;
	}
});
$("#edit_assembly_fenics_id_of_pmt_block").on("input", function(e){
	if( e.keyCode == 32 ){
		alert("SPACE isn't allowed");
		e.preventDefault();
		return;
	}
});

$("#edit_assembly_hv_divider_id_of_pmt_block").on("input", function(e){
	if( e.keyCode == 32 ){
		alert("SPACE isn't allowed");
		e.preventDefault();
		return;
	}
});

$("#edit_assembly_pmt_id_of_pmt_block").on("input", function(e){
	if( e.keyCode == 32 ){
		alert("SPACE isn't allowed");
		e.preventDefault();
		return;
	}
});
 
$("#edit_assembly_super_drawer_id_of_pmt_block").on("input", function(){
	if( this.value == "" )
	{
		$("#edit_assembly_super_drawer_of_pmt_block_info").text("Please fill this field");
		$("#edit_assembly_super_drawer_id_of_pmt_block").removeClass("ready_to_insert");
		$("#edit_assembly_super_drawer_id_of_pmt_block").addClass("not_ready_border_col");
		$("#edit_pmt_block_form_submit").addClass("notReady");
		return;
	}else{
		waitSuperDrawerIdCheck("edit_assembly_super_drawer_id_of_pmt_block").then(
			function (value) 
			{
				if (value == true) 
				{
					$("#edit_assembly_super_drawer_of_pmt_block_info").text("");
					$("#edit_assembly_super_drawer_id_of_pmt_block").removeClass("not_ready_border_col");
					$("#edit_assembly_super_drawer_id_of_pmt_block").addClass("ready_to_insert");
					checkAssemblyPmtBlockForm(true);
	
				} else 
				{
					$("#edit_assembly_super_drawer_of_pmt_block_info").text("Super Drawer with this ID doesn't exists");
					$("#edit_assembly_super_drawer_id_of_pmt_block").addClass("not_ready_border_col");
					$("#edit_assembly_super_drawer_id_of_pmt_block").removeClass("ready_to_insert");
					$("#edit_pmt_block_form_submit").addClass("notReady");
				}
			}
		);
	}
});

$("#edit_assembly_fenics_id_of_pmt_block").on("input", function(){
	if( this.value == "" )
	{
		$("#edit_assembly_fenics_of_pmt_block_info").text("Please fill this field");
		$("#edit_assembly_fenics_id_of_pmt_block").removeClass("ready_to_insert");
		$("#edit_assembly_fenics_id_of_pmt_block").addClass("not_ready_border_col");
		$("#edit_pmt_block_form_submit").addClass("notReady");
		return;
	}else if(recordsData[0].indexOf(this.value) != -1 )
	{
		$("#edit_assembly_fenics_of_pmt_block_info").text("Fenics with this ID is already assembled");
		$("#edit_assembly_fenics_id_of_pmt_block").removeClass("ready_to_insert");
		$("#edit_assembly_fenics_id_of_pmt_block").addClass("not_ready_border_col");
		$("#edit_pmt_block_form_submit").addClass("notReady");
		return;
	}else{
		waitFenicsIdCheck("edit_assembly_fenics_id_of_pmt_block").then(
			function (value) 
			{
				if (value == true) 
				{
					$("#edit_assembly_fenics_of_pmt_block_info").text("");
					$("#edit_assembly_fenics_id_of_pmt_block").removeClass("not_ready_border_col");
					$("#edit_assembly_fenics_id_of_pmt_block").addClass("ready_to_insert");
					checkAssemblyPmtBlockForm(true);
	
				} else 
				{
					$("#edit_assembly_fenics_of_pmt_block_info").text("Fenics with this ID doesn't exists");
					$("#edit_assembly_fenics_id_of_pmt_block").addClass("not_ready_border_col");
					$("#edit_assembly_fenics_id_of_pmt_block").removeClass("ready_to_insert");
					$(".edit_pmt_block_form_submit").addClass("notReady");
				}
			}
		);
	}
});

$("#edit_assembly_hv_divider_id_of_pmt_block").on("input", function(){
	if( this.value == "" )
	{
		$("#edit_assembly_hv_divider_of_pmt_block_info").text("Please fill this field");
		$("#edit_assembly_hv_divider_id_of_pmt_block").removeClass("ready_to_insert");
		$("#edit_assembly_hv_divider_id_of_pmt_block").addClass("not_ready_border_col");
		$("#edit_pmt_block_form_submit").addClass("notReady");
		return;
	}else if(recordsData[1].indexOf(this.value) != -1){
		$("#edit_assembly_hv_divider_of_pmt_block_info").text("HV Divider with this ID is already assembled");
		$("#edit_assembly_hv_divider_id_of_pmt_block").removeClass("ready_to_insert");
		$("#edit_assembly_hv_divider_id_of_pmt_block").addClass("not_ready_border_col");
		$("#edit_pmt_block_form_submit").addClass("notReady");
		return;
	}else {
		waitHvDividerIdCheck("edit_assembly_hv_divider_id_of_pmt_block").then(
			function (value) 
			{
				if (value == true) 
				{
					$("#edit_assembly_hv_divider_of_pmt_block_info").text("");
					$("#edit_assembly_hv_divider_id_of_pmt_block").removeClass("not_ready_border_col");
					$("#edit_assembly_hv_divider_id_of_pmt_block").addClass("ready_to_insert");
					checkAssemblyPmtBlockForm(true);
				} else 
				{
					$("#edit_assembly_hv_divider_of_pmt_block_info").text("HV Divider with this ID doesn't exists");
					$("#edit_assembly_hv_divider_id_of_pmt_block").addClass("not_ready_border_col");
					$("#edit_assembly_hv_divider_id_of_pmt_block").removeClass("ready_to_insert");
					$(".edit_pmt_block_form_submit").addClass("notReady");
				}
			}
		);
	}
});

$("#edit_assembly_pmt_id_of_pmt_block").on("input", function(){
	if( this.value == "" )
	{
		$("#edit_assembly_pmt_of_pmt_block_info").text("Please fill this field");
		$("#edit_assembly_pmt_id_of_pmt_block").removeClass("ready_to_insert");
		$("#edit_assembly_pmt_id_of_pmt_block").addClass("not_ready_border_col");
		$("#edit_pmt_block_form_submit").addClass("notReady");
		return;
	}else if(recordsData[2].indexOf(this.value) != -1)
	{
		$("#edit_assembly_pmt_of_pmt_block_info").text("PMT with this ID is already assembled");
		$("#edit_assembly_pmt_id_of_pmt_block").removeClass("ready_to_insert");
		$("#edit_assembly_pmt_id_of_pmt_block").addClass("not_ready_border_col");
		$("#edit_pmt_block_form_submit").addClass("notReady");
		return;
	}else {
		waitHvDividerIdCheck("edit_assembly_pmt_id_of_pmt_block").then(
			function (value) 
			{
				if (value == true) 
				{
					$("#edit_assembly_pmt_of_pmt_block_info").text("");
					$("#edit_assembly_pmt_id_of_pmt_block").removeClass("not_ready_border_col");
					$("#edit_assembly_pmt_id_of_pmt_block").addClass("ready_to_insert");
					checkAssemblyPmtBlockForm(true);
				} else 
				{
					$("#edit_assembly_pmt_of_pmt_block_info").text("PMT with this ID doesn't exists");
					$("#edit_assembly_pmt_id_of_pmt_block").addClass("not_ready_border_col");
					$("#edit_assembly_pmt_id_of_pmt_block").removeClass("ready_to_insert");
					$(".edit_pmt_block_form_submit").addClass("notReady");
				}
			}
		);
	}
});

//Update Pmt Block
$(document).on("click", "#edit_pmt_block_form_submit", function (e) {
	e.preventDefault();

	let id_pmt_block = $("#edit_assembly_pmt_block_id").val();
	let id_super_drawer = $("#edit_assembly_super_drawer_id_of_pmt_block").val();
	let id_mini_drawer = $("#edit_assembly_mini_drawer_id_of_pmt_block").val();
	let id_micro_drawer = $("#edit_assembly_micro_drawer_id_of_pmt_block").val();
	let id_fenics = $("#edit_assembly_fenics_id_of_pmt_block").val();
	let id_hv_divider = $("#edit_assembly_hv_divider_id_of_pmt_block").val();
	let id_pmt = $("#edit_assembly_pmt_id_of_pmt_block").val(); 
	let current_location = $("#edit_assembly_pmt_block_current_location").val(); 
	let remark = $("#edit_assembly_pmt_block_remark").val();

	if (id_pmt_block == "") 
	{
		alert('Pmt Block is Required');
		return;
	} else 
	{
		$.ajax({
			url: "pmt_block/update",
			type: "post",
			dataType: "json",
			data: {
				id_pmt_block: id_pmt_block,
				id_super_drawer: id_super_drawer,
				id_mini_drawer: id_mini_drawer,
				id_micro_drawer: id_micro_drawer,
				id_fenics: id_fenics,
				id_hv_divider: id_hv_divider,
				id_pmt: id_pmt,
				current_location: current_location,
				remark: remark,
			},
			success: function (data) {
				if (data.responce == "success") {
					$('#pmt_block_table').DataTable().destroy();
					$('#edit_assembly_pmt_block').modal('hide');
					fetchPmtBlock();
					recordsData = undefined;
					toastr["success"](data.message);
				} else {
					toastr["error"](data.message);
				}
			},
			error: function (data) {
				console.log(data);
			}
		})
	}
});




//////////////////////////// FUNCTIONS 
//////////////////////////// FUNCTIONS 
//////////////////////////// FUNCTIONS 
//////////////////////////// FUNCTIONS 
//////////////////////////// FUNCTIONS 
//////////////////////////// FUNCTIONS 
//////////////////////////// FUNCTIONS 
//////////////////////////// FUNCTIONS 



function checkAssemblyPmtBlockForm( edit = false)
{
	if( edit == false ){
		if( $("#id_pmt_block_of_assembly_pmt_block").hasClass("ready_to_insert") &&
			$("#id_super_drawer_of_assembly_pmt_block").hasClass("ready_to_insert") &&
			$("#id_assembly_fenics_of_assembly_pmt_block").hasClass("ready_to_insert") &&
			$("#id_assembly_hv_divider_of_assembly_pmt_block").hasClass("ready_to_insert") &&
			$("#id_assembly_pmt_of_assembly_pmt_block").hasClass("ready_to_insert") ){
			$(".add_pmt_block_form_submit").removeClass("notReady");
		}else{
			$(".add_pmt_block_form_submit").addClass("notReady")
		}
	}
	else{
		if( $("#edit_assembly_super_drawer_id_of_pmt_block").hasClass("ready_to_insert") &&
		 	$("#edit_assembly_fenics_id_of_pmt_block").hasClass("ready_to_insert") && 
			$("#edit_assembly_hv_divider_id_of_pmt_block").hasClass("ready_to_insert") &&
			$("#edit_assembly_pmt_id_of_pmt_block").hasClass("ready_to_insert") ){
			$("#edit_pmt_block_form_submit").removeClass("notReady");	
		}else{
			$("#edit_pmt_block_form_submit").addClass("notReady");
		}
	}
}


function fetchPmtBlockRecords () 
{
	$.ajax({
		url: "pmt_block/fetchPmtBlockRecords",
		type: "post",
		dataType: "json",
		success: function (data) {
			recordsData = data.records;
		},
		error: function (data){
			console.log(data.responseText);
		}			
	});
};

function fetchPmtBlock() {
	$.ajax({
		url: "pmt_block/fetch",
		type: "post",
		dataType: "json",
		success: function (data) {
			
			if (data.responce == "success") {
				$('#pmt_block_table').DataTable({
					"data": data.records,
					"rowId": "pmt_block",
					"columns": [
						{ "data": "id_pmt_block" },
						{ "data": "id_super_drawer" },
						{ "data": "id_mini_drawer" },
						{ "data": "id_micro_drawer" },
						{ "data": "id_fenics" },
						{ "data": "id_hv_divider" },
						{ "data": "id_pmt" },
						{ "data": "test_results" },
						{ "data": "current_location" },
						{ "data": "remark" },
						{
							"render": function (data, type, row, meta) {
								var a = `
								<i class="far fa-edit px-1" value="${row.id_pmt_block}" id ="edit_pmt_block" aria-hidden="true"></i>
								<i class="far fa-trash-alt px-1" value="${row.id_pmt_block}" id ="delete_pmt_block" aria-hidden="true"></i>
							`;
								return a;
							}
						}
					]
				});
			} else {
				toastr["error"](data.message);
			}
		},
		error: function (data) {
			console.log(data);
		}
	});
}






// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK
// ---------------- OLD PMT BLOCK




///////////////////// EVENTS
///////////////////// EVENTS
///////////////////// EVENTS
///////////////////// EVENTS
///////////////////// EVENTS
///////////////////// EVENTS
///////////////////// EVENTS







