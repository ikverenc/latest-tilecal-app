/*
	######## ##     ## ######## ##    ## ########  ######  
	##       ##     ## ##       ###   ##    ##    ##    ## 
	##       ##     ## ##       ####  ##    ##    ##       
	######   ##     ## ######   ## ## ##    ##     ######  
	##        ##   ##  ##       ##  ####    ##          ## 
	##         ## ##   ##       ##   ###    ##    ##    ## 
	########    ###    ######## ##    ##    ##     ###### 
*/

// show old super drawer records
$(document).on("click", "#show_old_3_in_1_board_table", function (e) 
{   
    e.preventDefault();
    $('#old_board_3_in_1_table').DataTable().clear().destroy();
    fetch_old_board_3_in_1();
    
});

// delete from old super drawer
$(document).on("click", ".delete_old_board_3_in_1", function (e) 
{
    e.preventDefault();
	delete_record(this, "delete_record_from_old_board_3_in_1" );
});


// //Fetch infromation from db to fill fields in edit window
$(document).on("click", ".edit_old_board_3_in_1", function (e) 
{	
	e.preventDefault();
	let edit_id = $(this).attr("value");

	$.ajax({
		url: "board_3_in_1/edit",
		type: "post",
		dataType: "json",
		data: {
			edit_id: edit_id
		},
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#edit_old_board_3_in_1_modal').modal('show');
				$("#edit_old_board_3_in_1").val(data.record.id_3_in_1);
				$("#edit_id_pmt_block_of_old_board_3_in_1").val(data.record.id_pmt_block);
				$("#edit_remark_of_old_board_3_in_1").val(data.record.remark);
				
			} else {
				toastr["error"](data.message);
	 		}
		}
	});
});
/*
	######## ##     ## ##    ##  ######  ######## ####  #######  ##    ##  ######  
	##       ##     ## ###   ## ##    ##    ##     ##  ##     ## ###   ## ##    ## 
	##       ##     ## ####  ## ##          ##     ##  ##     ## ####  ## ##       
	######   ##     ## ## ## ## ##          ##     ##  ##     ## ## ## ##  ######  
	##       ##     ## ##  #### ##          ##     ##  ##     ## ##  ####       ## 
	##       ##     ## ##   ### ##    ##    ##     ##  ##     ## ##   ### ##    ## 
	##        #######  ##    ##  ######     ##    ####  #######  ##    ##  ######  
*/

// fetch Old board 3 in 1
function fetch_old_board_3_in_1() 
{
    $.ajax({
        url: "board_3_in_1/fetch",
        type: "post",
        dataType: "json",
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#old_board_3_in_1_table').DataTable({
                    "data": data.records,
                    "rowId": "id_3_in_1",
                    "columns": [
                        { "data": "id_3_in_1" },
                        { "data": "id_pmt_block" },
                        { "data": "remark" },
                        { "data": "created_at" },
                        { "data": "updated_at" },
                        { "data": "created_by" },
                        { "data": "updated_by" },
                        {
                            "render": function (data, type, row, meta) 
                            {
                                var a = `
								<i class="far fa-edit px-1 edit_old_board_3_in_1" value="${row.id_3_in_1}" aria-hidden="true"></i>
								<i class="far fa-trash-alt px-1 delete_old_board_3_in_1" value="${row.id_3_in_1}" aria-hidden="true"></i>
							`;
                                return a;
                            }
                        }
                    ]
                });
            } else {
                toastr["error"](data.message);
            }
        }
    });
};

function delete_record_from_old_board_3_in_1( id, window )
{
	$.ajax({
        url: "board_3_in_1/delete",
        type: "post",
        dataType: "json",
        data: {
            del_id: id
        },
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#old_board_3_in_1_table').DataTable().clear().destroy();
                fetch_old_board_3_in_1();
				
				window.fire(
					'Deleted!',
					'Record has been deleted.',
					'success'
				);

			} else 
			{
				window.fire(
					'Cancelled',
					'Record is safe',
					'error'
				);
			}
		},
		error: function()
		{
			toastr["error"](warnings["unknown_problem"]);
		}
	});
}

function check_old_board_3_in_1_id_entry(id)
{
    return $.ajax({
        url: "board_3_in_1/check",
        type: "post",
        dataType: "json",
        data: {
            id: id
        }
    });
}


function add_old_board_3_in_1_in_reception_fn( obj )
{
    let id = obj["id_old_3_in_1_board_in_reception"];
    let remark = obj["remark"];

    $.ajax({    
        url: "board_3_in_1/insert",
        type: "post",
        dataType: "json",
        data: {
            id_3_in_1: id,
            remark: remark
        },
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#add_old_3_in_1_board_in_reception_modal').modal('hide');     
                $('#old_board_3_in_1_table').DataTable().clear().destroy();
                fetch_old_board_3_in_1();

                toastr["success"](data.message);
            
            } else {
                toastr["error"](data.message);
            }
        },
        error: function ()
        {
            toastr["error"](warnings["unknown_problem"]);            
        }
    });
}


function updapte_old_board_3_in_1( obj )
{
    let id = $("#edit_old_board_3_in_1").val();
    let pmt_block_id = obj["edit_id_pmt_block_of_old_board_3_in_1"];
    let remark = obj["remark"];

    $.ajax({
        url: "board_3_in_1/update",
        type: "post",
        dataType: "json",
        data: {
            id_3_in_1: id,
            id_pmt_block: pmt_block_id,
            remark: remark
        },
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#edit_old_board_3_in_1_modal').modal('hide');     
                $('#old_board_3_in_1_table').DataTable().clear().destroy();
                fetch_old_board_3_in_1();              

                toastr["success"](data.message);
            
            } else {
                toastr["error"](data.message);
            }
        },
        error: function ()
        {
            toastr["error"](warnings["unknown_problem"]);            
        }
    });
}
/*

    ##     ##    ###    ########   ######        ##     ##    ###    ########   ######     
    ###   ###   ## ##   ##     ## ##    ##       ###   ###   ## ##   ##     ## ##    ##    
    #### ####  ##   ##  ##     ## ##             #### ####  ##   ##  ##     ## ##          
    ## ### ## ##     ## ########   ######        ## ### ## ##     ## ########   ######     
    ##     ## ######### ##              ##       ##     ## ######### ##              ##    
    ##     ## ##     ## ##        ##    ##       ##     ## ##     ## ##        ##    ##    
    ##     ## ##     ## ##         ######        ##     ## ##     ## ##         ######       	 

*/


// add modal of old super drawer
let rec_old_board_3_in_1_map = new Map();
    rec_old_board_3_in_1_map.set("id_old_3_in_1_board_in_reception", {
		important: true,
		fn : "check_old_hv_divider_id_entry",
		check : function(val)
		{
			let record = new Object();

			if( val ) 
			{
                record.responce = false;
				record.warn = warnings["inDB"];
				
				return record;
				
			}
            else
			{
				record.responce = true;
				record.warn = undefined;
				
				return record;
			} 
		}
	});

// edit modal of old pmt block
let edit_old_board_3_in_1 = new Map();
    edit_old_board_3_in_1.set("edit_id_pmt_block_of_old_board_3_in_1", {
    important: false
});


/*

    ######   #######  ##     ## ########   #######  ##    ## ######## ##    ## ########  ######  
    ##    ## ##     ## ###   ### ##     ## ##     ## ###   ## ##       ###   ##    ##    ##    ## 
    ##       ##     ## #### #### ##     ## ##     ## ####  ## ##       ####  ##    ##    ##       
    ##       ##     ## ## ### ## ########  ##     ## ## ## ## ######   ## ## ##    ##     ######  
    ##       ##     ## ##     ## ##        ##     ## ##  #### ##       ##  ####    ##          ## 
    ##    ## ##     ## ##     ## ##        ##     ## ##   ### ##       ##   ###    ##    ##    ## 
    ######   #######  ##     ## ##         #######  ##    ## ######## ##    ##    ##     ######  

*/

let rec_old_board_3_in_1 = new Object();
    rec_old_board_3_in_1.target = "old_3_in_1_board_modal_target";
    rec_old_board_3_in_1.formID = "add_old_3_in_1_board_in_reception_form";
    rec_old_board_3_in_1.standart_inputs = rec_old_board_3_in_1_map;
    rec_old_board_3_in_1.comment = "remark_for_old_3_in_1_board_in_reception";
    rec_old_board_3_in_1.test_results = undefined;
    rec_old_board_3_in_1.submit_class = {
            cls: "add_old_board_3_in_1_in_reception_form_submit",
            fn: "add_old_board_3_in_1_in_reception_fn"
        };
    
//  edit old board 3 in 1
    rec_old_board_3_in_1.edit_target = "edit_old_board_3_in_1_form_submit";
    rec_old_board_3_in_1.edit_formID = "edit_old_board_3_in_1_form";
    rec_old_board_3_in_1.edit_standart_inputs = edit_old_board_3_in_1;
    rec_old_board_3_in_1.edit_comment = "edit_remark_of_old_board_3_in_1";
	rec_old_board_3_in_1.edit_test_results = undefined
    rec_old_board_3_in_1.edit_cancel_class = "edit_old_board_3_in_1_form_cancel";
	rec_old_board_3_in_1.edit_close_class = "close_btn_of_old_board_3_in_1";
	
	rec_old_board_3_in_1.edit_submit_class = {
		cls: "edit_old_board_3_in_1_form_submit",
		fn: "updapte_old_board_3_in_1"
	};
	

let reception_board_3_in_1 = new Object_control(rec_old_board_3_in_1);
    reception_board_3_in_1.assaignEvents();





/*
	##     ## ####  ######  ########  #######  ########  ##    ## 
	##     ##  ##  ##    ##    ##    ##     ## ##     ##  ##  ##  
	##     ##  ##  ##          ##    ##     ## ##     ##   ####   
	#########  ##   ######     ##    ##     ## ########     ##    
	##     ##  ##        ##    ##    ##     ## ##   ##      ##    
	##     ##  ##  ##    ##    ##    ##     ## ##    ##     ##    
	##     ## ####  ######     ##     #######  ##     ##    ##    
*/



//Fetch OLD HV Divider modifcation Records
$(document).on("click", "#show_old_3_in_1_board_table_modification_history", function (e) 
{
    e.preventDefault();
    $('#modification_old_board_3_in_1_table').DataTable().clear().destroy();
    fetch_old_board_3_in_1_modification_history();
});


function fetch_old_board_3_in_1_modification_history() 
{
    $.ajax({
        url: "board_3_in_1/fetch_old",
        type: "post",
        dataType: "json",
        success: function (data) 
        {
            if (data.responce == "success") 
            {
                $('#modification_old_board_3_in_1_table').DataTable({
                    "data": data.records,
                    "rowId": "id_3_in_1",
                    "columns": [
                        { "data": "id_3_in_1" },
                        { "data": "id_pmt_block" },
                        { "data": "remark" },
                        { "data": "start_date" },
                        { "data": "end_date" },
                        { "data": "created_by" },
                        { "data": "changed_by" },
                        {
                            "render": function (data, type, row, meta) 
                            {
                                var a = `
								<i class="far fa-trash-alt px-1 delete_old_board_3_in_1_hist" value="${row.id_3_in_1_hist}" aria-hidden="true"></i>
							`;
                                return a;
                            }
                        }
                    ]
                });
            } else {
                toastr["error"](data.message);
            }
        }
    });
};


//delete from old pmt block modification history
$(document).on("click", ".delete_old_board_3_in_1_hist", function (e) 
{
    e.preventDefault();
	delete_record(this, "delete_record_from_modification_of_old_board_3_in_1" );
});

function delete_record_from_modification_of_old_board_3_in_1( id, window )
{
    console.log(id);
	$.ajax({
		url: "board_3_in_1/delete_old_modification_hist",
		type: "post",
		dataType: "json",
		data: {
			del_id: id
		},
		success: function (data) 
		{
			if (data.responce == "success") 
			{
				$('#modification_old_board_3_in_1_table').DataTable().clear().destroy();
                fetch_old_board_3_in_1_modification_history();
				
				window.fire(
					'Deleted!',
					'Record has been deleted.',
					'success'
				);

			} else 
			{
				window.fire(
					'Cancelled',
					'Record is safe',
					'error'
				);
			}
		},
		error: function(data)
		{
            console.log(data.responseText);
			toastr["error"](warnings["unknown_problem"]);
		}
	});
}