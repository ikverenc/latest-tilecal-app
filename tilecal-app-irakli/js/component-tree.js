$( document ).ready(function() {
	// $('.sd-table').DataTable();

	$(".sidebar").mCustomScrollbar({
        theme: "minimal",
        mouseWheelPixels: 170,
        scrollInertia: 60,
   		autoDraggerLength:false 
    });
    // open or close navbar
    $('.sidebar-collapse').on('click', function () {
        $('.sidebar').toggleClass('active');
        $('.main').toggleClass('active');
        $(this).toggleClass("fa-chevron-left fa-chevron-right");

    });
    //show sd col
    // $('.sd-col-show').on('click', function () {
    //     $('.sd-col').toggle();
    // });
    $('.partition-col-show').on('click', function () {
        $('.partition-col').toggle();
    });
    $('.module-col-show').on('click', function () {
        $('.module-col').toggle();
    });
    
	//tree checkbox
	$( ".tc-tree-check-box" ).click(function() {	
		$(this).toggleClass('fa-square fa-check-square tree-component-checked');
		$(this).prev().toggleClass('tree-component-checked-p-active');
	});

	$( "#tc .tc-open-tree" ).click(function() {	
		if($(this).parent().siblings('.tc-tree1').is(":visible")){
			$(this).removeClass('fa-minus-square').addClass('fa-plus-square');
			$(this).parent().siblings('.tc-tree1').hide( "fast" );	
			$('.tc-sidebar').addClass('geometry-tree-closed');			
		}
		else{			
			$(this).removeClass('fa-plus-square').addClass('fa-minus-square');
			$(this).parent().siblings('.tc-tree1').show( "fast" );	
			$('.tc-sidebar').removeClass('geometry-tree-closed');		
		}		  
	}).children().click(function(e) {
		  return false;
	});

	$( "#partition .tc-open-tree" ).click(function() {	
		if($(this).parent().siblings('.tc-tree2').is(":visible")){
			$(this).removeClass('fa-minus-square').addClass('fa-plus-square');
			$(this).parent().siblings('.tc-tree2').hide( "fast" );	
			$('.tc-sidebar').addClass('geometry-tree-closed');			
		}
		else{			
			$(this).removeClass('fa-plus-square').addClass('fa-minus-square');
			$(this).parent().siblings('.tc-tree2').show( "fast" );	
			$('.tc-sidebar').removeClass('geometry-tree-closed');		
		}		  
	}).children().click(function(e) {
		  return false;
	});

	$( ".tc-tree2 .tc-open-tree" ).click(function() {	
		if($(this).parent().siblings('.tc-tree3').is(":visible")){
			$(this).removeClass('fa-minus-square').addClass('fa-plus-square');
			$(this).parent().siblings('.tc-tree3').hide( "fast" );				
		}
		else{
			$(this).removeClass('fa-plus-square').addClass('fa-minus-square');	
			$(this).parent().siblings('.tc-tree3').show( "fast" );				
		}		  
	}).children().click(function(e) {
		  return false;
	});

	$( ".tc-tree3 .tc-open-tree" ).click(function() {
		if($(this).parent().siblings('.tc-tree4').is(":visible")){
			$(this).removeClass('fa-minus-square').addClass('fa-plus-square');	
			$(this).parent().siblings('.tc-tree4').hide( "fast" );
		}
		else{			
			$(this).removeClass('fa-plus-square').addClass('fa-minus-square');	
			$(this).parent().siblings('.tc-tree4').show( "fast" );					
		}	
	});

	$( ".tc-tree4 .tc-open-tree" ).click(function() {
		if($(this).parent().siblings('.tc-tree5').is(":visible")){
			$(this).removeClass('fa-minus-square').addClass('fa-plus-square');	
			$(this).parent().siblings('.tc-tree5').hide( "fast" );
		}
		else{			
			$(this).removeClass('fa-plus-square').addClass('fa-minus-square');	
			$(this).parent().siblings('.tc-tree5').show( "fast" );		
		}	
	});

	$( ".tc-tree5 .tc-open-tree" ).click(function() {
		if($(this).parent().siblings('.tc-tree6').is(":visible")){
			$(this).removeClass('fa-minus-square').addClass('fa-plus-square');	
			$(this).parent().siblings('.tc-tree6').hide( "fast" );
		}
		else{			
			$(this).removeClass('fa-plus-square').addClass('fa-minus-square');	
			$(this).parent().siblings('.tc-tree6').show( "fast" );		
		}	
	});
});


/*-------new--------*/
$( ".tree-child" ).click(function() {	
		$('.tree-child').removeClass('tree-component-checked-p-active');
		$(this).toggleClass('tree-component-checked-p-active');
	});
