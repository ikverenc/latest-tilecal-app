// clear data on each call 
$(".daughterboard_modal_target").click(function () {

    $("#add_daughterboard_form")[0].reset();
    $("#id_daughterboard").css('border-color', "#ced4da");
    $("#id_mini_drawer_of_daughterboard").css("border-color", "#ced4da");
    $(".add_daughterboard_form_submit").addClass("notReady");

});



//Fetch Mainboard Records
$(document).on("click", "#show_daughterboard_table", function (e) {
    e.preventDefault();
    $('#daughterboard_table').DataTable().clear().destroy();
    fetchDaughterboard();
});

function fetchDaughterboard() {
    $.ajax({
        url: "daughterboard/fetch",
        type: "post",
        dataType: "json",
        success: function (data) {

            if (data.responce == "success") {

                $('#daughterboard_table').DataTable({
                    "data": data.records,
                    "rowId": "id_daughterboard",
                    "columns": [
                        { "data": "id_daughterboard" },
                        { "data": "id_mini_drawer" },
                        { "data": "component_status" },
                        { "data": "current_location" },
                        { "data": "remark" },
                        {
                            "render": function (data, type, row, meta) {
                                var a = `
								<i class="far fa-edit px-1" value="${row.id_daughterboard}" id ="edit_daughterboard" aria-hidden="true"></i>
								<i class="far fa-trash-alt px-1" value="${row.id_daughterboard}" id ="delete_daughterboard" aria-hidden="true"></i>
							`;
                                return a;
                            }
                        }
                    ]
                });
            } else {
                toastr["error"](data.message);
            }
        }
    });
};

// Add Fenics
$(document).on("click", ".add_daughterboard_form_submit", function (e) {
    e.preventDefault();

    // if ($(".add_mainboard_form_submit").hasClass("notReady")) return;

    let id_daughterboard = $("#id_daughterboard").val();
    let id_mini_drawer = $("#id_mini_drawer_of_daughterboard").val();
    let status = $("#daughterboard_status").val();
    let current_location = $("#daughterboard_current_location").val();
    let remark = $("#daughterboard_remark").val();


    waitDaughterboardIdCheck("id_daughterboard").then(
        function (value) {
            if (value == false) {
                waitMiniDrawerIdCheck("id_mini_drawer_of_daughterboard").then(
                    function (value) {
                        if (value == true) {

                            $.ajax({
                                url: "daughterboard/insert",
                                type: "post",
                                dataType: "json",
                                data: {
                                    id_daughterboard: id_daughterboard,
                                    id_mini_drawer: id_mini_drawer,
                                    component_status: status,
                                    current_location: current_location,
                                    remark: remark
                                },
                                success: function (data) {

                                    if (data.responce == "success") {
                                        $('#daughterboard_table').DataTable().destroy();
                                        $('#add_daughterboard_modal').modal('hide');

                                        fetchDaughterboard();
                                        toastr["success"](data.message);
                                    } else {
                                        toastr["error"](data.message);
                                    }
                                }
                            });
                            $("#add_daughterboard_form")[0].reset();
                        }
                    }
                )
            }
        }
    );
});

// Delete fenics 
$(document).on("click", "#delete_daughterboard", function (e) {
    e.preventDefault();

    let del_id = $(this).attr("value");

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-sm btn-success ml-1',
            cancelButton: 'btn btn-sm btn-danger'
        },
        buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Delete!',
        cancelButtonText: 'No, Cancel!',
        reverseButtons: true
    }).then((result) => {

        if (result.isConfirmed) {

            $.ajax({
                url: "daughterboard/delete",
                type: "post",
                dataType: "json",
                data: {
                    del_id: del_id
                },
                success: function (data) {

                    if (data.responce == "success") {

                        $('#daughterboard_table').DataTable().clear().destroy();
                        fetchDaughterboard();

                        swalWithBootstrapButtons.fire(
                            'Deleted!',
                            'Record has been deleted.',
                            'success'
                        );
                    } else {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Record is safe',
                            'error'
                        );
                    }
                }
            });

        } else if (
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
                'Cancelled',
                'Record is safe',
                'error'
            )
        }
    })
});


//Fetch infromation of daughterboard from database to fill fields in edit window
$(document).on("click", "#edit_daughterboard", function (e) {
    e.preventDefault();

    let edit_id = $(this).attr("value");

    $.ajax({
        url: "daughterboard/edit",
        type: "post",
        dataType: "json",
        data: {
            edit_id: edit_id
        },
        success: function (data) {
            if (data.responce == "success") {
                $('#edit_daughterboard_modal').modal('show');
                $("#edit_id_daughterboard").val(data.record.id_daughterboard);
                $("#edit_id_mini_drawer_of_daughterboard").val(data.record.id_mini_drawer);
                $("#edit_daughterboard_status").val(data.record.component_status);
                $("#edit_daughterboard_current_location").val(data.record.current_location);
                $("#edit_daughterboard_remark").val(data.record.remark);
            } else {
                toastr["error"](data.message);
            }
        }
    })
});

//Update Daughterboard
$(document).on("click", "#edit_daughterboard_form_submit", function (e) {
    e.preventDefault();

    let edit_id_daughterboard = $("#edit_id_daughterboard").val();
    let edit_id_mini_drawer = $("#edit_id_mini_drawer_of_daughterboard").val();
    let edit_daughterboard_status = $("#edit_daughterboard_status").val();
    let edit_daughterboard_current_location = $("#edit_daughterboard_current_location").val();
    let edit_daughterboard_remark = $("#edit_daughterboard_remark").val();

    console.log(edit_id_daughterboard);
    console.log(edit_id_mini_drawer);


    if (edit_id_daughterboard == "") {
        alert('Daughterboard ID is Required');
        return;
    } else if (edit_id_mini_drawer == "") {
        alert('Mini Drawer ID is Required');
        return;
    } else {
        $.ajax({
            url: "daughterboard/update",
            type: "post",
            dataType: "json",
            data: {
                id_daughterboard: edit_id_daughterboard,
                id_mini_drawer: edit_id_mini_drawer,
                component_status: edit_daughterboard_status,
                current_location: edit_daughterboard_current_location,
                remark: edit_daughterboard_remark
            },
            success: function (data) {

                if (data.responce == "success") {

                    $('#daughterboard_table').DataTable().destroy();
                    $('#edit_daughterboard_modal').modal('hide');

                    fetchDaughterboard();

                    toastr["success"](data.message);
                } else {
                    toastr["error"](data.message);
                }
            }
        })
    }
});


// check fenics ID if exists
$("#id_daughterboard").on('input', function () {

    $("#id_daughterboard").css('box-shadow', '1px 1px 5px #888');
    $(".add_daughterboard_form_submit").addClass("notReady")

    waitDaughterboardIdCheck("id_daughterboard").then(
        function (value) {

            if (value == false) {
                $("#daughterboard_id_info").text("");

                $("#id_daughterboard").removeClass("not_ready_border_col");
                $("#id_daughterboard").css('border-color', "#00ff00");

            } else {
                $("#daughterboard_id_info").text("Daughterboard with this ID already exist");
                $("#id_daughterboard").addClass("not_ready_border_col");
            }
        }
    );

});

$("#id_mini_drawer_of_daughterboard").on('input', function () {

    $("#id_mini_drawer_of_daughterboard").css('box-shadow', '1px 1px 5px #888');
    $(".add_daughterboard_form_submit").addClass("notReady");

    waitMiniDrawerIdCheck("id_mini_drawer_of_daughterboard").then(
        function (value) {
            if (value) {
                $("#daughterboard_mini_drawer_info").text("");

                $("#id_mini_drawer_of_daughterboard").removeClass("not_ready_border_col");
                $("#id_mini_drawer_of_daughterboard").css('border-color', "#00ff00");

                if (!$("#id_daughterboard").hasClass("not_ready_border_col") && !$("#id_mini_drawer_of_daughterboard").hasClass("not_ready_border_col")) {
                    $(".add_daughterboard_form_submit").removeClass("notReady");
                }

            } else {
                $("#daughterboard_mini_drawer_info").text("Mini Drawer with this ID doesn't exist");
                $("#id_mini_drawer_of_daughterboard").addClass("not_ready_border_col");

            }
        }
    );

});