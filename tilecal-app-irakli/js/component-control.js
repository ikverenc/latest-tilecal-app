let warnings = [];
	warnings["fill"] = "Please fill this field";
	warnings["disassembled"] = "With this ID already Disassembled";
	warnings["assembled"] = "With this ID already Assembled";
	warnings["inDB"] = "With this ID already is in Database";
	warnings["illegal_chars"] = "% & < > [ ] { } and SPACE aren't allowed!";
	warnings["unknown_problem"] = "Something went wrong, Please refresh the page or try again";
	warnings["nodata"] = "There are no data available for this test results";
	warnings["id_notreachable"] = "with this ID doesn't exists";
	warnings["chars_exceeded"] = "Characters limit exceeded";
    warnings["comment_limit"] = "Characters limit exceeded, Limit is 254 character";
    warnings["file_reader_er"] = "Can't read from file, Please refresh the page or try again";
    warnings["file_empty"] = "File is empty or can not be read, Please refresh the page or try again";
	warnings["not_supported"] = "File format or extension is not valid";
	warnings["file_large"] = "You can send up to 64 KB in attachment";
	warnings["slot_empty"] = "PMT Block is taken from this slot";
	warnings["slot_filled"] = "PMT Block is already inserted in this slot";


function check_for_duplicates(arr)
{
	let uniq = arr.map((name) => {
        return {
            count: 1,
            name: name
        }
    }).reduce((a, b) => {
        a[b.name] = (a[b.name] || 0) + b.count
        return a
    }, {})

    let duplicates = Object.keys(uniq).filter((a) => uniq[a] > 1);

	return duplicates;
}
// control ui for all objs 
class Object_control 
{
	constructor( object )
	{	
		this.component = object;
        
        this.test_results_record = new Object();
        this.test_results_record.uploaded = false;
        this.test_results_record.text = undefined;  
	}

    // async function for any fn which has access to db
    async responce(fn, val)
	{
        let record = new Object()
		try {
			let data = await window[fn](val);
            record.responce = "success";
            record.data = data;
            return record;

		} catch (error) {
            record.responce = "error";
            record.data = error;
			return record;
		}
	}

    // check illegal characters 
	check_illegal_character( text )
	{
		let record = new Object();
		let invalidChars = ['%', '&', '<', '>', '[', ']', '{', '}', ' '];
		record.status = false;

		for( var i=0; i<text.length; i++)
		{
			if( invalidChars.indexOf(text[i]) != -1 )
			{	
				record.status = true;
				break;
			}
		}
		
		return record;
	}

	// check for illegal characters, length, empty values, errors or conflict
	standart_input_control(global ,target)
	{	
		let scope = global;
		let e = target;

		if( e.keyCode == 13 )
		{
			e.preventDefault();
		}

		switch(e.type) 
		{
			case "keydown":
				let record = scope.check_illegal_character(e.key);

				if( record.status )
				{
					e.preventDefault();
					alert( warnings["illegal_chars"]);	
					return;
				}

				break;
			case "input":
                // if this field is mandatory to fill
                if ( scope.component.standart_inputs.get(e.target.id).important )
                {
                    if( e.target.value == "" )
                    {
                        $(e.target).parent().find("span").text( warnings["fill"]);
                        $(e.target).addClass("not_ready_border_col");
                        $(e.target).removeClass("ready_to_insert");
                        scope.handle_form();
                        return;
                    }
                }
                // restrict string length to be more than 21
				if( e.target.value.length > 21 )
				{
					$(e.target).parent().find("span").text( warnings["chars_exceeded"]);
					$(e.target).addClass("not_ready_border_col");
					$(e.target).removeClass("ready_to_insert"); 
                    scope.handle_form();
					return;
				
                }else
                {
                    // double check on illegal characters
					let record = scope.check_illegal_character(e.target.value);
					
					if( record.status )
					{
						$(e.target).addClass("not_ready_border_col");
						$(e.target).removeClass("ready_to_insert");
                        scope.handle_form();
						alert( warnings["illegal_chars"]);	
					
					}else 
                    {
						let fn = scope.component.standart_inputs.get(e.target.id).fn;
                        
                        // if this specific field has unique function 
                        if ( fn != undefined )
						{
							let a = scope.responce(fn, e.target.value );
							a.then(function(answer)
							{
                                if( answer.responce == "error" )
                                {
                                    toastr["error"](warnings["unknown_problem"]);
                                    $(e.target).addClass("not_ready_border_col");
                                    $(e.target).removeClass("ready_to_insert");     
                                    return;
                                }

								let ans = scope.component.standart_inputs.get(e.target.id).check( answer.data );
								
								if( ans.responce == false )
								{
									$(e.target).addClass("not_ready_border_col");
									$(e.target).removeClass("ready_to_insert");
									$(e.target).parent().find("span").text(ans.warn);
                                    scope.handle_form();

								}else
                                {
									$(e.target).removeClass("not_ready_border_col");
									$(e.target).addClass("ready_to_insert");
									$(e.target).parent().find("span").text("");
									scope.handle_form();
								}
							});	

						}else
                        {
							$(e.target).removeClass("not_ready_border_col");
							$(e.target).addClass("ready_to_insert");
							$(e.target).parent().find("span").text("");
							scope.handle_form();
						}
					}		
				}
				break;
			default:
		}
	}

    // check length of comment section
	comment_control(global, target)
	{
		let e = target;

		if( e.target.value.length > 254 )
		{
			$(e.target).parent().find("span").text( warnings["comment_limit"]);
			$(e.target).addClass("not_ready_border_col");
			$(e.target).removeClass("ready_to_insert"); 
			$(e.target).val( $(e.target).val().substr(0,254));

		}else
        {
			$(e.target).parent().find("span").text("");			
			$(e.target).removeClass("not_ready_border_col");
			$(e.target).removeClass("ready_to_insert");
		}
		
        $("#" + $(e.target)[0].id + "_count").text(e.target.value.length + "/254" );
	}

	
	
    // check if form is correctly filled 
	handle_form()
	{
		let status = true; 
		
        // if important fields are not filled correctly then restrict to insert
		this.component.standart_inputs.forEach((key, val) =>
        {
			if( this.component.standart_inputs.get(val).important )
			{
				if( $("#" + val ).hasClass("not_ready_border_col"))
				{
					status = false;
				}
			}
		});	

		if( status )
		{
			$("." + this.component.submit_class.cls).removeClass("notReady");
		
		}else{

			$("." + this.component.submit_class.cls).addClass("notReady");
		}

		return status;
	}

	// after submit get values and send obj to fn 
	handle_submit(global, target)
	{
		let scope = global;
        
        // double check on restriction
		if( scope.handle_form() )
		{
			let obj = new Object();
			
			scope.component.standart_inputs.forEach((fn, id) =>
            {
				obj[$('label[for="' + $("#" + id ).attr('id') + '"]')[0].getAttribute('for')] = $("#" +id ).val();
			});

			obj["remark"] = $("#" + scope.component.comment ).val();

            if( scope.test_results_record.uploaded == true )
            {
                if ( scope.test_results_record.text == undefined )
                {
                    toastr["error"](warnings["file_reader_er"]);
                    return;
                }
                else if( scope.test_results_record.text.length < 1 )
                {
                    toastr["error"](warnings["file_empty"]);
                    return;
                
                }else
                {
                    obj["test_results"] = scope.test_results_record.text;
        			window[scope.component.submit_class.fn](obj);
                }
            }
            else
            {   
                obj["test_results"] = undefined;
                window[scope.component.submit_class.fn](obj);
            }
		}
	}

    // reset form
    reset_form(global, target)
    {
        let scope = global;
        let e = target;
        
        e.preventDefault();

        scope.test_results_record.uploaded = false;
        scope.test_results_record.text = undefined;  
        
        scope.component.standart_inputs.forEach(( fn, id ) =>
        {
			$("#" + id ).removeClass("ready_to_insert");
			$("#" + id ).removeClass("not_ready_border_col");
            $("#" + id ).parent().find("span").text("");
        });

        $("#" + scope.component.comment).removeClass("ready_to_insert");
        $("#" + scope.component.comment).removeClass("not_ready_border_col");
        $("#" + scope.component.comment).parent().find("span").text("");
        $("#" + scope.component.comment+ "_count").text("0/254" );
        
        $("#" + scope.component.test_results).attr("type", "text");
        $("#" + scope.component.test_results).attr("type", "file");

        $("." + scope.component.submit_class.cls).addClass("notReady");
        $("#" + scope.component.formID)[0].reset();
    }

	 // -------------- edit handle section -------------- //
		        // -------------- edit handle section -------------- //
				        // -------------- edit handle section -------------- //
						        // -------------- edit handle section -------------- //
								        // -------------- edit handle section -------------- //
										        // -------------- edit handle section -------------- //
								        // -------------- edit handle section -------------- //
						        // -------------- edit handle section -------------- //
				        // -------------- edit handle section -------------- //
				// -------------- edit handle section -------------- //
		// -------------- edit handle section -------------- //		

	// check for illegal characters, length, empty values, errors or conflict
	edit_standart_input_control(global ,target)
	{	
		let scope = global;
		let e = target;

		switch(e.type) 
		{
			case "keydown":
				let record = scope.check_illegal_character(e.key);

				if( record.status )
				{
					e.preventDefault();
					alert( warnings["illegal_chars"]);	
					return;
				}

				break;
			case "input":
                // if this field is mandatory to fill or not if not then empty value is acceptable
                if ( scope.component.edit_standart_inputs.get(e.target.id).important )
                {
                    if( e.target.value == "" )
                    {
                        $(e.target).parent().find("span").text( warnings["fill"]);
                        $(e.target).addClass("not_ready_border_col");
                        $(e.target).removeClass("ready_to_insert");
                        scope.handle_edit_form();
                        return;
                    }
                }
                // restrict string length to be more then 21
				if( e.target.value.length > 21 )
				{
					$(e.target).parent().find("span").text( warnings["chars_exceeded"]);
					$(e.target).addClass("not_ready_border_col");
					$(e.target).removeClass("ready_to_insert"); 
                    scope.handle_edit_form();
					return;
				
                }else
                {
                    // double check on illegal characters
					let record = scope.check_illegal_character(e.target.value);
					
					if( record.status )
					{
						$(e.target).addClass("not_ready_border_col");
						$(e.target).removeClass("ready_to_insert");
                        scope.handle_edit_form();
						alert( warnings["illegal_chars"]);	
					
					}else 
                    {
						let fn = scope.component.edit_standart_inputs.get(e.target.id).fn;
                        
                        // if this specific field has unique function 
                        if ( fn != undefined )
						{
							let a = scope.responce(fn, e.target.value );
							a.then(function(answer)
							{
                                if( answer.responce == "error" )
                                {
                                    toastr["error"](warnings["unknown_problem"]);
                                    $(e.target).addClass("not_ready_border_col");
                                    $(e.target).removeClass("ready_to_insert");     
                                    return;
                                }

								let ans = scope.component.edit_standart_inputs.get(e.target.id).check( answer.data );
							
								if( ans.responce == false )
								{
									$(e.target).addClass("not_ready_border_col");
									$(e.target).removeClass("ready_to_insert");
									$(e.target).parent().find("span").text(ans.warn);
                                    scope.handle_edit_form();

								}else
                                {
									$(e.target).removeClass("not_ready_border_col");
									$(e.target).addClass("ready_to_insert");
									$(e.target).parent().find("span").text("");
									scope.handle_edit_form();
								}
							});	

						}else
                        {
							$(e.target).removeClass("not_ready_border_col");
							$(e.target).addClass("ready_to_insert");
							$(e.target).parent().find("span").text("");
							scope.handle_edit_form();
						}
					}		
				}
				break;
			default:
		}
	}

	// check if form is well filled 
	handle_edit_form()
	{
		let status = true; 

		// if important fields are not filled correctly then restrict to insert
		this.component.edit_standart_inputs.forEach((key, val) =>
		{
			if ( this.component.edit_standart_inputs.get(val).important )
			{
				if( $("#" + val ).hasClass("not_ready_border_col"))
				{
					status = false;
				}
			}
		});	

		if( status )
		{
			$("." + this.component.edit_submit_class.cls).removeClass("notReady");
		
		}else
		{
			$("." + this.component.edit_submit_class.cls).addClass("notReady");
		}

		return status;
	}

	// after submit get values and send obj to fn 
	handle_edit_submit(global, target)
	{
		let scope = global;

        // double check on restriction
		if( scope.handle_edit_form() )
		{
			let obj = new Object();
			
			scope.component.edit_standart_inputs.forEach((fn, id) =>
            {
				obj[$('label[for="' + $("#" + id ).attr('id') + '"]')[0].getAttribute('for')] = $("#" +id ).val();
			});

			obj["remark"] = $("#" + scope.component.edit_comment ).val();

            if( scope.test_results_record.uploaded == true )
            {
                if ( scope.test_results_record.text == undefined )
                {
                    toastr["error"](warnings["file_reader_er"]);
                    return;
                }
                else if( scope.test_results_record.text.length < 1 )
                {
                    toastr["error"](warnings["file_empty"]);
                    return;
                
                }else
                {
                    obj["test_results"] = scope.test_results_record.text;
        			window[scope.component.edit_submit_class.fn](obj);
					scope.reset_edit_form(global, target);
                }
            }
            else
            {   
                obj["test_results"] = undefined;
                window[scope.component.edit_submit_class.fn](obj);
				scope.reset_edit_form(global, target);
            }
		}
	}

	// reset form
	reset_edit_form(global, target)
	{
		let e = target;
		let scope = global;

		e.preventDefault();

		scope.test_results_record.uploaded = false;
		scope.test_results_record.text = undefined;  

		scope.component.edit_standart_inputs.forEach(( fn, id ) =>
		{
			$("#" + id ).removeClass("ready_to_insert");
			$("#" + id ).removeClass("not_ready_border_col");
			$("#" + id ).parent().find("span").text("");
		});

		$("#" + scope.component.edit_comment).removeClass("ready_to_insert");
		$("#" + scope.component.edit_comment).removeClass("not_ready_border_col");
		$("#" + scope.component.edit_comment).parent().find("span").text("");
		$("#" + scope.component.edit_comment+ "_count").text("0/254" );
		
		$("#" + scope.component.edit_test_results).attr("type", "text");
		$("#" + scope.component.edit_test_results).attr("type", "file");

		$("#" + scope.component.edit_formID)[0].reset();
	}

	// handle file upload
	handle_file_input(global, target)
	{
		let scope = global;
		let e = target;

		if( $(e.target)[0].files.length > 0 )
		{
			if( $(e.target)[0].files[0].type.indexOf("text") == -1 )
			{
				toastr["error"](warnings["not_supported"]);
				$(e.target).attr("type", "text");
				$(e.target).attr("type", "file");
				return;
			
			}else if ( $(e.target)[0].files[0].size > 63000 )
			{
				toastr["error"](warnings["file_large"]);
				$(e.target).attr("type", "text");
				$(e.target).attr("type", "file");
				return;
			}

			scope.test_results_record.uploaded = true;
			scope.test_results_record.text = undefined;

			let reader = new FileReader();
				reader.readAsText($(e.target)[0].files[0]);
				reader.onload = function() 
				{
					scope.test_results_record.text = reader.result;
				}
		}   
	}

	handle_file_remove(global, target)
	{
		let e = target;
		let scope = global;
		e.preventDefault();

		$("#" + scope.component.test_results ).attr("type", "text");
		$("#" + scope.component.test_results ).attr("type", "file");
	}
	assaignEvents()
	{
		// -------------- handle section -------------- //

        //handle standart inputs
		this.component.standart_inputs.forEach(( fn, id ) =>
        {
			document.getElementById(id).addEventListener("keydown", this.standart_input_control.bind(this, this));
			document.getElementById(id).addEventListener("input", this.standart_input_control.bind(this, this));
        });

        //handle file input
        if ( this.component.test_results != undefined ) 
        {
            document.getElementById(this.component.test_results).addEventListener("change", this.handle_file_input.bind(this, this));
        }

		// remove file 
		if( this.component.remove_file != undefined )
		{
			document.getElementById(this.component.remove_file).addEventListener("click", this.handle_file_remove.bind(this, this));
	
		}

        // handle comment
		document.getElementById(this.component.comment).addEventListener("input", this.comment_control.bind(this, this));

        // handle submit
        document.getElementsByClassName(this.component.submit_class.cls)[0].addEventListener("click", this.handle_submit.bind(this, this));
        
        // reset form
        document.getElementsByClassName(this.component.target)[0].addEventListener("click", this.reset_form.bind(this, this));


        // -------------- edit handle section -------------- //
		        // -------------- edit handle section -------------- //
				        // -------------- edit handle section -------------- //
						        // -------------- edit handle section -------------- //
								        // -------------- edit handle section -------------- //
										        // -------------- edit handle section -------------- //
								        // -------------- edit handle section -------------- //
						        // -------------- edit handle section -------------- //
				        // -------------- edit handle section -------------- //
				// -------------- edit handle section -------------- //
		// -------------- edit handle section -------------- //		

		// handle edit inputs 
		if (this.component.edit_standart_inputs == undefined ) return;

		this.component.edit_standart_inputs.forEach((fn, id) =>
		{
			document.getElementById(id).addEventListener("keydown", this.edit_standart_input_control.bind(this, this));
			document.getElementById(id).addEventListener("input", this.edit_standart_input_control.bind(this, this));
		});

		 //handle file input
		 if ( this.component.edit_test_results != undefined ) 
		 {
			document.getElementById(this.component.edit_test_results).addEventListener("change", this.handle_file_input.bind(this, this));
		 }

		// handle comment
		document.getElementById(this.component.edit_comment).addEventListener("input", this.comment_control.bind(this, this));

		// handle submit
        document.getElementsByClassName(this.component.edit_submit_class.cls)[0].addEventListener("click", this.handle_edit_submit.bind(this, this));

		// reset form
        document.getElementsByClassName(this.component.edit_cancel_class)[0].addEventListener("click", this.reset_edit_form.bind(this, this));
        document.getElementsByClassName(this.component.edit_close_class)[0].addEventListener("click", this.reset_edit_form.bind(this, this));

        // handle modification history

    }
}