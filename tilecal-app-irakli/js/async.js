
// wait for fenics ID response 
async function waitFenicsIdCheck(id) {
	let status = await checkFenicsID(id);
	return status;
};

// check fenics ID 
function checkFenicsID(id) {

	let del_id = $("#" + id).val();

	if (del_id == "" || del_id == undefined) {
		return;
	}

	return $.ajax({
		url: "fenics/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: del_id
		}
	});
};


// wait for HV Divider ID response 
async function waitHvDividerIdCheck(id) {
	let status = await checkHvDividerID(id);
	return status;
};

// check hv divider ID 
function checkHvDividerID(id) {

	let del_id = $("#" + id).val();

	if (del_id == "" || del_id == undefined) {
		return;
	}

	return $.ajax({
		url: "hv_divider/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: del_id
		}
	});
};

// wait for pmt block ID response 
async function waitPmtBlockIdCheck(id) {
	let status = await checkPmtBlockID(id);
	return status;
};

// check pmt block ID 
function checkPmtBlockID(id) {

	let del_id = $("#" + id).val();

	if (del_id == "" || del_id == undefined) {
		return;
	}

	return $.ajax({
		url: "pmt_block/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: del_id
		}
	});
};

// wait for super drawer id response 
async function waitSuperDrawerIdCheck(id) {
	let status = await checkSuperDrawerID(id);
	return status;
};

// check super drawer id 
function checkSuperDrawerID(id) {

	let del_id = $("#" + id).val();
	if (del_id == "" || del_id == undefined) {
		return;
	}

	return $.ajax({
		url: "super_drawer/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: del_id
		}
	});
};

// wait for mini drawer id response 
async function waitMiniDrawerIdCheck(id) {
	let status = await checkMiniDrawerID(id);
	return status;
};

// check mini drawer id
function checkMiniDrawerID(id) {

	let del_id = id;


	if (del_id == "" || del_id == undefined) {
		return;
	}

	return $.ajax({
		url: "mini_drawer/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: del_id
		}
	});
};

// wait for micro drawer id response 
async function waitMicroDrawerIdCheck(id) {
	let status = await checkMicroDrawerID(id);
	return status;
};

// check micro drawer id
function checkMicroDrawerID(id) {

	if ( id == "" || id == undefined ) return false;

	return $.ajax({
		url: "micro_drawer/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: id
		}
	});
};

// wait for mainboard id response 
async function waitMainboardIdCheck(id) {
	let status = await checkMainboardID(id);
	return status;
};

// check mainboard id
function checkMainboardID(id) {

	let del_id = $("#" + id).val();

	if (del_id == "" || del_id == undefined) {
		return;
	}

	return $.ajax({
		url: "mainboard/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: del_id
		}
	});
};

// wait for daughterboard id response 
async function waitDaughterboardIdCheck(id) {
	let status = await checkDaughterboardID(id);
	return status;
};

// check daughterboard id
function checkDaughterboardID(id) {

	let del_id = $("#" + id).val();

	if (del_id == "" || del_id == undefined) {
		return;
	}

	return $.ajax({
		url: "daughterboard/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: del_id
		}
	});
};

// wait for hv distribution bord id check 
async function waitHvDistributionBoardIdCheck(id) {
	let status = await checkHvDistributionBoardID(id);
	return status;
};

// check daughterboard id
function checkHvDistributionBoardID(id) {

	let del_id = $("#" + id).val();

	if (del_id == "" || del_id == undefined) {
		return;
	}

	return $.ajax({
		url: "hv_distribution_board/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: del_id
		}
	});
};

// wait for hv distribution bord id check 
async function waitModuleIdCheck(id) {
	let status = await checkModuleID(id);
	return status;
};

// check daughterboard id
function checkModuleID(id) {

	let del_id = $("#" + id).val();

	if (del_id == "" || del_id == undefined) {
		return;
	}

	return $.ajax({
		url: "module/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: del_id
		}
	});
};

// wait for pmt id check 
async function waitPmtIdCheck(id) {
	let status = await checkPmtID(id);
	return status;
};

// check daughterboard id
function checkPmtID(id) {

	let del_id = $("#" + id).val();

	if (del_id == "" || del_id == undefined) return;

	return $.ajax({
		url: "pmt/check",
		type: "post",
		dataType: "json",
		data: {
			del_id: del_id
		}
	});
};


// wait for old super drawer id response 
// async function waitPmtBlockIdCheckOfSuperDrawer(id) {
// 	let status = await checkPmtBlockIdOfSuperDrawer(id);
// 	return status;
// };

// check super drawer id 
// function checkPmtBlockIdOfSuperDrawer(id) {

// 	let del_id = $("#" + id).val();
// 	if (del_id == "" || del_id == undefined) {
// 		return;
// 	}

// 	return $.ajax({
// 		url: "old_super_drawer/checkPmtBlockEntry",
// 		type: "post",
// 		dataType: "json",
// 		data: {
// 			del_id: del_id
// 		}
// 	});
// };
