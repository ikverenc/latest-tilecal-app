<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
		<!-- Fontawesome -->
		<script src="https://kit.fontawesome.com/5008f2f377.js" crossorigin="anonymous"></script>
		<!-- Feather icons -->
		<!-- <script src="https://unpkg.com/feather-icons"></script> -->
		<!-- jquery mCustomScrollbar.css -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/component-tree.css">
		<!-- dataTables.min.css -->
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
		<!-- Toastr CSS -->
		<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
		<!-- Datatables -->
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.24/r-2.2.7/sb-1.0.1/sp-1.2.2/datatables.min.css"/>
		
		<title>TileCal-Electronics-DB</title>
	</head>
	<body> 
		<div class="wrapper">	 
			<div class="container-fluid">
				<!-- Assembly pmt block --> 
				<div class="modal fade" id="assembly_pmt_block" tabindex="-1" role="dialog" aria-labelledby="add_pmt_block_title" aria-hidden="true" data-backdrop="static"> 
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="pmt_block_title">Assembly PMT Block</h5>
								<button type="button" class="close close_assembly_pmy_block_btn" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="assembly_pmt_block_form">
									<div class="form-group">
										<label class="h6" for="assembly_of_pmt_block">PMT Block</label> <span id="assembly_pmt_block_id_info" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="assembly_of_pmt_block_input" id="id_pmt_block_of_assembly_pmt_block" aria-describedby="" placeholder="Pmt block ID">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_of_pmt_block">Super Drawer</label><span id="assembly_super_drawer_id_info_of_assembly_pmt_block" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="assembly_of_pmt_block_input" id="id_super_drawer_of_assembly_pmt_block" aria-describedby="" placeholder="Super Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_of_pmt_block">Mini Drawer</label> <span id="assembly_mini_drawer_id_info_of_assembly_pmt_block" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="assembly_of_pmt_block_input" id="id_mini_drawer_of_assembly_pmt_block" aria-describedby="" placeholder="Mini Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_of_pmt_block">Micro Drawer</label> <span id="assembly_micro_drawer_id_info_of_assembly_pmt_block" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="assembly_of_pmt_block_input" id="id_micro_drawer_of_assembly_pmt_block" aria-describedby="" placeholder="Micro Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_of_pmt_block">Fenics</label> <span id="assembly_fenics_id_info_of_assembly_pmt_block" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="assembly_of_pmt_block_input" id="id_assembly_fenics_of_assembly_pmt_block" aria-describedby="" placeholder="Fenics ID">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_of_pmt_block">HV Divider</label> <span id="assembly_hv_divider_id_info_of_assembly_pmt_block" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="assembly_of_pmt_block_input" id="id_assembly_hv_divider_of_assembly_pmt_block" aria-describedby="" placeholder="HV Divider ID">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_of_pmt_block">PMT</label> <span id="assembly_pmt_id_info_of_assembly_pmt_block" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="assembly_of_pmt_block_input" id="id_assembly_pmt_of_assembly_pmt_block" aria-describedby="" placeholder="PMT ID">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_of_pmt_block">Test results</label>
										<input type="file" class="form-control" name="assembly_of_pmt_block_input" id="id_assembly_test_results_of_assembly_pmt_block" aria-describedby="" placeholder="Test results">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_of_pmt_block">Current location</label>
										<input type="text" class="form-control" name="assembly_of_pmt_block_input" id="assembly_pmt_block_current_location" aria-describedby="" placeholder="Please enter current location">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_of_pmt_block">Comments</label>
										<textarea class="form-control" name="assembly_of_pmt_block_input" id="assembly_pmt_block_remark" rows="1" placeholder="Additional comments"></textarea>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" form="add_pmt_block_form" class="btn btn-sm btn-success add_pmt_block_form_submit notReady"><i class="fas fa-check pr-1"></i>Save</button>
								<button type="button" class="btn btn-sm btn-default add_pmt_block_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
				</div>
				<!-- dissasemble super drawer -->
				<div class="modal fade" id="dissasembly_super_drawer" tabindex="-1" role="dialog" aria-labelledby="add_pmt_block_title" aria-hidden="true" data-backdrop="static"> 
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="dissasembly_super_drawer_title">Disassembly of Super Drawer</h5>
								<button type="button" class="close close-btn-of-disassembly-of-super-drawer" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="dissasembly_super_drawer_form">
									<div class="form-group">
										<label class="h6" for="id_dissasembly_super_drawer">Super Drawer</label> <span></span>
										<input type="text" class="form-control" name="disassembly_of_old_super_drawer" id="id_dissasembly_super_drawer" aria-describedby="" placeholder="Super Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_dissasembly_pmt_block_of_old_super_drawer">PMT Block</label> <span></span>
										<input type="text" class="form-control" name="disassembly_of_old_super_drawer" id="id_dissasembly_pmt_block_of_old_super_drawer" aria-describedby="" placeholder="Pmt Block ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_dissasembly_pmt_block_position">Position of PMT Block</label> <span></span>
										<input type="text" class="form-control" name="disassembly_of_old_super_drawer" id="id_dissasembly_pmt_block_position" aria-describedby="" placeholder="Pmt Block Position">
									</div>
									<div class="form-group">
										<label class="h6" for="">Test Results</label>
										<input type="file" class="form-control" name="disassembly_of_old_super_drawer" id="id_dissasembly_super_drawer_test_results" aria-describedby="" placeholder="Pmt Block Position">
										<a href="" class="remove_file" id="remove_file_of_disassembly_super_drawer">Remove attached file</a>
									</div>
									<div class="form-group">
										<label class="h6" for="id_dissasembly_super_drawer_comment">Comments</label>
										<input type="text" class="form-control" name="disassembly_of_old_super_drawer" id="id_dissasembly_super_drawer_comment" aria-describedby="" placeholder="Comment">
									</div>
									<div class="form-group"> 
										<span style="position:absolute; right:5%; bottom:0" id="id_dissasembly_super_drawer_comment_count">0/255</span>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" form="dissasemble_super_drawer_form" class="btn btn-sm btn-success dissasemble_super_drawer_form_submit notReady"><i class="fas fa-check pr-1"></i>Submit</button>
								<button type="button" class="btn btn-sm btn-default dissasemble_super_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
				</div>

				<!-- disassembly of PMT Block -->
				<div class="modal fade" id="dissasembly_pmt_block" tabindex="-1" role="dialog" aria-labelledby="diss_pmt_block_title" aria-hidden="true" data-backdrop="static"> 
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="dissasembly_pmt_block_title">Disassembly PMT Block</h5>
								<button type="button" class="close close_btn_of_disassembly_pmt_block" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="disassembly_pmt_block_form" autocomplete="off">
									<div class="form-group">
										<label class="h6" for="id_dissasembly_pmt_block">PMT Block</label> <span></span>
										<input type="text" class="form-control" name="disassembly_of_pmt_block" id="id_dissasembly_pmt_block" aria-describedby="" placeholder="PMT Block ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_super_drawer_of_disassembly_pmt_block">Super Drawer</label> <span></span>
										<input type="text" class="form-control" onpaste="return false;" name="disassembly_of_pmt_block" id="id_super_drawer_of_disassembly_pmt_block" aria-describedby="" placeholder="Super Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_dissasembly_hv_divider_of_pmt_block">HV Divider</label> <span></span>
										<input type="text" class="form-control" onpaste="return false;" name="disassembly_of_pmt_block" id="id_dissasembly_hv_divider_of_pmt_block" aria-describedby="" placeholder="HV Divider ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_dissasembly_3_in_1_board_of_pmt_block">3 in 1 board</label> <span></span>
										<input type="text" class="form-control" onpaste="return false;" name="disassembly_of_pmt_block" id="id_dissasembly_3_in_1_board_of_pmt_block" aria-describedby="" placeholder="3 in 1 board ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_dissasembly_pmt_of_pmt_block">PMT</label> <span></span>
										<input type="text" class="form-control" onpaste="return false;" name="disassembly_of_pmt_block" id="id_dissasembly_pmt_of_pmt_block" aria-describedby="" placeholder="PMT ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_test_results_of_pmt_block">Test results</label> <spa></span>
										<input type="file" class="form-control" name="disassembly_of_pmt_block" id="id_test_results_of_pmt_block" aria-describedby="">
										<a href="" class="remove_file" id="remove_file_of_disassembly_pmt_block">Remove attached file</a>
									</div>
									<div class="form-group">
										<label class="h6" for="id_dissasembly_pmt_block_comment">Remark</label>
										<input type="text" class="form-control" name="disassembly_of_pmt_block" id="id_dissasembly_pmt_block_comment" aria-describedby="" placeholder="Additional comments">
									</div>
									<div class="form-group"> 
										<span style="position:absolute; right:5%; bottom:0" id="id_dissasembly_pmt_block_comment_count">0/255</span>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" form="dissasemble_pmt_block_form" class="btn btn-sm btn-success dissasemble_pmt_block_form_submit notReady"><i class="fas fa-check pr-1"></i>Submit</button>
								<button type="button" class="btn btn-sm btn-default dissasemble_pmt_block_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Assembly micro drawer --> 
				<div class="modal fade" id="assembly_micro_drawer" tabindex="-1" role="dialog" aria-labelledby="add_micro_drawer_title" aria-hidden="true" data-backdrop="static"> 
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="micro_drawer_title">Assembly Micro drawer</h5>
								<button type="button" class="close close_btn_of_assembly_micro_drawer" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="assembly_micro_drawer_form">
									<div class="form-group">
										<label class="h6" for="id_micro_drawer">Micro Drawer</label> <span id="assembly_micro_drawer_id_info" ></span>
										<input type="text" class="form-control" name="assembly_micro_drawer" id="id_micro_drawer" aria-describedby="" placeholder="Micro Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_super_drawer_of_assembly_micro_drawer">Super Drawer</label> <span id="super_drawer_id_info_of_assembly_micro_drawer"></span>
										<input type="text" class="form-control" name="assembly_micro_drawer" id="id_super_drawer_of_assembly_micro_drawer" aria-describedby="" placeholder="Super Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_module_of_assembly_micro_drawer">Module</label> <span id="module_id_info_of_assembly_micro_drawer"></span>
										<input type="text" class="form-control" name="" id="id_module_of_assembly_micro_drawer" aria-describedby="" placeholder="Module ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_pmt_block_of_assembly_micro_drawer">PMT Block</label> <span id="pmt_block_info_of_assembly_micro_drawer"></span>
										<input type="text" class="form-control" name="assembly_micro_drawer" id="id_pmt_block_of_assembly_micro_drawer" aria-describedby="" placeholder="PMT Block ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_pmt_block_position_of_assembly_micro_drawer">Position of PMT Block</label> <span id="pmt_block_position_inf_of_assembly_micro_drawer" ></span>
										<input type="text" class="form-control" name="assembly_micro_drawer" id="id_pmt_block_position_of_assembly_micro_drawer" aria-describedby="" placeholder="Position of PMT Block">
									</div>
									<div class="form-group">
										<label class="h6" for="micro_drawer_current_location">Current Location</label> <span id="micro_drawer_current_location_info"></span>
										<input type="text" class="form-control" name="" id="micro_drawer_current_location" aria-describedby="" >
									</div>
									<div class="form-group">
										<label class="h6" for="">Comments</label>
										<textarea class="form-control" name="" id="micro_drawer_remark" rows="1" placeholder="Additional comments"></textarea>
									</div>
									<div class="form-group"> 
										<span style="position:absolute; right:5%; bottom:0" id="micro_drawer_remark_count">0/255</span>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" form="add_micro_drawer_form" class="btn btn-sm btn-success add_micro_drawer_form_submit notReady"><i class="fas fa-check pr-1"></i>Save</button>
								<button type="button" class="btn btn-sm btn-default add_micro_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Assembly super drawer --> 
				<div class="modal fade" id="assembly_super_drawer" tabindex="-1" role="dialog" aria-labelledby="add_super_drawer_title" aria-hidden="true" data-backdrop="static"> 
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="assembly_of_super_drawer_title">Assembly of LB Super Drawer</h5>
								<button type="button" class="close close-assembly-super-drawer-modal" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="assembly_super_drawer_form">
									<div class="form-group">
										<select name="super_drawer" id="super_drawer_types">
											<option value="reg_super_drawer">LB Super Drawer</option>
											<option value="ext_super_drawer">Extended Super Drawer</option>
										</select>	
									</div>
									<div class="form-group">
										<label class="h6" for="id_super_drawer">Super Drawer</label> <span id="check_assembly_super_drawer_id_info"></span>
										<input type="text" class="form-control" name="" id="id_super_drawer" aria-describedby="" placeholder="Super drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_assembly_mini_drawer1_of_super_drawer">Mini Drawer 1</label> <span id="assembly_mini_drawer1_id_info_of_super_drawer" style=""></span>
										<input type="text" class="form-control" name="mini_drawer" id="id_assembly_mini_drawer1_of_super_drawer" aria-describedby="" placeholder="1 Mini Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_assembly_mini_drawer2_of_super_drawer">Mini Drawer 2</label> <span id="assembly_mini_drawer2_id_info_of_super_drawer"></span>
										<input type="text" class="form-control" name="mini_drawer" id="id_assembly_mini_drawer2_of_super_drawer" aria-describedby="" placeholder="2 Mini Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_assembly_mini_drawer3_of_super_drawer">Mini Drawer 3</label> <span id="assembly_mini_drawer3_id_info_of_super_drawer" ></span>
										<input type="text" class="form-control" name="mini_drawer" id="id_assembly_mini_drawer3_of_super_drawer" aria-describedby="" placeholder="3 Mini Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_assembly_mini_drawer4_of_super_drawer">Mini Drawer 4</label> <span id="assembly_mini_drawer4_id_info_of_super_drawer" ></span>
										<input type="text" class="form-control" name="mini_drawer" id="id_assembly_mini_drawer4_of_super_drawer" aria-describedby="" placeholder="4 Mini Drawer ID">
									</div>
									<div class="form-group displ_none">
										<label class="h6" for="id_assembly_micro_drawer1_of_super_drawer">Micro Drawer 1</label> <span id="assembly_micro_drawer1_id_info_of_super_drawer" ></span>
										<input type="text" class="form-control" name="micro_drawer" id="id_assembly_micro_drawer1_of_super_drawer" aria-describedby="" placeholder="1 Micro Drawer ID">
									</div>	
									<div class="form-group displ_none">
										<label class="h6" for="id_assembly_micro_drawer2_of_super_drawer">Micro Drawer 2</label> <span id="assembly_micro_drawer2_id_info_of_super_drawer" ></span>
										<input type="text" class="form-control" name="micro_drawer" id="id_assembly_micro_drawer2_of_super_drawer" aria-describedby="" placeholder="2 Micro Drawer ID">
									</div>								
									<div class="form-group">
										<label class="h6" for="super_drawer_status">Status</label>
										<input type="text" class="form-control" name="status" id="super_drawer_status" aria-describedby="" placeholder="Please enter status">
									</div>
									<div class="form-group">
										<label class="h6" for="super_drawer_current_location">Current location</label>
										<input type="text" class="form-control" name="super_location" id="super_drawer_current_location" aria-describedby="" placeholder="Please enter current location">
									</div>
									<div class="form-group">
										<label class="h6" for="remark">Comments</label>
										<textarea class="form-control" name="remark" id="super_drawer_remark" rows="1" placeholder="Additional comments"></textarea>
									</div>
									<div class="form-group"> 
										<span style="position:absolute; right:5%; bottom:0" id="super_drawer_remark_count">0/255</span>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" form="add_super_drawer_form" class="btn btn-sm btn-success add_super_drawer_form_submit notReady"><i class="fas fa-check pr-1"></i>Save</button>
								<button type="button" class="btn btn-sm btn-default add_supper_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Edit Pmt Block -->
				<div class="modal fade" id="edit_assembly_pmt_block" tabindex="-1" role="dialog" aria-labelledby="edit_assembly_pmt_block" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_assembly_pmt_block_title">Update PMT Block</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_assembly_pmt_block_form">
										<div class="form-group">
											<label class="h6" for="">PMT Block</label> 
											<input type="text" class="form-control" name="edit_assembly_of_pmt_block" id="edit_assembly_pmt_block_id" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="">Super Drawer</label> <span id="edit_assembly_super_drawer_of_pmt_block_info"></span>
											<input type="text" class="form-control" name="edit_assembly_of_pmt_block" id="edit_assembly_super_drawer_id_of_pmt_block" aria-describedby="" placeholder="Super Drawer ID">
										</div>
										<div class="form-group">
											<label class="h6" for="">Mini Drawer</label> <span id="edit_assembly_mini_drawer_of_pmt_block_info"></span>
											<input type="text" class="form-control" name="edit_assembly_of_pmt_block" id="edit_assembly_mini_drawer_id_of_pmt_block" aria-describedby="" placeholder="Mini Drawer ID">
										</div>
										<div class="form-group">
											<label class="h6" for="">Micro Drawer</label> <span id="edit_assembly_micro_drawer_of_pmt_block_info"></span>
											<input type="text" class="form-control" name="edit_assembly_of_pmt_block" id="edit_assembly_micro_drawer_id_of_pmt_block" aria-describedby="" placeholder="Micro Drawer ID">
										</div>
										<div class="form-group">
											<label class="h6" for="">Fenics</label> <span id="edit_assembly_fenics_of_pmt_block_info"></span>
											<input type="text" class="form-control" name="edit_assembly_of_pmt_block" id="edit_assembly_fenics_id_of_pmt_block" aria-describedby="" placeholder="Fenics ID">
										</div>
										<div class="form-group">
											<label class="h6" for="">HV Divider</label> <span id="edit_assembly_hv_divider_of_pmt_block_info"></span>
											<input type="text" class="form-control" name="edit_assembly_of_pmt_block" id="edit_assembly_hv_divider_id_of_pmt_block" aria-describedby="" placeholder="HV Divider ID">
										</div>
										<div class="form-group">
											<label class="h6" for="">PMT</label> <span id="edit_assembly_pmt_of_pmt_block_info"></span>
											<input type="text" class="form-control" name="edit_assembly_of_pmt_block" id="edit_assembly_pmt_id_of_pmt_block" aria-describedby="" placeholder="PMT ID">
										</div>
										<div class="form-group">
											<label class="h6" for="">Test Results</label>
											<input type="file" class="form-control" name="edit_assembly_of_pmt_block" id="edit_assembly_test_results_of_pmt_block" aria-describedby="">
										</div>
										<div class="form-group">
											<label class="h6" for="">Current location</label>
											<input type="text" class="form-control" name="edit_assembly_of_pmt_block" id="edit_assembly_pmt_block_current_location" aria-describedby="" placeholder="Please enter current location">
										</div>
										<div class="form-group">
											<label class="h6" for="">Remark</label>
											<input type="text" class="form-control" name="edit_assembly_of_pmt_block" id="edit_assembly_pmt_block_remark" aria-describedby="" placeholder="Comments">
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_pmt_block_form" class="btn btn-sm btn-success edit_pmt_form_submit" id="edit_pmt_block_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>

				<!-- edit old pmt block -->
				<div class="modal fade" id="edit_disassembly_pmt_block" tabindex="-1" role="dialog" aria-labelledby="edit_disassembly_pmt_block" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_disassembly_pmt_block_title">Update PMT Block</h5>
									<button type="button" class="close close_btn_of_old_pmt_block" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_disassembly_pmt_block_form">
										<div class="form-group">
											<label class="h6" for="">PMT Block</label> 
											<input type="text" class="form-control" name="edit_disdisassembly_of_pmt_block" id="edit_disassembly_pmt_block_id" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="edit_disassembly_super_drawer_id_of_pmt_block">Super Drawer</label> <span id="edit_disassembly_super_drawer_id_of_pmt_block_info"></span>
											<input type="text" class="form-control" name="edit_disassembly_of_pmt_block" id="edit_disassembly_super_drawer_id_of_pmt_block" aria-describedby="" placeholder="Super Drawer ID">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_disassembly_hv_divider_id_of_pmt_block">HV Divider</label> <span></span>
											<input type="text" class="form-control" name="edit_disassembly_of_pmt_block" id="edit_disassembly_hv_divider_id_of_pmt_block" aria-describedby="" placeholder="HV Divider ID">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_disassembly_3_in_1_board_id_of_pmt_block">3 in 1 Board</label> <span></span>
											<input type="text" class="form-control" name="edit_disassembly_of_pmt_block" id="edit_disassembly_3_in_1_board_id_of_pmt_block" aria-describedby="" placeholder="3 in 1 Board ID">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_disassembly_pmt_id_of_pmt_block">PMT</label> <span></span>
											<input type="text" class="form-control" name="edit_disassembly_of_pmt_block" id="edit_disassembly_pmt_id_of_pmt_block" aria-describedby="" placeholder="PMT ID">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_disassembly_test_results_of_pmt_block">Test Results</label> <span></span>
											<input type="file" class="form-control" name="edit_disassembly_of_pmt_block" id="edit_disassembly_test_results_of_pmt_block">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_comment_of_disassembly_pmt_block">Remark</label> <span></span>
											<input type="text" class="form-control" name="edit_disassembly_of_pmt_block" id="edit_comment_of_disassembly_pmt_block" aria-describedby="" placeholder="Additional Comments">
										</div>
										<div class="form-group"> 
											<span style="position:absolute; right:5%; bottom:0" id="edit_comment_of_disassembly_pmt_block_count">0/255</span>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_pmt_block_form" class="btn btn-sm btn-success edit_old_pmt_block_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_old_pmt_block_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>

				<!-- Edit super drawer -->
				<div class="modal fade" id="edit_assembly_super_drawer" tabindex="-1" role="dialog" aria-labelledby="edit_assembly_super_drawer" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_edit_assembly_super_drawer_title">Update Super drawer</h5><span id="origin_value_of_super_drawer"></span>
									<button type="button"  class="close close-update-super-drawer-btn" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_assembly_super_drawer_form">
										<div class="form-group">
											<label class="h6" for="id_edit_assembly_super_drawer">Super drawer</label>
											<input type="text" class="form-control" name="ID" id="id_edit_assembly_super_drawer" aria-describedby="" >
										</div>
										<div class="form-group">
											<label class="h6" for="id_edit_assembly_mini_drawer1_of_super_drawer">Mini Drawer 1</label> <span id="edit_assembly_mini_drawer1_id_info_of_super_drawer"></span>
											<input type="text" class="form-control" name="mini_drawer" id="id_edit_assembly_mini_drawer1_of_super_drawer" aria-describedby="" placeholder="Mini Drawer 1 ID">
										</div>
										<div class="form-group">
											<label class="h6" for="id_edit_assembly_mini_drawer2_of_super_drawer">Mini Drawer 2</label> <span id="edit_assembly_mini_drawer2_id_info_of_super_drawer"></span>
											<input type="text" class="form-control" name="mini_drawer" id="id_edit_assembly_mini_drawer2_of_super_drawer" aria-describedby="" placeholder="Mini Drawer 2 ID">
										</div>
										<div class="form-group">
											<label class="h6" for="id_edit_assembly_mini_drawer3_of_super_drawer">Mini Drawer 3</label> <span id="edit_assembly_mini_drawer3_id_info_of_super_drawer"></span>
											<input type="text" class="form-control" name="mini_drawer" id="id_edit_assembly_mini_drawer3_of_super_drawer" aria-describedby="" placeholder="Mini Drawer 3 ID">
										</div>
										<div class="form-group">
											<label class="h6" for="id_edit_assembly_mini_drawer4_of_super_drawer">Mini Drawer 4</label> <span id="edit_assembly_mini_drawer4_id_info_of_super_drawer"></span>
											<input type="text" class="form-control" name="mini_drawer" id="id_edit_assembly_mini_drawer4_of_super_drawer" aria-describedby="" placeholder="Mini Drawer 4 ID">
										</div>
										<div class="form-group">
											<label class="h6" for="id_edit_assembly_micro_drawer1_of_super_drawer">Micro Drawer 1</label> <span id="edit_assembly_micro_drawer1_id_info_of_super_drawer"></span>
											<input type="text" class="form-control" name="mini_drawer" id="id_edit_assembly_micro_drawer1_of_super_drawer" aria-describedby="" placeholder="Micro Drawer 1 ID">
										</div>
										<div class="form-group">
											<label class="h6" for="id_edit_assembly_micro_drawer2_of_super_drawer">Micro Drawer 2</label> <span id="edit_assembly_micro_drawer2_id_info_of_super_drawer"></span>
											<input type="text" class="form-control" name="mini_drawer" id="id_edit_assembly_micro_drawer2_of_super_drawer" aria-describedby="" placeholder="Mini Drawer 2 ID" > 
										</div>
										<div class="form-group">
											<label class="h6" for="edit_super_drawer_status">Status</label>
											<input type="text" class="form-control" name="status" id="edit_assembly_super_drawer_status" aria-describedby="" placeholder="Please enter status e.g. delivered">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_super_drawer">Current location</label>
											<input type="text" class="form-control" name="current_location" id="edit_assembly_super_drawer_current_location" aria-describedby="" placeholder="Please enter current location">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_super_drawer_remark">Comments</label>
											<textarea class="form-control" name="remark" id="edit_super_drawer_remark" rows="1" placeholder="Additional comments"></textarea>
										</div>
										<div class="form-group"> 
											<span style="position:absolute; right:5%; bottom:0" id="edit_super_drawer_remark_count">0/255</span>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_super_drawer_form" class="btn btn-sm btn-success edit_super_drawer_form_submit notReady" id="edit_super_drawer_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_super_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>

				<!-- Edit OLD super drawer -->
				<div class="modal fade" id="edit_disassembly_super_drawer" tabindex="-1" role="dialog" aria-labelledby="edit_assembly_super_drawer" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_edit_disassembly_super_drawer_title">Edit Old Super drawer</h5>
									<button type="button" class="close close_btn_of_edit_old_sp_drawer" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_disassembly_super_drawer_form">
										<span id="originval_of_old_super_drawer_edit" style="display:none"></span>
										<div class="form-group">
											<label class="h6" for="id_edit_disassembly_super_drawer">Old Super drawer</label> <span></span>
											<input type="text" class="form-control" name="edit_disassembly_of_super_drawer" id="id_edit_disassembly_super_drawer" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="id_edit_disassembly_pmt_block_of_super_drawer">PMT Block</label> <span></span>
											<input type="text" class="form-control" name="edit_disassembly_of_super_drawer" id="id_edit_disassembly_pmt_block_of_super_drawer" aria-describedby="" placeholder="Pmt Block ID">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_disassembly_pmt_block_position_of_super_drawer">PMT Block Position</label> <span></span>
											<input type="text" class="form-control" name="edit_disassembly_of_super_drawer" id="edit_disassembly_pmt_block_position_of_super_drawer" aria-describedby="" placeholder="Position of Pmt Block">
										</div>
										<div class="form-group">
											<label class="h6" for="">Test Result</label> <span id=""></span>
											<input type="file" class="form-control" name="edit_disassembly_of_super_drawer" id="edit_disassembly_test_results_of_super_drawer">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_disassembly_super_drawer_remark">Comments</label> <span></span>
											<textarea class="form-control" name="edit_disassembly_of_super_drawer" id="edit_disassembly_super_drawer_remark" rows="1" placeholder="Additional comments"></textarea>
										</div>
										<div class="form-group"> 
											<span style="position:absolute; right:5%; bottom:0" id="edit_disassembly_super_drawer_remark_count">0/255</span>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_old_super_drawer_form" class="btn btn-sm btn-success edit_old_super_drawer_form_submit notReady" id="edit_old_super_drawer_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_old_super_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>

				<!-- Edit Micro Drawer -->
				<div class="modal fade" id="edit_assembly_micro_drawer" tabindex="-1" role="dialog" aria-labelledby="edit_assembly_micro_drawer" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_edit_assembly_micro_drawer_title">Update Micro drawer</h5>
									<button type="button" class="close close_edit_assembly_of_micro_drawer_window" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_edit_assembly_micro_drawer_form">
									<span id="originValMicroDrawer" style="display:none"></span>
										<div class="form-group">
											<label class="h6" for="edit_id_assembly_micro_drawer">Micro drawer</label> <span id="edit_assembly_micro_drawer_info"></span>
											<input type="text" class="form-control" name="edit_assembly_of_micro_drawer" id="edit_id_assembly_micro_drawer" aria-describedby="" >
										</div>
										<div class="form-group">
											<label class="h6" for="edit_id_super_drawer_of_assembly_micro_drawer">Super drawer</label> <span id="edit_assembly_super_drawer_id_of_micro_drawer_info"></span>
											<input type="text" class="form-control" name="edit_assembly_of_micro_drawer" id="edit_id_super_drawer_of_assembly_micro_drawer" aria-describedby="" placeholder="Super Drawer ID">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_id_module_of_assembly_micro_drawer">Module</label> <span id="edit_assembly_module_id_of_micro_drawer_info"></span>
											<input type="text" class="form-control" name="edit_assembly_of_micro_drawer" id="edit_id_module_of_assembly_micro_drawer" aria-describedby="" placeholder="Module ID">
										</div>
										<div class="form-group">
											<label class="h6" for="id_edit_assembly_pmt_block_of_micro_drawer">PMT Block</label> <span id="edit_assembly_pmt_block_id_of_micro_drawer_info"></span>
											<input type="text" class="form-control" name="edit_assembly_of_micro_drawer" id="id_edit_assembly_pmt_block_of_micro_drawer" aria-describedby="" placeholder="PMT Block ID">
										</div>
										<div class="form-group">
											<label class="h6" for="id_edit_assembly_pmt_block_pos_of_micro_drawer">Position of PMT Block</label> <span id="edit_assembly_pmt_block_pos_of_micro_drawer_info"></span> 
											<input type="text" class="form-control" name="edit_assembly_of_micro_drawer" id="id_edit_assembly_pmt_block_pos_of_micro_drawer" aria-describedby="" placeholder="Position of PMT Block">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_assembly_micro_drawer_current_location">Current Location</label>
											<input type="text" class="form-control" name="status" id="edit_assembly_micro_drawer_current_location" aria-describedby="">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_micro_drawer_remark">Comments</label>
											<textarea class="form-control" name="remark" id="edit_micro_drawer_remark" rows="1" placeholder="Additional comments"></textarea>
										</div>
										<div class="form-group"> 
											<span style="position:absolute; right:5%; bottom:0" id="edit_micro_drawer_remark_count">0/255</span>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_micro_drawer_form" class="btn btn-sm btn-success edit_micro_drawer_form_submit" id="edit_micro_drawer_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>


			 <!-- Add Fenics Modal -->
			    <div class="modal fade" id="add_fenics_modal" tabindex="-1" role="dialog" aria-labelledby="add_fenics_modal_title" aria-hidden="true" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="add_fenics_modal_title">Add Fenics</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="add_fenics_form">
									<div class="form-group">
										<label class="h6" for="fenics_id">Fenics</label> <span id="fenics_id_info" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="id_fenics" id="id_fenics" aria-describedby="" placeholder="Fenics ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_pmt_block_fenics">PMT block</label> <span id="fenics_pmt_block_info" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="id_pmt_block_fenics" id="id_pmt_block_fenics" aria-describedby="" placeholder="Pmt Block ID">
									</div>
									<div class="form-group">
										<label class="h6" for="fenics_status">Status</label>
										<input type="text" class="form-control" name="status" id="fenics_status" aria-describedby="" placeholder="Please enter status e.g. delivered">
									</div>
									<div class="form-group">
										<label class="h6" for="fenics_current_location">Current location</label>
										<input type="text" class="form-control" name="current_location" id="fenics_current_location" aria-describedby="" placeholder="Please enter current location">
									</div>
									<div class="form-group">
										<label class="h6" for="remark">Comments</label>
										<textarea class="form-control" name="remark" id="fenics_remark" rows="1" placeholder="Additional comments"></textarea>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" form="add_fenics_form" class="btn btn-sm btn-success add_fenics_form_submit notReady"><i class="fas fa-check pr-1"></i>Save</button>
								<button type="button" class="btn btn-sm btn-default add_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
			    </div>


				<!-- add old super drawer modal -->
				<div class="modal fade" id="add_old_super_drawer_in_reception_modal" tabindex="-1" role="dialog" aria-labelledby="add_old_super_drawer_in_reception_modal_title" aria-hidden="true" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="add_old_super_drawer_in_reception_title">Add Super Drawer</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="add_old_super_drawer_in_reception_form">
									<div class="form-group">
										<label class="h6" for="id_old_super_drawer_in_reception">Super Drawer</label> <span id=""></span>
										<input type="text" class="form-control" name="" id="id_old_super_drawer_in_reception" aria-describedby="" placeholder="Super Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="remark">Comments</label>
										<textarea class="form-control" name="remark" id="remark_for_old_super_drawer_in_reception" rows="1" placeholder="Additional comments"></textarea>
									</div>
									<div class="form-group"> 
										<span style="position:absolute; right:5%; bottom:0" id="remark_for_old_super_drawer_in_reception_count">0/255</span>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" class="btn btn-sm btn-success add_old_super_drawer_in_reception_form_submit notReady"><i class="fas fa-check pr-1"></i>Submit</button>
								<button type="button" class="btn btn-sm btn-default add_old_super_drawer_in_reception_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
				</div>
			
			<!-- add old pmt block modal -->
				<div class="modal fade" id="add_old_pmt_block_in_reception_modal" tabindex="-1" role="dialog" aria-labelledby="add_old_pmt_block_in_reception_modal_title" aria-hidden="true" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="add_old_pmt_block_in_reception_title">Add PMT Block</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="add_old_pmt_block_in_reception_form">
									<div class="form-group">
										<label class="h6" for="id_old_pmt_block_in_reception">PMT Block</label> <span></span>
										<input type="text" class="form-control" name="" id="id_old_pmt_block_in_reception" aria-describedby="" placeholder="PMT Block ID">
									</div>
									<div class="form-group">
										<label class="h6" for="remark_for_old_pmt_block_in_reception">Comments</label> <span></span>
										<textarea class="form-control" name="reception_pmt_block_remark_count_info" id="remark_for_old_pmt_block_in_reception" rows="1" placeholder="Additional comments"></textarea>
									</div>
									<div class="form-group"> 
										<span style="position:absolute; right:5%; bottom:0" id="remark_for_old_pmt_block_in_reception_count">0/255</span>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" class="btn btn-sm btn-success add_old_pmt_block_in_reception_form_submit notReady"><i class="fas fa-check pr-1"></i>Submit</button>
								<button type="button" class="btn btn-sm btn-default add_old_pmt_block_in_reception_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
			    </div>

				<!-- add old hv divider modal -->
				<div class="modal fade" id="add_old_hv_divider_in_reception_modal" tabindex="-1" role="dialog" aria-labelledby="add_old_hv_divider_in_reception_modal_title" aria-hidden="true" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="add_old_hv_divider_in_reception_title">Add HV Divider</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="add_old_hv_divider_in_reception_form">
									<div class="form-group">
										<label class="h6" for="id_old_hv_divider_in_reception">HV Divider ID</label> <span></span>
										<input type="text" class="form-control" name="" id="id_old_hv_divider_in_reception" aria-describedby="" placeholder="HV Divider ID">
									</div>
									<div class="form-group">
										<label class="h6" for="remark_for_old_hv_divider_in_reception">Comments</label> <span></span>
										<textarea class="form-control" name="reception_pmt_block_remark_count_info" id="remark_for_old_hv_divider_in_reception" rows="1" placeholder="Additional comments"></textarea>
									</div>
									<div class="form-group"> 
										<span style="position:absolute; right:5%; bottom:0" id="remark_for_old_hv_divider_in_reception_count">0/255</span>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" class="btn btn-sm btn-success add_old_hv_divider_in_reception_form_submit notReady"><i class="fas fa-check pr-1"></i>Submit</button>
								<button type="button" class="btn btn-sm btn-default add_old_hv_divider_in_reception_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
			    </div>

				<!-- add mini drawer in reception -->
				<div class="modal fade" id="add_mini_drawer_in_reception_modal" tabindex="-1" role="dialog" aria-labelledby="add_mini_drawer_in_reception_modal_title" aria-hidden="true" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="add_mini_drawer_in_reception_title">Add Mini Drawer</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="add_mini_drawer_in_reception_form">
									<div class="form-group">
										<label class="h6" for="id_mini_drawer_in_reception">Mini Drawer</label> <span></span>
										<input type="text" class="form-control" name="" id="id_mini_drawer_in_reception" aria-describedby="" placeholder="Mini Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="remark_for_mini_drawer_in_reception">Comments</label> <span></span>
										<textarea class="form-control" name="" id="remark_for_mini_drawer_in_reception" rows="1" placeholder="Additional comments"></textarea>
									</div>
									<div class="form-group"> 
										<span style="position:absolute; right:5%; bottom:0" id="remark_for_mini_drawer_in_reception_count">0/255</span>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" class="btn btn-sm btn-success add_mini_drawer_in_reception_form_submit notReady"><i class="fas fa-check pr-1"></i>Submit</button>
								<button type="button" class="btn btn-sm btn-default add_mini_drawer_in_reception_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
			    </div>

				<!-- add micro drawer in reception -->
				<div class="modal fade" id="add_micro_drawer_in_reception_modal" tabindex="-1" role="dialog" aria-labelledby="add_micro_drawer_in_reception_modal_title" aria-hidden="true" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="add_micro_drawer_in_reception_title">Add Micro Drawer</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="add_micro_drawer_in_reception_form">
									<div class="form-group">
										<label class="h6" for="id_micro_drawer_in_reception">Micro Drawer</label> <span></span>
										<input type="text" class="form-control" name="" id="id_micro_drawer_in_reception" aria-describedby="" placeholder="Micro Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="remark_for_micro_drawer_in_reception">Comments</label> <span></span>
										<textarea class="form-control" name="" id="remark_for_micro_drawer_in_reception" rows="1" placeholder="Additional comments"></textarea>
									</div>
									<div class="form-group"> 
										<span style="position:absolute; right:5%; bottom:0" id="remark_for_micro_drawer_in_reception_count">0/255</span>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" class="btn btn-sm btn-success add_micro_drawer_in_reception_form_submit notReady"><i class="fas fa-check pr-1"></i>Submit</button>
								<button type="button" class="btn btn-sm btn-default add_micro_drawer_in_reception_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
			    </div>

			<!-- add old 3 in 1 board modal -->
			<div class="modal fade" id="add_old_3_in_1_board_in_reception_modal" tabindex="-1" role="dialog" aria-labelledby="add_old_3_in_1_board_in_reception_modal_title" aria-hidden="true" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="add_old_3_in_1_board_in_reception_title">Add 3 in 1 Board</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="add_old_3_in_1_board_in_reception_form">
									<div class="form-group">
										<label class="h6" for="id_old_3_in_1_board_in_reception">3 in 1 Board ID</label> <span></span>
										<input type="text" class="form-control" name="" id="id_old_3_in_1_board_in_reception" aria-describedby="" placeholder="3 in Board ID">
									</div>
									<div class="form-group">
										<label class="h6" for="remark_for_old_3_in_1_board_in_reception">Comments</label> <span></span>
										<textarea class="form-control" name="reception_pmt_block_remark_count_info" id="remark_for_old_3_in_1_board_in_reception" rows="1" placeholder="Additional comments"></textarea>
									</div>
									<div class="form-group"> 
										<span style="position:absolute; right:5%; bottom:0" id="remark_for_old_3_in_1_board_in_reception_count">0/255</span>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" class="btn btn-sm btn-success add_old_board_3_in_1_in_reception_form_submit notReady"><i class="fas fa-check pr-1"></i>Submit</button>
								<button type="button" class="btn btn-sm btn-default add_old_board_3_in_1_in_reception_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
			    </div>

			<!-- Add ppr blade Modal -->
			<div class="modal fade" id="add_ppr_blade_modal" tabindex="-1" role="dialog" aria-labelledby="add_ppr_blade_modal_title" aria-hidden="true" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="add_ppr_blade_modal_title">Add PPR Blade</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="add_ppr_blade_form">
									<div class="form-group">
										<label class="h6" for="ppr_blade_id">PPR Blade</label> <span id="ppr_blade_id_info" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="id_ppr_blade" id="id_ppr_blade" aria-describedby="" placeholder="PPR BLade ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_rack">Rack Number</label>
										<input type="text" class="form-control" name="id_rack" id="rack_number_of_ppr_blade" aria-describedby="" placeholder="Rack Number">
									</div>
									<div class="form-group">
										<label class="h6" for="id_shelf">Shelf Number</label>
										<input type="text" class="form-control" name="id_shelf" id="shelf_number_of_ppr_blade" aria-describedby="" placeholder="Shelf Number">
									</div>
									<div class="form-group">
										<label class="h6" for="id_slot">Slot Number</label>
										<input type="text" class="form-control" name="id_slot" id="slot_number_of_ppr_blade" aria-describedby="" placeholder="Slot Number">
									</div>
									<div class="form-group">
										<label class="h6" for="id_production">Production Batch</label>
										<input type="text" class="form-control" name="id_production" id="production_batch_of_ppr_blade" aria-describedby="" placeholder="Production Batch">
									</div>
									<div class="form-group">
										<label class="h6" for="id_hardware">Hardware Version</label>
										<input type="text" class="form-control" name="vers" id="hardware_version_of_ppr_blade" aria-describedby="" placeholder="Hardware Version">
									</div>
									<div class="form-group">
										<label class="h6" for="id_firmware">Firmware Version</label>
										<input type="text" class="form-control" name="vers" id="firmware_version_of_ppr_blade" aria-describedby="" placeholder="Firmware Version">
									</div>
									<div class="form-group">
										<label class="h6" for="date">Installation Date</label>
										<input type="text" class="form-control" name="date" id="instalation_date_of_ppr_blade" aria-describedby="" placeholder="Date">
									</div>
									<div class="form-group">
										<label class="h6" for="remark">Comments</label>
										<textarea class="form-control" name="remark" id="ppr_blade_remark" rows="1" placeholder="Additional comments"></textarea>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" form="add_ppr_blade_form" class="btn btn-sm btn-success add_ppr_blade_form_submit notReady"><i class="fas fa-check pr-1"></i>Save</button>
								<button type="button" class="btn btn-sm btn-default add_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Add HV Distribution board -->
				<div class="modal fade" id="add_hv_distribution_board_modal" tabindex="-1" role="dialog" aria-labelledby="add_hv_distribution_board_modal_title" aria-hidden="true" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="add_hv_distribution_board_modal_title">Add HV Distribution Board</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="add_hv_distribution_board_form">
									<div class="form-group">
										<label class="h6" for="hv_distribution_board_id">HV Distribution board</label> <span id="hv_distribution_board_id_info" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="id_hv_distribution_board" id="id_hv_distribution_board" aria-describedby="" placeholder="HV Distribution board ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_mini_drawer_of_hv_distribution_board">Mini Drawer</label> <span id="mini_drawer_of_hv_distribution_board_info" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="id_mini_drawer_of_hv_distribution_board" id="id_mini_drawer_of_hv_distribution_board" aria-describedby="" placeholder="Mini Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="hv_distribution_board_status">Status</label>
										<input type="text" class="form-control" name="status" id="hv_distribution_board_status" aria-describedby="" placeholder="Please enter status e.g. delivered">
									</div>
									<div class="form-group">
										<label class="h6" for="hv_distribution_board_current_location">Current location</label>
										<input type="text" class="form-control" name="current_location" id="hv_distribution_board_current_location" aria-describedby="" placeholder="Please enter current location">
									</div>
									<div class="form-group">
										<label class="h6" for="remark">Comments</label>
										<textarea class="form-control" name="remark" id="hv_distribution_board_remark" rows="1" placeholder="Additional comments"></textarea>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" form="add_hv_distribution_board_form" class="btn btn-sm btn-success add_hv_distribution_board_form_submit notReady"><i class="fas fa-check pr-1"></i>Save</button>
								<button type="button" class="btn btn-sm btn-default add_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
				</div>

				<!-- add daughterboard modal -->
				<div class="modal fade" id="add_daughterboard_modal" tabindex="-1" role="dialog" aria-labelledby="add_daughterboard_modal_title" aria-hidden="true" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="add_daughterboard_modal_title">Add Daughterboard</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="add_daughterboard_form">
									<div class="form-group">
										<label class="h6" for="daughterboard_id">Daughterboard</label> <span id="daughterboard_id_info" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="id_fenics" id="id_daughterboard" aria-describedby="" placeholder="Daughterboard ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_mini_drawer_of_daughterboard">Mini Drawer</label> <span id="daughterboard_mini_drawer_info" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="id_mini_drawer_of_daughterboard" id="id_mini_drawer_of_daughterboard" aria-describedby="" placeholder="Mini Drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="daughterboard_status">Status</label>
										<input type="text" class="form-control" name="status" id="daughterboard_status" aria-describedby="" placeholder="Please enter status e.g. delivered">
									</div>
									<div class="form-group">
										<label class="h6" for="daughterboard_current_location">Current location</label>
										<input type="text" class="form-control" name="current_location" id="daughterboard_current_location" aria-describedby="" placeholder="Please enter current location">
									</div>
									<div class="form-group">
										<label class="h6" for="remark">Comments</label>
										<textarea class="form-control" name="remark" id="daughterboard_remark" rows="1" placeholder="Additional comments"></textarea>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" form="add_daughterboard_form" class="btn btn-sm btn-success add_daughterboard_form_submit notReady"><i class="fas fa-check pr-1"></i>Save</button>
								<button type="button" class="btn btn-sm btn-default add_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
				</div>

				<!-- add mainboard modal -->
				<div class="modal fade" id="add_mainboard_modal" tabindex="-1" role="dialog" aria-labelledby="add_mainboard_modal_title" aria-hidden="true" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="add_mainboard_modal_title">Add Mainboard</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="add_mainboard_form">
									<div class="form-group">
										<label class="h6" for="mainboard_id">Mainboard</label> <span id="mainboard_id_info" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="id_mainboard" id="id_mainboard" aria-describedby="" placeholder="Mainboard ID">
									</div>
									<div class="form-group">
										<label class="h6" for="id_mini_drawer_of_mainboard">Mini Drawer</label> <span id="mainboard_mini_drawer_info" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="id_mini_drawer_of_mainboard" id="id_mini_drawer_mainboard" aria-describedby="" placeholder="Mini drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="mainboard_status">Status</label>
										<input type="text" class="form-control" name="status" id="mainboard_status" aria-describedby="" placeholder="Please enter status e.g. delivered">
									</div>
									<div class="form-group">
										<label class="h6" for="mainboard_current_location">Current location</label>
										<input type="text" class="form-control" name="current_location" id="mainboard_current_location" aria-describedby="" placeholder="Please enter current location">
									</div>
									<div class="form-group">
										<label class="h6" for="mainboard_current_location">Test Results</label>
										<input type="file" class="form-control" name="current_location" id="mainboard_test_results" aria-describedby="" placeholder="Please enter current location">
									</div>
									<div class="form-group">
										<label class="h6" for="remark">Comments</label>
										<textarea class="form-control" name="remark" id="mainboard_remark" rows="1" placeholder="Additional comments"></textarea>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" form="add_mainboard_form" class="btn btn-sm btn-success add_mainboard_form_submit notReady"><i class="fas fa-check pr-1"></i>Save</button>
								<button type="button" class="btn btn-sm btn-default add_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
				</div>

			<!-- Add HV Divider --> 
			<div class="modal fade" id="add_hv_divider_modal" tabindex="-1" role="dialog" aria-labelledby="add_hv_divider_modal_title" aria-hidden="true" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="add_hv_divider_modal_title">Add HV Divider</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="add_hv_divider_form">
									<div class="form-group">
										<label class="h6" for="add_new_hv_divider_in_reception">HV Divider</label> <span id="hv_divider_id_info" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="id_hv_divider" id="add_new_hv_divider_in_reception" aria-describedby="" placeholder="HV Divider ID">
									</div>
									<div class="form-group whatever">
										<label class="h6" for="id_pmt_block_hv_divider">PMT block</label> <span id="pmt_block_of_fenics_id_info" style="margin-left: 30px" ></span>
										<input type="text" class="form-control" name="id_pmt_block_hv_divider" id="id_pmt_block_hv_divider" aria-describedby="" placeholder="Pmt Block ID">
									</div>
									<div class="form-group">
										<label class="h6" for="hv_divider_status">Status</label>
										<input type="text" class="form-control" name="status" id="hv_divider_status" aria-describedby="" placeholder="Please enter status e.g. delivered">
									</div>
									<div class="form-group">
										<label class="h6" for="hv_divider_current_location">Current location</label>
										<input type="text" class="form-control" name="current_location" id="hv_divider_current_location" aria-describedby="" placeholder="Please enter current location">
									</div>
									<div class="form-group">
										<label class="h6" for="remark">Comments</label>
										<textarea class="form-control" name="remark" id="hv_divider_remark" rows="1" placeholder="Additional comments"></textarea>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" form="add_hv_divider_form" class="btn btn-sm btn-success add_hv_divider_form_submit notReady"><i class="fas fa-check pr-1"></i>Save</button>
								<button type="button" class="btn btn-sm btn-default add_mini_drawer_form_cancel hv_divider_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
				</div>


				<!-- Edit Fenics Modal -->
				<div class="modal fade" id="edit_fenics_modal" tabindex="-1" role="dialog" aria-labelledby="edit_fenics_modal_title" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_fenics_modal_title">Update Fenics</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_fenics_form">
										<div class="form-group">
											<label class="h6" for="fenics_id">Fenics</label>
											<input type="text" class="form-control" name="id_fenics" id="edit_id_fenics" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="fenics_pmt_block">Pmt Block</label>
											<input type="text" class="form-control" name="id_pmt" id="edit_id_fenics_pmt_block" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="fenics_status">Status</label>
											<input type="text" class="form-control" name="status" id="edit_fenics_status" aria-describedby="" placeholder="Please enter status e.g. delivered">
										</div>
										<div class="form-group">
											<label class="h6" for="fenics_current_location">Current location</label>
											<input type="text" class="form-control" name="current_location" id="edit_fenics_current_location" aria-describedby="" placeholder="Please enter current location">
										</div>
										<div class="form-group">
											<label class="h6" for="remark">Comments</label>
											<textarea class="form-control" name="remark" id="edit_fenics_remark" rows="1" placeholder="Additional comments"></textarea>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_fenics_form" class="btn btn-sm btn-success edit_fenics_form_submit" id="edit_fenics_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>

				<!-- Edit Old hv divider Modal -->
				<div class="modal fade" id="edit_old_hv_divider_modal" tabindex="-1" role="dialog" aria-labelledby="edit_old_hv_divider_modal_title" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_old_hv_divider_modal_title">Update HV Divider</h5>
									<button type="button" class="close close_btn_of_old_hv_divider" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_old_hv_divider_form">
										<div class="form-group">
											<label class="h6" for="edit_old_hv_divider">HV Divider</label> <span></span>
											<input type="text" class="form-control" name="" id="edit_old_hv_divider" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="edit_id_pmt_block_of_old_hv_divider">PMT Block</label> <span></span>
											<input type="text" class="form-control" name="" id="edit_id_pmt_block_of_old_hv_divider" aria-describedby="" placeholder="PMT Block ID">
										</div>
										<div class="form-group">
											<label class="h6" for="remark">Comments</label>
											<textarea class="form-control" name="remark" id="edit_remark_of_old_hv_divider" rows="1" placeholder="Additional comments"></textarea>
										</div>
										<div class="form-group"> 
											<span style="position:absolute; right:5%; bottom:0" id="edit_remark_of_old_hv_divider_count">0/255</span>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_old_hv_divider_form" class="btn btn-sm btn-success edit_old_hv_divider_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_old_hv_divider_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>

					<!-- edit old board 3 in 1  -->
					<div class="modal fade" id="edit_old_board_3_in_1_modal" tabindex="-1" role="dialog" aria-labelledby="edit_old_board_3_in_1_modal_title" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_old_board_3_in_1_modal_title">Update Board 3 in 1</h5>
									<button type="button" class="close close_btn_of_old_board_3_in_1" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_old_board_3_in_1_form">
										<div class="form-group">
											<label class="h6" for="edit_old_board_3_in_1">Board 3 in 1</label> <span></span>
											<input type="text" class="form-control" name="" id="edit_old_board_3_in_1" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="edit_id_pmt_block_of_old_board_3_in_1">PMT Block</label> <span></span>
											<input type="text" class="form-control" name="" id="edit_id_pmt_block_of_old_board_3_in_1" aria-describedby="" placeholder="PMT Block ID">
										</div>
										<div class="form-group">
											<label class="h6" for="remark">Comments</label>
											<textarea class="form-control" name="remark" id="edit_remark_of_old_board_3_in_1" rows="1" placeholder="Additional comments"></textarea>
										</div>
										<div class="form-group"> 
											<span style="position:absolute; right:5%; bottom:0" id="edit_remark_of_old_board_3_in_1_count">0/255</span>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_old_hv_divider_form" class="btn btn-sm btn-success edit_old_board_3_in_1_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_old_board_3_in_1_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>

					<!-- Edit ppr blade -->
					<div class="modal fade" id="edit_ppr_blade_modal" tabindex="-1" role="dialog" aria-labelledby="edit_ppr_blade_modal_title" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_ppr_blade_modal_title">Update PPR Blade</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_ppr_blade_form">
										<div class="form-group">
											<label class="h6" for="ppr_blade_id">PPR Blade</label>
											<input type="text" class="form-control" name="id_ppr_blade" id="edit_id_ppr_blade" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="Rack_number">Rack Number</label>
											<input type="text" class="form-control" name="" id="edit_rack_number_of_ppr_blade" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="shelf_number">Shelf Number</label>
											<input type="text" class="form-control" name="" id="edit_slhef_number_of_ppr_blade" aria-describedby="" placeholder="Please enter status e.g. delivered">
										</div>
										<div class="form-group">
											<label class="h6" for="Slot_number">Slot Number</label>
											<input type="text" class="form-control" name="" id="edit_slot_number_of_ppr_blade" aria-describedby="" placeholder="Please enter current location">
										</div>
										<div class="form-group">
											<label class="h6" for="Production batch">Production Batch</label>
											<input type="text" class="form-control" name="" id="edit_production_batch_of_ppr_blade" aria-describedby="" placeholder="Please enter current location">
										</div>
										<div class="form-group">
											<label class="h6" for="Version">Hardware Version</label>
											<input type="text" class="form-control" name="" id="edit_hardware_version_of_ppr_blade" aria-describedby="" placeholder="Please enter current location">
										</div>
										<div class="form-group">
											<label class="h6" for="Version">firmware Version</label>
											<input type="text" class="form-control" name="" id="edit_firmware_version_of_ppr_blade" aria-describedby="" placeholder="Please enter current location">
										</div>
										<div class="form-group">
											<label class="h6" for="date">Installation date</label>
											<input type="text" class="form-control" name="" id="edit_installation_date_of_ppr_blade" aria-describedby="" placeholder="Please enter current location">
										</div>
										<div class="form-group">
											<label class="h6" for="remark">Comments</label>
											<textarea class="form-control" name="remark" id="edit_ppr_blade_remark" rows="1" placeholder="Additional comments"></textarea>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_ppr_blade_form" class="btn btn-sm btn-success edit_ppr_blade_form_submit" id="edit_ppr_blade_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>

				<!-- Edit HV Distribution board Modal -->
				<div class="modal fade" id="edit_hv_distribution_board_modal" tabindex="-1" role="dialog" aria-labelledby="edit_hv_distribution_board_modal_title" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_hv_distribution_board_modal_title">Update HV Distribution board</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_hv_distribution_board_form">
										<div class="form-group">
											<label class="h6" for="hv_distribution_board_id">HV Distributon board</label>
											<input type="text" class="form-control" name="id_hv_distribution_board" id="edit_id_hv_distribution_board" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="mini_drawer_of_hv_distribution_board">Mini Drawer</label>
											<input type="text" class="form-control" name="id_pmt" id="edit_id_mini_drawer_of_hv_distribution_board" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="hv_distribution_board_status">Status</label>
											<input type="text" class="form-control" name="status" id="edit_hv_distribution_board" aria-describedby="" placeholder="Please enter status e.g. delivered">
										</div>
										<div class="form-group">
											<label class="h6" for="hv_distribution_board_current_location">Current location</label>
											<input type="text" class="form-control" name="current_location" id="edit_hv_distribution_board_current_location" aria-describedby="" placeholder="Please enter current location">
										</div>
										<div class="form-group">
											<label class="h6" for="remark">Comments</label>
											<textarea class="form-control" name="remark" id="edit_hv_distribution_board_remark" rows="1" placeholder="Additional comments"></textarea>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_hv_distribution_board_form" class="btn btn-sm btn-success edit_hv_distribution_board_form_submit" id="edit_hv_distribution_board_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>

				<!-- Edit mainboard Modal -->
				<div class="modal fade" id="edit_mainboard_modal" tabindex="-1" role="dialog" aria-labelledby="edit_mainboard_modal_title" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_mainboard_modal_title">Update Mainboard</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_mainboard_form">
										<div class="form-group">
											<label class="h6" for="mainboard_id">Mainboard</label>
											<input type="text" class="form-control" name="edit_id_mainboard" id="edit_id_mainboard" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="mainboard_mini_drawer">Mini drawer</label>
											<input type="text" class="form-control" name="id_mini_drawer" id="edit_id_mini_drawer_of_mainboard" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="fenics_status">Status</label>
											<input type="text" class="form-control" name="status" id="edit_mainboard_status" aria-describedby="" placeholder="Please enter status e.g. delivered">
										</div>
										<div class="form-group">
											<label class="h6" for="mainboard_current_location">Current location</label>
											<input type="text" class="form-control" name="current_location" id="edit_mainboard_current_location" aria-describedby="" placeholder="Please enter current location">
										</div>
										<div class="form-group">
											<label class="h6" for="remark">Comments</label>
											<textarea class="form-control" name="remark" id="edit_mainboard_remark" rows="1" placeholder="Additional comments"></textarea>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_mainboard_form" class="btn btn-sm btn-success edit_mainboard_form_submit" id="edit_mainboard_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>
				<!-- Edit daughterboard modal  -->
				<div class="modal fade" id="edit_daughterboard_modal" tabindex="-1" role="dialog" aria-labelledby="edit_daughterboard_modal_title" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_daughterboard_modal_title">Update Daughterboard</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_daughterboard_form">
										<div class="form-group">
											<label class="h6" for="daughterboard_id">Daughterboard</label>
											<input type="text" class="form-control" name="edit_id_daughterboard" id="edit_id_daughterboard" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="daughterboard_mini_drawer">Mini drawer</label>
											<input type="text" class="form-control" name="id_mini_drawer" id="edit_id_mini_drawer_of_daughterboard" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="daughterboard_status">Status</label>
											<input type="text" class="form-control" name="status" id="edit_daughterboard_status" aria-describedby="" placeholder="Please enter status e.g. delivered">
										</div>
										<div class="form-group">
											<label class="h6" for="daughterboard_current_location">Current location</label>
											<input type="text" class="form-control" name="current_location" id="edit_daughterboard_current_location" aria-describedby="" placeholder="Please enter current location">
										</div>
										<div class="form-group">
											<label class="h6" for="remark">Comments</label>
											<textarea class="form-control" name="remark" id="edit_daughterboard_remark" rows="1" placeholder="Additional comments"></textarea>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_daughterboard_form" class="btn btn-sm btn-success edit_daughterboard_form_submit" id="edit_daughterboard_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>

				<!-- Edit HV_divider Modal -->
				<div class="modal fade" id="edit_hv_divider_modal" tabindex="-1" role="dialog" aria-labelledby="edit_hv_divider_title" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_hv_divider_modal_title">Update HV_divider</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_hv_divider_form">
										<div class="form-group">
											<label class="h6" for="hv_divider_id">HV_Divider</label>
											<input type="text" class="form-control" name="id_hv_divider" id="edit_id_hv_divider" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="hv_divider_pmt_block">Pmt Block</label>
											<input type="text" class="form-control" name="id_pmt" id="edit_id_hv_divider_pmt_block" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="hv_divider_status">Status</label>
											<input type="text" class="form-control" name="status" id="edit_hv_divider_status" aria-describedby="" placeholder="Please enter status e.g. delivered">
										</div>
										<div class="form-group">
											<label class="h6" for="hv_divider_current_location">Current location</label>
											<input type="text" class="form-control" name="current_location" id="edit_hv_divider_current_location" aria-describedby="" placeholder="Please enter current location">
										</div>
										<div class="form-group">
											<label class="h6" for="remark">Comments</label>
											<textarea class="form-control" name="remark" id="edit_hv_divider_remark" rows="1" placeholder="Additional comments"></textarea>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_fenics_form" class="btn btn-sm btn-success edit_hv_divider_form_submit" id="edit_hv_divider_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>

			 <!-- Assembly Mini Drawer Modal -->
			<div class="modal fade" id="add_mini_drawer_modal" tabindex="-1" role="dialog" aria-labelledby="add_mini_drawer_modal_title" aria-hidden="true" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="add_mini_drawer_modal_title">Assembly Mini Drawer</h5> 
								<button type="button" class="close close_assembly_mini_drawer_window" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="post" action="" id="add_mini_drawer_form">
									<div class="form-group">
										<label class="h6" for="assembly_mini_drawer_id">Mini Drawer</label> <span id=""></span>
										<input type="text" class="form-control" name="assembly_mini_drawer_input" id="assembly_mini_drawer_id" aria-describedby="" placeholder="Mini drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_super_drawer_id_of_mini_drawer">Super Drawer</label> <span id="" ></span>
										<input type="text" class="form-control" name="assembly_mini_drawer_input" id="assembly_super_drawer_id_of_mini_drawer" aria-describedby="" placeholder="Super drawer ID">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_module_id_of_mini_drawer">Module</label> <span id="" ></span>
										<input type="text" class="form-control" name="assembly_mini_drawer_input" id="assembly_module_id_of_mini_drawer" aria-describedby="" placeholder="Module ID">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_mainboard_id_of_mini_drawer">Mainboard</label> <span id="mainboard_id_info_of_assembly_mini_drawer"></span>
										<input type="text" class="form-control" name="assembly_mini_drawer_input" id="assembly_mainboard_id_of_mini_drawer" aria-describedby="" placeholder="Mainboard ID">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_daughterboard_id_of_mini_drawer">Daughterboard</label> <span id="daughterboard_id_info_of_assembly_mini_drawer" ></span>
										<input type="text" class="form-control" name="assembly_mini_drawer_input" id="assembly_daughterboard_id_of_mini_drawer" aria-describedby="" placeholder="Daughterboard ID">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_hv_distribution_board_id_of_mini_drawer">HV Distribution board</label> <span id="hv_distribution_board_id_info_of_assembly_mini_drawer"></span>
										<input type="text" class="form-control" name="assembly_mini_drawer_input" id="assembly_hv_distribution_board_id_of_mini_drawer" aria-describedby="" placeholder="HV Distribution board ID">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_pmt_block_id_of_mini_drawer">PMT Block</label> <span id="pmt_block_id_info_of_assembly_mini_drawer" ></span>
										<input type="text" class="form-control" name="assembly_mini_drawer_input" id="assembly_pmt_block_id_of_mini_drawer" aria-describedby="" placeholder="PMT Block ID">
									</div>
									<div class="form-group">
										<label class="h6" for="assembly_pmt_block_position_of_mini_drawer">Position of PMT Block</label> <span id="" ></span>
										<input type="text" class="form-control" name="assembly_mini_drawer_input" id="assembly_pmt_block_position_of_mini_drawer" aria-describedby="" placeholder="Position of PMT Block">
									</div>
									<div class="form-group">
										<label class="h6" for="current_location_of_mini_drawer">Current Location</label> <span id="" ></span>
										<input type="text" class="form-control" name="" id="current_location_of_mini_drawer" aria-describedby="" placeholder="Location of Mini Drawer">
									</div>
									<div class="form-group">
										<label class="h6" for="mini_drawer_remark">Comments</label>
										<textarea class="form-control" name="remark" id="mini_drawer_remark" rows="1" placeholder="Additional comments"></textarea>
									</div>
									<div class="form-group"> 
										<span style="position:absolute; right:5%; bottom:0" id="mini_drawer_remark_count">0/255</span>
									</div>
								</form>
							</div>
							<div class="modal-footer">							
								<button type="button" form="add_mini_drawer_form" class="btn btn-sm btn-success add_mini_drawer_form_submit"><i class="fas fa-check pr-1"></i>Save</button>
								<button type="button" class="btn btn-sm btn-default add_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
							</div>
						</div>
					</div>
				</div>
				<!-- Edit Mini Drawer Modal -->
				<div class="modal fade" id="edit_mini_drawer_modal" tabindex="-1" role="dialog" aria-labelledby="edit_mini_drawer_modal_title" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_mini_drawer_modal_title">Update Mini Drawer</h5>
									<button type="button" class="close close_btn_of_update_assembly_mini_drawer_window" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_mini_drawer_form">
										<span id="originValOfMiniDrawer"></span>
										<div class="form-group">
											<label class="h6" for="edit_id_of_assembly_mini_drawer">Mini Drawer</label> <span id="edit_id_of_assembly_mini_drawer_info"></span>
 											<input type="text" class="form-control" name="edit_assembly_of_mini_drawer" id="edit_id_of_assembly_mini_drawer" aria-describedby="" placeholder="Mini Drawer ID">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_super_drawer_id_of_assembly_mini_drawer">Super Drawer</label> <span id="edit_super_drawer_id_of_assembly_mini_drawer_info"></span>
											<input type="text" class="form-control" name="edit_assembly_of_mini_drawer" id="edit_super_drawer_id_of_assembly_mini_drawer" aria-describedby="" placeholder="Super Drawer ID">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_module_id_of_assembly_mini_drawer">Module</label> <span id="edit_module_id_of_assembly_mini_drawer_info"></span>
											<input type="text" class="form-control" name="edit_assembly_of_mini_drawer" id="edit_module_id_of_assembly_mini_drawer" aria-describedby="" placeholder="Module ID">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_mainboard_id_of_mini_drawer">Mainboard</label> <span id="edit_id_mainboard_info_of_assembly_mini_drawer"></span>
											<input type="text" class="form-control" name="edit_assembly_of_mini_drawer" id="edit_mainboard_id_of_mini_drawer" aria-describedby="" placeholder="Mainboard ID">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_daughterboard_id_of_mini_drawer">Daughterboard</label> <span id="edit_id_daughterboard_info_of_assembly_mini_drawer"></span>
											<input type="text" class="form-control" name="edit_assembly_of_mini_drawer" id="edit_daughterboard_id_of_mini_drawer" aria-describedby="" placeholder="Daughterboard ID">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_hv_distribution_board_id_of_mini_drawer">HV Distribution board</label> <span id="edit_id_hv_distribution_board_info_of_assembly_mini_drawer"></span>
											<input type="text" class="form-control" name="edit_assembly_of_mini_drawer" id="edit_hv_distribution_board_id_of_mini_drawer" aria-describedby="" placeholder="HV Distribution board ID">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_pmt_block_id_of_mini_drawer">PMT Block</label> <span id="edit_id_pmt_block_info_of_assembly_mini_drawer"></span>
											<input type="text" class="form-control" name="edit_assembly_of_mini_drawer" id="edit_pmt_block_id_of_mini_drawer" aria-describedby="" placeholder="PMT Block ID">
										</div>
										<div class="form-group">
											<label class="h6" for="edit_id_of_position_of_pmt_block_of_mini_drawer">Position of PMT Block</label> <span id="edit_id_pmt_block_position_info_of_assembly_mini_drawer"></span>
											<input type="text" class="form-control" name="edit_assembly_of_mini_drawer" id="edit_id_of_position_of_pmt_block_of_mini_drawer" aria-describedby="" placeholder="Position of PMT Block">
										</div>
										<div class="form-group">
											<label class="h6" for="">Current Location</label>
											<input type="text" class="form-control" name="edit_assembly_of_mini_drawer" id="edit_current_location_of_mini_drawer" aria-describedby="">
										</div>
										<div class="form-group">
											<label class="h6" for="">Comments</label>
											<textarea class="form-control" name="remark" id="edit_comments_of_mini_drawer" rows="1" placeholder="Additional comments"></textarea>
										</div>
										<div class="form-group"> 
											<span style="position:absolute; right:5%; bottom:0" id="edit_comments_of_mini_drawer_count">0/255</span>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_mini_drawer_form" class="btn btn-sm btn-success edit_mini_drawer_form_submit" id=""><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_mini_drawer_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>
				<!-- Edit Pmt Modal -->
				<div class="modal fade" id="edit_pmt_modal" tabindex="-1" role="dialog" aria-labelledby="edit_pmt_modal_title" aria-hidden="true" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="edit_pmt_modal_title">Update PMT</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="post" action="" id="edit_pmt_form">
										<div class="form-group">
											<label class="h6" for="id_pmt_frame">PMT</label>
											<input type="text" class="form-control" name="id_pmt_frame" id="edit_id_pmt_frame" aria-describedby="" readonly>
										</div>
										<div class="form-group">
											<label class="h6" for="pmt_status">Status</label>
											<input type="text" class="form-control" name="status" id="edit_pmt_status" aria-describedby="" placeholder="Please enter status e.g. delivered">
											<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
										</div>
										<div class="form-group">
											<label class="h6" for="pmt_current_location">Current location</label>
											<input type="text" class="form-control" name="current_location" id="edit_pmt_current_location" aria-describedby="" placeholder="Please enter current location">
											<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
										</div>
										<div class="form-group">
											<label class="h6" for="remark">Comments</label>
											<textarea class="form-control" name="remark" id="edit_pmt_remark" rows="1" placeholder="Additional comments"></textarea>
										</div>
									</form>
								</div>
								<div class="modal-footer">							
									<button type="button" form="edit_pmt_form" class="btn btn-sm btn-success edit_pmt_form_submit" id="edit_pmt_form_submit"><i class="fas fa-check pr-1"></i>Update</button>
									<button type="button" class="btn btn-sm btn-default edit_pmt_form_cancel" data-dismiss="modal"><i class="fas fa-exclamation-triangle pr-1"></i>Cancel</button>
								</div>
							</div>
						</div>
					</div>
				<div class="row no-gutters">
					<nav class="col bg-light sidebar">
						<div class="sidebar-header px-3 d-flex align-items-center">
							<a class="navbar-brand" href="#">
							TILECAL ELECTRONICS <b>DATABASE</b>
							</a>
						</div>
						<ul class="tc-tree pt-3">
							<li class="">
								<a id="tc" href="#">
									<i class="far fa-minus-square tc-open-tree trc-open-tree-first"></i>
									<span class="tile-calorimeter">Tile Calorimeter</span>
								</a>
								<ul class="tree tc-tree1">
									<li class="tree-1-li last-component">
										<a id="partition" class="level-2" href="#">
											<i class="far fa-minus-square tc-open-tree"></i>
											<span class="tree-child tree-1-child" id="show_partition_table">Partition <span class="badge badge-success">Filled</span></span>
										</a>
										<ul class="tree tc-tree2">
											<li class="tree-2-li" style="border-left:1px solid #333;">
												<a href="#">
													<i class="far fa-minus-square tc-open-tree"></i> 
													<span class="tree-child tree-2-child" id="show_module_table">Module <span class="badge badge-success">Filled</span></span> 
												</a>
												<ul class="module-tree3 tree tc-tree3">
													<li class="tree-3-li "style="border-left:1px solid #333;">
														<a href="#"> 
															<i class="far fa-minus-square tc-open-tree"></i>
															<span class="tree-child tree-3-child" id="show_super_drawer_table">Super Drawer</span> <span class="badge badge-success">New</span>
															
														</a>
														<ul class="superdrawer-tree-4 tree tc-tree4">
															<li class="tree-4-li" style="border-left:1px solid #333;">
																<a href="#">
																	<i class="far fa-minus-square tc-open-tree"></i>
																	<span class="tree-child tree-4-child" id="show_micro_drawer_table">Micro drawer</span>
																	
																</a>
																<ul class="tree tc-tree5">
																	<li class="tree-5-li last-component"> 
																		<a href="#">
																			<i class="far fa-minus-square tc-open-tree"></i> 
																			<span class="tree-child tree-5-child" id="show_pmt_block_table_of_micro_drawer">PMT block</span>
																			
																		</a>
																		<ul class="tree tc-tree6">
																			<li class="tree-6-li" style="border-left:1px solid #333;">
																				<a href="#">
																					<span class="tree-child tree-6-child" id="show_fenics_table">FENICS</span>
																				</a>
																			</li>
																			<li class="tree-6-li" style="border-left:1px solid #333;">
																				<a href="#">
																					<span class="tree-child tree-6-child" id="show_hv_divider_table">HV divider</span>
																					
																				</a>
																			</li>
																			<li class="tree-6-li last-component">
																				<a href="#" class="show_pmt_table">
																					<span class="tree-child tree-6-child">PMT <span class="badge badge-info">Filled with old Data</span></span>																					
																				</a>
																			</li>
																		</ul>
																	</li>
																</ul>
															</li>
															<li class="tree-4-li last-component">
																<a href="#">
																	<i class="far fa-minus-square tc-open-tree"></i>
																	<span class="tree-child tree-4-child" id="show_mini_drawer_table">Mini drawer</span>
																	
																</a>
																
																<ul class="mini-drawer-tree5 tree tc-tree5">
																	<li class="tree-5-li" style="border-left:1px solid #333;">
																		<a href="#">
																			<span class="tree-child tree-5-child" id="show_mainboard_table">Mainboard</span>
																		</a>
																	</li>
																	<li class="tree-5-li" style="border-left:1px solid #333;">
																		<a href="#">
																			<span class="tree-child tree-5-child" id="show_daughterboard_table">Daughterboard</span>
																			
																		</a>
																	</li>
																	<li class="tree-5-li" style="border-left:1px solid #333;">
																		<a href="#">
																			<span class="tree-child tree-5-child" id="show_hv_distribution_board_table">HV distribution board</span>
																			
																		</a>
																	</li>
																	<li class="tree-5-li last-component"> 
																		<a href="#">
																			<i class="far fa-minus-square tc-open-tree"></i> 
																			<span class="tree-child tree-5-child" id="show_pmt_block_table">PMT block</span>
																			
																		</a>
																		<ul class="pmt-block-tree6 tree tc-tree6">
																			<li class="tree-6-li" style="border-left:1px solid #333;">
																				<a href="#">
																					<span class="tree-child tree-6-child" id="show_second_fenics_table">FENICS</span>
																					
																				</a>
																			</li>
																			<li class="tree-6-li" style="border-left:1px solid #333;">
																				<a href="#">
																					<span class="tree-child tree-6-child" id="show_second_hv_divider_table">HV divider</span>
																					
																				</a>
																			</li>	
																			<li class="tree-6-li last-component ">
																				<a href="#" class="show_pmt_table">
																					<span class="tree-child tree-6-child">PMT <span class="badge badge-info">Filled with old Data</span></span>
																					
																				</a>
																			</li>
																		</ul>
																	</li>
																</ul>
															</li>
														</ul>
													</li>
												</ul>

												<!-- old -->
												<ul class="module-tree3 tree tc-tree3">
													<li class="tree-3-li last-component">
														<a href="#"> 
															<i class="far fa-minus-square tc-open-tree"></i>
															<span class="tree-child tree-3-child" id="show_old_super_drawer_table">Super Drawer</span> <span class="badge badge-warning">Old</span>
														</a>
														<ul class="superdrawer-tree-4 tree tc-tree4" edit_old_pmt_block_form_submit>
															<li class="tree-4-li last-component">
																<a href="#">
																	<i class="far fa-minus-square tc-open-tree"></i>
																	<span class="tree-child tree-4-child" id="show_old_pmt_block_table">PMT Block</span>
																</a>
																<ul class="mini-drawer-tree5 tree tc-tree5">
																	<li class="tree-5-li" style="border-left:1px solid #333;">
																		<a href="#">
																			<span class="tree-child tree-5-child" id="show_old_hv_divider_table">HV Divider</span> 
																		</a>
																	</li>
																	<li class="tree-5-li" style="border-left:1px solid #333;">
																		<a href="#">
																			<span class="tree-child tree-5-child" id="show_old_3_in_1_board_table">3 in 1 Board</span>
																			
																		</a>
																	</li>
																	<li class="tree-5-li last-component">
																		<a href="#">
																			<span class="tree-child tree-5-child" id="show_old_pmt_table">PMT</span>			
																		</a>
																	</li>
																</ul>
															</li>
														</ul>
													</li>
												</ul>
											</li> 
										</ul> 
										<ul class="tree tc-tree2 no-pt">
											<li class="tree-2-li last-component">
												<a href="#">
													<i class="far fa-minus-square tc-open-tree"></i>
													<span class="tree-child tree-2-child" id="show_ppr_blade_table">PPr blade</span>
													 
												</a>
												<ul class="tree tc-tree3">
													<li class="tree-3-li" style="border-left:1px solid #333;">
														<a href="#">
															<i class="far fa-plus-square tc-open-tree"></i>
															<span class="tree-child tree-3-child ">Carrier board</span>
															 
														</a>
														<ul class="tree tc-tree4" style="display:none">
															<li class="tree-4-li" style="border-left:1px solid #333;">
																<a class="tree-a" href="#">
																	<span class="tree-child tree-4-child">IPMC</span> 
																	
																</a>
															</li>
															<li class="tree-4-li" style="border-left:1px solid #333;">
																<a class="tree-a" href="#">
																	<span class="tree-child tree-4-child">CPM</span> 
																	
																</a>
															</li>
															<li class="tree-4-li" style="border-left:1px solid #333;">
																<a class="tree-a" href="#">
																	<span class="tree-child tree-4-child">TileCoM</span> 
																	
																</a>
															</li>
															<li class="tree-4-li last-component">
																<a class="tree-a" href="#">
																	<span class="tree-child tree-4-child">GbE switch</span>
																	 
																</a>
															</li>
														</ul>
													</li>
												</ul> 
												<ul class="tree tc-tree3">
													<li class="tree-3-li last-component">
														<a href="#">
															<span class="tree-child tree-3-child ">TDAQi</span> 
															
														</a>
													</li>
												</ul>
											</li>
										</ul>
									</li>
								</ul> 
							</li> 
						</ul>


						<!-- OLD TILE CAL -->
						<ul class="tc-tree pt-3">
							<li class="">
								<a id="tc" href="#">
									<i class="far fa-minus-square tc-open-tree trc-open-tree-first"></i>
									<span class="tile-calorimeter">Tile Calorimeter <span class="badge badge-warning">Modification History</span></span>
								</a>
								<ul class="tree tc-tree1">
									<li class="tree-1-li last-component">
										<a id="partition" class="level-2" href="#">
											<i class="far fa-minus-square tc-open-tree"></i>
											<span class="tree-child tree-1-child" id="show_partition_table">Partition</span>
										</a>
										<ul class="tree tc-tree2">
											<li class="tree-2-li" style="border-left:1px solid #333;">
												<a href="#">	
													<i class="far fa-minus-square tc-open-tree"></i> 
													<span class="tree-child tree-2-child" id="show_module_table">Module</span> 
												</a>
												<ul class="module-tree3 tree tc-tree3">
													<li class="tree-3-li "style="border-left:1px solid #333;">
														<a href="#"> 
															<i class="far fa-plus-square tc-open-tree"></i>
															<span class="tree-child tree-3-child" id="show_super_drawer_table">Super Drawer <span class="badge badge-success"> New</span> </span>
															
														</a>
														<ul class="superdrawer-tree-4 tree tc-tree4" style="display:none">
															<li class="tree-4-li" style="border-left:1px solid #333;">
																<a href="#">
																	<i class="far fa-minus-square tc-open-tree"></i>
																	<span class="tree-child tree-4-child" id="show_micro_drawer_modification_table">Micro drawer</span>
																	
																</a>
																<ul class="tree tc-tree5">
																	<li class="tree-5-li last-component"> 
																		<a href="#">
																			<i class="far fa-minus-square tc-open-tree"></i> 
																			<span class="tree-child tree-5-child" id="show_pmt_block_table_of_micro_drawer">PMT block</span>
																			
																		</a>
																		<ul class="tree tc-tree6">
																			<li class="tree-6-li" style="border-left:1px solid #333;">
																				<a href="#">
																					<span class="tree-child tree-6-child" id="show_fenics_table">FENICS</span>
																				</a>
																			</li>
																			<li class="tree-6-li" style="border-left:1px solid #333;">
																				<a href="#">
																					<span class="tree-child tree-6-child" id="show_hv_divider_table">HV divider</span>
																					
																				</a>
																			</li>
																			<li class="tree-6-li last-component">
																				<a href="#" class="show_pmt_table">
																					<span class="tree-child tree-6-child">PMT <span class="badge badge-info">Filled with old Data</span></span>																					
																				</a>
																			</li>
																		</ul>
																	</li>
																</ul>
															</li>
															<li class="tree-4-li last-component">
																<a href="#">
																	<i class="far fa-minus-square tc-open-tree"></i>
																	<span class="tree-child tree-4-child" id="show_mini_drawer_table_modification_history">Mini drawer</span>
																	
																</a>
																
																<ul class="mini-drawer-tree5 tree tc-tree5">
																	<li class="tree-5-li" style="border-left:1px solid #333;">
																		<a href="#">
																			<span class="tree-child tree-5-child" id="show_mainboard_table">Mainboard</span>
																		</a>
																	</li>
																	<li class="tree-5-li" style="border-left:1px solid #333;">
																		<a href="#">
																			<span class="tree-child tree-5-child" id="show_daughterboard_table">Daughterboard</span>
																			
																		</a>
																	</li>
																	<li class="tree-5-li" style="border-left:1px solid #333;">
																		<a href="#">
																			<span class="tree-child tree-5-child" id="show_hv_distribution_board_table">HV distribution board</span>
																			
																		</a>
																	</li>
																	<li class="tree-5-li last-component"> 
																		<a href="#">
																			<i class="far fa-minus-square tc-open-tree"></i> 
																			<span class="tree-child tree-5-child" id="show_pmt_block_table">PMT block</span>
																			
																		</a>
																		<ul class="pmt-block-tree6 tree tc-tree6">
																			<li class="tree-6-li" style="border-left:1px solid #333;">
																				<a href="#">
																					<span class="tree-child tree-6-child">FENICS</span>
																					
																				</a>
																			</li>
																			<li class="tree-6-li" style="border-left:1px solid #333;">
																				<a href="#">
																					<span class="tree-child tree-6-child">HV divider</span>
																					
																				</a>
																			</li>	
																			<li class="tree-6-li last-component ">
																				<a href="#" class="show_pmt_table">
																					<span class="tree-child tree-6-child">PMT <span class="badge badge-info">Filled with old Data</span></span>
																					
																				</a>
																			</li>
																		</ul>
																	</li>
																</ul>
															</li>
														</ul>
													</li>
												</ul>

												<!-- old -->
												<ul class="module-tree3 tree tc-tree3">
													<li class="tree-3-li last-component">
														<a href="#"> 
															<i class="far fa-plus-square tc-open-tree"></i>
															<span class="tree-child tree-3-child" id="show_old_super_drawer_modification_history_table">Super Drawer <span class="badge badge-warning"> Old</span> </span> 
														</a>
														<ul class="superdrawer-tree-4 tree tc-tree4" style="display:none">
															<li class="tree-4-li last-component">
																<a href="#">
																	<i class="far fa-minus-square tc-open-tree"></i>
																	<span class="tree-child tree-4-child" id="show_old_pmt_block_modification_history_table">PMT Block</span>
																</a>
																<ul class="mini-drawer-tree5 tree tc-tree5">
																	<li class="tree-5-li" style="border-left:1px solid #333;">
																		<a href="#">
																			<span class="tree-child tree-5-child" id="show_old_hv_divider_table_modification_history">HV Divider</span> 
																		</a>
																	</li>
																	<li class="tree-5-li" style="border-left:1px solid #333;">
																		<a href="#">
																			<span class="tree-child tree-5-child" id="show_old_3_in_1_board_table_modification_history">3 in 1 Board</span>
																			
																		</a>
																	</li>
																	<li class="tree-5-li last-component">
																		<a href="#">
																			<span class="tree-child tree-5-child" id="show_old_pmt_modification_table">PMT</span>			
																		</a>
																	</li>
																</ul>
															</li>
														</ul>
													</li>
												</ul>
											</li> 
										</ul> 
										<ul class="tree tc-tree2 no-pt">
											<li class="tree-2-li last-component">
												<a href="#">
													<i class="far fa-minus-square tc-open-tree"></i>
													<span class="tree-child tree-2-child" id="show_ppr_blade_table">PPr blade</span>
													 
												</a>
												<ul class="tree tc-tree3">
													<li class="tree-3-li" style="border-left:1px solid #333;">
														<a href="#">
															<i class="far fa-plus-square tc-open-tree"></i>
															<span class="tree-child tree-3-child ">Carrier board</span>
															 
														</a>
														<ul class="tree tc-tree4" style="display:none">
															<li class="tree-4-li" style="border-left:1px solid #333;">
																<a class="tree-a" href="#">
																	<span class="tree-child tree-4-child">IPMC</span> 
																	
																</a>
															</li>
															<li class="tree-4-li" style="border-left:1px solid #333;">
																<a class="tree-a" href="#">
																	<span class="tree-child tree-4-child">CPM</span> 
																	
																</a>
															</li>
															<li class="tree-4-li" style="border-left:1px solid #333;">
																<a class="tree-a" href="#">
																	<span class="tree-child tree-4-child">TileCoM</span> 
																	
																</a>
															</li>
															<li class="tree-4-li last-component">
																<a class="tree-a" href="#">
																	<span class="tree-child tree-4-child">GbE switch</span>
																	 
																</a>
															</li>
														</ul>
													</li>
												</ul> 
												<ul class="tree tc-tree3">
													<li class="tree-3-li last-component">
														<a href="#">
															<span class="tree-child tree-3-child ">TDAQi</span> 
															
														</a>
													</li>
												</ul>
											</li>
										</ul>
									</li>
								</ul> 
							</li> 
						</ul>
					</nav>
					<div class="col main" role="main">
						<nav class="tc-navbar navbar bg-dark py-2">
							<i class="fas fa-chevron-left sidebar-collapse p-2"></i>
							<ul class="navbar-nav">							
								<li class="nav-item ">								
									<a class="nav-link" href="#">Sign out</a>
								</li>
							</ul>
						</nav> 
						<div class="col main-content pt-3">
							<div class="row d-flex justify-content-start p-1">							
								<div class="btn-group ml-3">
									<div class="btn-group" role="group">										
										<button id="disassembly-btn-grp-drop" class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Disassembly</button>
										<div class="dropdown-menu" aria-labelledby="disassembly-btn-grp-drop">
											<a class="dropdown-item dissasembly_super_drawer_modal_target" href="#" data-toggle="modal" data-target="#dissasembly_super_drawer">Super drawer</a>
											<a class="dropdown-item dissasembly_pmt_block_modal_target" href="#" data-toggle="modal" data-target="#dissasembly_pmt_block">PMT block</a>
										</div>
									</div>
									<div class="btn-group" role="group">
										<button id="disassembly-btn-grp-drop" class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Assembly</button>
										<div class="dropdown-menu" aria-labelledby="assembly-btn-grp-drop">
											<a class="dropdown-item assembly_super_drawer_modal_target" href="#" data-toggle="modal" data-target="#assembly_super_drawer">Super drawer</a>
											<a class="dropdown-item assembly_mini_drawer_modal_target" href="#" data-toggle="modal" data-target="#add_mini_drawer_modal">Mini drawer</a>
											<a class="dropdown-item assembly_micro_drawer_modal_target" href="#" data-toggle="modal" data-target="#assembly_micro_drawer">Micro drawer</a>
											<a class="dropdown-item assembly_pmt_block_target" href="#" data-toggle="modal" data-target="#assembly_pmt_block">PMT block</a>
										</div>
									</div>
									<div class="btn-group" role="group">										
										<button id="insert-btn-grp-drop" class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Insert</button>
									</div>
									<div class="btn-group" role="group">
										<button id="component-delivery-btn-grp-drop" class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Reception of components at CERN</button>
										<div class="dropdown-menu" aria-labelledby="component-delivery-btn-grp-drop">
											<a class="dropdown-item old_super_drawer_modal_target" href="#" data-toggle="modal" data-target="#add_old_super_drawer_in_reception_modal">Super Drawer <span class="badge badge-warning">Old</span></a>
											<a class="dropdown-item old_pmt_block_modal_target" href="#" data-toggle="modal" data-target="#add_old_pmt_block_in_reception_modal">PMT Block <span class="badge badge-warning">Old</span></a>
											<a class="dropdown-item old_hv_divider_modal_target" href="#" data-toggle="modal" data-target="#add_old_hv_divider_in_reception_modal">HV Divider <span class="badge badge-warning">Old</span></a>
											<a class="dropdown-item old_3_in_1_board_modal_target" href="#" data-toggle="modal" data-target="#add_old_3_in_1_board_in_reception_modal">3 in 1 Board <span class="badge badge-warning">Old</span></a>
											<a class="dropdown-item unknown" href="#" data-toggle="modal" data-target="#unknown">PMT</a>
											<a class="dropdown-item hv_divider_modal_target" href="#" data-toggle="modal" data-target="#add_hv_divider_modal">HV Divider</a>
											<a class="dropdown-item fenics_modal_target" href="#" data-toggle="modal" data-target="#add_fenics_modal">FENICS</a>
											<a class="dropdown-item mainboard_modal_target" href="#" data-toggle="modal" data-target="#add_mainboard_modal">Mainboard</a>
											<a class="dropdown-item daughterboard_modal_target" href="#" data-toggle="modal" data-target="#add_daughterboard_modal">Daughterboard</a>
											<a class="dropdown-item hv_distribution_board_modal_target" href="#" data-toggle="modal" data-target="#add_hv_distribution_board_modal">HV Distribution Board</a>
											<a class="dropdown-item unknown" href="#" data-toggle="modal" data-target="#unknwon">PMT Block</a>
											<a class="dropdown-item unknown add_mini_drawer_in_reception_target" href="#" data-toggle="modal" data-target="#add_mini_drawer_in_reception_modal">Mini Drawer</a>
											<a class="dropdown-item unknown add_micro_drawer_in_reception_target" href="#" data-toggle="modal" data-target="#add_micro_drawer_in_reception_modal">Micro Drawer</a>
											<a class="dropdown-item unknown" href="#" data-toggle="modal" data-target="#unknwon">Super Drawer</a>
											<a class="dropdown-item unknown" href="#" data-toggle="modal" data-target="#unknwon">Module</a>
										</div>
									</div>
								</div>
							</div>                       
						<div class="col main-content pt-2">
							<div class="row component-table-row partition-table-row">
								<div class="col table-section partition-col p-1">  
								<h5>Partition</h5>                          
									<div class="partition-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="partition_table">
											<thead>
												<tr>
													<th>Partition</th>
													<th>200 HV Bulk PS ID</th>
													<th>HV Bulk PS ID</th>
													<th>Comment</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div>   
								</div>	                                 
							</div>
							
							<!-- Module table -->
							<div class="row component-table-row module-table-row">
								<div class="col table-section module-col p-1">
								<h5>Module</h5> 
									<div class="module-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="module_table" style="width:100%">
											<thead>
												<tr>
													<th>Module</th>
													<th>Partition</th>
													<th>Module Number</th>
													<th>LV Box IDr</th>
													<th>PPR Blade ID</th>
													<th>Auxiliary Board ID</th>
													<th>HV Regulation Board ID</th>
													<th>Comment</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>
							
							<!-- Mini drawer table -->
							<div class="row component-table-row mini-drawer-table-row">
								<div class="col table-section mini-drawer-col p-1">
								<h5>Mini Drawer</h5> 
									<div class="mini-drawer-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="mini_drawer_table" style="width:100%">
											<thead>
												<tr>
													<th>Mini Drawer ID</th>
													<th>Super Drawer ID</th>
													<th>Module ID</th>
													<th>Mainboard ID</th>
													<th>Daughterboard ID</th>
													<th>HV Distribution board ID</th>
													<th>PMT Block ID</th>
													<th>Position of PMT Block</th>
													<th>Current Location</th>
													<th>Comment</th>
													<th>Created At</th>
													<th>Updated At</th>
													<th>Created By</th>
													<th>Modified By</th>
													<th>Action</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Mini Drawer ID" id="filter_new_mini_drawer">
													</th>
													<th rowspan="1" colspan="1" >
														<input style="width:100%;" type="text" placeholder="Search By Super Drawer ID" id="filter_super_drawer_of_mini_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Module ID" id="filter_module_of_mini_drawer">
													</th> 
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Mainboard ID" id="filter_mainboard_of_mini_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Daughterboard ID" id="filter_daughterboard_of_mini_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By HV Distribution ID" id="filter_hv_distribution_board_of_mini_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By PMT Block ID" id="filter_pmt_block_id_of_mini_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Position of PMT Block ID" id="filter_position_of_pmt_block_of_mini_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Current Location" id="filter_current_location_of_mini_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Comment" id="filter_comment_of_mini_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By creation time" id="filter_by_creation_time_of_mini_drawer" >
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By updated time" id="filter_by_updated_time_of_mini_drawer" >
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Created By" id="filter_created_by_of_mini_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Modified By" id="filter_updated_by_of_mini_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="" id="" readonly>
													</th>
												</tr>
											</tfoot>
										</table>
									</div> 
								</div>
							</div>

							<!-- Mini drawer modification history -->
							<div class="row component-table-row mini-drawer-modification-history-table-row">
								<div class="col table-section mini-drawer-col p-1">
								<h5>Mini Drawer</h5> 
									<div class="mini-drawer-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="mini_drawer_modification_history_table" style="width:100%">
											<thead>
												<tr>
													<th>Mini Drawer ID</th>
													<th>Super Drawer ID</th>
													<th>Module ID</th>
													<th>Mainboard ID</th>
													<th>Daughterboard ID</th>
													<th>HV Distribution board ID</th>
													<th>PMT Block ID</th>
													<th>Position of PMT Block</th>
													<th>Current Location</th>
													<th>Comment</th>
													<th>Created At</th>
													<th>Updated At</th>
													<th>Created By</th>
													<th>Modified By</th>
													<th>Action</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Mini Drawer ID" id="filter_new_mini_drawer_hist">
													</th>
													<th rowspan="1" colspan="1" >
														<input style="width:100%;" type="text" placeholder="Search By Super Drawer ID" id="filter_super_drawer_of_mini_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Module ID" id="filter_module_of_mini_drawer_hist">
													</th> 
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Mainboard ID" id="filter_mainboard_of_mini_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Daughterboard ID" id="filter_daughterboard_of_mini_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By HV Distribution ID" id="filter_hv_distribution_board_of_mini_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By PMT Block ID" id="filter_pmt_block_id_of_mini_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Position of PMT Block ID" id="filter_position_of_pmt_block_of_mini_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Current Location" id="filter_current_location_of_mini_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Comment" id="filter_comment_of_mini_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By creation time" id="filter_by_creation_time_of_mini_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By updated time" id="filter_by_updated_time_of_mini_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Created By" id="filter_created_by_of_mini_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Modified By" id="filter_updated_by_of_mini_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="" id="" readonly>
													</th>
												</tr>
											</tfoot>
										</table>
									</div> 
								</div>
							</div>

							<!-- Fenics table -->
							<div class="row component-table-row fenics-table-row">
								<div class="col table-section mini-drawer-col p-1">
								<h5>Fenics</h5> 
									<div class="mini-drawer-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="fenics_table" style="width:100%">
											<thead>
												<tr>
													<th>Fenics ID</th>
													<th>Pmt block ID</th>
													<th>Status</th>
													<th>Location</th>
													<th>Comment</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>

							<!-- PPR Blade table -->
							<div class="row component-table-row ppr-blade-table-row">
								<div class="col table-section mini-drawer-col p-1">
								<h5>PPR Blade</h5> 
									<div class="mini-drawer-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="ppr_blade_table" style="width:100%">
											<thead>
												<tr>
													<th>PPR Blade ID</th>
													<th>Rack Number</th>
													<th>Shelf Number</th>
													<th>Slot Number</th>
													<th>Production Batch</th>
													<th>Hardware Version</th>
													<th>Firmware Version</th>
													<th>Installation Date</th>
													<th>Comments</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>

							<!-- OLD HV Divider table -->
							<div class="row component-table-row old-hv-divider-table-row">
								<div class="col table-section mini-drawer-col p-1">
								<h5>HV Divider</h5> 
									<div class="mini-drawer-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="old_hv_divider_table" style="width:100%">
											<thead>
												<tr>
													<th>HV Divider ID</th>
													<th>PMT Block ID</th>
													<th>Remark</th>
													<th>Created At</th>
													<th>Updated At</th>
													<th>Created By</th>
													<th>Updated By</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>
						
							<!-- old hv divider modification hist taqble -->
							<div class="row component-table-row modification-old-hv-divider-table-row">
								<div class="col table-section mini-drawer-col p-1">
								<h5>HV Divider</h5> 
									<div class="mini-drawer-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="modification_old_hv_divider_table" style="width:100%">
											<thead>
												<tr>
													<th>HV Divider ID</th>
													<th>PMT Block ID</th>
													<th>Remark</th>
													<th>Created At</th>
													<th>Updated At</th>
													<th>Created By</th>
													<th>Updated By</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>

							
							<!-- OLD board 3 in 1  table -->
							<div class="row component-table-row old-board-3-in-1-table-row">
								<div class="col table-section mini-drawer-col p-1">
								<h5>3 in 1 Board</h5> 
									<div class="mini-drawer-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="old_board_3_in_1_table" style="width:100%">
											<thead>
												<tr>
													<th>3 in 1 Board ID</th>
													<th>PMT Block ID</th>
													<th>Remark</th>
													<th>Created At</th>
													<th>Updated At</th>
													<th>Created By</th>
													<th>Updated By</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>
						
							<!-- old board 3 in 1 modification hist taqble -->
							<div class="row component-table-row modification-old-board-3-in-1-table-row">
								<div class="col table-section mini-drawer-col p-1">
								<h5>3 in 1 Board</h5> 
									<div class="mini-drawer-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="modification_old_board_3_in_1_table" style="width:100%">
											<thead>
												<tr>
													<th>3 in 1 Board ID</th>
													<th>PMT Block ID</th>
													<th>Remark</th>
													<th>Created At</th>
													<th>Updated At</th>
													<th>Created By</th>
													<th>Updated By</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>

							<!-- HV Distribution board table  -->
							<div class="row component-table-row hv-distribution-board-table-row">
								<div class="col table-section mini-drawer-col p-1">
								<h5>HV Distribution Board</h5> 
									<div class="mini-drawer-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="hv_distribution_board_table" style="width:100%">
											<thead>
												<tr>
													<th>HV Distribution board ID</th>
													<th>Mini Drawer ID</th>
													<th>Status</th>
													<th>Location</th>
													<th>Comment</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>

							<!-- Mainboard table -->
							<div class="row component-table-row mainboard-table-row">
								<div class="col table-section mini-drawer-col p-1">
								<h5>Mainboard</h5> 
									<div class="mini-drawer-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="mainboard_table" style="width:100%">
											<thead>
												<tr>
													<th>Mainboard ID</th>
													<th>Mini Drawer ID</th>
													<th>Status</th>
													<th>Location</th>
													<th>Comment</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>

							<!-- Daughterboard table -->
							<div class="row component-table-row daughterboard-table-row">
								<div class="col table-section mini-drawer-col p-1">
								<h5>Daughterboard</h5> 
									<div class="mini-drawer-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="daughterboard_table" style="width:100%">
											<thead>
												<tr>
													<th>Daughterboard ID</th>
													<th>Mini Drawer ID</th>
													<th>Status</th>
													<th>Location</th>
													<th>Comment</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>

							<!-- hv_divier table -->
							<div class="row component-table-row hv-divier-table-row">
								<div class="col table-section mini-drawer-col p-1">
								<h5>HV Divider</h5> 
									<div class="mini-drawer-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="hv_divider_table" style="width:100%">
											<thead>
												<tr>
													<th>hv_divier ID</th>
													<th>Pmt block ID</th>
													<th>Status</th>
													<th>Location</th>
													<th>Comment</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>							

							<!-- PMT Block table -->
							<div class="row component-table-row pmt-block-table-row">
								<div class="col table-section pmt-col p-1">
									<h5>PMT Block</h5>                          
									<div class="pmt-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="pmt_block_table" style="width:100%">
											<thead>
												<tr>
													<th>Pmt block ID</th>
													<th>Super Drawer ID</th>
													<th>Mini Drawer ID</th>
													<th>Micro Drawer ID</th>
													<th>Fenics ID</th>
													<th>HV Divider ID</th>
													<th>PMT ID</th>
													<th>Test results</th>
													<th>Current location</th>
													<th>Comment</th>
													<th>Action</th>
												</tr>
											</thead>
											
										</table>
									</div>  
								</div>	                                 
							</div>

							<!-- OLD PMT Block table -->
							<div class="row component-table-row old-pmt-block-table-row">
								<div class="col table-section pmt-col p-1">
									<h5>PMT Block</h5>                          
									<div class="pmt-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="old_pmt_block_table" style="width:100%">
											<thead>
												<tr>
													<th>PMT block ID</th>
													<th>Super Drawer ID</th>
													<th>HV Divider ID</th>
													<th>3 in 1 Board ID</th>
													<th>PMT ID</th>
													<th>Test results</th>
													<th>remark</th>
													<th>created at</th>
													<th>updated at</th>
													<th>created by</th>
													<th>updated by</th>
													<th>Action</th>
												</thead>
												</tr>
										</table>
									</div>  
								</div>	                                 
							</div>

							<!-- Micro drawer table -->
							<div class="row component-table-row micro-drawer-table-row">
								<div class="col table-section pmt-col p-1">
									<h5>Micro Drawer</h5>                          
									<div class="pmt-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="micro_drawer_table" style="width:100%">
											<thead>
												<tr>
													<th>Micro Drawer ID</th>
													<th>Super Drawer ID</th>
													<th>Module ID</th>
													<th>PMT Block ID</th>
													<th>Position of PMT Block ID</th>
													<th>Current Location</th>
													<th>Comment</th>
													<th>Created At</th>
													<th>Updated At</th>
													<th>Created By</th>
													<th>Updated By</th>
													<th>Action</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th rowspan="1" colspan="1">
														<input type="text" placeholder="Search By Micro Drawer ID" id="filter_new_micro_drawer_by_micro_drawer_id">
													</th>
													<th rowspan="1" colspan="1" >
														<input style="width:100%;" type="text" placeholder="Search By Super Drawer ID" id="filter_super_drawer_id_in_micro_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Module ID" id="filter_module_id_in_micro_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By PMT Block ID" id="filter_pmt_block_id_in_micro_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Position of pmt block" id="filter_position_of_pmt_block_in_micro_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Current location" id="filter_current_location_in_micro_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Comment" id="filter_remark_in_micro_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Creation time" id="filter_creation_time_in_micro_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Update time" id="filter_updated_time_in_micro_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By User" id="filter_created_by_in_micro_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By User" id="filter_updated_by_in_micro_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="" id="" readonly>
													</th>
												</tr>
											</tfoot>
										</table>
									</div>  
								</div>	                                 
							</div>

							<!-- micro drawer modification rable -->
							<div class="row component-table-row micro-drawer-modification-hist-table-row">
								<div class="col table-section pmt-col p-1">
									<h5>Micro Drawer</h5>                          
									<div class="pmt-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="micro_drawer_modification_table" style="width:100%">
											<thead>
												<tr>
													<th>Micro Drawer ID</th>
													<th>Super Drawer ID</th>
													<th>Module ID</th>
													<th>PMT Block ID</th>
													<th>Position of PMT Block ID</th>
													<th>Current Location</th>
													<th>Comment</th>
													<th>Created At</th>
													<th>Updated At</th>
													<th>Created By</th>
													<th>Updated By</th>
													<th>Action</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th rowspan="1" colspan="1">
														<input type="text" placeholder="Search By Micro Drawer ID" id="filter_new_micro_drawer_by_micro_drawer_id_hist">
													</th>
													<th rowspan="1" colspan="1" >
														<input style="width:100%;" type="text" placeholder="Search By Super Drawer ID" id="filter_super_drawer_id_in_micro_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Module ID" id="filter_module_id_in_micro_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By PMT Block ID" id="filter_pmt_block_id_in_micro_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Position of pmt block" id="filter_position_of_pmt_block_in_micro_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Current location" id="filter_current_location_in_micro_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Comment" id="filter_remark_in_micro_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Creation time" id="filter_creation_time_in_micro_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Update time" id="filter_updated_time_in_micro_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By User" id="filter_created_by_in_micro_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By User" id="filter_updated_by_in_micro_drawer_hist">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="" id="" readonly>
													</th>
												</tr>
											</tfoot>
										</table>
									</div>  
								</div>	                                 
							</div>

							<!-- Super drawer table -->
							<div class="row component-table-row super-drawer-table-row">
								<div class="col table-section pmt-col p-1">
									<h5>Super Drawer</h5>                          
									<div class="pmt-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="super_drawer_table" style="width:100%">
											<thead>
												<tr>
													<th>Super Drawer ID</th>
													<th>Mini Drawer ID 1</th>
													<th>Mini Drawer ID 2</th>
													<th>Mini Drawer ID 3</th>
													<th>Mini Drawer ID 4</th>
													<th>Micro Drawer ID 1</th>
													<th>Micro Drawer ID 2</th>
													<th>Test Results</th>
													<th>Component status</th>
													<th>Current location</th>
													<th>Comment</th>
													<th>Created At</th>
													<th>Updated At</th>
													<th>Created By</th>
													<th>Updated By</th>
													<th>Action</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th rowspan="1" colspan="1">
														<input type="text" placeholder="Search By Super Drawer ID" id="filter_new_super_drawer_by_sp_id">
													</th>
													<th rowspan="1" colspan="1" >
														<input style="width:100%;" type="text" placeholder="Search By Mini Drawer 1 ID" id="filter_by_mini_drawer_1_of_sp_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Micro Drawer 2 ID" id="filter_by_mini_drawer_2_of_sp_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Micro Drawer 3 ID" id="filter_by_mini_drawer_3_of_sp_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Micro Drawer 4 ID" id="filter_by_mini_drawer_4_of_sp_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Micro Drawer 1 ID" id="filter_by_micro_drawer_1_of_sp_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Micro Drawer 2 ID" id="filter_by_micro_drawer_2_of_sp_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="" readonly>
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Component Status" id="filter_by_component_status_of_sp_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Current Location" id="filter_by_current_location_of_sp_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Comment" id="filter_by_comment_status_of_sp_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Creation time" id="filter_by_creation_time_status_of_sp_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By Update time" id="filter_by_update_time_of_sp_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By User" id="filter_by_created_by_of_sp_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text" placeholder="Search By User" id="filter_by_updated_by_of_sp_drawer">
													</th>
													<th rowspan="1" colspan="1">
														<input style="width:100%;" type="text"  id="" readonly>
													</th>
												</tr>
											</tfoot>
										</table>
									</div>  
								</div>	                                 
							</div>

							<!-- OLD Super drawer table -->
							<div class="row component-table-row old-super-drawer-table-row">
								<div class="col table-section pmt-col p-1">
									<h5>Old Super Drawer</h5>                          
									<div class="pmt-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="old_super_drawer_table" style="width:100%">
											<thead>
												<tr>
													<th>Super Drawer ID</th>
													<th>Pmt Block ID</th>
													<th>Position of PMT Block</th>
													<th>Test Results</th>
													<th>Comment</th>
													<th>Created At</th>
													<th>Updated At</th>
													<th>Created By</th>
													<th>Updated By</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div>  
								</div>	                                 
							</div>
							
							<!-- Old Super Drawer Modification TABLE -->
							<div class="row component-table-row super-drawer-modification-table-row">
								<div class="col table-section pmt-col p-1">
									<h5>OLD Super Drawer History</h5>                          
									<div class="pmt-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="modification_super_drawer_table" style="width:100%">
											<thead>
												<tr>
													<th>Super Drawer ID</th>
													<th>PMT Block ID</th>
													<th>Position of PMT Block</th>
													<th>Test Results</th>
													<th>Comments</th>
													<th>Start Date</th>
													<th>End Date</th>
													<th>Created By</th>
													<th>Updated By</th>
													<th>Deleted By</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div>  
								</div>	                                 
							</div>

							<!-- Old pmt block Modification TABLE -->
							<div class="row component-table-row old_pmt_block_modification_table_row">
								<div class="col table-section pmt-col p-1">
									<h5>OLD PMT Block History</h5>                          
									<div class="pmt-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="modification_old_pmt_block_table" style="width:100%">
											<thead>
												<tr>
													<th>PMT Block ID</th>
													<th>Super Drawer ID</th>
													<th>HV Divider ID</th>
													<th>3 in 1 Board ID</th>
													<th>PMT ID</th>
													<th>Test Results</th>
													<th>Remark</th>
													<th>Start Date</th>
													<th>End Date</th>
													<th>Created By</th>
													<th>Updated By</th>
													<th>Deleted By</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div>  
								</div>	                                 
							</div>
							
							<!-- PMT table -->
							<div class="row component-table-row pmt-table-row">
								<div class="col table-section pmt-col p-1">
									<h5>PMT</h5>                          
									<div class="pmt-content table-responsive">                                          
										<table class="table table-sm table-bordered" id="pmt_table" style="width:100%">
											<thead>
												<tr>
													<th>Serial Number</th>
													<th>PMT Block ID</th>
													<th>Phase2 Module</th>
													<th>Old Module</th>
													<th>Phase2 Position in SD</th>
													<th>Old Position</th>
													<th>Beta</th>
													<th>HV Nomimal</th>
													<th>QE</th>
													<th>Type</th>
													<th>Run Number</th>
													<th>Status</th>
													<th>Location</th>
													<th>Comment</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>  
								</div>	                                 
							</div>																
				        </div>
			        </div>
		        </div>
	        </div>
        </div>    

		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
		<!-- jQuery Custom Scroller CDN -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
		<!-- jQuery dataTables.min.js CDN -->
		<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
		<!-- Toastr -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
		<!-- Datatables -->
		<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.24/r-2.2.7/sb-1.0.1/sp-1.2.2/datatables.min.js"></script>
		<!-- Sweet Alert 2-->
		<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>


		<!-- modules JS -->
		<script type="text/javascript" src="<?php echo base_url(); ?>js/FileSaver.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/script.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/component-control.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/component-tree.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/async.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/partition.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/module.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/super_drawer.js"></script>		
		<script type="text/javascript" src="<?php echo base_url(); ?>js/old_super_drawer.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/micro_drawer.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/mini_drawer.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/pmt_block.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/old_pmt_block.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/fenics.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/hv_divider.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/pmt.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/mainboard.js"></script>		
		<script type="text/javascript" src="<?php echo base_url(); ?>js/daughterboard.js"></script>		
		<script type="text/javascript" src="<?php echo base_url(); ?>js/hv_distribution_board.js"></script>		
		<script type="text/javascript" src="<?php echo base_url(); ?>js/old_hv_divider.js"></script>	
		<script type="text/javascript" src="<?php echo base_url(); ?>js/board_3_in_1.js"></script>
		
	
	</body>
</html>

<script>
</script>

