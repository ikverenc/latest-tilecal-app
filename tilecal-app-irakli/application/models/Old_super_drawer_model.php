<?php 

class Old_super_drawer_model extends CI_Model {
    //get entries query, etiteba romeli table-dan gvinda wamovigot informacia, funqcia tolfasia shemdegi query-s: SELECT * from fenics;
    public function get_entries()
    {
        $query = $this->db->get('super_drawer_old');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }   
        
    }

    public function get_old_entries()
    {
        $otherdb = $this->load->database('default_hist', TRUE);
        $query = $otherdb->get('super_drawer_old_hist');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
    }
    
    public function fetch_test_results($id)
    {
        $this->db->select("*");
        $this->db->from("super_drawer_old");
        $this->db->where("old_super_drawer_id_frame", $id);
        $query = $this->db->get();
        if(count($query->result()) > 0){
            return $query->row();
        }

    }
    public function fetch_hist_test_results($id)
    {
        $otherdb = $this->load->database('default_hist', TRUE);
        $otherdb->select("*");
        $otherdb->from("super_drawer_old_hist");
        $otherdb->where("old_super_drawer_id_frame", $id);
        
        $query = $otherdb->get();  
        
        if(count($query->result()) > 0){
            return $query->row();
        }

    }
    

    public function insert_entry($data)
    {
       return  $this->db->insert('super_drawer_old', $data);
    }

    public function delete_entry($id)
    {
       return  $this->db->delete('super_drawer_old', array('old_super_drawer_id_frame' => $id));
    }
    
    public function delete_hist_entry($id)
    {
        $otherdb = $this->load->database('default_hist', TRUE);
        return $otherdb->delete('super_drawer_old_hist', array('old_super_drawer_id_frame' => $id));
       
    }

    public function edit_entry($id)
    {
        $this->db->select("*");
        $this->db->from("super_drawer_old");
        $this->db->where("old_super_drawer_id_frame", $id);
        $query = $this->db->get();
        if(count($query->result()) > 0){
            return $query->row();
        }
    }
    //update entry, shemodis cvladi(masivi) $data
    public function update_entry($data)
    {
        return $this->db->update('super_drawer_old', $data, array('old_super_drawer_id_frame' => $data['old_super_drawer_id_frame']));

    }
    public function check_pmt_block_entry($id)
    {
        $this->db->select("*");
        $this->db->from("super_drawer_old");
        $this->db->where("pmt_block_id", $id);   
        
        $query = $this->db->get();
        
        if(count($query->result()) > 0)
        {
            return true;
        }else{
            return false;
        }
    }
    public function check_super_drawer_entry($id)
    {
        $this->db->select("*");
        $this->db->from("super_drawer_old");
        $this->db->where("old_super_drawer_id", $id);   
        
        $query = $this->db->get();
        
        if(count($query->result()) > 0)
        {
            return true;
        }else{
            return false;
        }
    }

    public function get_pmt_block_positions($id)
    {
        $this->db->select("*");
        $this->db->from("super_drawer_old");
        $this->db->where("old_super_drawer_id", $id);
        $result_query = $this->db->get();

        $result = array();
        
        foreach($result_query->result() as $row)
        {
            array_push($result, $row->pos_pmt_block);
        }

        return $result;
    }
}

?>  