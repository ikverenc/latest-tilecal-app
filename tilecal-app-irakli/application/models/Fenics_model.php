<?php 

class Fenics_model extends CI_Model {
    //get entries query, etiteba romeli table-dan gvinda wamovigot informacia, funqcia tolfasia shemdegi query-s: SELECT * from fenics;
    public function get_entries()
    {
        $query = $this->db->get('fenics');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
        
    }
    // hist db
    public function get_old_entries()
    {
        $otherdb = $this->load->database('default_hist', TRUE);
        $query = $otherdb->get('fenics_old_hist');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
        
    }
    //insert entry query, shemodis cvladi $data(masivi) Fenics.php-s insert funqciidan
    public function insert_entry($data)
    {
       return  $this->db->insert('fenics', $data);
    }
    //delete entry, shemodis cvladi $id fenics.php delete funqciidan, ris mixedvitac funqcia shlis shesabamisi id-s mqonde row-s.
    public function delete_entry($id)
    {
       return  $this->db->delete('fenics', array('id_fenics' => $id));
    }
    //edit entry, shemodis cvladi $id, Fenics.php edit funqciidan, funqcia abrunebs records am shemosul id-ze.
    public function edit_entry($id)
    {
        $this->db->select("*");
        $this->db->from("fenics");
        $this->db->where("id_fenics", $id);
        $query = $this->db->get();
        if(count($query->result()) > 0){
            return $query->row();
        }
    }
    //update entry, shemodis cvladi(masivi) $data, Fenics.php update funqciidan
    public function update_entry($data)
    {
        return $this->db->update('fenics', $data, array('id_fenics' => $data['id_fenics']));

    }
    public function check_entry($id)
    {
        $this->db->select("*");
        $this->db->from("fenics");
        $this->db->where("id_fenics", $id);
        
        $query = $this->db->get();
        
        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }
}

?>