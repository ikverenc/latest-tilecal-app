<?php 

class Pmt_block_model extends CI_Model {
    //get entries query, etiteba romeli table-dan gvinda wamovigot informacia, funqcia tolfasia shemdegi query-s: SELECT * from mainboard;
    public function get_entries()
    {
        $query = $this->db->get('pmt_block');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
    }
    public function get_old_entries()
    {
        $query = $this->db->get('pmt_block_old');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
    }

    public function get_old_modification_entries()
    {
        $otherdb = $this->load->database('default_hist', TRUE);
        $query = $otherdb->get('pmt_block_old_hist');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
    }

    public function fetch_hist_test_results($id)
    {
        $otherdb = $this->load->database('default_hist', TRUE);
        $otherdb->select("*");
        $otherdb->from("pmt_block_old_hist");
        $otherdb->where("id_pmt_block_frame", $id);
        
        $query = $otherdb->get();  
        
        if(count($query->result()) > 0){
            return $query->row();
        }

    }

    //insert entry query, shemodis cvladi $data(masivi) Pmt_Block.php-s insert funqciidan
    public function insert_entry($data)
    {
       return  $this->db->insert('pmt_block', $data);
    }

    public function insert_in_old_entry($data)
    {
       return  $this->db->insert('pmt_block_old', $data);
    }
    //delete entry, shemodis cvladi $id Pmt_Block.php delete funqciidan, ris mixedvitac funqcia shlis shesabamisi id-s mqonde row-s.
    public function delete_entry($id)
    {
       return  $this->db->delete('pmt_block', array('id_pmt_block' => $id));
    }

    public function delete_old_entry($id)
    {
       return  $this->db->delete('pmt_block_old', array('id_pmt_block' => $id));
    }

    public function delete_hist_entry($id)
    {
        $otherdb = $this->load->database('default_hist', TRUE);
        return $otherdb->delete('pmt_block_old_hist', array('id_pmt_block_frame' => $id));
       
    }

    //edit entry, shemodis cvladi $id, Pmt_Block.php edit funqciidan, funqcia abrunebs records am shemosul id-ze.
    public function edit_entry($id)
    {
        $this->db->select("*");
        $this->db->from("pmt_block");
        $this->db->where("id_pmt_block", $id);
        $query = $this->db->get();
        if(count($query->result()) > 0){
            return $query->row();
        }
    }
    public function edit_old_entry($id)
    {
        $this->db->select("*");
        $this->db->from("pmt_block_old");
        $this->db->where("id_pmt_block", $id);
        $query = $this->db->get();
        if(count($query->result()) > 0){
            return $query->row();
        }
    }
    //update entry, shemodis cvladi(masivi) $data, Pmt_block.php update funqciidan
    public function update_entry($data)
    {
        return $this->db->update('pmt_block', $data, array('id_pmt_block' => $data['id_pmt_block']));

    }
    
    public function fetch_test_results_of_old_pmt_block($id)
    {
        $this->db->select("*");
        $this->db->from("pmt_block_old");
        $this->db->where("id_pmt_block", $id);
        $query = $this->db->get();
        if(count($query->result()) > 0)
        {
            return $query->row();
        }
    }
        //update entry, shemodis cvladi(masivi) $data, Pmt_block.php update funqciidan
    public function update_old_entry($data)
    {
        return $this->db->update('pmt_block_old', $data, array('id_pmt_block' => $data['id_pmt_block']));

    }

    public function check_entry($id)
    {
        $this->db->select("*");
        $this->db->from("pmt_block");
        $this->db->where("id_pmt_block", $id);
        
        $query = $this->db->get();
        
        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }
    public function check_old_entry($id)
    {
        $this->db->select("*");
        $this->db->from("pmt_block_old");
        $this->db->where("id_pmt_block", $id);
        
        $query = $this->db->get();
        if(count($query->result()) > 0){
            return $query->row();
        }else{
            return null;
        }

    }

    public function get_fhp_id()
    {
        $result = array();
        $fenicsArr = array();
        $hvDividerArr = array();
        $pmtArr = array();

        $result_query = $this->db->get('pmt_block');
        
        foreach($result_query->result() as $row)
        {
            array_push($fenicsArr, $row->id_fenics);
            array_push($hvDividerArr, $row->id_hv_divider);
            array_push($pmtArr, $row->id_pmt);
        }

        array_push($result, $fenicsArr);
        array_push($result, $hvDividerArr);
        array_push($result, $pmtArr);

        return $result;
    }

    public function get_pmt_block_from_micro_mini_drawer()
    {
        $result = array();
        $microDrawer = array();
        $miniDrawer = array();

        $this->db->select("pmt_block_id");
        $this->db->from("micro_drawer");
        $result_query = $this->db->get();

        foreach($result_query->result() as $row)
        {
            array_push($microDrawer, $row->pmt_block_id);
        }

        $this->db->select("pmt_block_id");
        $this->db->from("mini_drawer");
        $result_query = $this->db->get();

        foreach($result_query->result() as $row)
        {
            array_push($miniDrawer, $row->pmt_block_id);
        }

        array_push($result, $microDrawer);
        array_push($result, $miniDrawer);

        return $result;
    }

    public function get_pos_from_micro_drawer($id)
    {
        
        $arr = array();
        $this->db->select("pos_pmt_block");
        $this->db->from("micro_drawer");
        $this->db->where("id_micro_drawer", $id);
        $query = $this->db->get();


        foreach($query->result() as $row)
        {
            array_push($arr, $row->pos_pmt_block);
        }

        return $arr;
    }

    public function get_pos_from_mini_drawer($id)
    {
        
        $arr = array();
        $this->db->select("pos_pmt_block");
        $this->db->from("mini_drawer");
        $this->db->where("id_mini_drawer", $id);
        $query = $this->db->get();


        foreach($query->result() as $row)
        {
            array_push($arr, $row->pos_pmt_block);
        }

        return $arr;
    }
}

?>