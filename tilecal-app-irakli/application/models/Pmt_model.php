<?php 

class Pmt_model extends CI_Model {
    //get entries query, etiteba romeli table-dan gvinda wamovigot informacia, funqcia tolfasia shemdegi query-s: SELECT * from pmt;
    public function get_entries()
    {
        $query = $this->db->get('pmt');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
        
    }
    //insert entry query, shemodis cvladi $data(masivi) Pmt.php-s insert funqciidan
    public function insert_entry($data)
    {
       return  $this->db->insert('pmt', $data);
    }
    //delete entry, shemodis cvladi $id Pmt.php delete funqciidan, ris mixedvitac funqcia shlis shesabamisi id-s mqonde row-s.
    public function delete_entry($id)
    {
       return  $this->db->delete('pmt', array('serial_number' => $id));
    }
    //edit entry, shemodis cvladi $id, Pmt.php edit funqciidan, funqcia abrunebs records am shemosul id-ze.
    public function edit_entry($id)
    {
        $this->db->select("*");
        $this->db->from("pmt");
        $this->db->where("serial_number", $id);
        $query = $this->db->get();
        if(count($query->result()) > 0){
            return $query->row();
        }
    }
    //update entry, shemodis cvladi(masivi) $data, Mini_drawer.php update funqciidan
    public function update_entry($data)
     {
         return $this->db->update('pmt', $data, array('serial_number' => $data['serial_number']));
 
     }

    public function check_entry($id)
    {
        $this->db->select("*");
        $this->db->from("pmt");
        $this->db->where("serial_number", $id);
        
        $query = $this->db->get();
        
        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }

}

?>