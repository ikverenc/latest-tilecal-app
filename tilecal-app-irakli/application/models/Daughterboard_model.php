<?php 

class Daughterboard_model extends CI_Model {
    //get entries query, etiteba romeli table-dan gvinda wamovigot informacia, funqcia tolfasia shemdegi query-s: SELECT * from fenics;
    public function get_entries()
    {
        $query = $this->db->get('daughterboard');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
        
    }

    public function insert_entry($data)
    {
       return  $this->db->insert('daughterboard', $data);
    }

    //edit entry, shemodis cvladi $id, Fenics.php edit funqciidan, funqcia abrunebs records am shemosul id-ze.
    public function edit_entry($id)
    {
        $this->db->select("*");
        $this->db->from("daughterboard");
        $this->db->where("id_daughterboard", $id);
        $query = $this->db->get();
        if(count($query->result()) > 0){
            return $query->row();
        }
    }
    //update entry, shemodis cvladi(masivi) $data, Fenics.php update funqciidan
    public function update_entry($data)
    {
        return $this->db->update('daughterboard', $data, array('id_daughterboard' => $data['id_daughterboard']));
    }

    public function delete_entry($id)
    {
       return  $this->db->delete('daughterboard', array('id_daughterboard' => $id));
    }
    
    public function check_entry($id)
    {
        $this->db->select("*");
        $this->db->from("daughterboard");
        $this->db->where("id_daughterboard", $id);
        
        $query = $this->db->get();
        
        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }
}

?>