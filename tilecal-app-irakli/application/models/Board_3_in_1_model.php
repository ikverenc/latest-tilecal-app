<?php 

class Board_3_in_1_model extends CI_Model {
    //get entries query, etiteba romeli table-dan gvinda wamovigot informacia, funqcia tolfasia shemdegi query-s: SELECT * from fenics;
    public function get_entries()
    {
        $query = $this->db->get('board_3_in_1');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
        
    }

    public function get_old_entries()
    {
        $otherdb = $this->load->database('default_hist', TRUE);
        $query = $otherdb->get('board_3_in_1_hist');

        if(count( $query->result() ) > 0) {
            return $query->result();
        }
    }

    public function delete_old_modification_entry($id)
    {
        $otherdb = $this->load->database('default_hist', TRUE);
        
        return $otherdb->delete('board_3_in_1_hist', array('id_3_in_1_hist' => $id)); 
    }

    public function delete_entry($id)
    {
       return  $this->db->delete('board_3_in_1', array('id_3_in_1' => $id));
    }

    public function edit_entry($id)
    {
        $this->db->select("*");
        $this->db->from("board_3_in_1");
        $this->db->where("id_3_in_1", $id);

        $query = $this->db->get();

        if(count($query->result()) > 0){

            return $query->row();
        }
    }

    public function update_entry($data)
    {
        return $this->db->update('board_3_in_1', $data, array('id_3_in_1' => $data['id_3_in_1']));
    }

    public function check_entry($id)
    {
        $this->db->select("*");
        $this->db->from("board_3_in_1");
        $this->db->where("id_3_in_1", $id);
        
        $query = $this->db->get();
        
        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }
    public function insert_entry($data)
    {
       return  $this->db->insert('board_3_in_1', $data);
    }
}

?>