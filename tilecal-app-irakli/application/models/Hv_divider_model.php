<?php 

class Hv_divider_model extends CI_Model {
    //get entries query, etiteba romeli table-dan gvinda wamovigot informacia, funqcia tolfasia shemdegi query-s: SELECT * from fenics;
    public function get_entries()
    {
        $query = $this->db->get('hv_divider');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
    }
    public function get_old_entries()
    {
        $query = $this->db->get('hv_divider_old');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
    }

    public function get_old_modification_entries()
    {
        $otherdb = $this->load->database('default_hist', TRUE);
        $query = $otherdb->get('hv_divider_old_hist');

        if(count( $query->result() ) > 0) {
            return $query->result();
        }
    }


    //insert entry query, shemodis cvladi $data(masivi) hv_divider.php-s insert funqciidan
    public function insert_entry($data)
    {
       return  $this->db->insert('hv_divider', $data);
    }
    public function insert_in_old_entry($data)
    {
       return  $this->db->insert('hv_divider_old', $data);
    }
    //delete entry, shemodis cvladi $id hv_divider.php delete funqciidan, ris mixedvitac funqcia shlis shesabamisi id-s mqonde row-s.
    public function delete_entry($id)
    {
       return  $this->db->delete('hv_divider', array('id_hv_divider' => $id));
    }
    
    public function delete_old_entry($id)
    {
       return  $this->db->delete('hv_divider_old', array('id_hv_divider_old' => $id));
    }

    public function delete_old_modification_entry($id)
    {
       $otherdb = $this->load->database('default_hist', TRUE);
       return  $otherdb->delete('hv_divider_old_hist', array('id_hv_divider_old_frame' => $id));
    }

    //edit entry, shemodis cvladi $id, hv_divider.php edit funqciidan, funqcia abrunebs records am shemosul id-ze.
    public function edit_entry($id)
    {
        $this->db->select("*");
        $this->db->from("hv_divider");
        $this->db->where("id_hv_divider", $id);
        $query = $this->db->get();
        if(count($query->result()) > 0){
            return $query->row();
        }
    }
    //edit entry, shemodis cvladi $id, hv_divider.php edit funqciidan, funqcia abrunebs records am shemosul id-ze.
    public function edit_old_entry($id)
    {
        $this->db->select("*");
        $this->db->from("hv_divider_old");
        $this->db->where("id_hv_divider_old", $id);
        $query = $this->db->get();


        if(count($query->result()) > 0){
            return $query->row();
        }
    }

    //update entry, shemodis cvladi(masivi) $data, hv_divider.php update funqciidan
    public function update_entry($data)
    {
        return $this->db->update('hv_divider', $data, array('id_hv_divider' => $data['id_hv_divider']));

    }

    public function update_old_entry($data)
    {
        return $this->db->update('hv_divider_old', $data, array('id_hv_divider_old' => $data['id_hv_divider_old']));

    }

    public function check_entry($id)
    {
        $this->db->select("*");
        $this->db->from("hv_divider");
        $this->db->where("id_hv_divider", $id);
        
        $query = $this->db->get();
        
        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }
    public function check_old_entry($id)
    {
        $this->db->select("*");
        $this->db->from("hv_divider_old");
        $this->db->where("id_hv_divider_old", $id);
        
        $query = $this->db->get();
        
        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }
}

?>