<?php 

class Super_drawer_model extends CI_Model {
    //get entries query, etiteba romeli table-dan gvinda wamovigot informacia, funqcia tolfasia shemdegi query-s: SELECT * from fenics;
    public function get_entries()
    {
        $query = $this->db->get('super_drawer');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
    }

    public function get_mini_drawers_id()
    {
        $result = array();
        $result_query = $this->db->get('super_drawer');
        
        foreach($result_query->result() as $row)
        {
            array_push($result, $row->mini_drawer_id1);
            array_push($result, $row->mini_drawer_id2);
            array_push($result, $row->mini_drawer_id3);
            array_push($result, $row->mini_drawer_id4);

        }

        return $result;
    }

    public function get_micro_drawers_id()
    {
        $result = array();
        $result_query = $this->db->get('super_drawer');
        
        foreach($result_query->result() as $row)
        {
            array_push($result, $row->micro_drawer_id1);
            array_push($result, $row->micro_drawer_id2);
        }

        return $result;
    }

    public function insert_entry($data)
    {
       return  $this->db->insert('super_drawer', $data);
    }

    public function delete_entry($id)
    {
       return  $this->db->delete('super_drawer', array('id_super_drawer_frame' => $id));
    }
    public function edit_entry($id)
    {
        $this->db->select("*");
        $this->db->from("super_drawer");
        $this->db->where("id_super_drawer_frame", $id);
        $query = $this->db->get();
        if(count($query->result()) > 0){
            return $query->row();
        }
    }

    public function fetch_test_results($id)
    {
        $this->db->select("*");
        $this->db->from("super_drawer");
        $this->db->where("id_super_drawer_frame", $id);
        $query = $this->db->get();
        if(count($query->result()) > 0){
            return $query->row();
        }

    }
    //update entry, shemodis cvladi(masivi) $data, Fenics.php update funqciidan
    public function update_entry($data)
    {
        return $this->db->update('super_drawer', $data, array('id_super_drawer_frame' => $data['id_super_drawer_frame']));

    }
    public function check_entry($id)
    {
        $this->db->select("*");
        $this->db->from("super_drawer");
        $this->db->where("id_super_drawer", $id);
        
        $query = $this->db->get();
        
        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }
}

?>