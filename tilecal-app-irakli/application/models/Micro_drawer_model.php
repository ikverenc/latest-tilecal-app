<?php 

class Micro_drawer_model extends CI_Model {
    //get entries query, etiteba romeli table-dan gvinda wamovigot informacia, funqcia tolfasia shemdegi query-s: SELECT * from fenics;
    public function get_entries()
    {
        $query = $this->db->get('micro_drawer');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
        
    }
    public function get_old_entries()
    {
        $otherdb = $this->load->database('default_hist', TRUE);
        $query = $otherdb->get('micro_drawer_hist');

        if(count( $query->result() ) > 0) {
            return $query->result();
        }
        
    }

    public function insert_entry($data)
    {
       return  $this->db->insert('micro_drawer', $data);
    }
    public function delete_entry($id)
    {
       return  $this->db->delete('micro_drawer', array('id_micro_drawer_frame' => $id));
    }
    public function edit_entry($id)
    {
        $this->db->select("*");
        $this->db->from("micro_drawer");
        $this->db->where("id_micro_drawer_frame", $id);
        $query = $this->db->get();
        if(count($query->result()) > 0){
            return $query->row();
        }
    }
    //update entry, shemodis cvladi(masivi) $data, Fenics.php update funqciidan
    public function update_entry($data)
    {
        return $this->db->update('micro_drawer', $data, array('id_micro_drawer_frame' => $data['id_micro_drawer_frame']));

    }
    public function check_entry($id)
    {
        $this->db->select("*");
        $this->db->from("micro_drawer");
        $this->db->where("id_micro_drawer", $id);
        
        $query = $this->db->get();
        
        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }
}

?>