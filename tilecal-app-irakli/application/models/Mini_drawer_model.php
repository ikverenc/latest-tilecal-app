<?php 

class Mini_drawer_model extends CI_Model {
    //get entries query, etiteba romeli table-dan gvinda wamovigot informacia, funqcia tolfasia shemdegi query-s: SELECT * from mini_drawer;
    public function get_entries()
    {
        $query = $this->db->get('mini_drawer');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
        
    }
    public function get_old_entries()
    {
        $otherdb = $this->load->database('default_hist', TRUE);
        $query = $otherdb->get('mini_drawer_hist');

        if(count( $query->result() ) > 0) {
            return $query->result();
        }
        
    }
    //insert entry query, shemodis cvladi $data(masivi) Mini_drawer.php-s insert funqciidan
    public function insert_entry($data)
    {
       return  $this->db->insert('mini_drawer', $data);
    }
    //delete entry, shemodis cvladi $id Mini_drawer.php delete funqciidan, ris mixedvitac funqcia shlis shesabamisi id-s mqonde row-s.
    public function delete_entry($id)
    {
       return  $this->db->delete('mini_drawer', array('id_mini_drawer_frame' => $id));
    }
    public function delete_old_modification_entry($id)
    {
       $otherdb = $this->load->database('default_hist', TRUE);
       return  $otherdb->delete('mini_drawer_hist', array('id_mini_drawer_frame_hist' => $id));
    }
    //edit entry, shemodis cvladi $id, Mini_drawer.php edit funqciidan, funqcia abrunebs records am shemosul id-ze.
    public function edit_entry($id)
    {
        $this->db->select("*");
        $this->db->from("mini_drawer");
        $this->db->where("id_mini_drawer_frame", $id);
        $query = $this->db->get();

        if(count($query->result()) > 0){
            return $query->row();
        }
    }

    public function check_mainboard_in_mini_drawer($id)
    {
        $this->db->select("*");
        $this->db->from("mini_drawer");
        $this->db->where("mainboard_id", $id);
        $query = $this->db->get();

        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }
    public function check_daughterboard_in_mini_drawer($id)
    {
        $this->db->select("*");
        $this->db->from("mini_drawer");
        $this->db->where("daughterboard_id", $id);
        $query = $this->db->get();

        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }
    public function check_hv_distribution_board_in_mini_drawer($id)
    {
        $this->db->select("*");
        $this->db->from("mini_drawer");
        $this->db->where("hv_distribution_board_id", $id);
        $query = $this->db->get();

        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }
    //update entry, shemodis cvladi(masivi) $data, Mini_drawer.php update funqciidan
    public function update_entry($data)
    {
        return $this->db->update('mini_drawer', $data, array('id_mini_drawer_frame' => $data['id_mini_drawer_frame']));

    }
    public function check_entry($id)
    {
        $this->db->select("*");
        $this->db->from("mini_drawer");
        $this->db->where("id_mini_drawer", $id);
        
        $query = $this->db->get();
        
        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }
}

?>