<?php 

class Hv_distribution_board_model extends CI_Model {
    //get entries query, etiteba romeli table-dan gvinda wamovigot informacia, funqcia tolfasia shemdegi query-s: SELECT * from fenics;
    public function get_entries()
    {
        $query = $this->db->get('hv_distribution_board');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }
        
    }

    public function insert_entry($data)
    {
       return  $this->db->insert('hv_distribution_board', $data);
    }
    
    public function delete_entry($id)
    {
       return  $this->db->delete('hv_distribution_board', array('id_hv_distribution_board' => $id));
    }

    //edit entry, shemodis cvladi $id, Fenics.php edit funqciidan, funqcia abrunebs records am shemosul id-ze.
    public function edit_entry($id)
    {
        $this->db->select("*");
        $this->db->from("hv_distribution_board");
        $this->db->where("id_hv_distribution_board", $id);
        $query = $this->db->get();
        if(count($query->result()) > 0){
            return $query->row();
        }
    }
    //update entry, shemodis cvladi(masivi) $data, Fenics.php update funqciidan
    public function update_entry($data)
    {
        return $this->db->update('hv_distribution_board', $data, array('id_hv_distribution_board' => $data['id_hv_distribution_board']));

    }

    public function check_entry($id)
    {
        $this->db->select("*");
        $this->db->from("hv_distribution_board");
        $this->db->where("id_hv_distribution_board", $id);
        
        $query = $this->db->get();
        
        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }
}

?>