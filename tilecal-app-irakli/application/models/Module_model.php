<?php 

class Module_model extends CI_Model {
    //get entries query, etiteba romeli table-dan gvinda wamovigot informacia, funqcia tolfasia shemdegi query-s: SELECT * from module;
    public function get_entries()
    {
        $query = $this->db->get('module');
        if(count( $query->result() ) > 0) {
            return $query->result();
        }        
    }

    public function check_entry($id)
    {
        $this->db->select("*");
        $this->db->from("module");
        $this->db->where("module", $id);
        
        $query = $this->db->get();
        
        if(count($query->result()) > 0){
            return true;
        }else{
            return false;
        }
    }
}

?>