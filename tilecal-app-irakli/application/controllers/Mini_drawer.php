<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mini_drawer extends CI_Controller {
    public function __construct(){
        parent::__construct();
        //xdeba codeigniter-is bibliotekis gamodzaxeba
        $this->load->helper(array('form', 'url'));
        //xdeba codeigniter-is bibliotekis gamodzaxeba
        $this->load->library('form_validation');
        //ukavshirdeba Mini_drawer_model.php fails
        $this->load->model('mini_drawer_model');

    }
    //index funqcia romelsac am momentshi ar aqvs gamoyeneba
	public function index(){
    }
    //Fetch Table, funqcia uzrunvelyofs mini_drawer table-dan wamogebuli informaciis miwodebas ajax-is requestistvis. ajax request igzavneba xeshi Mini Drawer-ze click-isas. faili: views/layout/Main.php
    public function fetch(){
        if ($this->input->is_ajax_request()) {
            if ($records = $this->mini_drawer_model->get_entries())
            {
                $data = array('responce' => 'success', 'records' => $records);
            }
            else
            {
                $data = array('responce' => 'error', 'message' => 'Failed to fetch Mini Drawer data');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    public function fetch_old()
    {
        if ($this->input->is_ajax_request()) {
            if ($records = $this->mini_drawer_model->get_old_entries())
            {
                $data = array('responce' => 'success', 'records' => $records);
            }
            else
            {
                $data = array('responce' => 'error', 'message' => 'Failed to fetch Mini Drawer data');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    
    //Insert Record, funqcia uzrunvelyofs monacemebis mini_drawer table-shi chaweras, post-it moaqvs views/layout/Main.php failidan data da wers mini_drawer table-shi.
    public function insert(){
        if ($this->input->is_ajax_request()){
              $this->form_validation->set_rules('id_mini_drawer', 'Mini Drawer ID', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $data = array('responce' => 'error', 'message' => validation_errors());
            }
            else
            {
                $ajax_data = $this->input->post();
                if($this->mini_drawer_model->insert_entry($ajax_data)){
                    $data = array('responce' => 'success', 'message' => 'Record added Successfully');
                }
                else{
                    $data = array('responce' => 'error', 'message' => 'Failed to insert Record');
                }                
            }
            echo json_encode($data);
        }
        else{
            echo "no direct script access allowed";
        }
    }
    
    //Delete record, funqcia uzrunvelyofs wasashleli record-is ID-s miwodebas Mini_drawer_model.php/delete_entry funqciistvis da idzaxebs funqcias. funqciashi ajax-dan shemodis wasashleli record-is id.
    public function delete(){
        if ($this->input->is_ajax_request()) {
            $del_id = $this->input->post('del_id');
            if ($this->mini_drawer_model->delete_entry($del_id)) {
                $data = array('responce' => 'success');
            }else{
                $data = array('responce' => 'error');
            }            
            // $data = array('del_id' => $del_id);

            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    public function delete_old_modification_entry()
    {
        if ($this->input->is_ajax_request()) 
        {
            $del_id = $this->input->post('del_id');

            if ($record = $this->mini_drawer_model->delete_old_modification_entry($del_id)) 
            {
                $data = array('responce' => 'success', 'record' => $record);
            }
            else
            {
                $data = array('responce' => 'error', 'record' => $record);
            }            
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    //Edit Record, funqcia uzrunvelyofs dasa-edit-ebeli record-is ID-s miwodebas Mini_drawer_model.php/edit_entry funqciistvis da idzaxebs funqcias. funqciashi ajax-dan shemodis dasaeditebeli record-is id da isev ajax-s ubrunebs response-s saxit migebul shedegs.
    public function edit(){
        if ($this->input->is_ajax_request()) {
            $edit_id = $this->input->post('edit_id');
            if ($record = $this->mini_drawer_model->edit_entry($edit_id)) {
                $data = array('responce' => 'success', 'record' => $record);
            }else{
                $data = array('responce' => 'error', 'message' => 'Failed to fetch record');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    public function mainboard_id_check(){
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            if ($record = $this->mini_drawer_model->check_mainboard_in_mini_drawer($id)) {
                $data = array('responce' => 'success', 'record' => $record);
            }else{
                $data = array('responce' => 'error', 'message' => 'Failed to fetch record');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
    public function daughterboard_id_check(){
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            if ($record = $this->mini_drawer_model->check_daughterboard_in_mini_drawer($id)) {
                $data = array('responce' => 'success', 'record' => $record);
            }else{
                $data = array('responce' => 'error', 'message' => 'Failed to fetch record');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
    public function hv_distribution_board_id_check(){
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            if ($record = $this->mini_drawer_model->check_hv_distribution_board_in_mini_drawer($id)) {
                $data = array('responce' => 'success', 'record' => $record);
            }else{
                $data = array('responce' => 'error', 'message' => 'Failed to fetch record');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    //Update Record, funqcia uzrunvelyofs da-update-ebuli monacemebis shenaxvas mini_drawer table-shi. idzaxebs Mini_drawer_model.php/update_entry funqcias da gadascems data-s.
    public function update(){
        if ($this->input->is_ajax_request()) {            

            $this->form_validation->set_rules('id_mini_drawer_frame', 'Mini Drawer ID', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $data = array('responce' => 'error', 'message' => validation_errors());
            }
            else
            {
                $data['id_mini_drawer_frame'] = $this->input->post('id_mini_drawer_frame');
                $data['id_mini_drawer'] = $this->input->post('edit_id_mini_drawer');
                $data['id_super_drawer'] = $this->input->post('edit_id_super_drawer');
                $data['module_id'] = $this->input->post('edit_id_module');
                $data['mainboard_id'] = $this->input->post('edit_id_mainboard');
                $data['daughterboard_id'] = $this->input->post('edit_id_daughterboard');
                $data['hv_distribution_board_id'] = $this->input->post('edit_id_hv_distribution_board');
                $data['pmt_block_id'] = $this->input->post('edit_id_pmt_block');
                $data['pos_pmt_block'] = $this->input->post('edit_pos_pmt_block');
                $data['current_location'] = $this->input->post('current_location');
                $data['remark'] = $this->input->post('edit_remark');
                
                if($this->mini_drawer_model->update_entry($data)){
                    $data = array('responce' => 'success', 'message' => 'Record updated Successfully');
                }
                else{
                    $data = array('responce' => 'error', 'message' => 'Failed to update record');
                }                
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
    public function check(){
       
        if ($this->input->is_ajax_request()) {
       
            $del_id = $this->input->post('del_id');
            $data = $this->mini_drawer_model->check_entry($del_id);
                      
            echo json_encode($data);
       
        }else{
            echo "No direct script access allowed";
        }
    }
}
