<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daughterboard extends CI_Controller {
    public function __construct(){
        parent::__construct();
        //xdeba codeigniter-is bibliotekis gamodzaxeba
        $this->load->helper(array('form', 'url'));
        //xdeba codeigniter-is bibliotekis gamodzaxeba
        $this->load->library('form_validation');
        //ukavshirdeba super_drawer_model.php fails
        $this->load->model('daughterboard_model');

    }
    //index funqcia romelsac am momentshi ar aqvs gamoyeneba
	public function index(){
    }

    public function fetch(){
        if ($this->input->is_ajax_request()) {
            if ($records = $this->daughterboard_model->get_entries())
            {
                $data = array('responce' => 'success', 'records' => $records);
            }
            else
            {
                $data = array('responce' => 'error', 'message' => 'Failed to fetch Daughterboard data');
            }
            
            echo json_encode($data);

        }else{
            echo "No direct script access allowed";
        }
    }

    public function insert(){
        if ($this->input->is_ajax_request()){
              $this->form_validation->set_rules('id_daughterboard', 'Daughterboard ID', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $data = array('responce' => 'error', 'message' => validation_errors());
            }
            else
            {
                $ajax_data = $this->input->post();
                
                if($this->daughterboard_model->insert_entry($ajax_data)){
                    $data = array('responce' => 'success', 'message' => 'Record added Successfully');
                }
                else{
                    $data = array('responce' => 'error', 'message' => 'Failed to insert Record');
                }                
            }
            echo json_encode($data);
        }
        else{
            echo "no direct script access allowed";
        }
    }

    public function delete(){
        if ($this->input->is_ajax_request()) {
            $del_id = $this->input->post('del_id');
            if ($this->daughterboard_model->delete_entry($del_id)) {
                $data = array('responce' => 'success');
            }else{
                $data = array('responce' => 'error');
            }            
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
   
    //Edit Record, funqcia uzrunvelyofs dasa-edit-ebeli record-is ID-s miwodebas fenics_model.php/edit_entry funqciistvis da idzaxebs funqcias. funqciashi ajax-dan shemodis dasaeditebeli record-is id da isev ajax-s ubrunebs response-s saxit migebul shedegs.
    public function edit(){
        if ($this->input->is_ajax_request()) {
            $edit_id = $this->input->post('edit_id');
            if ($record = $this->daughterboard_model->edit_entry($edit_id)) {
                $data = array('responce' => 'success', 'record' => $record);
            }else{
                $data = array('responce' => 'error', 'message' => 'Failed to fetch record');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
    //Update Record, funqcia uzrunvelyofs da-update-ebuli monacemebis shenaxvas fenics table-shi. idzaxebs Mini_drawer_model.php/update_entry funqcias da gadascems data-s.
    public function update()
    {
        if ($this->input->is_ajax_request()) {            

            $this->form_validation->set_rules('id_daughterboard', 'Daughterboard ID', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $data = array('responce' => 'error', 'message' => validation_errors());
            }
            else
            {
                $data['id_daughterboard'] = $this->input->post('id_daughterboard');
                $data['id_mini_drawer'] = $this->input->post('id_mini_drawer');
                $data['component_status'] = $this->input->post('component_status');
                $data['current_location'] = $this->input->post('current_location');
                $data['remark'] = $this->input->post('remark');
                
                if($this->daughterboard_model->update_entry($data)){
                    $data = array('responce' => 'success', 'message' => 'Record updated Successfully');
                }
                else{
                    $data = array('responce' => 'error', 'message' => 'Failed to update record');
                }                
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    // amowmebs aris tu ara databazeshi super draweri ID-it
    public function check(){
       
        if ($this->input->is_ajax_request()) {
       
            $del_id = $this->input->post('del_id');
            $data = $this->daughterboard_model->check_entry($del_id);
                      
            echo json_encode($data);
       
        }else{
            echo "No direct script access allowed";
        }
    }
}
