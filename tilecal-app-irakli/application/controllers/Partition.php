<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partition extends CI_Controller {
    public function __construct(){
        parent::__construct();
        //xdeba codeigniter-is bibliotekis gamodzaxeba
        $this->load->helper(array('form', 'url'));
        //xdeba codeigniter-is bibliotekis gamodzaxeba
        $this->load->library('form_validation');
        //ukavshirdeba module_model.php fails
        $this->load->model('partition_model');

    }
    //index funqcia romelsac am momentshi ar aqvs gamoyeneba
	public function index(){
    }
    //Fetch Table, funqcia uzrunvelyofs tc_partition table-dan wamogebuli informaciis miwodebas ajax-is requestistvis. ajax request igzavneba xeshi partition-ze click-isas. faili: views/layout/Main.php
    public function fetch(){
        if ($this->input->is_ajax_request()) {
            if ($records = $this->partition_model->get_entries())
            {
                $data = array('responce' => 'success', 'records' => $records);
            }
            else
            {
                $data = array('responce' => 'error', 'message' => 'Failed to fetch Pmt data');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
}
