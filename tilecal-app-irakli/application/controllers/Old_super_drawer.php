<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Old_super_drawer extends CI_Controller {
    public function __construct(){
        parent::__construct();
        //xdeba codeigniter-is bibliotekis gamodzaxeba
        $this->load->helper(array('form', 'url'));
        //xdeba codeigniter-is bibliotekis gamodzaxeba
        $this->load->library('form_validation');
        //ukavshirdeba super_drawer_model.php fails
        $this->load->model('old_super_drawer_model');

    }
    //index funqcia romelsac am momentshi ar aqvs gamoyeneba
	public function index(){
    }

    public function fetch(){
        if ($this->input->is_ajax_request()) {
            if ($records = $this->old_super_drawer_model->get_entries())
            {
                $data = array('responce' => 'success', 'records' => $records);
            }
            else
            {
                $data = array('responce' => 'error', 'message' => 'Failed to fetch Super drawer data');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }


    public function fetchHistory()
    {
        if ($this->input->is_ajax_request()) {
            if ($records = $this->old_super_drawer_model->get_old_entries())
            {
                $data = array('responce' => 'success', 'records' => $records);
            }
            else
            {
                $data = array('responce' => 'error', 'message' => 'Failed to fetch Super drawer data');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }


    public function fetchPmtBlockPositions(){
        if ($this->input->is_ajax_request()) 
        {
            $id = $this->input->post('id');
        
            if ($records = $this->old_super_drawer_model->get_pmt_block_positions($id))
            {
                $data = array('responce' => 'success', 'records' => $records);
            }else
            {
                $data = array('responce' => 'error', 'message' => 'Failed to fetch Old Super Drawer data');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    public function insert(){
        if ($this->input->is_ajax_request()){
              $this->form_validation->set_rules('old_super_drawer_id', 'Super Drawer ID', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $data = array('responce' => 'error', 'message' => validation_errors());
            }
            else
            {
                $ajax_data = $this->input->post();

                if($this->old_super_drawer_model->insert_entry($ajax_data)){
                    $data = array('responce' => 'success', 'message' => 'Record added Successfully');
                }
                else{
                    $data = array('responce' => 'error', 'message' => 'Failed to insert Record');
                }                
            }
            echo json_encode($data);
        }
        else{
            echo "no direct script access allowed";
        }
    }

    public function delete(){
        if ($this->input->is_ajax_request()) {
            $del_id = $this->input->post('del_id');
            if ($this->old_super_drawer_model->delete_entry($del_id)) {
                $data = array('responce' => 'success');
            }else{
                $data = array('responce' => 'error');
            }            
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    public function delete_from_hist(){
        if ($this->input->is_ajax_request()) {
            $del_id = $this->input->post('del_id');
            if ($this->old_super_drawer_model->delete_hist_entry($del_id)) {
                $data = array('responce' => 'success');
            }else{
                $data = array('responce' => 'error');
            }            
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
    
    public function edit(){
        if ($this->input->is_ajax_request()) {
            $edit_id = $this->input->post('edit_id');
            if ($record = $this->old_super_drawer_model->edit_entry($edit_id)) {
                $data = array('responce' => 'success', 'record' => $record);
            }else{
                $data = array('responce' => 'error', 'message' => 'Failed to fetch record');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
    //Update Record, funqcia uzrunvelyofs da-update-ebuli monacemebis shenaxvas fenics table-shi. idzaxebs Mini_drawer_model.php/update_entry funqcias da gadascems data-s.
    public function update(){
        if ($this->input->is_ajax_request()) {            

            $this->form_validation->set_rules('old_super_drawer_id', 'Old Super Drawer ID', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $data = array('responce' => 'error', 'message' => validation_errors());
            }
            else
            {
                if( $this->input->post('test_results') == null )
                {
                    $data['old_super_drawer_id_frame'] = $this->input->post('old_super_drawer_id_frame');
                    $data['old_super_drawer_id'] = $this->input->post('old_super_drawer_id');
                    $data['pmt_block_id'] = $this->input->post('pmt_block_id');
                    $data['pos_pmt_block'] = $this->input->post('pos_pmt_block');
                    $data['comments'] = $this->input->post('comments');
                    
                    if($this->old_super_drawer_model->update_entry($data))
                    {
                        $data = array('responce' => 'success', 'message' => 'Record updated Successfully');
                    }
                    else{
                        $data = array('responce' => 'error', 'message' => 'Failed to update record');
                    }
                }
                else
                {
                    $data['old_super_drawer_id_frame'] = $this->input->post('old_super_drawer_id_frame');
                    $data['old_super_drawer_id'] = $this->input->post('old_super_drawer_id');
                    $data['pmt_block_id'] = $this->input->post('pmt_block_id');
                    $data['pos_pmt_block'] = $this->input->post('pos_pmt_block');
                    $data['test_results'] = $this->input->post('test_results');
                    $data['comments'] = $this->input->post('comments');
                    
                    if($this->old_super_drawer_model->update_entry($data))
                    {
                        $data = array('responce' => 'success', 'message' => 'Record updated Successfully');
                    }
                    else{
                        $data = array('responce' => 'error', 'message' => 'Failed to update record');
                    }
                }
                
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
    // amowmebs aris tu ara databazeshi super draweri ID-it
    public function checkPmtBlockEntry(){
       
        if ($this->input->is_ajax_request()) 
        {
       
            $del_id = $this->input->post('del_id');
            $data = $this->old_super_drawer_model->check_pmt_block_entry($del_id);
                      
            echo json_encode($data);
       
        }else{
            echo "No direct script access allowed";
        }
    }

    // amowmebs aris tu ara databazeshi super draweri ID-it
    public function checkSuperDrawerEntry(){
       
        if ($this->input->is_ajax_request()) 
        {
       
            $id = $this->input->post('id');
            $data = $this->old_super_drawer_model->check_super_drawer_entry($id);
                      
            echo json_encode($data);
       
        }else{
            echo "No direct script access allowed";
        }
    }

    public function fetch_test(){
        
        if ($this->input->is_ajax_request()) 
        {
            $id = $this->input->post('id');
            if ($record = $this->old_super_drawer_model->fetch_test_results($id)) {
                $data = array('responce' => 'success', 'record' => $record);
            }else
            {
                $data = array('responce' => 'error', 'message' => 'Test Result of this super drawer does not exists');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
    
    public function fetch_test_from_hist(){
        
        if ($this->input->is_ajax_request()) 
        {
            $id = $this->input->post('id');
            if ($record = $this->old_super_drawer_model->fetch_hist_test_results($id)) {
                $data = array('responce' => 'success', 'record' => $record);
            }else
            {
                $data = array('responce' => 'error', 'message' => 'Test Result of this super drawer does not exists');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
}
