<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pmt_block extends CI_Controller {
    public function __construct(){
        parent::__construct();
        //xdeba codeigniter-is bibliotekis gamodzaxeba
        $this->load->helper(array('form', 'url'));
        //xdeba codeigniter-is bibliotekis gamodzaxeba
        $this->load->library('form_validation');
        //ukavshirdeba mainboard_model.php fails
        $this->load->model('pmt_block_model');

    }
    //index funqcia romelsac am momentshi ar aqvs gamoyeneba
	public function index(){
    }
    //Fetch Table, funqcia uzrunvelyofs mainboard table-dan wamogebuli informaciis miwodebas ajax-is requestistvis. ajax request igzavneba xeshi Pmt_block-ze click-isas. faili: views/layout/Main.php
    public function fetch(){
        if ($this->input->is_ajax_request()) {
            if ($records = $this->pmt_block_model->get_entries())
            {
                $data = array('responce' => 'success', 'records' => $records);
            }
            else
            {
                $data = array('responce' => 'error', 'message' => 'Failed to fetch Pmt Block data');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    public function fetchOldPmtBlock(){
        if ($this->input->is_ajax_request()) {
            if ($records = $this->pmt_block_model->get_old_entries())
            {
                $data = array('responce' => 'success', 'records' => $records);
            }
            else
            {
                $data = array('responce' => 'error', 'message' => 'Failed to fetch Pmt Block data');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
    
    public function fetchPmtBlockRecords(){
        if ($this->input->is_ajax_request()) {
            if ($records = $this->pmt_block_model->get_fhp_id())
            {
                $data = array('responce' => 'success', 'records' => $records);
            }
            else
            {
                $data = array('responce' => 'error', 'message' => 'Failed to fetch Pmt Block Records');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    public function fetchPmtBlockRecordsFromMicroMiniDrawer(){
        if ($this->input->is_ajax_request()) {
            if ($records = $this->pmt_block_model->get_pmt_block_from_micro_mini_drawer())
            {
                $data = array('responce' => 'success', 'records' => $records);
            }
            else
            {
                $data = array('responce' => 'error', 'message' => $records);
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    public function fetchPosFromMicroDrawer(){
        if ($this->input->is_ajax_request()) {
            $del_id = $this->input->post('del_id');
            if ($records = $this->pmt_block_model->get_pos_from_micro_drawer($del_id)) {
                $data = array('responce' => 'success', 'records' => $records);
            }else{
                $data = array('responce' => 'error', 'message' => validation_errors() );
            }            
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    public function fetchHistory()
    {
        if ($this->input->is_ajax_request()) 
        {
            if ($records = $this->pmt_block_model->get_old_modification_entries())
            {
                $data = array('responce' => 'success', 'records' => $records);
            }
            else
            {
                $data = array('responce' => 'error', 'message' => 'Failed to fetch PMT Block data');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    public function fetch_test_from_hist()
    {
        
        if ($this->input->is_ajax_request()) 
        {
            $id = $this->input->post('id');
            if ($record = $this->pmt_block_model->fetch_hist_test_results($id)) 
            {
                $data = array('responce' => 'success', 'record' => $record);
            }else
            {
                $data = array('responce' => 'error', 'message' => 'Test Result of this PMT Block does not exists');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    public function delete_from_hist()
    {
        if ($this->input->is_ajax_request()) 
        {
            $del_id = $this->input->post('del_id');
  
            if ($this->pmt_block_model->delete_hist_entry($del_id)) 
            {
                $data = array('responce' => 'success');
            }
            else
            {
                $data = array('responce' => 'error');
            }            
            echo json_encode($data);
        
        }
        else{
            echo "No direct script access allowed";
        }
    }

    public function delete_from_old()
    {
        if ($this->input->is_ajax_request()) 
        {
            $del_id = $this->input->post('del_id');
  
            if ($this->pmt_block_model->delete_old_entry($del_id)) 
            {
                $data = array('responce' => 'success');
            }
            else
            {
                $data = array('responce' => 'error');
            }            
            echo json_encode($data);
        
        }
        else{
            echo "No direct script access allowed";
        }
    }

    public function fetchPosFromMiniDrawer(){
        if ($this->input->is_ajax_request()) {
            $del_id = $this->input->post('del_id');
            if ($records = $this->pmt_block_model->get_pos_from_mini_drawer($del_id)) {
                $data = array('responce' => 'success', 'records' => $records);
            }else{
                $data = array('responce' => 'error', 'message' => validation_errors() );
            }            
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    //Insert Record, funqcia uzrunvelyofs monacemebis mainboard table-shi chaweras, post-it moaqvs views/layout/Main.php failidan data da wers mini_drawer table-shi.
    public function insert(){
        if ($this->input->is_ajax_request()){
              $this->form_validation->set_rules('id_pmt_block', 'Pmt_Block ID', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $data = array('responce' => 'error', 'message' => validation_errors());
            }
            else
            {
                $ajax_data = $this->input->post();
                if($this->pmt_block_model->insert_entry($ajax_data)){
                    $data = array('responce' => 'success', 'message' => 'Record added Successfully');
                }
                else{
                    $data = array('responce' => 'error', 'message' => 'Failed to insert Record');
                }                
            }
            echo json_encode($data);
        }
        else{
            echo "no direct script access allowed";
        }
    }
    public function insert_in_old_pmt_block(){
        if ($this->input->is_ajax_request()){
              $this->form_validation->set_rules('id_pmt_block', 'PMT Block ID', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $data = array('responce' => 'error', 'message' => validation_errors());
            }
            else
            {
                $ajax_data = $this->input->post();

                if($this->pmt_block_model->insert_in_old_entry($ajax_data)){
                    $data = array('responce' => 'success', 'message' => 'Record added Successfully');
                }
                else{
                    $data = array('responce' => 'error', 'message' => 'Failed to insert Record');
                }                
            }
            echo json_encode($data);
        }
        else{
            echo "no direct script access allowed";
        }
    }
    
    //Delete record, funqcia uzrunvelyofs wasashleli record-is ID-s miwodebas Pmt_block_model.php/delete_entry funqciistvis da idzaxebs funqcias. funqciashi ajax-dan shemodis wasashleli record-is id.
    public function delete(){
        if ($this->input->is_ajax_request()) {
            $del_id = $this->input->post('del_id');
            if ($this->pmt_block_model->delete_entry($del_id)) {
                $data = array('responce' => 'success');
            }else{
                $data = array('responce' => 'error');
            }            
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
    //Edit Record, funqcia uzrunvelyofs dasa-edit-ebeli record-is ID-s miwodebas Pmt_block_model.php/edit_entry funqciistvis da idzaxebs funqcias. funqciashi ajax-dan shemodis dasaeditebeli record-is id da isev ajax-s ubrunebs response-s saxit migebul shedegs.
    public function edit(){
        if ($this->input->is_ajax_request()) {
            $edit_id = $this->input->post('edit_id');
            if ($record = $this->pmt_block_model->edit_entry($edit_id)) {
                $data = array('responce' => 'success', 'record' => $record);
            }else{
                $data = array('responce' => 'error', 'message' => 'Failed to fetch record');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
    public function edit_old_pmt_block(){
        if ($this->input->is_ajax_request()) {
            $edit_id = $this->input->post('edit_id');
            if ($record = $this->pmt_block_model->edit_old_entry($edit_id)) {
                $data = array('responce' => 'success', 'record' => $record);
            }else{
                $data = array('responce' => 'error', 'message' => 'Failed to fetch record');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
    //Update Record, funqcia uzrunvelyofs da-update-ebuli monacemebis shenaxvas mini_drawer table-shi. idzaxebs Pmt_block_model.php/update_entry funqcias da gadascems data-s.
    public function update(){
        if ($this->input->is_ajax_request()) {            

            $this->form_validation->set_rules('id_pmt_block', 'Pmt Block ID', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $data = array('responce' => 'error', 'message' => validation_errors());
            }
            else
            {

                $data['id_pmt_block'] = $this->input->post('id_pmt_block');
                $data['id_super_drawer'] = $this->input->post('id_super_drawer');
                $data['id_mini_drawer'] = $this->input->post('id_mini_drawer');
                $data['id_micro_drawer'] = $this->input->post('id_micro_drawer');
                $data['id_fenics'] = $this->input->post('id_fenics');
                $data['id_hv_divider'] = $this->input->post('id_hv_divider');
                $data['id_pmt'] = $this->input->post('id_pmt');
                $data['current_location'] = $this->input->post('current_location');
                $data['remark'] = $this->input->post('remark');

                if($this->pmt_block_model->update_entry($data)){
                    $data = array('responce' => 'success', 'message' => 'Record updated Successfully');
                }
                else{
                    $data = array('responce' => 'error', 'message' => 'Failed to update record');
                }                
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
    
    //Update Record, funqcia uzrunvelyofs da-update-ebuli monacemebis shenaxvas mini_drawer table-shi. idzaxebs Pmt_block_model.php/update_entry funqcias da gadascems data-s.
    public function update_old_pmt_block()
    {
        if ($this->input->is_ajax_request()) 
        {            
            $this->form_validation->set_rules('id_pmt_block', 'Pmt Block ID', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $data = array('responce' => 'error', 'message' => validation_errors());
            }
            else
            {
                if( $this->input->post('test_results') == null )
                {
                    $data['id_pmt_block'] = $this->input->post('id_pmt_block');
                    $data['super_drawer_id'] = $this->input->post('super_drawer_id');
                    $data['id_hv_divider'] = $this->input->post('id_hv_divider');
                    $data['board_3_in_1_id'] = $this->input->post('board_3_in_1_id');
                    $data['id_pmt'] = $this->input->post('id_pmt');
                    $data['remark'] = $this->input->post('remark');

                    if($this->pmt_block_model->update_old_entry($data))
                    {
                        $data = array('responce' => 'success', 'message' => 'Record updated Successfully');
                    }
                    else{
                        $data = array('responce' => 'error', 'message' => 'Failed to update record');
                    }
                }else if($this->input->post('test_results') != null )
                {
                    $data['id_pmt_block'] = $this->input->post('id_pmt_block');
                    $data['super_drawer_id'] = $this->input->post('super_drawer_id');
                    $data['id_hv_divider'] = $this->input->post('id_hv_divider');
                    $data['board_3_in_1_id'] = $this->input->post('board_3_in_1_id');
                    $data['id_pmt'] = $this->input->post('id_pmt');
                    $data['test_results'] = $this->input->post('test_results');
                    $data['remark'] = $this->input->post('remark');

                    if($this->pmt_block_model->update_old_entry($data))
                    {
                        $data = array('responce' => 'success', 'message' => 'Record updated Successfully');
                    }
                    else{
                        $data = array('responce' => 'error', 'message' => 'Failed to update record');
                    }
                }
                
            }
            echo json_encode($data);

        }else{
            echo "No direct script access allowed";
        }
    }

    public function fetch_test_of_old_pmt_block(){
        
        if ($this->input->is_ajax_request()) 
        {
            $id = $this->input->post('id');
            if ($record = $this->pmt_block_model->fetch_test_results_of_old_pmt_block($id)) 
            {
                $data = array('responce' => 'success', 'record' => $record);
            }else
            {
                $data = array('responce' => 'error', 'message' => 'Test Result of this PMT Block does not exists');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    public function check(){
       
        if ($this->input->is_ajax_request()) {
       
            $del_id = $this->input->post('del_id');
            $data = $this->pmt_block_model->check_entry($del_id);
                      
            echo json_encode($data);
       
        }else{
            echo "No direct script access allowed";
        }
    }
    public function checkOldPMtBlockID(){
       
        if ($this->input->is_ajax_request()) 
        {
       
            $id = $this->input->post('id');
            $data = $this->pmt_block_model->check_old_entry($id);
                      
            echo json_encode($data);
       
        }else{
            echo "No direct script access allowed";
        }
    }
}
