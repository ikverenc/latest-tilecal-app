<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Board_3_in_1 extends CI_Controller {
    public function __construct(){
        parent::__construct();
        //xdeba codeigniter-is bibliotekis gamodzaxeba
        $this->load->helper(array('form', 'url'));
        //xdeba codeigniter-is bibliotekis gamodzaxeba
        $this->load->library('form_validation');
        //ukavshirdeba super_drawer_model.php fails
        $this->load->model('board_3_in_1_model');

    }
    //index funqcia romelsac am momentshi ar aqvs gamoyeneba
	public function index(){
    }
    public function fetch()
    {
        if ($this->input->is_ajax_request()) {
            if ($records = $this->board_3_in_1_model->get_entries())
            {
                $data = array('responce' => 'success', 'records' => $records);
            }
            else
            {
                $data = array('responce' => 'error', 'message' => 'Failed to fetch 3 in 1 Board data');
            }
            
            echo json_encode($data);

        }else{
            echo "No direct script access allowed";
        }
    }

    public function fetch_old()
    {
        if ($this->input->is_ajax_request()) {
            if ($records = $this->board_3_in_1_model->get_old_entries())
            {
                $data = array('responce' => 'success', 'records' => $records);
            }
            else
            {
                $data = array('responce' => 'error', 'message' => 'Failed to fetch 3 in 1 Board data');
            }
            
            echo json_encode($data);

        }else{
            echo "No direct script access allowed";
        }
    }

    public function delete()
    {
        if ($this->input->is_ajax_request()) 
        {
            $del_id = $this->input->post('del_id');

            if ($this->board_3_in_1_model->delete_entry($del_id)) 
            {
                $data = array('responce' => 'success');
            }else{
                $data = array('responce' => 'error');
            }            
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    public function update()
    {
        if ($this->input->is_ajax_request()) {            

            $this->form_validation->set_rules('id_3_in_1', '3 in 1 Board ID', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $data = array('responce' => 'error', 'message' => validation_errors());
            }
            else
            {
                $data['id_3_in_1'] = $this->input->post('id_3_in_1');
                $data['id_pmt_block'] = $this->input->post('id_pmt_block');
                $data['remark'] = $this->input->post('remark');
                
                if($this->board_3_in_1_model->update_entry($data))
                {
                    $data = array('responce' => 'success', 'message' => 'Record updated Successfully');
                }
                else{
                    $data = array('responce' => 'error', 'message' => 'Failed to update record');
                }                
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    public function edit()
    {
        if ($this->input->is_ajax_request()) 
        {
            $edit_id = $this->input->post('edit_id');

            if ($record = $this->board_3_in_1_model->edit_entry($edit_id)) 
            {
                $data = array('responce' => 'success', 'record' => $record);
            }else{
                $data = array('responce' => 'error', 'message' => 'Failed to fetch record');
            }
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }

    // amowmebs aris tu ara databazeshi super draweri ID-it
    public function check(){
       
        if ($this->input->is_ajax_request()) {
       
            $id = $this->input->post('id');
            $data = $this->board_3_in_1_model->check_entry($id);
                      
            echo json_encode($data);
       
        }else{
            echo "No direct script access allowed";
        }
    }
    public function insert()
    {
        if ($this->input->is_ajax_request())
        {
              $this->form_validation->set_rules('id_3_in_1', '3 in 1 Board ID', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $data = array('responce' => 'error', 'message' => validation_errors());
            }
            else
            {
                $ajax_data = $this->input->post();
                
                if($this->board_3_in_1_model->insert_entry($ajax_data)){
                    $data = array('responce' => 'success', 'message' => 'Record added Successfully');
                }
                else{
                    $data = array('responce' => 'error', 'message' => 'Failed to insert Record');
                }                
            }
            echo json_encode($data);
        }
        else{
            echo "no direct script access allowed";
        }
    }

    public function delete_old_modification_hist()
    {
        if ($this->input->is_ajax_request()) 
        {
            $del_id = $this->input->post('del_id');

            if ($record = $this->board_3_in_1_model->delete_old_modification_entry($del_id)) 
            {
                $data = array('responce' => 'success', 'record' => $record);
            }
            else
            {
                $data = array('responce' => 'error', 'record' => $record);
            }            
            echo json_encode($data);
        }else{
            echo "No direct script access allowed";
        }
    }
}
